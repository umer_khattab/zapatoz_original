package com.selklik.zapatos.utils

import android.content.Context
import android.support.design.widget.BottomSheetDialogFragment
import android.os.Bundle
import android.view.ViewGroup
import android.view.LayoutInflater
import android.view.View
import android.view.WindowManager
import com.selklik.zapatos.R


/**
 * Created by faridhadiselklik on 24/01/2018.
 */
class NewQuestionBottomSheetModal : BottomSheetDialogFragment() {

    companion object{
        private val ARG_CAUGHT = "myFragment_caught"
        fun newInstance(context: Context):NewQuestionBottomSheetModal{
            val args: Bundle = Bundle()
            val fragment = NewQuestionBottomSheetModal()
            fragment.arguments = args
            return fragment
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        dialog.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE or WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)

        return inflater.inflate(R.layout.new_ques_fragment_bottom_sheet_dialog, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

    }
}