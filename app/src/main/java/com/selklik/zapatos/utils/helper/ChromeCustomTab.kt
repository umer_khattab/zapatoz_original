package com.selklik.zapatos.utils.helper

import android.support.customtabs.CustomTabsClient
import android.content.ComponentName
import android.support.customtabs.CustomTabsServiceConnection
import android.os.Bundle
import android.support.customtabs.CustomTabsCallback
import android.support.customtabs.CustomTabsSession
import android.support.customtabs.CustomTabsIntent
import android.R.anim.slide_out_right
import android.R.anim.slide_in_left
import android.app.Activity
import android.net.Uri
import android.util.Log
import com.selklik.zapatos.R


/**
 * Created by faridhadiselklik on 30/01/2018.
 */
class ChromeCustomTab(private val activity: Activity, private val url: String) {

    private var customTabsSession: CustomTabsSession? = null
    private var client: CustomTabsClient? = null
    private var connection: CustomTabsServiceConnection? = null

    private var warmupWhenReady = false
    private var mayLaunchWhenReady = false

    private val session: CustomTabsSession? get() {
            Log.d("CustomTabs", "client : ${(client == null)}")
            if (client == null) {
                customTabsSession = null
            } else if (customTabsSession == null) {
                customTabsSession = client!!.newSession(object : CustomTabsCallback() {
                    override fun onNavigationEvent(navigationEvent: Int, extras: Bundle?) {
                        Log.w("CustomTabs", "onNavigationEvent: Code = " + navigationEvent)
                    }
                })
            }
            return customTabsSession
        }

    init {
        Log.w("CustomTabs", "inside custom tab ")
        bindCustomTabsService()
    }

    fun warmup() {
        Log.d("CustomTabs", "client " + (client != null).toString())
        if (client != null) {
            warmupWhenReady = false
            client!!.warmup(0)
        } else {
            warmupWhenReady = true
        }
    }

    fun mayLaunch() {
        val session = session
        if (client != null) {
            mayLaunchWhenReady = false
            session!!.mayLaunchUrl(Uri.parse(url), null, null)
        } else {
            mayLaunchWhenReady = true
        }
    }

    fun show() {
        val builder = CustomTabsIntent.Builder(session)
//        builder.setToolbarColor(activity.resources.getColor(R.color.colorSelkl)).setShowTitle(true)
//        builder.setStartAnimations(activity, R.anim.slide_in_left, R.anim.slide_out_right)
//        builder.setExitAnimations(activity, R.anim.slide_in_right, R.anim.slide_out_left)
        val customTabsIntent = builder.build()
        customTabsIntent.launchUrl(activity, Uri.parse(url))
    }

    private fun bindCustomTabsService() {
        if (client != null) {
            return
        }
        connection = object : CustomTabsServiceConnection() {
            override fun onCustomTabsServiceConnected(name: ComponentName, client: CustomTabsClient) {
                this@ChromeCustomTab.client = client

                if (warmupWhenReady) {
                    warmup()
                }

                Log.d("CustomTabs", mayLaunchWhenReady.toString())
                if (mayLaunchWhenReady) {
                    mayLaunch()
                }
            }

            override fun onServiceDisconnected(name: ComponentName) {
                client = null
            }
        }
        val ok = CustomTabsClient.bindCustomTabsService(activity, CUSTOM_TAB_PACKAGE_NAME, connection)
        if (!ok) {
            connection = null
        }
    }

    fun unbindCustomTabsService() {
        if (connection == null) {
            return
        }
        activity.unbindService(connection)
        client = null
        customTabsSession = null
    }

    companion object {
        const val CUSTOM_TAB_PACKAGE_NAME = "com.android.chrome"
    }
}