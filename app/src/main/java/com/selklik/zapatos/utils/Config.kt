package com.selklik.zapatos.utils

/**
 * Created by faridhadiselklik on 29/11/2017.
 */
class Config {

    companion object {

        // Selklik
        /*val API_KEY = "384515838451504"
        val API_SECRET = "405b13c21cfa7a022dcbd159b280c66a"*/

        val API_KEY = "434773459584405"
        val API_SECRET = "67d5fb88e4aa95f5aa34011efb0bbd6f"

        val API_URL = "http://myyb2.mobsocial.net/fan-api/1.0"
        val API_VERSION = "1.0.0"

        val ARTIST_ID = "3277"
        val ARTIST_ID_2 = "256"

        val API_LOGIN = "/auth/login"
        val API_FORGET_PASSWORD = "/auth/reset-password"
        val API_SIGNUP = "/auth/signup"
        val API_PROFILE_PIC = "/me/profile-photos"
        val API_ARTIST = "/artists"
        val API_ME = "/me"
        val API_FEED = "/me/feed"
        val API_EVENT = "/events"
        val API_FEATURED = "/artists/featured"
        val API_POST = "/posts"
        val API_ROOMS = "/me/rooms"
        val API_QNA = "/question-and-answers"
        val API_INSTAGRAM = "/instagram-comments"
        val API_FACEBOOK = "/facebook-comments"
        val API_TRANSACTION = API_ME + "/transactions"
        val API_FB_LOGIN = "/auth/facebook/login"
        val API_FB_SIGNUP = "/auth/facebook/signup"
        val API_RELOAD_STAR = "/me/android-topups"
        val API_ABOUT_ARTIST = "/artists/$ARTIST_ID/about"

        val API_LOGOUT = "/auth/logout"

        //------------------------------ PREF_CONFIG ------------------//

        val PREF_KEEP_LOGIN = "sk_keep_login"
        val PREF_USER_DATA = "sk_user_data"
        val PREF_ACCESS_TOKEN = "sk_access_token"
        val PREF_ABOUT_ARTIST = "sk_about_artist"
        val PREF_USER_CARD = "sk_user_card"
        val PREF_USER_ADDRESS = "sk_user_address"
        val PREF_DEVICE_ID = "sk_device_id"

        val PREF_CARDS = "sk_cards"

        //----------------- URL --------------//
        val tosUrl = "https://myyb2.mobsocial.net/pages/tos"
        val privacyUrl = "https://myyb2.mobsocial.net/pages/privacy"
        val faqUrl = "https://myyb2.mobsocial.net/pages/faq"

        //---------------SHOP URL ------------------//
        /* GENERAL CATEGORY */
        val ShopeMen_Url = "http://shoplot.biz/zapatos-admin/pages/inside.php?category=Men"


        val ShopNewArrival = "http://shoplot.biz/zapatos-admin/pages/inside.php?category=Men&type=NewArrival"

        /* PROMOTION-SELLER CATEGORY */
        val PromotionSeller_Url = "http://shoplot.biz/zapatos-admin/pages/inside.php?category=men&type=Promotion"

        /* BEST-SELLER CATEGORY */
        val BestSeller_Url = "http://shoplot.biz/zapatos-admin/pages/inside.php?category=Men&type=BestSeller" //


        /* BANNER-API */
        var Banner_Url = "http://shoplot.biz/zapatos-admin/pages/banner.php?banner=img"

        /* BASE-API */
        var Base_Url = "http://shoplot.biz/zapatos-admin/pages/cust.php"

        var myCountry : String = ""
        var payMethod : String = "nothing"

        /* Currency Units */
        var myCurrencyUnit : String = ""

    }

}