package com.selklik.zapatos.utils.cartdb;

public class CartModel {

    private String PRODUCT_ID;
    private String PRODUCT_NAME;
    private String PRODUCT_PRICE;
    private String PRODUCT_COLOR;
    private int PRODUCT_QUANTITY;
    private String PRODUCT_IMG;

    public CartModel(String PRODUCT_ID, String PRODUCT_NAME, String PRODUCT_PRICE, String PRODUCT_COLOR, int PRODUCT_QUANTITY, String PRODUCT_IMG, String PRODUCT_SIZE) {
        this.PRODUCT_ID = PRODUCT_ID;
        this.PRODUCT_NAME = PRODUCT_NAME;
        this.PRODUCT_PRICE = PRODUCT_PRICE;
        this.PRODUCT_COLOR = PRODUCT_COLOR;
        this.PRODUCT_QUANTITY = PRODUCT_QUANTITY;
        this.PRODUCT_IMG = PRODUCT_IMG;
        this.PRODUCT_SIZE = PRODUCT_SIZE;
    }

    public String getPRODUCT_SIZE() {
        return PRODUCT_SIZE;
    }

    public void setPRODUCT_SIZE(String PRODUCT_SIZE) {
        this.PRODUCT_SIZE = PRODUCT_SIZE;
    }

    private String PRODUCT_SIZE;

    public CartModel()
    { }


    public String getPRODUCT_ID() {
        return PRODUCT_ID;
    }

    public void setPRODUCT_ID(String PRODUCT_ID) {
        this.PRODUCT_ID = PRODUCT_ID;
    }

    public String getPRODUCT_NAME() {
        return PRODUCT_NAME;
    }

    public void setPRODUCT_NAME(String PRODUCT_NAME) {
        this.PRODUCT_NAME = PRODUCT_NAME;
    }

    public String getPRODUCT_PRICE() {
        return PRODUCT_PRICE;
    }

    public void setPRODUCT_PRICE(String PRODUCT_PRICE) {
        this.PRODUCT_PRICE = PRODUCT_PRICE;
    }

    public String getPRODUCT_COLOR() {
        return PRODUCT_COLOR;
    }

    public void setPRODUCT_COLOR(String PRODUCT_COLOR) {
        this.PRODUCT_COLOR = PRODUCT_COLOR;
    }

    public int getPRODUCT_QUANTITY() {
        return PRODUCT_QUANTITY;
    }

    public void setPRODUCT_QUANTITY(int PRODUCT_QUANTITY) {
        this.PRODUCT_QUANTITY = PRODUCT_QUANTITY;
    }

    public String getPRODUCT_IMG() {
        return PRODUCT_IMG;
    }

    public void setPRODUCT_IMG(String PRODUCT_IMG) {
        this.PRODUCT_IMG = PRODUCT_IMG;
    }

}
