package com.selklik.zapatos.utils.helper

import android.app.AlertDialog
import android.content.Context
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import com.android.volley.VolleyError
import com.selklik.zapatos.R
import com.selklik.zapatos.utils.AppController
import com.selklik.zapatos.utils.TypefaceHelper
import com.stripe.android.SourceCallback
import com.stripe.android.Stripe
import com.stripe.android.model.Source
import com.stripe.android.model.SourceParams
import kotlinx.android.synthetic.main.layout_add_address_dialog.view.*
import kotlinx.android.synthetic.main.layout_add_card_dialog.view.*
import android.text.Spanned
import android.text.style.RelativeSizeSpan
import android.text.SpannableStringBuilder
import android.widget.*
import com.selklik.zapatos.MainActivity
import android.widget.TextView




class AlertDialogHelper {
    fun showAlertX(message: String, context: Context) {
        val builder = AlertDialog.Builder(context)
        builder.setMessage(message.replace("[^a-zA-Z0-9 ]".toRegex(), ""))
                .setCancelable(false)
                .setPositiveButton("OK") { dialog, id -> dialog.cancel() }
        //AlertDialog alert = builder.create();
        val alert = builder.show()
        val titlex = TextView(context)
        titlex.gravity = Gravity.CENTER
        titlex.textSize = 22f
        alert.setCustomTitle(titlex)

        val messageText = alert.findViewById<View>(android.R.id.message) as TextView
        messageText.gravity = Gravity.CENTER
        messageText.textSize = 18f

    }

    fun showAlert(title: String, message: String, context: Context) {
        val builder = AlertDialog.Builder(context)
        builder.setMessage(message.replace("[^a-zA-Z0-9 \n]".toRegex(), ""))
                .setCancelable(false)
        builder.setPositiveButton("OK") { dialog, id -> dialog.dismiss() }

        val ssBuilder = SpannableStringBuilder(title)

        // Initialize a new relative size span instance
        // Double the text size on this span
        val largeSizeText = RelativeSizeSpan(.8f)

        // Apply the relative size span
        ssBuilder.setSpan(
                largeSizeText, // Span to add
                0, // Start of the span
                title.length, // End of the span
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE // Do not extent the span when text add later
        )

        // Set the alert dialog title using spannable string builder
        // It will display double size text on title
        builder.setTitle(ssBuilder)
        val alert = builder.show()

        val messageText = alert.findViewById<View>(android.R.id.message) as TextView
        messageText.gravity = Gravity.CENTER
        messageText.textSize = 12f

    }

    fun showAlertToken(title: String, message: String, context: Context) {
        val builder = AlertDialog.Builder(context)
        builder.setMessage(message.replace("[^a-zA-Z0-9 \n]".toRegex(), "")).setTitle(title.toUpperCase())
                .setCancelable(false)

        builder.setPositiveButton("OK") { dialog, id ->
            //            LogoutHelper.logoutSession(context)
//            SharedPrefHelper().ClearPreferences(context)
            dialog.dismiss()
        }

        //AlertDialog alert = builder.create();
        val alert = builder.show()
        val titlex = TextView(context)
        titlex.gravity = Gravity.CENTER
        titlex.textSize = 16f
        alert.setCustomTitle(titlex)

        val messageText = alert.findViewById<View>(android.R.id.message) as TextView
        messageText.gravity = Gravity.CENTER
        messageText.textSize = 12f


    }

    fun showAddCardDialog(context: Context,
                          onSubmitClick: (response: String) -> Unit,
                          onCardReady: (cardSourceId: String) -> Unit) {

        val mBuilder = android.support.v7.app.AlertDialog.Builder(context!!, R.style.CardDialogTheme)

        val mView = LayoutInflater.from(context).inflate(R.layout.layout_add_card_dialog, null)

        mBuilder.setView(mView)
        val dialog = mBuilder.create()
        dialog.setCanceledOnTouchOutside(true)
        // Log.d(TAG, "preview url : " + previewUrl)
        mView.text_add_card_dialog_title.typeface = TypefaceHelper().setTypeface("bold")
        mView.text_cancel.typeface = TypefaceHelper().setTypeface("regular")
        mView.btn_submit_add_card.typeface = TypefaceHelper().setTypeface("regular")
        mView.btn_submit_add_card.setOnClickListener {
            onSubmitClick("Button Click")
            var card = mView.card_input_widget.card
            if (card != null) {
                if (!card!!.validateCard()) {
                    // Do not continue token creation.
                    Log.d("Add Card", "card invalidate")
                } else {
                    Log.d("Add Card", "card validate : " + card.brand)
                    val stripe = Stripe(context, "pk_live_Qs4OjvJhy9Tx6ofILcVMVgZy")
                    //val stripe = Stripe(context, "pk_test_O2CtbTj29L33SKIAtkQ4gAU0")
                    val cardSourceParams = SourceParams.createCardParams(card)
                    stripe.createSource(cardSourceParams, object : SourceCallback {
                        override fun onError(error: Exception) {
                            Log.d("SOURCE ERROR", error.toString())
                        }

                        override fun onSuccess(source: Source) {
                            Log.d("SOURCE ID:", source.id)
                            onCardReady(source.id)
                            dialog.dismiss()
                        }
                    })
                }
            }
        }

        mView.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()
    }


    fun showAddAddressDialog(context: Context,
                             onAddressResponse: (response: String) -> Unit,
                             onError: (errorString: String?, error: VolleyError?) -> Unit) {

        var arrayofState = arrayListOf(
            "Johor",
            "Kedah",
            "Perak",
            "Selangor",
            "Malacca",
            "Negeri Sembilan",
            "Pahang",
            "Perlis",
            "Penang",
            "Sabah",
            "Sarawak",
            "Terengganu")

        var selectedState : String ?= ""

        val mBuilder = android.support.v7.app.AlertDialog.Builder(context!!, R.style.CardDialogTheme)

        val addressView: View = LayoutInflater.from(context).inflate(R.layout.layout_add_address_dialog, null)

        val adapter = ArrayAdapter<String>(context,
                android.R.layout.simple_spinner_item, arrayofState)

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        addressView.state.setAdapter(adapter)
        addressView.state.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{

            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                (parent!!.getChildAt(0) as TextView).textSize = 14f
                selectedState = parent!!.getItemAtPosition(position).toString()

            }

        }



        mBuilder.setView(addressView)
        val dialog = mBuilder.create()
        dialog.setCanceledOnTouchOutside(true)
//        Log.d(TAG, "preview url : " + previewUrl)

        addressView.btn_submit_add_address.setOnClickListener {

            Log.d("addressValidation", "address isValid : ${addressValidation(addressView,
                    addressView.edittext_fullname.text.toString(),
                    addressView.edittext_address_1.text.toString(),
                    addressView.edittext_address_2.text.toString(),
                    addressView.edittext_zip_code.text.toString(),
                    addressView.edittext_city.text.toString(),
                    selectedState!!,
                    addressView.edittext_country.text.toString(),
                    addressView.edittext_contactno.toString().replaceBeforeLast("[\\W_&&[^°]]+","").replace(".", ""),
                    context)}")

            if (addressValidation(addressView,
                    addressView.edittext_fullname.text.toString(),
                    addressView.edittext_address_1.text.toString(),
                    addressView.edittext_address_2.text.toString(),
                    addressView.edittext_zip_code.text.toString(),
                    addressView.edittext_city.text.toString(),
                    selectedState!!,
                    addressView.edittext_country.text.toString(),
                    addressView.edittext_contactno.toString().replaceBeforeLast("[\\W_&&[^°]]+","").replace(".", ""),
                    context)) {

                showProgressDialog(context)
                AppController.instance.postAddress(context,
                        addressView.edittext_fullname.text.toString(),
                        addressView.edittext_address_1.text.toString(),
                        addressView.edittext_address_2.text.toString(),
                        addressView.edittext_zip_code.text.toString(),
                        addressView.edittext_city.text.toString(),
                        selectedState!!,
                        addressView.edittext_country.text.toString(),
                        "+60"+ addressView.edittext_contactno.text.toString().replaceBeforeLast("[\\W_&&[^°]]+","").replace(".", ""),
                        onSuccess = { response: String? ->
                            if (progressDialog!!.isShowing) {
                                progressDialog!!.dismiss()
                            }
                            onAddressResponse(response!!)
                            dialog.dismiss()
                        },
                        onError = { errorString: String?, error: VolleyError? ->
                            if (progressDialog!!.isShowing) {
                                progressDialog!!.dismiss()
                            }
                            onError(errorString, error)

                        })
            }
        }


        addressView.text_address_cancel.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()
    }

    private fun addressValidation(itemView: View,
                                  fullName : String,
                                  addressLine1: String,
                                  addressLine2: String,
                                  zipCope: String,
                                  city: String,
                                  state: String,
                                  country: String,
                                  phoneno : String,
                                  context: Context): Boolean {
        var valid = true

        if (fullName.isEmpty() || fullName == null)
        {
            itemView.edittext_fullname.error = "empty full name"
            valid = false
        }

        if (addressLine1!!.isEmpty() || addressLine1 == null) {
            itemView.edittext_address_1.error = "empty line 1"
            valid = false
        } else {
            itemView.edittext_address_1.error = null
            if (addressLine2!!.isEmpty() || addressLine2 == null) {
                itemView.edittext_address_2.error = "empty line 2"
                valid = false
            } else {
                itemView.edittext_address_2.error = null
                if (zipCope!!.isEmpty() || zipCope == "" || zipCope == null) {
                    itemView.edittext_zip_code.error = "empty zipcode"
                    valid = false
                } else {
                    itemView.edittext_zip_code.error = null
                    if (city!!.isEmpty() || city == null) {
                        itemView.edittext_city.error = "empty city"
                        valid = false
                    } else {
                        itemView.edittext_city.error = null
                        if (state!!.isEmpty() || state == null) {
                            Toast.makeText(context, "empty state", Toast.LENGTH_SHORT).show()
                            valid = false
                        } else {
                            if (country!!.isEmpty() || country == null) {
                                itemView.edittext_country.error = "empty country"
                                valid = false
                            } else {
                                itemView.edittext_country.error = null

                                if (phoneno!!.isEmpty() || phoneno == null){
                                    itemView.edittext_contactno.error = "empty contact number"
                                    valid = false

                                } else {

                                    itemView.edittext_contactno.error = null

//                                    if (itemView.edittext_contactno.text.length < 9){
//                                        itemView.edittext_contactno.error = "Invalid contact number"
//                                        valid = false
//                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return valid
    }

    private var progressDialog: android.support.v7.app.AlertDialog? = null

    private fun showProgressDialog(context: Context) {
        val mBuilder = android.support.v7.app.AlertDialog.Builder(context, R.style.CustomDialogTheme)
        val mView = LayoutInflater.from(context).inflate(R.layout.dialog_progress, null)

        mBuilder.setView(mView)
        mBuilder.setCancelable(false)
        progressDialog = mBuilder.create()
        progressDialog!!.show()
    }
}