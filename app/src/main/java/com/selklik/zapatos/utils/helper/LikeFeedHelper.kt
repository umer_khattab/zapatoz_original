package com.selklik.zapatos.utils.helper

import android.content.Context
import android.util.Log
import android.widget.Toast
import com.android.volley.Request
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.selklik.zapatos.utils.Config
import com.selklik.zapatos.utils.ConnectivityReceiver
import com.selklik.zapatos.utils.SharedPrefHelper
import com.selklik.zapatos.utils.volley.ApiHelper
import java.util.HashMap

/**
 * Created by faridhadiselklik on 18/12/2017.
 */
class LikeFeedHelper(context: Context) {

    var context: Context = context
    fun doLikeFeedPost(postId: String,
                       like: Boolean,
                       likeListener: (success: Boolean) -> Unit) {

        val method: Int = if (!like) {
            Request.Method.POST
        } else {
            Request.Method.DELETE
        }
        if (!ConnectivityReceiver.isConnected()) {
            //            Utils.connectionShowSnack(isConnected, layoutMain);
            Toast.makeText(context, "No internet Connection", Toast.LENGTH_SHORT).show()
        } else {
            val accessToken = SharedPrefHelper().getSharedPreferenceString(context, Config.PREF_ACCESS_TOKEN, null)
            val jsonParams = HashMap<String, String>()
            jsonParams.put("access_token", accessToken!!)
            val api = ApiHelper(context)

            api.makeRequest(Config.API_POST + "/" + postId + "/likes",
                    method,
                    jsonParams,
                    false,
                    false,
                    onSuccess = { responseString: String, gson: Gson ->

                        Log.d("FollowHelper", "response : " + responseString)
                        //                    response = responseString;
                        if (responseString == null) {
                            Log.d("FollowHelper", "unable to follow artist")
                            likeListener(like)
                        } else {
                            var likeObj: JsonObject = JsonParser().parse(responseString).asJsonObject
                            Log.d("Like helper", "like response : " + likeObj.get("liked").asString)
                            likeListener(likeObj.get("liked").asBoolean)
                        }
                    },
                    onError = {
                        likeListener(like)
                    })

        }

//        fun onError(error: String?) {
//            likeListener(false)
//            //                    StringBuilder builder = new StringBuilder();
//            if (error != null) {
//                try {
//                    val errorObg = JSONObject(error)
//                    val errorArray = errorObg.getJSONArray("errors")
//                    for (i in 0 until errorArray.length()) {
//                        Log.d("FollowHelper", "Error : " + errorArray.get(i).toString())
//                    }
//                } catch (e: JSONException) {
//                    e.printStackTrace()
//                }
//
//                //                        AlertHelper.showAlert("Errors !", error, AppController.getInstance());
//            }
//
//        }
//    })


    }
}