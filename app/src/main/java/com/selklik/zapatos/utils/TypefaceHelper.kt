package com.selklik.zapatos.utils

import android.graphics.Typeface

/**
 * Created by faridhadiselklik on 30/11/2017.
 */
class TypefaceHelper{
    fun setTypeface(type: String): Typeface {
        val font: Typeface
        when (type) {
            "regular" -> {
                font = Typeface.createFromAsset(AppController.instance.assets, "font/opensans_regular.ttf")
                return font
            }
            "semi_bold" -> {
                font = Typeface.createFromAsset(AppController.instance.assets, "font/opensans_semibold.ttf")
                return font
            }
            "light" -> {
                font = Typeface.createFromAsset(AppController.instance.assets, "font/opensans_light.ttf")
                return font
            }
            "bold" -> {
                font = Typeface.createFromAsset(AppController.instance.assets, "font/opensans_bold.ttf")
                return font
            }
            "extra_bold" -> {
                font = Typeface.createFromAsset(AppController.instance.assets, "font/opensans_extrabold.ttf")
                return font
            }
            else -> {
                font = Typeface.createFromAsset(AppController.instance.assets, "font/opensans_regular.ttf")
                return font
            }
        }
    }
}