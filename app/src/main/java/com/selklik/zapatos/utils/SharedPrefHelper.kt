package com.selklik.zapatos.utils

import android.content.Context
import android.content.SharedPreferences

/**
 * Created by faridhadiselklik on 29/11/2017.
 */
class SharedPrefHelper {

    private val PREF_FILE: String = "SITI_SELKLIK_PREF"

    /**
     * Set a string shared preference
     * @param key - Key to set shared preference
     * @param value - Value for the key
     */
    fun setSharedPreferenceString(context: Context, key: String, value: String) {
//        Log.d("SharedPrefHelper", "app controller instance : " + AppController().getInstance().applicationContext)
        val settings: SharedPreferences = context.getSharedPreferences(PREF_FILE, 0)
        val editor: SharedPreferences.Editor = settings.edit()
        editor.putString(key, value)
        editor.apply()
    }

    /**
     * Set a integer shared preference
     * @param key - Key to set shared preference
     * @param value - Value for the key
     */
    fun setSharedPreferenceInt(context: Context, key: String, value: Int) {
        val settings = context.getSharedPreferences(PREF_FILE, 0)
        val editor = settings.edit()
        editor.putInt(key, value)
        editor.apply()
    }

    fun setSharedPreferencesSet(context: Context, key: String, cartset: Set<String>)
    {
        val settings = context.getSharedPreferences(PREF_FILE, 0)
        val editor = settings.edit()
        editor.putStringSet(key, cartset)
        editor.apply()
    }


    /**
     * Set a Boolean shared preference
     * @param key - Key to set shared preference
     * @param value - Value for the key
     */
    fun setSharedPreferenceBoolean(key: String, value: Boolean) {
        val settings: SharedPreferences = AppController.instance.getSharedPreferences(PREF_FILE, 0)
        val editor: SharedPreferences.Editor = settings.edit()
        editor.putBoolean(key, value)
        editor.apply()
    }

    /**
     * Get a string shared preference
     * @param key - Key to look up in shared preferences.
     * @param defValue - Default value to be returned if shared preference isn't found.
     * @return value - String containing value of the shared preference if found.
     */
    fun getSharedPreferenceString(context: Context, key: String, defValue: String?): String? {
        val settings = AppController.instance.getSharedPreferences(PREF_FILE, 0)
        return settings.getString(key, defValue)
    }

    /**
     * Get a integer shared preference
     * @param key - Key to look up in shared preferences.
     * @param defValue - Default value to be returned if shared preference isn't found.
     * @return value - String containing value of the shared preference if found.
     */
    fun getSharedPreferenceInt(context: Context, key: String, defValue: Int): Int {
        val settings = context.getSharedPreferences(PREF_FILE, 0)
        return settings.getInt(key, defValue)
    }

    /**
     * Get a boolean shared preference
     * @param key - Key to look up in shared preferences.
     * @param defValue - Default value to be returned if shared preference isn't found.
     * @return value - String containing value of the shared preference if found.
     */
    fun getSharedPreferenceBoolean(key: String, defValue: Boolean): Boolean {
        val settings = AppController.instance.getSharedPreferences(PREF_FILE, 0)
        return settings.getBoolean(key, defValue)
    }

    fun ClearPreferences(activity: Context) {
        val sharedPreferences = AppController.instance.getSharedPreferences(PREF_FILE, 0)
        val editor = sharedPreferences.edit()
        editor.clear()
        editor.commit()
    }
}