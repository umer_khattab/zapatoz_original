package com.selklik.zapatos.utils

import android.app.Application
import android.content.Context
import android.content.Intent
import android.os.Build
import android.text.TextUtils
import android.util.Log
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.VolleyError
import com.android.volley.toolbox.ImageLoader
import com.android.volley.toolbox.Volley
import com.facebook.FacebookSdk
import com.facebook.appevents.AppEventsLogger
import com.facebook.login.LoginManager
import com.google.gson.Gson
import com.onesignal.OSNotificationAction
import com.onesignal.OSNotificationOpenResult
import com.onesignal.OneSignal
import com.selklik.zapatos.MainActivity
import com.selklik.zapatos.ui.ViewPostActivity
import com.selklik.zapatos.utils.cartdb.Cartdb
import com.selklik.zapatos.utils.volley.ApiHelper
import com.selklik.zapatos.utils.volley.LruBitmapCache
import org.json.JSONObject
import org.ocpsoft.prettytime.PrettyTime
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


/**
 * Created by faridhadiselklik on 22/11/2017.
 */

class AppController : Application() {
    val TAG = AppController::class.java
            .simpleName!!
    override fun onCreate() {
        super.onCreate()
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);

        instance = this
        Log.d("AppController", "run appcontroller oncreate")
        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .unsubscribeWhenNotificationsAreDisabled(true)
                .setNotificationOpenedHandler({
                    result: OSNotificationOpenResult? ->
                    val actionType = result!!.action.type
                    val data = result.notification.payload.additionalData
                    val gson = Gson()
                    Log.d("NotificationHandler", "result : " + gson.toJson(data))

                    val intent = Intent(this, MainActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
                    startActivity(intent)

                    if (data != null) {
                        var postId = data.optString("post_id", null)
                        if (postId != null) {
                            Log.i("OneSignalExample", "postId : " + postId)
                            //                Intent intent = new Intent();
                            val intent = Intent(this, ViewPostActivity::class.java)
                            intent.putExtra("postId", postId)
                            intent.putExtra("from", "notification")
                            intent.flags = Intent.FLAG_ACTIVITY_REORDER_TO_FRONT or Intent.FLAG_ACTIVITY_NEW_TASK
                            startActivity(intent)
                        } else {
                            Log.i("OneSignalExample", "postId null ")
                            val intent = Intent(this, MainActivity::class.java)
                            intent.flags = Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_NEW_TASK
                            startActivity(intent)
                        }
                    }

                    if (actionType == OSNotificationAction.ActionType.ActionTaken)
                        Log.i("OneSignalExample", "Button pressed with id: " + result.action.actionID)

                })
                .init()
    }



    companion object {
        lateinit var instance: AppController
            private set

        lateinit var userAccessToken: String
    }

    //    companion object {
    private var mRequestQueue: RequestQueue? = null

    private var mImageLoader: ImageLoader? = null
    private var mLruBitmapCache: LruBitmapCache? = null


    fun getUserAccessToken(): String {
        return SharedPrefHelper().getSharedPreferenceString(instance, Config.PREF_ACCESS_TOKEN, null)!!
    }

    fun getUserCardData(): String? {
        return SharedPrefHelper().getSharedPreferenceString(instance, Config.PREF_USER_CARD, null)
    }

    fun getUserData(): String {
        return SharedPrefHelper().getSharedPreferenceString(instance, Config.PREF_USER_DATA, null)!!
    }

    fun getUserAddress(): String? {
        return SharedPrefHelper().getSharedPreferenceString(instance, Config.PREF_USER_ADDRESS, null)
    }

    fun getRequestQueue(): RequestQueue? {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(instance)
        }

        return mRequestQueue
    }

    fun <T> addToRequestQueue(req: Request<T>, tag: String) {
        // set the default tag if tag is empty
        req.tag = if (TextUtils.isEmpty(tag)) TAG else tag
        getRequestQueue()?.add(req)
    }

    fun <T> addToRequestQueue(req: Request<T>) {
        req.tag = TAG
        getRequestQueue()!!.add(req)
    }

    fun cancelPendingRequests(tag: Any) {
        if (mRequestQueue != null) {
            mRequestQueue!!.cancelAll(tag)
        }
    }

    fun getImageLoader(): ImageLoader {
        getRequestQueue()
        if (mImageLoader == null) {
            getLruBitmapCache()
            mImageLoader = ImageLoader(this.mRequestQueue, mLruBitmapCache)
        }

        return this.mImageLoader!!
    }

    fun getLruBitmapCache(): LruBitmapCache {
        if (mLruBitmapCache == null)
            mLruBitmapCache = LruBitmapCache()
        return this.mLruBitmapCache!!
    }
//    }

    fun setPrettyDateFormat(utcDate: String): String {

        var formatter = SimpleDateFormat("yyyy-MM-dd hh:mm:ss")
        formatter.timeZone = TimeZone.getTimeZone("UTC")
        var convertedDate = Date()
        try {
            convertedDate = formatter.parse(utcDate)
            Log.d("convertedDate", convertedDate.toString())
        } catch (e: ParseException) {
            // TODO Auto-generated catch block
            e.printStackTrace()
        }
        var locale: Locale? = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            instance.resources.configuration.locales.get(0)
        } else {
            instance.resources.configuration.locale
        }
        val newDate = formatter.format(convertedDate)
        Log.d("SetPrettyTime", "new date : " + newDate)
        val prettyTime = PrettyTime().setLocale(locale!!)
        val datetime = prettyTime.format(convertedDate)
        Log.d("PrettyTime", datetime)
        return prettyTime.format(convertedDate)
    }

    fun logoutSession(context: Context, onResponse: (response: Any?) -> Unit) {
        val accesstoken = SharedPrefHelper().getSharedPreferenceString(this, Config.PREF_ACCESS_TOKEN, null)
        if (accesstoken != null) {

            val jsonParams = HashMap<String, String>()
            jsonParams.put("access_token", accesstoken)
            val api = ApiHelper(context)
            val isConnected = ConnectivityReceiver.isConnected()
            if (!isConnected) {
                Toast.makeText(context, "No internet Connection", Toast.LENGTH_SHORT).show()
            } else {
                Log.d("LogoutHelper", "json params : " + jsonParams.toString())

                api.makeRequest(Config.API_LOGOUT,
                        Request.Method.POST,
                        jsonParams,
                        true,
                        false,
                        onSuccess = { responseString: String, gson: Gson ->
                            Log.d("LogoutHelper", "response : " + responseString)
                            //                        LoginManager.getInstance().logOut();

                            Cartdb(context).clearTable()
                            LocalMutators.resetAll()
                            onResponse(responseString)
                            LoginManager.getInstance().logOut()
                            Toast.makeText(context, "You've been logout.", Toast.LENGTH_SHORT).show()
                            SharedPrefHelper().ClearPreferences(context)
                            val i = baseContext.packageManager
                                    .getLaunchIntentForPackage(baseContext.packageName)
                            i!!.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                            startActivity(i)

                        },
                        onError = { error: VolleyError ->

                            Cartdb(context).clearTable()
                            LocalMutators.resetAll()
                            onResponse(error)
                            SharedPrefHelper().ClearPreferences(context)
                            LoginManager.getInstance().logOut()
                            val i = baseContext.packageManager
                                    .getLaunchIntentForPackage(baseContext.packageName)
                            i!!.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                            startActivity(i)
                        })
            }
        }
    }

    fun fetchCard(context: Context, onResponse: (response: Any?) -> Unit) {
        if (!ConnectivityReceiver.isConnected()) {
            Log.d("Add Card", "No internet connection")
        } else {
            val jsonParams = HashMap<String, String>()
            jsonParams.put("access_token", AppController.instance.getUserAccessToken())

            ApiHelper(context).makeRequest(
                    Config.API_ME + "/cards/default",
                    Request.Method.GET,
                    jsonParams,
                    false,
                    false,
                    onSuccess = { responseString: String, gson: Gson ->
                        if (responseString != "[]") {
                            Log.d("Add Card", "response string : " + responseString)
//                        if (dialog!!.isShowing) {
//                            dialog!!.dismiss()
//                        }
                            onResponse(responseString)

                        }
                    },
                    onError = { error: VolleyError ->
                        //                        ApiHelper(context).errorHandler(error)
//                        if (dialog!!.isShowing) {
//                            dialog!!.dismiss()
//                        }
                        onResponse(error)
                    })
        }

    }

    public fun fetchUserAddressDefault(context: Context, onSuccess: (response: String?) -> Unit, onError: (error: VolleyError) -> Unit) {
        if (!ConnectivityReceiver.isConnected()) {
            Log.d(TAG, "No internet connection")
        } else {
            val jsonParams = HashMap<String, String>()
            jsonParams.put("access_token", AppController.instance.getUserAccessToken())

            ApiHelper(context).makeRequest(
                    Config.API_ME + "/addresses/default",
                    Request.Method.GET,
                    jsonParams,
                    false,
                    false,
                    onSuccess = { responseString: String, gson: Gson ->
                        Log.d("Splash Screen", "response string : " + responseString)

                        if (responseString != null) {
                            onSuccess(responseString)
                        } else {
                            onSuccess(null)
                        }
                    },
                    onError = { error: VolleyError ->
                        onError(error)
                    })
        }
    }

    fun postCard(context: Context,
                 sourceId: String,
                 onSuccess: (response: String?) -> Unit,
                 onError: (errorString: String?, error: VolleyError?) -> Unit) {
        if (!ConnectivityReceiver.isConnected()) {
            Log.d("Add Card", "No internet connection")
            onError("No Connection", null)

        } else {
            val jsonParams = HashMap<String, String>()
            jsonParams.put("access_token", AppController.instance.getUserAccessToken())
            jsonParams.put("source_id", sourceId)

            ApiHelper(context).makeRequest(
                    Config.API_ME + "/cards",
                    Request.Method.POST,
                    jsonParams,
                    false,
                    false,
                    onSuccess = { responseString: String, gson: Gson ->
                        onSuccess(responseString)
                    },
                    onError = { error: VolleyError ->
                        //                        ApiHelper(this).errorHandler(error)
                        onError(null, error)
                    })
        }
    }

    fun postAddress(context: Context,
                    fullName: String,
                    addressLine1: String,
                    addressLine2: String,
                    zipCode: String,
                    city: String,
                    state: String,
                    country: String,
                    contactNo: String,
                    onSuccess: (response: String?) -> Unit,
                    onError: (errorString: String?, error: VolleyError?) -> Unit) {
        if (!ConnectivityReceiver.isConnected()) {
            Log.d("Add Card", "No internet connection")
            onError("No Connection", null)

        } else {
            val jsonParams = HashMap<String, String>()


            var json = JSONObject()
            json.put("name",  LocalMutators.fullName)
            json.put("email", LocalMutators.email)
            json.put("phone", LocalMutators.phoneNo)
            json.put("address", "("+addressLine1+" "+addressLine2+"), "+
                                       "Zipcode: "+zipCode+", City: "+city+", State: "+state+", Country: "+country+", PhoneNo: "+contactNo+".")


            Log.d("Hello", Config.Base_Url+"?"+json.toString())

            ApiHelper(context).makeZopatozRequest(
                    Config.Base_Url+"?custadd="+json.toString(),
                    jsonParams,
                    Request.Method.POST,
                    false,
                    false,
                    onSuccess = { responseString: String, gson: Gson ->
                        onSuccess(responseString)
                    },
                    onError = { error: VolleyError ->
                        //                        ApiHelper(this).errorHandler(error)
                        onError(null, error)
                    })
        }
    }

    fun setUserIdnotification(context: Context) {

        OneSignal.idsAvailable { userId, registrationId ->
            Log.d("debug", "User:" + userId)
            if (registrationId != null)
                Log.d("Onesignal", "registrationId:" + registrationId)

            SharedPrefHelper().setSharedPreferenceString(context, "mp_device_id", userId)

            val url = Config.API_ME
            Log.d("Onesignal", "url : " + url)

            val jsonParams = HashMap<String, String>()
            jsonParams.put("access_token", SharedPrefHelper().getSharedPreferenceString(context, Config.PREF_ACCESS_TOKEN, null)!!)
            jsonParams.put("device_id", userId)

            val api = ApiHelper(context)
            api.makeRequest(Config.API_ME,
                    Request.Method.PUT,
                    jsonParams,
                    onSuccess = {
                        responseString: String, gson: Gson ->
                        if (responseString !== "[]") {
                            Log.d(TAG, "register user notification success")
                        }
                    },
                    onError = {
                        error: VolleyError ->

                    })
        }

    }

}