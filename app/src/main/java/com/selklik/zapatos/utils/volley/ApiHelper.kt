package com.selklik.zapatos.utils.volley

import android.content.Context
import android.util.Base64
import android.util.Log
import android.widget.Toast
import com.android.volley.*
import com.android.volley.DefaultRetryPolicy.DEFAULT_MAX_RETRIES
import com.android.volley.DefaultRetryPolicy.DEFAULT_TIMEOUT_MS
import com.android.volley.toolbox.StringRequest
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.selklik.zapatos.utils.AppController
import com.selklik.zapatos.utils.Config
import com.selklik.zapatos.utils.helper.AlertDialogHelper
import org.json.JSONException
import org.json.JSONObject
import java.util.HashMap

/**
 * Created by faridhadiselklik on 29/11/2017.
 */
class ApiHelper(context: Context) {
    val TAG = "ApiHelper"
    var context: Context = context
    private val BASE_URL = Config.API_URL

    interface ApiListener {
        fun onSuccess(responseString: String, gson: Gson)
        fun onError(error: VolleyError)
    }

    fun makeRequest(module: String,
                    method: Int,
                    params: Map<String, String>,
                    shouldDisplayError: Boolean? = false,
                    shouldCache: Boolean = false,
                    onSuccess: (responseString: String, gson: Gson) -> Unit,
                    onError: (error: VolleyError) -> Unit) {
        var url = ""
        val map = params
        url = BASE_URL + module + "?"
        Log.d(TAG, "url : $url, method : $method")

        var index = 0
        when (method) {
            Request.Method.POST -> {
                url = BASE_URL + module
                //                Log.d("API", "url post : " + url);
                postRequest(url, method, params, shouldDisplayError!!, onSuccess, onError, shouldCache)
            }
            Request.Method.GET -> {

                for ((key, value) in map) {
                    index++
                    if (index >= map.size) {
                        url += key + "=" + value
                    } else {
                        url += "$key=$value&"
                    }
                }

                Log.d("API", "url get : " + url)
                getRequest(url, method, params, shouldDisplayError!!, onSuccess, onError, shouldCache)
            }
//            Request.Method.DELETE ->{
//                postRequest(url, method, params, shouldDisplayError, onSuccess, onError, shouldCache)
//            }
            else -> {
                for ((key, value) in map) {
                    index++
                    if (index >= map.size) {
                        url += key + "=" + value
                    } else {
                        url += "$key=$value&"
                    }
                }
                //                url = BASE_URL+module;
                Log.d("API", "url default : " + url)
                getRequest(url, method, params, shouldDisplayError!!, onSuccess, onError, shouldCache)
            }
        }

    }

    fun makeZopatozRequest(url: String,
                    params: Map<String, String>,
                    method: Int,
                    shouldDisplayError: Boolean? = false,
                    shouldCache: Boolean = false,
                    onSuccess: (responseString: String, gson: Gson) -> Unit,
                    onError: (error: VolleyError) -> Unit) {

        var index = 0
        when (method) {
            Request.Method.POST -> {
                postRequest(url, method, params, shouldDisplayError!!, onSuccess, onError, shouldCache)
            }
            Request.Method.GET -> {

                getRequest(url, method, params, shouldDisplayError!!, onSuccess, onError, shouldCache)

                /*for ((key, value) in map) {
                    index++
                    if (index >= map.size) {
                        url += key + "=" + value
                    } else {
                        url += "$key=$value&"
                    }
                }

                Log.d("API", "url get : " + url)*/


            }
//            Request.Method.DELETE ->{
//                postRequest(url, method, params, shouldDisplayError, onSuccess, onError, shouldCache)
//            }
            /*else -> {
                for ((key, value) in map) {
                    index++
                    if (index >= map.size) {
                        url += key + "=" + value
                    } else {
                        url += "$key=$value&"
                    }
                }
                //                url = BASE_URL+module;
                Log.d("API", "url default : " + url)*/
                //getRequest(url, method, params, shouldDisplayError!!, onSuccess, onError, shouldCache)

        }

    }


    private fun getRequest(url: String,
                           method: Int,
                           params: Map<String, String>,
                           shouldDisplayError: Boolean,
                           onSuccess: (responseString: String, gson: Gson) -> Unit,
                           onError: (error: VolleyError) -> Unit,
                           shouldCache: Boolean) {
        val request = object : StringRequest(method, url, Response.Listener { responseString ->

            if (context != null) // OWAIS EDITION
                onSuccess(responseString, GsonBuilder().create())


        }, Response.ErrorListener { error ->
            onError(error)
            if (shouldDisplayError) {
                errorHandler(error)
                Log.e("ERROR", "Error response : " + error.toString())

            }
        }) {
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                val auth = "Basic " + Base64.encodeToString((Config.API_KEY + ":" + Config.API_SECRET).toByteArray(), Base64.NO_WRAP)
                headers.put("Authorization", auth)
                headers.put("Accept", "application/json")
                Log.d("TAG", "auth : " + auth)
                return headers
            }

        }
        request.setShouldCache(shouldCache)
        request.retryPolicy = DefaultRetryPolicy(DEFAULT_TIMEOUT_MS, DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        AppController().addToRequestQueue(request)

    }

    private fun postRequest(url: String, method: Int,
                            params: Map<String, String>,
                            shouldDisplayError: Boolean,
                            onSuccess: (responseString: String, gson: Gson) -> Unit,
                            onError: (error: VolleyError) -> Unit,
                            shouldCache: Boolean) {
        val request = object : StringRequest(method, url, Response.Listener { responseString ->
            onSuccess(responseString, GsonBuilder().create())

        }, Response.ErrorListener { error ->
            //                String[] errors = {"User id is required", "post id is required"};
            onError(error)

            if (shouldDisplayError) {
                errorHandler(error)
                Log.e("ERROR", "Error response : " + error.toString())

            }
        }) {

            @Throws(AuthFailureError::class)
            override fun getParams(): Map<String, String> {
                Log.d("TAG", "params : " + params.toString() + "size : " + params.size)
                //                Map<String, String> mParam = new HashMap<String, String >();
                return params
            }

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                val auth = "Basic " + Base64.encodeToString((Config.API_KEY + ":" + Config.API_SECRET).toByteArray(), Base64.NO_WRAP)
                headers.put("Authorization", auth)
                headers.put("Accept", "application/json")
                Log.d("TAG", "auth : " + auth)
                return headers
            }

        }
        request.setShouldCache(shouldCache)
        request.retryPolicy = DefaultRetryPolicy(10000, -1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        AppController().addToRequestQueue(request)
    }

    fun makeMultipartRequest(module: String,
                             method: Int,
                             params: Map<String, String>,
                             byteArrays: ByteArray,
                             shouldDisplayError: Boolean? = false,
                             shouldCache: Boolean? = false,
                             shouldShowProgress: Boolean? = false,
                             type: String,
                             onSuccess: (responseString: String, gson: Gson) -> Unit,
                             onError: (error: VolleyError) -> Unit) {

        var url = ""
        Log.d("API", "method " + Integer.toString(method))
        url = BASE_URL + module + "?"

        val multipartRequest = object : VolleyMultipartRequest(Request.Method.POST,
                url,
                Response.Listener<NetworkResponse> { response ->
            val resultResponse = String(response.data)
            Log.d("Response", resultResponse)
            onSuccess(resultResponse, GsonBuilder().create())
        }, Response.ErrorListener { error ->
            onError(error)
            if (shouldDisplayError!!) {
                errorHandler(error)
                Log.e("ERROR", "Error response : " + error.toString())

            }
        }) {
            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                val headers = HashMap<String, String>()
                val auth = "Basic " + Base64.encodeToString((Config.API_KEY + ":" + Config.API_SECRET).toByteArray(), Base64.NO_WRAP)
                headers.put("Authorization", auth)
                headers.put("Accept", "application/json")
                return headers
            }

            @Throws(AuthFailureError::class)
            override fun getParams(): Map<String, String> {
                return params
            }

            override fun getByteData(): Map<String, DataPart> {
                val params = HashMap<String, DataPart>()

                when (type) {
                    "image" -> params.put("image", DataPart("dm_image.png", byteArrays, "image/jpeg"))
                    "video" -> params.put("video", DataPart("qna_video.mp4", byteArrays, "video/mp4"))
                    "file" ->  params.put("file", DataPart("profile_image.png", byteArrays, "image/jpeg"))
                }
                return params
            }
        }

        multipartRequest.retryPolicy = DefaultRetryPolicy(60000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        AppController.instance.addToRequestQueue(multipartRequest)

    }

    fun errorHandler(error: VolleyError) {
        val jsonErrorNetworkResponse: String? = null
        var json: String? = null
        val networkResponse = error.networkResponse
        if (networkResponse != null) {
            if (networkResponse.data != null) {
                val builder = StringBuilder()
                when (networkResponse.statusCode) {
                    400 -> {
                        json = String(networkResponse.data)
                        try {
                            val errorObg = JSONObject(json)
                            val errorArray = errorObg.getJSONArray("errors")
                            for (i in 0 until errorArray.length()) {
                                builder.append(errorArray.get(i).toString() + "\n")
                            }
                            Log.d("Error Api : ", builder.toString())
                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }

                        AlertDialogHelper().showAlert("Errors !", builder.toString(), context)
                    }
                    401 -> {
                        json = String(networkResponse.data)
                        Log.d("Error", "Error : " + json)
                        try {
                            val errorObg = JSONObject(json)
                            if (errorObg.has("status") && errorObg.has("error")) {
//                                builder.append(errorObg.get("status").toString() + "\n")
                                builder.append(errorObg.get("error"))
                            } else if (errorObg.get("errors") != null) {
                                val errorArray = errorObg.getJSONArray("errors")
                                for (i in 0 until errorArray.length()) {
                                    builder.append(errorArray.get(i).toString() + "\n")
                                }
//                                builder.append(errorObg.get("errors"))
                            }
                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }

                        AlertDialogHelper().showAlertToken("Token error !", builder.toString(), context)
                    }
                    404 -> {
                        json = String(networkResponse.data)
                        Log.d("Error", "Error : " + json)
                        try {
                            val errorObg = JSONObject(json)
                            if (errorObg.has("status") && errorObg.has("error")) {
//                                builder.append(errorObg.get("status").toString() + "\n")
                                builder.append(errorObg.get("error"))
                            } else if (errorObg.get("errors") != null) {
                                val errorArray = errorObg.getJSONArray("errors")
                                for (i in 0 until errorArray.length()) {
                                    builder.append(errorArray.get(i).toString() + "\n")
                                }
//                                builder.append(errorObg.get("errors"))
                            }
                            //                        }
                        } catch (e: JSONException) {
                            e.printStackTrace()
                        }

                        AlertDialogHelper().showAlert("ProductsShoes error !", builder.toString(), context)
                    }
                    else -> {
                        json = String(networkResponse.data)
                        Log.d("Error", "Error : " + json)
                        try {
                            val errorObg = JSONObject(json)
                            if (errorObg.has("status") && errorObg.has("error")) {
//                                builder.append(errorObg.get("status").toString() + "\n")
                                builder.append(errorObg.get("error"))
                            } else if (errorObg.get("errors") != null) {
                                val errorArray = errorObg.getJSONArray("errors")
                                for (i in 0 until errorArray.length()) {
                                    builder.append(errorArray.get(i).toString() + "\n")
                                }
//                                builder.append(errorObg.get("errors"))
                            }
                        } catch (e: JSONException) {
                            e.printStackTrace()
                            builder.append("Server error")
                        }

                        AlertDialogHelper().showAlert("Server Error !", builder.toString(), context)
                    }
                }
            }
        } else {
            if (error.javaClass == TimeoutError::class.java) {
                // Show timeout error message
                Toast.makeText(context,
                        "Oops. Timeout error!",
                        Toast.LENGTH_LONG).show()
            }
            Toast.makeText(context, "Failed to load", Toast.LENGTH_SHORT).show()
        }
    }

}