package com.selklik.zapatos.utils

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.util.Log

/**
 * Created by faridhadiselklik on 29/11/2017.
 */
class ConnectivityReceiver : BroadcastReceiver() {

    var connectivityReceiverListener: ConnectivityReceiverListener? = null

    fun ConnectivityReceiver()  {
//        super.()
    }

    override fun onReceive(context: Context, intent: Intent) {
        val cm = context
                .getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork = cm.activeNetworkInfo
        val isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting

        if (connectivityReceiverListener != null) {
            connectivityReceiverListener!!.onNetworkConnectionChanged(isConnected)
        }

    }

    companion object {
        fun isConnected(): Boolean {
            val connect: Boolean
            val cm = AppController.instance
                    .getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val activeNetwork = cm.activeNetworkInfo
            //        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
            connect = activeNetwork != null && activeNetwork.isConnectedOrConnecting
            Log.d("TAG", java.lang.Boolean.toString(connect))
            return connect

        }
    }



    interface ConnectivityReceiverListener {
        fun onNetworkConnectionChanged(isConnected: Boolean)
    }
}