package com.selklik.zapatos.utils.cartdb

import android.util.Log
import com.selklik.zapatos.utils.Config
import com.selklik.zapatos.utils.LocalMutators

class CartInfo()
{

    companion object {

        val TABLE_NAME = "myZopatozCart_"+Config.myCountry+"_"+LocalMutators.fullName.replaceBeforeLast("[\\W_&&[^°]]+","").replace(" ", "")
        val PRODUCT_ID = "product_id"
        val PRODUCT_NAME = "product_name"
        val PRODUCT_PRICE = "product_price"
        val PRODUCT_COLOR = "product_color"
        val PRODUCT_QUANTITY = "product_qnty"
        val PRODUCT_IMG =  "product_img"
        val PRODUCT_SIZE = "product_size"

        val CREATE_TABLE = (
                "CREATE TABLE " + TABLE_NAME + "("
                        + PRODUCT_ID         + " TEXT,"
                        + PRODUCT_NAME       + " TEXT,"
                        + PRODUCT_PRICE      + " TEXT,"
                        + PRODUCT_COLOR      + " TEXT,"
                        + PRODUCT_QUANTITY   + " INTEGER,"
                        + PRODUCT_IMG        + " TEXT,"
                        + PRODUCT_SIZE       + " TEXT"
                        + ")")

    }
}