package com.selklik.zapatos.utils.cartdb

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.content.ContentValues
import android.database.Cursor
import android.util.Log
import android.widget.Toast

private val DATABASE_VERSION = 1

class Cartdb (context: Context) : SQLiteOpenHelper(context, CartInfo.TABLE_NAME, null, DATABASE_VERSION) {

    private var context : Context

    init {

        this.context = context
    }

    override fun onCreate(db: SQLiteDatabase?) {
        db!!.execSQL(CartInfo.CREATE_TABLE)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db!!.execSQL("DROP TABLE IF EXISTS " + CartInfo.TABLE_NAME);
        onCreate(db);
    }

    fun InsertCart(productid: String,
                   productname: String,
                   productimg: String,
                   productprice: String,
                   productcolor: String,
                   productsize: String,
                   productqnt: Int): Long {

        var db = this.writableDatabase

        var values = ContentValues()
        var id : Long = 0

        var quantity : Int = getSpecificCartItemCount(productid)

        if (quantity == 0) {

            values.put(CartInfo.PRODUCT_ID, productid)
            values.put(CartInfo.PRODUCT_NAME, productname)
            values.put(CartInfo.PRODUCT_IMG, productimg)
            values.put(CartInfo.PRODUCT_PRICE, productprice)
            values.put(CartInfo.PRODUCT_COLOR, productcolor)
            values.put(CartInfo.PRODUCT_SIZE, productsize)
            values.put(CartInfo.PRODUCT_QUANTITY, productqnt)

            id = db.insert(CartInfo.TABLE_NAME, null, values)


        } else {

            UpdateCartItem(productqnt + getSpecificCartItemCount(productid), productid, productsize.trim())
        }

        //getAllNotes()

        db.close()

        return id
    }

    /* Use to update quantity and size */

    fun UpdateCartItem(qnty : Int, productid : String, demandedSize : String)
    {

        var strSQL_GET = "SELECT * FROM " + CartInfo.TABLE_NAME + " WHERE " + CartInfo.PRODUCT_ID + " = '" + productid + "'"

        var cursor = this.writableDatabase.rawQuery(strSQL_GET, null)

        cursor.moveToFirst()

        var currentQnty = cursor.getString(cursor.getColumnIndex(CartInfo.PRODUCT_QUANTITY))
        var currentSize = cursor.getString(cursor.getColumnIndex(CartInfo.PRODUCT_SIZE))

            currentSize = currentSize +", "+demandedSize

        val strSQL = "UPDATE "+ CartInfo.TABLE_NAME + " SET " + CartInfo.PRODUCT_QUANTITY +" = "+ qnty + ", " +
                             CartInfo.PRODUCT_SIZE + " = '" + currentSize + "' WHERE " + CartInfo.PRODUCT_ID + " = '" + productid + "'"

        this.writableDatabase.execSQL(strSQL)

    }

    fun getSpecificCartItemCount(id: String): Int {

        val db = this.readableDatabase

        var sql_query =  " select "  + CartInfo.PRODUCT_QUANTITY + " from " + CartInfo.TABLE_NAME +
                                "  where "   + CartInfo.PRODUCT_ID + " = '${id}'"

        var mCur = db.rawQuery(sql_query, null)

        mCur.moveToFirst()

        var qnty : Int ?= null

        try {

            qnty = mCur.getInt(mCur.getColumnIndex(CartInfo.PRODUCT_QUANTITY))

        }catch (e : IndexOutOfBoundsException){
            qnty = 0
            e.printStackTrace() }

        return qnty!!

    }

    fun getAllItemsFromCarts(): ArrayList<CartModel> {

        val cartArray = ArrayList<CartModel>()

        // Select All Query
        val selectQuery = "SELECT  * FROM " + CartInfo.TABLE_NAME

        val db = this.writableDatabase
        val cursor = db.rawQuery(selectQuery, null)

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                val cartModel = CartModel()

                    cartModel.producT_ID = cursor.getString(cursor.getColumnIndex(CartInfo.PRODUCT_ID))
                    cartModel.producT_NAME = cursor.getString(cursor.getColumnIndex(CartInfo.PRODUCT_NAME))
                    cartModel.producT_IMG = cursor.getString(cursor.getColumnIndex(CartInfo.PRODUCT_IMG))
                    cartModel.producT_COLOR = cursor.getString(cursor.getColumnIndex(CartInfo.PRODUCT_COLOR))
                    cartModel.producT_QUANTITY = cursor.getInt(cursor.getColumnIndex(CartInfo.PRODUCT_QUANTITY))
                    cartModel.producT_SIZE  = cursor.getString(cursor.getColumnIndex(CartInfo.PRODUCT_SIZE))
                    cartModel.producT_PRICE = cursor.getString(cursor.getColumnIndex(CartInfo.PRODUCT_PRICE))

                    cartArray.add(cartModel)

            } while (cursor.moveToNext())
        }

        // close db connection
        db.close()

        // return notes list
        return cartArray
    }

    fun getCartsItem() : Array<String>? {

        val selectQuery = "SELECT * FROM "+ CartInfo.TABLE_NAME
        val db = this.getReadableDatabase()
        val cursor = db.rawQuery(selectQuery, null)
        val data: Array<String>? = null

        Toast.makeText(context, " HELLO ", Toast.LENGTH_SHORT).show()

        if (cursor.moveToFirst()) {
            do {

                Log.d("Hello", cursor.getString(0))

                // get the data into array, or class variable
            } while (cursor.moveToNext())
        }
        cursor.close()
        return data
    }

    fun clearTable() {

        //Log.d("Hello", "SIZE " + getAllNotes().size.toString() + " ");
        this.writableDatabase.execSQL("delete from " + CartInfo.TABLE_NAME);

    }

    fun deleteCart(productid : String) {

        val db = this.writableDatabase
        db.delete(CartInfo.TABLE_NAME, CartInfo.PRODUCT_ID + " = ?", arrayOf(productid))
        db.close()
    }

    fun getCartQuantity() : Int{

        var quantity = 0
        var totalItemsCount  = 0

            val db = this.writableDatabase
            val query = "SELECT  * FROM " + CartInfo.TABLE_NAME
            val cursor = db.rawQuery(query, null)
            if (cursor.moveToFirst()) {

                do {

                    quantity = cursor.getInt(cursor.getColumnIndex(CartInfo.PRODUCT_QUANTITY))

                    totalItemsCount = totalItemsCount + quantity

                } while (cursor.moveToNext())
            }

            return totalItemsCount
    }


    fun getTotalItem() : Int {

        val db = this.writableDatabase
        val countQuery = "SELECT  * FROM " + CartInfo.TABLE_NAME
        val cursor = db.rawQuery(countQuery, null)

        return cursor.count
    }

}
