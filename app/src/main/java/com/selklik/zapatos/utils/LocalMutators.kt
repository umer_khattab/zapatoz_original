package com.selklik.zapatos.utils

import android.app.Activity


class  LocalMutators {


    companion object {

        var activity: Activity? = null
        var email: String? = null

        var cardNumber = "null"
        var cvcCode = "null"

        var expiryMonth = "null"
        var expiryYear = "null"

        var line1 = "null"
        var line2 = "null"

        var city = "null"
        var state = "null"

        var country = "null"
        var zipcode = 0


        var totalPayment: Double? = 0.0


        var fullName = "null"
        var phoneNo = ""

        var fullAddress = "null"


        fun resetAll(){

            activity = null
            email    = null
            city     = "null"
            line1    = "null"
            line2    = "null"
            state    = "null"
            country  = "null"
            zipcode  = 0
            totalPayment = 0.0
            fullName = "null"
            phoneNo = "null"
            fullAddress = "null"

        }

    }

}