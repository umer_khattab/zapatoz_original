package com.selklik.zapatos

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.Toast
import com.google.android.exoplayer2.*
import com.google.android.exoplayer2.Player.*
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory
import com.google.android.exoplayer2.mediacodec.MediaCodecRenderer
import com.google.android.exoplayer2.mediacodec.MediaCodecUtil
import com.google.android.exoplayer2.source.ExtractorMediaSource
import com.google.android.exoplayer2.source.TrackGroupArray
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.trackselection.TrackSelectionArray
import com.google.android.exoplayer2.upstream.*
import com.google.android.exoplayer2.util.Util
import kotlinx.android.synthetic.main.activity_video_player.*

/**
 * Created by faridhadiselklik on 20/12/2017.
 */
class VideoPlayerActivity : AppCompatActivity() {
    private var player: SimpleExoPlayer? = null
    private var mainHandler: Handler? = null

    private var window: Timeline.Window? = null
    private var mediaDataSourceFactory: DataSource.Factory? = null
    private var trackSelector: DefaultTrackSelector? = null
    private var shouldAutoPlay: Boolean = false
    private var bandwidthMeter: BandwidthMeter? = null
    private var videoUrl: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_video_player)

        var intent: Intent = intent


        videoUrl = intent.getStringExtra("video_url")

        try {

            if (intent.getBooleanExtra("videoEnded", false))
                shouldAutoPlay = false


        }catch (e : NullPointerException)
        {e.printStackTrace()
            shouldAutoPlay = true
        }

        try {

            var premimumContent = intent.getBooleanExtra("premium", false)


            Handler().postDelayed(Runnable {

                if (premimumContent)
                {
                    waterMark_iv.visibility = View.VISIBLE
                    /*  waterMark_iv.y = (player_view.height/2).toFloat() + (spectaculum.height/2) - waterMark_iv.height;
                      waterMark_iv.x = (screenWidth -  waterMark_iv.width).toFloat() -  waterMark_iv.width/15*/
                }
                else
                {
                    waterMark_iv.visibility = View.INVISIBLE

                }


            }, 200)


        }catch (e : NullPointerException)
        {e.printStackTrace()}


        bandwidthMeter = DefaultBandwidthMeter()
        mediaDataSourceFactory = DefaultDataSourceFactory(this, Util.getUserAgent(this, "mediaPlayerSample"), bandwidthMeter as TransferListener<in DataSource>?) as DataSource.Factory?

        window = Timeline.Window()

    }

    private fun initializePlayer() {
        player_view!!.requestFocus()

        val videoTrackSelectionFactory = AdaptiveTrackSelection.Factory(bandwidthMeter)

        trackSelector = DefaultTrackSelector(videoTrackSelectionFactory)

        player = ExoPlayerFactory.newSimpleInstance(this, trackSelector!!)
        player!!.addListener(object : Player.EventListener {
            override fun onTimelineChanged(timeline: Timeline?, manifest: Any?) {

            }

            override fun onTracksChanged(trackGroups: TrackGroupArray, trackSelections: TrackSelectionArray) {

            }

            override fun onLoadingChanged(isLoading: Boolean) {
                Log.d("TAG", "is loading : " + isLoading)
            }

            override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
                Log.d("TAG", "playback state : " + playbackState)
                when (playbackState) {
                    STATE_BUFFERING -> layout_progress!!.visibility = View.VISIBLE
                    STATE_READY -> layout_progress!!.visibility = View.GONE
                    STATE_ENDED -> {
                        shouldAutoPlay = false
                        initializePlayer()


                    }
                }

            }

            override fun onRepeatModeChanged(repeatMode: Int) {

            }

            override fun onPlayerError(e: ExoPlaybackException) {
                var errorString: String? = null
                if (e.type == ExoPlaybackException.TYPE_RENDERER) {
                    val cause = e.rendererException
                    if (cause is MediaCodecRenderer.DecoderInitializationException) {
                        // Special case for decoder initialization failures.
                        if (cause.decoderName == null) {
                            if (cause.cause is MediaCodecUtil.DecoderQueryException) {
                                errorString = getString(R.string.error_querying_decoders)
                            } else if (cause.secureDecoderRequired) {
                                errorString = getString(R.string.error_no_secure_decoder,
                                        cause.mimeType)
                            } else {
                                errorString = getString(R.string.error_no_decoder,
                                        cause.mimeType)
                            }
                        } else {
                            errorString = getString(R.string.error_instantiating_decoder,
                                    cause.decoderName)
                        }
                    }
                }
                if (errorString != null) {
                    Toast.makeText(this@VideoPlayerActivity, errorString, Toast.LENGTH_SHORT).show()
                    //                    showToast(errorString);
                }
            }

            override fun onPositionDiscontinuity() {

            }

            override fun onPlaybackParametersChanged(playbackParameters: PlaybackParameters) {

            }
        })

        player_view!!.player = player

        player!!.playWhenReady = shouldAutoPlay

        mainHandler = Handler()
        //        MediaSource mediaSource = new HlsMediaSource(Uri.parse("https://d1vntv0m6mp43l.cloudfront.net/answers/fe9fc289c3ff0af142b6d3bead98a923/video.mov"),
        //                mediaDataSourceFactory, mainHandler, null);

        val extractorsFactory = DefaultExtractorsFactory()
        //"http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4"
        if (videoUrl != null) {
            val mediaSource = ExtractorMediaSource(Uri.parse(videoUrl!!),
                    mediaDataSourceFactory, extractorsFactory, null, null)
            player!!.prepare(mediaSource)
        }

//        ivHideControllerButton!!.setOnClickListener { player_view!!.hideController() }
    }

    private fun releasePlayer() {
        if (player != null) {
            shouldAutoPlay = player!!.playWhenReady
            player!!.release()
            player = null
            trackSelector = null
        }
    }

    public override fun onStart() {
        super.onStart()
        if (Util.SDK_INT > 23) {
            initializePlayer()
        }
    }

    public override fun onResume() {
        super.onResume()
        if (Util.SDK_INT <= 23 || player == null) {
            initializePlayer()
        }
    }

    public override fun onPause() {
        super.onPause()
        if (Util.SDK_INT <= 23) {
            releasePlayer()
        }
    }

    public override fun onStop() {
        super.onStop()
        if (Util.SDK_INT > 23) {
            releasePlayer()
        }
    }
}