package com.selklik.zapatos.model

/**
 * Created by faridhadiselklik on 29/11/2017.
 */
class Filmography(var title: String,
                  var year: String,
                  var url: String,
                  var photos: Photo)