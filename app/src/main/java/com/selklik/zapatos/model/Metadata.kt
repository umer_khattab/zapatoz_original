package com.selklik.zapatos.model

/**
 * Created by faridhadiselklik on 15/12/2017.
 */
class Metadata(var account_name: String,
               var account_username: String,
               var post_id: String,
               var comment_count: Int,
               var like_count: Int,
               var price: Int,
               var full_access: Boolean,
               var user: User,
               var question_and_answer_id: String) {
}

//"metadata": {
//    "category": {
//        "id": 5,
//        "name": "Fans Only",
//        "icon_light": "https://d1vntv0m6mp43l.cloudfront.net/categories/e4da3b7fbbce2345d7772b0674a318d5/__Fans_Only_Light.png",
//        "icon_dark": "https://d1vntv0m6mp43l.cloudfront.net/categories/e4da3b7fbbce2345d7772b0674a318d5/__Fans_Only.png"
//    },
//    "price": 4000,
//    "full_access": false,
//    "number_of_purchases": 2,
//    "comment_count": 0,
//    "like_count": 0,
//    "liked": false
//},