package com.selklik.zapatos.model

/**
 * Created by faridhadiselklik on 29/12/2017.
 */
class History(var reference_id: String?,
              var type: String?,
              var value: Int,
              var post: Feed?,
              var timestamp: String?) {
}