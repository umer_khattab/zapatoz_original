package com.selklik.zapatos.model

import com.google.gson.JsonArray

/**
 * Created by faridhadiselklik on 20/12/2017.
 */
class Comment(var count: Int,
              var comments: JsonArray)
