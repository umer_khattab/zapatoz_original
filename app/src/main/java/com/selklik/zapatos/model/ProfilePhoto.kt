package com.selklik.zapatos.model

/**
 * Created by faridhadiselklik on 29/11/2017.
 */
class ProfilePhoto(var full_resolution: String,
                   var square: String,
                   var thumbnail: String) {
}