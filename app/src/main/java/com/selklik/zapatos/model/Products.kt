package com.selklik.zapatos.model

import com.google.gson.JsonArray

/**
 * Created by faridhadiselklik on 08/12/2017.
 */
class Products(var id: String,
               var bought_by: User,
               var sku: String,
               var title: String,
               var currency: String,
               var currency_symbol: String,
               var numeric_price: Double,
               var price: String,
               var details: String,
               var view_count: Int,
               var photos: JsonArray){

}