package com.selklik.zapatos.model

/**
 * Created by faridhadiselklik on 22/12/2017.
 */
class Address(var id: String,
              var email: String,
              var address: String,
              var phone: String,
              var default_address: String,
              /*------------------ */
              var line1: String,
              var line2: String,
              var city: String,
              var state: String,
              var country: String,
              var zipcode: Int,
              var is_default: Boolean,
              var formatted: String,
              var user: User) {
}