package com.selklik.zapatos.model

/**
 * Created by faridhadiselklik on 22/12/2017.
 */
class Messages(var id: String,
               var kind: String,
               var question: String,
               var user: User,
               var artist: Artist,
               var answer: Answer,
               var timestamp: String) {

}