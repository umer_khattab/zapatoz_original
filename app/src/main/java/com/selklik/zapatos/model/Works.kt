package com.selklik.zapatos.model

import com.google.gson.JsonArray

/**
 * Created by faridhadiselklik on 29/11/2017.
 */
class Works(var filmography: JsonArray,
            var discography: JsonArray)

