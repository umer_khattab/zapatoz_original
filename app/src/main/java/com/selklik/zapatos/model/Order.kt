package com.selklik.zapatos.model

/**
 * Created by faridhadiselklik on 22/12/2017.
 */
class Order(var id: String,
            var status: String,
            var reference_id: String,
            var user: User,
            var product: Products,
            var card: Card,
            var address: Address)