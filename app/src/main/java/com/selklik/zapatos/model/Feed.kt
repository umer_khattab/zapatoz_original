package com.selklik.zapatos.model

import com.google.gson.JsonArray
import com.google.gson.JsonElement

/**
 * Created by faridhadiselklik on 06/12/2017.
 */
class Feed(var id: String,
           var artist: Artist,
           var type: String,
           var body: String,
           var attachment: String?,
           var attachment_type: String?,
           var photos: JsonElement?,
           var metadata: Metadata,
           var comment_count: Int,
           var like_count: Int,
           var liked: Boolean,
           var timestamp: String) {
}

//{
//    "id": 4411071,
//    "artist": {
//        "id": 1100,
//        "name": "Ussy Sulistiawaty",
//        "gender": "Female",
//        "following": false,
//        "followers_count": 5,
//        "genre": "Entertainment",
//        "country": "id",
//        "country_name": "Indonesia",
//        "profile_photos": {
//            "full_resolution": "https://d1vntv0m6mp43l.cloudfront.net/profile_photos/74791edf1f8e8b8289a5067737630874/24175669_155419198559161_8182722368934772736_n.jpg",
//            "square": "https://d1vntv0m6mp43l.cloudfront.net/profile_photos/74791edf1f8e8b8289a5067737630874/square_24175669_155419198559161_8182722368934772736_n.jpg",
//            "thumbnail": "https://d1vntv0m6mp43l.cloudfront.net/profile_photos/74791edf1f8e8b8289a5067737630874/thumbnail_24175669_155419198559161_8182722368934772736_n.jpg"
//            },
//        "referral_code": "USSY-1100",
//        "qna_enabled": true,
//        "state": null,
//        "parliament": "",
//        "parliament_code": "",
//        "registered_voters": 0,
//        "social_network_accounts": {
//        "facebook": "https://www.facebook.com/ussypratama13",
//        "twitter": null,
//        "instagram": "https://www.instagram.com/ussypratama/"
//    }
//    },
//    "type": "premium",
//    "body": "Hello",
//    "attachment": "https://d1vntv0m6mp43l.cloudfront.net/premium_post_photos/e7f8a7fb0b77bcb3b283af5be021448f/1512724578.51223.jpg",
//    "attachment_type": "photo",
//    "photos": [
//    {
//        "width": 576,
//        "height": 1024,
//        "versions": {
//        "full_resolution": "https://d1vntv0m6mp43l.cloudfront.net/premium_post_photos/e7f8a7fb0b77bcb3b283af5be021448f/1512724578.51223.jpg",
//        "thumbnail": "https://d1vntv0m6mp43l.cloudfront.net/premium_post_photos/e7f8a7fb0b77bcb3b283af5be021448f/thumbnail_1512724578.51223.jpg",
//        "preview": "https://d1vntv0m6mp43l.cloudfront.net/premium_post_photos/e7f8a7fb0b77bcb3b283af5be021448f/preview_1512724578.51223.jpg"
//    }
//    }
//    ],
//    "metadata": {
//        "category": {
//            "id": 5,
//            "name": "Fans Only",
//            "icon_light": "https://d1vntv0m6mp43l.cloudfront.net/categories/e4da3b7fbbce2345d7772b0674a318d5/__Fans_Only_Light.png",
//            "icon_dark": "https://d1vntv0m6mp43l.cloudfront.net/categories/e4da3b7fbbce2345d7772b0674a318d5/__Fans_Only.png"
//        },
//        "price": 4000,
//        "full_access": false,
//        "number_of_purchases": 2,
//        "comment_count": 0,
//        "like_count": 0,
//        "liked": false
//    },
//    "comment_count": 0,
//    "like_count": 0,
//    "liked": false,
//    "timestamp": "2017-12-08 09:16:57"
//},