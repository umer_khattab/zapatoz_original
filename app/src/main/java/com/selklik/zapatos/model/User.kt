package com.selklik.zapatos.model

/**
 * Created by faridhadiselklik on 29/11/2017.
 */
class User(var id: String,
           var name: String,
           var email: String,
           var notification: Boolean,
           var profile_photos: ProfilePhoto,
           var profile_photo: String,
           var current_balance: Double,
           var access_token: String) {
}
