package com.selklik.zapatos.model

import com.google.gson.JsonObject

/**
 * Created by faridhadiselklik on 04/12/2017.
 */
class Artist(var id: String,
             var name: String,
             var email: String,
             var gender: String,
             var social_network_accounts: JsonObject,
             var followers_count: Int,
             var profile_photos: ProfilePhoto)

{
//    "id": 1100,
//    "name": "Ussy Sulistiawaty",
//    "email": null,
//    "gender": "Female",
//    "country_code": 65,
//    "phone_no": 8119502668,
//    "date_of_birth": null,
//    "earning": 4000,
//    "following": false,
//    "followers_count": 3,
//    "genre": "Entertainment",
//    "country": "id",
//    "country_name": "Indonesia",
//    "profile_photos": {
//        "full_resolution": "https://d1vntv0m6mp43l.cloudfront.net/profile_photos/74791edf1f8e8b8289a5067737630874/24175669_155419198559161_8182722368934772736_n.jpg",
//        "square": "https://d1vntv0m6mp43l.cloudfront.net/profile_photos/74791edf1f8e8b8289a5067737630874/square_24175669_155419198559161_8182722368934772736_n.jpg",
//        "thumbnail": "https://d1vntv0m6mp43l.cloudfront.net/profile_photos/74791edf1f8e8b8289a5067737630874/thumbnail_24175669_155419198559161_8182722368934772736_n.jpg"
//    },
//    "referral_code": "USSY-1100",
//    "qna_enabled": true,
//    "state": null,
//    "parliament": "",
//    "parliament_code": "",
//    "registered_voters": 0,
//    "social_network_accounts": {
//        "facebook": "https://www.facebook.com/ussypratama13",
//        "twitter": null,
//        "instagram": "https://www.instagram.com/ussypratama/"
//    },
//    "access_token": "MiVWnUfKEzgVnPP2MTrA9MTJ"
}