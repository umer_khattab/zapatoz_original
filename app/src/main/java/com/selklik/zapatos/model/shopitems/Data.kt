package com.selklik.zapatos.model.shopitems

import java.util.*

class Data(var id : Int,
           var prod_id : String,
           var cat_id : String,
           var prod_name : String,
           var description : String,
           var price : String,
           var int_price : String,
           var prod_size : String,
           var prod_stoke : String,
           var products : ArrayList<Products>)
