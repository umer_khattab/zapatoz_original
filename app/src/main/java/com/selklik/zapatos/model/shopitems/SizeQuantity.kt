package com.selklik.zapatos.model.shopitems

class SizeQuantity(var Size6:  Int,
                   var Size7:  Int,
                   var Size8:  Int,
                   var Size9:  Int,
                   var Size10: Int)