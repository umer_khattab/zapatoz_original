package com.selklik.zapatos.model

import com.google.gson.JsonObject

/**
 * Created by faridhadiselklik on 29/11/2017.
 */
class About(var about: String,
            var welcome_message: String,
            var welcome_video_url: String,
            var welcome_video_photos: JsonObject,
            var works: JsonObject) {

}
