package com.selklik.zapatos.model

/**
 * Created by faridhadiselklik on 20/12/2017.
 */
class CommentDetails(var id: String,
                     var from: User,
                     var body: String,
                     var owner: Boolean,
                     var timestamp: String) {
}