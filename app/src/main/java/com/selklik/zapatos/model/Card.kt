package com.selklik.zapatos.model

/**
 * Created by faridhadiselklik on 22/12/2017.
 */
class Card(var id: String,
           var source_id: String,
           var brand: String,
           var last4: String,
           var exp_month: Int,
           var exp_year: Int,
           var country: String,
           var is_default: Boolean,
           var user: User,
           var kind: String) {
}