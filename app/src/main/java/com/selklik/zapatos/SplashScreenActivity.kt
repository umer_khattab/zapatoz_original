package com.selklik.zapatos

import android.content.DialogInterface
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.content.Intent
import android.support.design.widget.Snackbar
import android.util.Log
import android.view.View
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.VolleyError
import com.google.gson.Gson
import com.selklik.zapatos.model.User
import com.selklik.zapatos.ui.WelcomeActivity
import com.selklik.zapatos.utils.Config.Companion.PREF_ABOUT_ARTIST
import com.selklik.zapatos.utils.Config.Companion.PREF_KEEP_LOGIN
import com.selklik.zapatos.utils.volley.ApiHelper
import java.util.HashMap
import android.content.pm.PackageManager
import android.provider.SyncStateContract.Helpers.update
import android.content.pm.PackageInfo
import android.os.Handler
import android.support.v7.app.AlertDialog
import android.util.Base64
import android.view.WindowManager
import com.selklik.zapatos.utils.*
import com.vansuita.library.CheckNewAppVersion
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException


/**
 * Created by faridhadiselklik on 29/11/2017.
 */
class SplashScreenActivity : AppCompatActivity() {
    private var TAG = this@SplashScreenActivity.javaClass.simpleName
    private var gson = Gson()
    private var user: User? = null
    private var keepLogin: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.splashscreen)

        try {
            var info = getPackageManager().getPackageInfo(
                    this.packageName,
                    PackageManager.GET_SIGNATURES)

            for ( signature in info.signatures) {

                var md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }

        } catch (e : PackageManager.NameNotFoundException) {
            e.printStackTrace();
        }

        keepLogin = SharedPrefHelper().getSharedPreferenceBoolean(PREF_KEEP_LOGIN, false)
        Log.d("Splash Screen", "keep login : $keepLogin")

        when (keepLogin) {
            true -> {

                user = gson.fromJson(AppController.instance.getUserData(), User::class.java)
                LocalMutators.fullName = user!!.name
                LocalMutators.email = user!!.email


//                SharedPrefHelper().setSharedPreferenceString(this, Config.PREF_USER_DATA, userData)
//                SharedPrefHelper().setSharedPreferenceString(this, Config.PREF_ACCESS_TOKEN, user!!.access_token)
                AppController.instance.setUserIdnotification(this)
                var aboutUssyData = SharedPrefHelper().getSharedPreferenceString(this, PREF_ABOUT_ARTIST, "")
                if (aboutUssyData == "") {
                    fetchAboutUssy()
                } else {

                    Handler().postDelayed(Runnable { startMainActivity() }, 2000)


                }
            }
            false -> {
                startActivity(Intent(this, WelcomeActivity::class.java))
                overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out)
                finish()
            }
        }


    }

    private fun startMainActivity() {
        startActivity(Intent(this, MainActivity::class.java).putExtra("previousLogged",true))
//        startActivity(Intent(this, WelcomeActivity::class.java))
        overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out)
        finish()
    }

    private fun fetchAboutUssy() {
        if (!ConnectivityReceiver.isConnected()) {
            Log.d(TAG, "No internet connection")
        } else {
            val jsonParams = HashMap<String, String>()
            jsonParams.put("access_token", user!!.access_token)

            ApiHelper(this).makeRequest(
                    module = Config.API_ABOUT_ARTIST,
                    method = Request.Method.GET,
                    params = jsonParams,
                    onSuccess = { responseString: String, gson: Gson ->
                        Log.d("Splash Screen", "response string : " + responseString)
                        SharedPrefHelper().setSharedPreferenceString(this, PREF_ABOUT_ARTIST, responseString)
//                        fetchUserAddress()
                        startMainActivity()

                        //Toast.makeText(applicationContext, "FETCH ABOUT USSY", Toast.LENGTH_SHORT).show()

                    }, onError = {})
        }
    }

//    private fun fetchUserAddress() {
//        if (!ConnectivityReceiver.isConnected()) {
//            Log.d(TAG, "No internet connection")
//        } else {
//            val jsonParams = HashMap<String, String>()
//            jsonParams.put("access_token", user!!.access_token)
//
//            ApiHelper(this).makeRequest(
//                    Config.API_ME + "/addresses",
//                    Request.Method.GET,
//                    jsonParams,
//                    false,
//                    false,
//                    onSuccess = { responseString: String, gson: Gson ->
//                        Log.d("Splash Screen", "response string : " + responseString)
//                        if (responseString != "[]") {
//
//                            SharedPrefHelper().setSharedPreferenceString(this, Config.PREF_USER_ADDRESS, responseString)
//                            fetchCard()
//                        } else {
//                            //                            SharedPrefHelper().setSharedPreferenceString(this, Config.PREF_USER_CARD, null)
//                            fetchCard()
//
//                        }
//                    }, onError = {})
//        }
//    }


    private fun loadJSONFromAsset(): String {
        val file_name = "user.json"
        val json_string = application.assets.open(file_name).bufferedReader().use {
            it.readText()
        }

        return json_string
    }

    inline fun View.snack(message: String, length: Int = Snackbar.LENGTH_LONG, f: Snackbar.() -> Unit) {
        val snack = Snackbar.make(this, message, length)
        snack.f()
        snack.show()
    }

}