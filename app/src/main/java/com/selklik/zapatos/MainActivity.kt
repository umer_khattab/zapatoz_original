package com.selklik.zapatos
import android.content.Context
import android.content.Intent
import android.graphics.PorterDuff
import android.os.Bundle
import android.os.Handler
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import kotlinx.android.synthetic.main.activity_main.*
import android.view.LayoutInflater
import android.widget.TextView
import com.selklik.zapatos.ui.fragment.*
import android.view.WindowManager
import android.view.View
import com.selklik.zapatos.ui.adapter.ShopAdapter.MainFragmentsAdapter
import com.selklik.zapatos.utils.cartdb.Cartdb
import android.widget.Toast
import android.telephony.TelephonyManager
import android.view.animation.AnimationUtils
import android.widget.LinearLayout
import com.android.volley.Request
import com.github.tbouron.shakedetector.library.ShakeDetector
import com.selklik.zapatos.ui.ShoppingCartActivity
import com.selklik.zapatos.utils.Config
import com.selklik.zapatos.utils.LocalMutators
import com.selklik.zapatos.utils.volley.ApiHelper
import kotlinx.android.synthetic.main.raw_promo.*
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap


class MainActivity : AppCompatActivity() {

    private var arrayFragments = arrayListOf<Fragment> (IntroduceMainFragment(),
                                                        MainStoreFragment(),
                                                        FeedMainFragment(),
                                                        QnaMainFragment(),
                                                        AccountMainFragment())

    private var arrayTabIcons = arrayListOf<Int>(
            R.drawable.ic_tab_home,
            R.drawable.ic_tab_store,
            R.drawable.ic_tab_feed,
            R.drawable.ic_tab_qna,
            R.drawable.ic_tab_profile)

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)
        if (hasFocus) {
            getWindow().getDecorView().setSystemUiVisibility(
            View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    or  View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    or  View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    or  View.SYSTEM_UI_FLAG_FULLSCREEN
                    or  View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY)
        }
    }

    private fun setupIcons() {
        for (i in 0..(arrayTabIcons.size - 1)) {
                tabLayout2.getTabAt(i)!!.setIcon(arrayTabIcons[i])
        }

    }

    override fun onResume() {
        super.onResume()

        if (Cartdb(applicationContext).getTotalItem() > 0)
            count_tv.setText(Cartdb(applicationContext).getTotalItem().toString().trim())
        else
            count_tv.setText("0")

        tabLayout2.getTabAt(0)!!.getIcon()!!.setColorFilter(ContextCompat.getColor(applicationContext, R.color.white), PorterDuff.Mode.SRC_IN);
        tabLayout2.getTabAt(1)!!.getIcon()!!.setColorFilter(ContextCompat.getColor(applicationContext, R.color.white), PorterDuff.Mode.SRC_IN);
        tabLayout2.getTabAt(2)!!.getIcon()!!.setColorFilter(ContextCompat.getColor(applicationContext, R.color.white), PorterDuff.Mode.SRC_IN);
        tabLayout2.getTabAt(3)!!.getIcon()!!.setColorFilter(ContextCompat.getColor(applicationContext, R.color.white), PorterDuff.Mode.SRC_IN);
        tabLayout2.getTabAt(4)!!.getIcon()!!.setColorFilter(ContextCompat.getColor(applicationContext, R.color.white), PorterDuff.Mode.SRC_IN);

            //Find all childs for tablayout
            for (i in 0 until tabLayout2.getChildCount()) {
                    var linearList = (tabLayout2!!.getChildAt(i) as LinearLayout)

                    for(x in 0 until linearList.getChildCount()) {
            //One by one again rotate layout for text and icons
                        var item=( linearList.getChildAt(x) as LinearLayout);
                        item!!.rotationX = 180.0f
                    }
                }

    }

    override fun onPause() {
        super.onPause()

        Config.myCountry = "my"
        Config.myCurrencyUnit = getString(R.string.local_unit)+" "
        Config.payMethod = "billplz"

    }

    override fun onStop() {
        super.onStop()

        Config.myCountry = "my"
        Config.myCurrencyUnit = getString(R.string.local_unit)+" "
        Config.payMethod = "billplz"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        setContentView(R.layout.activity_main)

        Config.myCountry = "my"
        Config.myCurrencyUnit = getString(R.string.local_unit)+" "
        Config.payMethod = "billplz"



        /* Get My Current Country */
        /*try {
            Config.myCountry = getUserCountry(applicationContext)!!
        }catch (e : NullPointerException)
            { Config.myCountry = getResources().getConfiguration().locale.getCountry() }*/

        /*if (Config.myCountry.trim().equals("my"))
        {
            Config.myCurrencyUnit = getString(R.string.local_unit)+" "
            Config.payMethod = "billplz"

        } else
        {
            Config.myCurrencyUnit = getString(R.string.international_unit)+" "
            Config.payMethod = "stripe"
        }*/

        tabLayout2.addTab(tabLayout2.newTab(), true)
        tabLayout2.addTab(tabLayout2.newTab())
        tabLayout2.addTab(tabLayout2.newTab())
        tabLayout2.addTab(tabLayout2.newTab())
        tabLayout2.addTab(tabLayout2.newTab())


        //Cartdb(applicationContext).clearTable()

        btn_cart.setOnClickListener(View.OnClickListener {

            var intent = Intent(this, ShoppingCartActivity::class.java)
            startActivity(intent)

        })


        var mainAdapter = MainFragmentsAdapter(this, supportFragmentManager, arrayFragments)
        ViewPager_Main.adapter = mainAdapter

        tabLayout2.setupWithViewPager(ViewPager_Main,true)

        setupIcons()

        tabLayout2.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {

            override fun onTabSelected(tab: TabLayout.Tab) {

                resetTabColors(tab.position)
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {

            }

            override fun onTabReselected(tab: TabLayout.Tab) {

            }
        })

        ShakeDetector.create(this, {


            promo_dialog.visibility = View.VISIBLE

            var anim = AnimationUtils.loadAnimation(applicationContext, R.anim.shake)
            shake_parent.startAnimation(anim)

            promo_dialog.setOnClickListener(View.OnClickListener {

                it.visibility = View.GONE

            })

            var minimum = 0
            var maximum = 1000

            val sdf = SimpleDateFormat("EEEE")
            val d = Date()
            val dayOfTheWeek = sdf.format(d)

            if (dayOfTheWeek.equals("Friday"))
            {
                val rn = Random()
                val range = maximum - minimum + 1
                val randomNum = rn.nextInt(range) + minimum

                var json = JSONObject()
                        json.put("name", LocalMutators.fullName)
                        json.put("email",LocalMutators.email)

                var hashMap = HashMap<String, String>()

                    ApiHelper(this).makeZopatozRequest(
                            Config.Base_Url+"?shake="+json.toString(),
                            hashMap,Request.Method.POST, false, false,

                            onSuccess = {responseString, gson ->

                                if (responseString.trim().equals("success")){

                                    if (randomNum == 555) {

                                        promo_status.setText("Congratulations")
                                        zapatos_promo.setText("Your name has been nominated for lucky draw.")
                                        Handler().postDelayed(Runnable {  zapatos_promo.animate().alpha(1.0f).setDuration(500).start() }, 2000)

                                    } else {

                                        zapatos_promo.alpha = 1.0f
                                        promo_status.setText("Sorry, Better luck next time.")
                                        zapatos_promo.setText("Try on next Friday")

                                    }

                                } else {

                                        zapatos_promo.alpha = 1.0f
                                        promo_status.setText("Sorry, You tried already")
                                        zapatos_promo.setText("Try on next Friday")
                                }


                            }, onError = {error ->

                    })


            }  else {

                promo_status.setText("Have patience")
                zapatos_promo.setText("Please try on Friday Only")
                Handler().postDelayed(Runnable {  zapatos_promo.animate().alpha(1.0f).setDuration(500).start() }, 2000)

            }

        })

    }

    fun resetTabColors(tabposition : Int) {

        for (i in 0..(tabLayout2.tabCount-1))
        {
            if (i == tabposition)
                tabLayout2.getTabAt(i)!!.getIcon()!!.setColorFilter(ContextCompat.getColor(applicationContext, R.color.colorZopatozGold), PorterDuff.Mode.SRC_IN);
            else
                tabLayout2.getTabAt(i)!!.getIcon()!!.setColorFilter(ContextCompat.getColor(applicationContext, R.color.white), PorterDuff.Mode.SRC_IN);

        }

    }

    fun getUserCountry(context: Context): String? {
        try {
            val tm = context.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
            val simCountry = tm.simCountryIso
            if (simCountry != null && simCountry.length == 2) { // SIM country code is available
                return simCountry.toLowerCase(Locale.US)
            } else if (tm.phoneType != TelephonyManager.PHONE_TYPE_CDMA) { // device is not 3G (would be unreliable)
                val networkCountry = tm.networkCountryIso
                if (networkCountry != null && networkCountry.length == 2) { // network country code is available
                    return networkCountry.toLowerCase(Locale.US)
                }
            }
        } catch (e: Exception) {
        }

        return null
    }

}

