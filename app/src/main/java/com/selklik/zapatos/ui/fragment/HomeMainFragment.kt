package com.selklik.zapatos.ui.fragment

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.android.volley.toolbox.NetworkImageView
import com.google.gson.Gson
import com.selklik.zapatos.R
import com.selklik.zapatos.utils.Config.Companion.PREF_ABOUT_ARTIST
import kotlinx.android.synthetic.main.fragment_home_main.*
import android.support.v4.widget.NestedScrollView
import android.util.Log
import com.google.gson.reflect.TypeToken
import android.support.v4.content.ContextCompat
import android.support.v4.graphics.drawable.DrawableCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.Toast
import com.android.volley.Request
import com.facebook.FacebookSdk.getApplicationContext
import com.google.gson.JsonObject
import com.selklik.zapatos.VideoPlayerActivity
import com.selklik.zapatos.model.*
import com.selklik.zapatos.ui.*
import com.selklik.zapatos.ui.adapter.FeaturedProductAdapter
import com.selklik.zapatos.utils.*
import com.selklik.zapatos.utils.volley.ApiHelper
import kotlinx.android.synthetic.main.fragment_home_main.view.*
import java.util.HashMap


class HomeMainFragment : Fragment() {
    private val TAG = this@HomeMainFragment.javaClass.simpleName
    private var toolbar: Toolbar? = null
    private var ussyAboutData: String? = null
    private var textAboutTitle: TextView? = null
    private var textAboutShort: ExpandableTextView? = null
    private var gson: Gson = Gson()
    private var nestedScrollView: NestedScrollView? = null
    private var layoutSocialMediaOne = null
    private var imageLoader = AppController().getImageLoader()
    private var userToken: String? = null
    private var recyclerview: RecyclerView? = null
    val buyProductRequestCode = 888


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        userToken = SharedPrefHelper().getSharedPreferenceString(context!!, Config.PREF_ACCESS_TOKEN, "")

        ussyAboutData = SharedPrefHelper().getSharedPreferenceString(context!!, PREF_ABOUT_ARTIST, null)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater!!.inflate(R.layout.fragment_home_main, container, false)
        toolbar = view.findViewById(R.id.toolbar)
        (activity as AppCompatActivity).setSupportActionBar(toolbar)
        (activity as AppCompatActivity).supportActionBar!!.setDisplayShowTitleEnabled(false)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        try {


            val bundle = this.arguments
            val myValue = bundle!!.getBoolean("previousLogged")

            if (myValue)
            {
                view.siti_info_btn.visibility = View.VISIBLE
                view.parent_moreInfo.visibility = View.GONE
                view.siti_info_btn.setBackgroundResource(R.drawable.down)

            }



        }catch (e : kotlin.KotlinNullPointerException)
                {e.printStackTrace()

                    view.siti_info_btn.setBackgroundResource(R.drawable.up)

                }



        view.siti_info_btn.setOnClickListener(View.OnClickListener { btn_View ->

            if (view.parent_moreInfo.visibility == View.VISIBLE)
            {
                view.siti_info_btn.setBackgroundResource(R.drawable.down)
                view.parent_moreInfo.visibility = View.GONE
            }

            else
            {
                view.siti_info_btn.setBackgroundResource(R.drawable.up)
                view.parent_moreInfo.visibility = View.VISIBLE
            }


        })


        swipeRefreshLayout.isEnabled = false
        nestedScrollView = view.findViewById(R.id.nested_scroll_view)
        nestedScrollView!!.setOnScrollChangeListener(NestedScrollView.OnScrollChangeListener { v, scrollX, scrollY, oldScrollX, oldScrollY ->
            var color = Color.parseColor("#FFFFFFFF") // ideally a global variable
            if (scrollY == 0) {

                parent_infobtn.setBackgroundColor(Color.TRANSPARENT)
                siti_info_btn.background.setColorFilter(ContextCompat.getColor(context!!, R.color.white), android.graphics.PorterDuff.Mode.MULTIPLY);


                        Log.i(TAG, "TOP SCROLL")

                swipeRefreshLayout.isEnabled = true

            } else {

                parent_infobtn.setBackgroundColor(Color.WHITE)

                siti_info_btn.background.setColorFilter(ContextCompat.getColor(context!!, R.color.colorCustomDialogBackground),
                 android.graphics.PorterDuff.Mode.MULTIPLY);

                swipeRefreshLayout.isEnabled = false

            }
//            toolbar!!.setBackgroundColor(color)
        })

        swipeRefreshLayout.setOnRefreshListener {

            AppController().getRequestQueue()!!.getCache().clear();
            fetchUssyFeed(view)

        }

        fetchUssyFeed(view)
        if (ussyAboutData != null) {
            val about: About = gson.fromJson(ussyAboutData, About::class.java)
            setAboutView(view, about)
        }
        text_featured_product_title.typeface = TypefaceHelper().setTypeface("regular")
        text_latest_posts_title.typeface = TypefaceHelper().setTypeface("regular")
        text_featured_product_view_more.typeface = TypefaceHelper().setTypeface("light")
        text_featured_product_view_more.setOnClickListener {
            startActivity(Intent(context, ProductActivity::class.java))
        }

        text_siti_app.typeface = TypefaceHelper().setTypeface("light")
        text_powered_by.typeface = TypefaceHelper().setTypeface("regular")
        btn_similar_apps.typeface = TypefaceHelper().setTypeface("regular")
        btn_similar_apps.setOnClickListener {
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/developer?id=Selklik+Inc.")))
//            val launchIntent = context!!.packageManager.getLaunchIntentForPackage("com.selklik")
//            launchIntent?.let {
//                startActivity(launchIntent)
//            }
        }

        img_simply_siti.setOnClickListener {
            startActivity(Intent(context, BrowserActivity::class.java).putExtra("url",
                    "https://selklik.xyz/simplysiti").putExtra("from", "Simply Siti"))
        }
    }

    private fun setAboutView(view: View, about: About) {
        textAboutTitle = view.findViewById(R.id.text_about_title)
        textAboutTitle!!.text = "About " + resources.getString(R.string.artist_full_name)
        textAboutTitle!!.typeface = TypefaceHelper().setTypeface("bold")

        textAboutShort = view.findViewById(R.id.text_about_short)
        textAboutShort!!.trimLength = 200
        textAboutShort!!.text = getString(R.string.about_artists)
        textAboutShort!!.typeface = TypefaceHelper().setTypeface("regular")

        var textWelcomeSelklik = view.findViewById<TextView>(R.id.text_welcome_selklik)
        textWelcomeSelklik!!.text = about.welcome_message
        textWelcomeSelklik.typeface = TypefaceHelper().setTypeface("regular")
        var imgThumbnailVideoWelcome = view.findViewById<NetworkImageView>(R.id.img_thumbnail_video_welcome)
        //imgThumbnailVideoWelcome.setDefaultImageResId(R.drawable.img_default_thumbnail)
        imgThumbnailVideoWelcome.setImageUrl(about.welcome_video_photos.get("full_resolution").asString, imageLoader)
        imgThumbnailVideoWelcome.setOnClickListener {
            startActivity(Intent(context, VideoPlayerActivity::class.java).putExtra("video_url",
                    about.welcome_video_url).putExtra("premium", true))}

        var work: Works = gson.fromJson(about.works, Works::class.java)

        if (work.filmography != null) {
            layout_about_filmography.visibility = View.VISIBLE

            var textFilmographyTitle = view.findViewById<TextView>(R.id.text_filmography_title)
            textFilmographyTitle.typeface = TypefaceHelper().setTypeface("bold")

            var filmJsonArray = work.filmography
            var filmList: List<Filmography> = gson.fromJson(filmJsonArray, object : TypeToken<List<Filmography>>() {}.type)

            var imgFilmographyOne = view.findViewById<NetworkImageView>(R.id.img_filmography_one)
            imgFilmographyOne.setImageUrl(filmList[0].photos.full_resolution, imageLoader)
            var textFilmographyContentOne = view.findViewById<TextView>(R.id.text_filmography_content_one)
            textFilmographyContentOne.typeface = TypefaceHelper().setTypeface("semi-bold")
            textFilmographyContentOne.text = filmList[0].title + "\n(" + filmList[0].year + ")"

            var imgFilmographyTwo = view.findViewById<NetworkImageView>(R.id.img_filmography_two)
            imgFilmographyTwo.setImageUrl(filmList[1].photos.full_resolution, imageLoader)
            var textFilmographyContentTwo = view.findViewById<TextView>(R.id.text_filmography_content_two)
            textFilmographyContentTwo.typeface = TypefaceHelper().setTypeface("semi-bold")
            textFilmographyContentTwo.text = filmList[1].title + "\n(" + filmList[1].year + ")"

            var imgFilmographyThree = view.findViewById<NetworkImageView>(R.id.img_filmography_three)
            imgFilmographyThree.setImageUrl(filmList[2].photos.full_resolution, imageLoader)
            var textFilmographyContentThree = view.findViewById<TextView>(R.id.text_filmography_content_three)
            textFilmographyContentThree.typeface = TypefaceHelper().setTypeface("semi-bold")
            textFilmographyContentThree.text = filmList[2].title + "\n(" + filmList[2].year + ")"
        } else {
            layout_about_filmography.visibility = View.GONE
        }



        if (work.discography != null) {
            var textDiscographyTitle = view.findViewById<TextView>(R.id.text_discography_title)
            textDiscographyTitle.typeface = TypefaceHelper().setTypeface("bold")
            textDiscographyTitle.text = "Siti's Discography"


            var discoJsonArray = work.discography
            var discoList: List<Discography> = gson.fromJson(discoJsonArray, object : TypeToken<List<Discography>>() {}.type)

            var imgDiscographyOne = view.findViewById<NetworkImageView>(R.id.img_discography_one)
            imgDiscographyOne.setImageUrl(discoList[0].photos.full_resolution, imageLoader)
            img_discography_one.setOnClickListener {
                startActivity(Intent(context, BrowserActivity::class.java).putExtra("url", discoList[0].url).putExtra("from", discoList[0].title))
            }
            var textDiscographyContentOne = view.findViewById<TextView>(R.id.text_discography_content_one)
            textDiscographyContentOne.typeface = TypefaceHelper().setTypeface("semi-bold")
            textDiscographyContentOne.text = discoList[0].title + "\n(" + discoList[0].year + ")"


            var imgDiscographyTwo = view.findViewById<NetworkImageView>(R.id.img_discography_two)
            imgDiscographyTwo.setImageUrl(discoList[1].photos.full_resolution, imageLoader)
            img_discography_two.setOnClickListener {
                startActivity(Intent(context, BrowserActivity::class.java).putExtra("url", discoList[1].url).putExtra("from", discoList[1].title))
            }
            var textDiscographyContentTwo = view.findViewById<TextView>(R.id.text_discography_content_two)
            textDiscographyContentTwo.typeface = TypefaceHelper().setTypeface("semi-bold")
            textDiscographyContentTwo.text = discoList[1].title + "\n(" + discoList[1].year + ")"

            var imgDiscographyThree = view.findViewById<NetworkImageView>(R.id.img_discography_three)
            imgDiscographyThree.setImageUrl(discoList[2].photos.full_resolution, imageLoader)
            img_discography_three.setOnClickListener {
                startActivity(Intent(context, BrowserActivity::class.java).putExtra("url", discoList[2].url).putExtra("from", discoList[2].title))
            }
            var textDiscographyContentThree = view.findViewById<TextView>(R.id.text_discography_content_three)
            textDiscographyContentThree.typeface = TypefaceHelper().setTypeface("semi-bold")
            textDiscographyContentThree.text = discoList[2].title + "\n(" + discoList[2].year + ")"
        } else {
            layout_about_discography.visibility = View.GONE
        }

        var textSocialMediaTitle = view.findViewById<TextView>(R.id.text_social_media_title)
        textSocialMediaTitle.typeface = TypefaceHelper().setTypeface("bold")


    }

    private fun fetchUssyFeed(view: View) {

        if (!ConnectivityReceiver.isConnected()) {
            Log.d(TAG, "No internet connection")
        } else {
            val jsonParams = HashMap<String, String>()
            jsonParams.put("access_token", userToken!!)
            jsonParams.put("type", "instagram")

            ApiHelper(context!!).makeRequest(
                    Config.API_ARTIST + "/" + Config.ARTIST_ID + "/posts",
                    Request.Method.GET,
                    jsonParams,
                    true,
                    false,
                    onSuccess = { responseString: String, gson: Gson ->
                        Log.d(TAG, "response string : " + responseString)

                        var feedArrayList: ArrayList<Feed> = gson.fromJson(responseString, genericType<ArrayList<Feed>>())

                        Log.d(TAG, "Feed array size : " + feedArrayList.size)
                        var artistElement: Artist = feedArrayList[0].artist

                        var artist: Artist = artistElement
                        Log.d(TAG, "artist name : " + artist.name)

                        var layoutSocialMediaOne = view.findViewById<RelativeLayout>(R.id.layout_social_media_one)
                        var imgSocialMediaOne = view.findViewById<ImageView>(R.id.img_social_media_one)
                        var layoutSocialMediaTwo = view.findViewById<RelativeLayout>(R.id.layout_social_media_two)
                        var imgSocialMediaTwo = view.findViewById<ImageView>(R.id.img_social_media_two)
                        var layoutSocialMediaThree = view.findViewById<RelativeLayout>(R.id.layout_social_media_three)
                        var imgSocialMediaThree = view.findViewById<ImageView>(R.id.img_social_media_three)

                        var socialMediaObject: JsonObject = artist.social_network_accounts
                        if (!socialMediaObject.get("instagram").isJsonNull) {
                            layoutSocialMediaOne.visibility = View.VISIBLE
                            imgSocialMediaOne.setImageDrawable(ContextCompat.getDrawable(context!!, R.drawable.ic_insta_about))

                            layout_social_media_one.setOnClickListener {
                                startActivity(Intent(context, BrowserActivity::class.java).putExtra("url", artist.social_network_accounts.get("instagram").asString).putExtra("from", "instagram"))
                            }
                            text_social_media_content_one.typeface = TypefaceHelper().setTypeface("regular")
                            var sapareted = artist.social_network_accounts.get("instagram").asString.split("/")
                            Log.d(TAG, "last : ${sapareted[sapareted.size - 2]}")
                            text_social_media_content_one.text = "@${sapareted[sapareted.size - 2]}"
                        } else {
                            layoutSocialMediaOne.visibility = View.GONE
                        }

                        if (!socialMediaObject.get("facebook").isJsonNull) {
                            layoutSocialMediaTwo.visibility = View.VISIBLE
                            imgSocialMediaTwo.setImageDrawable(ContextCompat.getDrawable(context!!, R.drawable.ic_fb_about))
                            layout_social_media_two.setOnClickListener {
                                startActivity(Intent(context, BrowserActivity::class.java).putExtra("url", artist.social_network_accounts.get("facebook").asString).putExtra("from", "Facebook"))
                            }
                            text_social_media_two.typeface = TypefaceHelper().setTypeface("regular")
                            text_social_media_two.text = artist.name
                        } else {
                            layoutSocialMediaTwo.visibility = View.GONE
                        }

                        if (!socialMediaObject.get("twitter").isJsonNull) {
                            layoutSocialMediaThree.visibility = View.VISIBLE
                            imgSocialMediaThree.setImageDrawable(ContextCompat.getDrawable(context!!, R.drawable.ic_twitter_about))
                            layout_social_media_three.setOnClickListener {
                                startActivity(Intent(context, BrowserActivity::class.java).putExtra("url", artist.social_network_accounts.get("twitter").asString).putExtra("from", "twitter"))
                            }
                            text_social_media_content_three.typeface = TypefaceHelper().setTypeface("regular")
                            var sapareted = artist.social_network_accounts.get("twitter").asString.split("/")
                            Log.d(TAG, "last : ${sapareted[sapareted.size - 1]}")
                            var twitterLastSaparated = sapareted[sapareted.size - 1]
                            if (twitterLastSaparated.contains("?")){
                                var lastSaparate = twitterLastSaparated.split("?")
                                Log.d(TAG, "lastSaparate ${lastSaparate[0]}, ${lastSaparate[1]}")
                                text_social_media_content_three.text = lastSaparate[0]
                            } else {
                                text_social_media_content_three.text = "@${sapareted[sapareted.size - 1]}"
                            }
                        } else {
                            layoutSocialMediaThree.visibility = View.GONE
                        }

                        setLatestPostView(view, feedArrayList)
                        fetchFeaturedProduct()
                    },
                    onError = {

                        try{

                            swipeRefreshLayout?.let {
                                if (it.isRefreshing) {
                                    it.isRefreshing = false
                                }
                            }


                        }catch (e : java.lang.IllegalStateException)
                        {

                        }
                    })
        }
    }

    inline fun <reified T> genericType() = object : TypeToken<T>() {}.type!!

    private fun setLatestPostView(view: View, arrayArtistFeed: ArrayList<Feed>) {
        var photoFeedArrayList: ArrayList<Feed> = ArrayList()

        for (feed: Feed in arrayArtistFeed) {
            photoFeedArrayList.add(feed)
        }

        Log.d(TAG, "photo size : " + photoFeedArrayList.size)
        img_thumb_video.setImageUrl(photoFeedArrayList[0].photos!!.asJsonArray[0].asJsonObject.get("versions").asJsonObject.get("thumbnail").asString, imageLoader)
        img_thumb_video.setOnClickListener {
            startActivity(Intent(context, ViewPostActivity::class.java).putExtra("postData", gson.toJson(arrayArtistFeed[0])))
        }
        img_thumb_one.setImageUrl(photoFeedArrayList[1].photos!!.asJsonArray[0].asJsonObject.get("versions").asJsonObject.get("thumbnail").asString, imageLoader)
        img_thumb_one.setOnClickListener {
            startActivity(Intent(context, ViewPostActivity::class.java).putExtra("postData", gson.toJson(arrayArtistFeed[1])))
        }
        img_thumb_two.setImageUrl(photoFeedArrayList[2].photos!!.asJsonArray[0].asJsonObject.get("versions").asJsonObject.get("thumbnail").asString, imageLoader)
        img_thumb_two.setOnClickListener {
            startActivity(Intent(context, ViewPostActivity::class.java).putExtra("postData", gson.toJson(arrayArtistFeed[2])))
        }
        img_thumb_three.setImageUrl(photoFeedArrayList[3].photos!!.asJsonArray[0].asJsonObject.get("versions").asJsonObject.get("thumbnail").asString, imageLoader)
        img_thumb_three.setOnClickListener {
            startActivity(Intent(context, ViewPostActivity::class.java).putExtra("postData", gson.toJson(arrayArtistFeed[3])))
        }
        img_thumb_four.setImageUrl(photoFeedArrayList[4].photos!!.asJsonArray[0].asJsonObject.get("versions").asJsonObject.get("thumbnail").asString, imageLoader)
        img_thumb_four.setOnClickListener {
            startActivity(Intent(context, ViewPostActivity::class.java).putExtra("postData", gson.toJson(arrayArtistFeed[4])))
        }
        img_thumb_five.setImageUrl(photoFeedArrayList[5].photos!!.asJsonArray[0].asJsonObject.get("versions").asJsonObject.get("thumbnail").asString, imageLoader)
        img_thumb_five.setOnClickListener {
            startActivity(Intent(context, ViewPostActivity::class.java).putExtra("postData", gson.toJson(arrayArtistFeed[5])))
        }

//        for (feed: Feed in arrayArtistFeed) {
//            when (feed.attachment_type) {
//                "video" -> img_thumb_video.setImageUrl(feed.photos!!.asJsonArray.get(0).asJsonObject.get("versions").asJsonObject.get("thumbnail").asString, imageLoader)
//            }
//        }
    }

    private fun fetchFeaturedProduct() {
        if (!ConnectivityReceiver.isConnected()) {
            Log.d(TAG, "No internet connection")
        } else {
            val jsonParams = HashMap<String, String>()
            jsonParams.put("access_token", userToken!!)

            ApiHelper(context!!).makeRequest(
                    Config.API_ARTIST + "/" + Config.ARTIST_ID + "/ProductsShoes",
                    Request.Method.GET,
                    jsonParams,
                    true,
                    false,
                    onSuccess = { responseString: String, gson: Gson ->

                        swipeRefreshLayout?.let {
                            if (it.isRefreshing)
                                it.isRefreshing = false

                        }

                        Log.d(TAG, "featured product : " + responseString)
                        var featuredProductArrayList: ArrayList<Products> = gson.fromJson(responseString, genericType<ArrayList<Products>>())
                        var newFeaturedProductArraylist: ArrayList<Products> = ArrayList()
                        if (featuredProductArrayList.size >= 5) {
                            (0 until 5).mapTo(newFeaturedProductArraylist) { featuredProductArrayList[it] }
                        } else {
                            newFeaturedProductArraylist = featuredProductArrayList
                        }



                        Log.d(TAG, "featured product : " + newFeaturedProductArraylist.size)
                        var mLayoutManager = LinearLayoutManager(context)
                        home_recycler_view.addItemDecoration(DividerItemDecoration(ContextCompat.getDrawable(context!!, R.drawable.item_decorator)!!))
                        home_recycler_view.layoutManager = mLayoutManager
                        home_recycler_view.adapter = FeaturedProductAdapter(context!!,
                                newFeaturedProductArraylist!!,
                                onItemViewClick = { product: Products ->
                                    startActivityForResult(Intent(context, ProductDetailsActivity::class.java)
                                            .putExtra("productData", Gson().toJson(product)),
                                            buyProductRequestCode)
                                })
                        home_recycler_view.isNestedScrollingEnabled = false

                        product_progress_bar.visibility = View.GONE

                    },
                    onError = {
                        if(swipeRefreshLayout.isRefreshing){
                            swipeRefreshLayout.isRefreshing = false
                        }
                    })
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            buyProductRequestCode -> if (resultCode == Activity.RESULT_OK) {
                Log.d("HomeFragment", "Result ok, id : ${data!!.getStringExtra("orderId")}")
                fetchFeaturedProduct()
                data!!.getStringExtra("orderId")?.let {
                    var url = "https://selklik.xyz/receipts/${data!!.getStringExtra("orderId")}?access_token=${AppController.instance.getUserAccessToken()}"
                    startActivity(Intent(context, BrowserActivity::class.java).putExtra("url", url).putExtra("from", "Receipt"))
                }
            } else {
                Log.d("HomeFragment", "Result cancel")
            }
        }
    }


}
