package com.selklik.zapatos.ui

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.support.design.widget.BottomSheetDialog
import android.support.design.widget.Snackbar
import android.support.v4.content.FileProvider
import android.support.v7.app.AlertDialog
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.VolleyError
import com.google.gson.Gson
import com.google.gson.JsonParser
import com.selklik.zapatos.BuildConfig
import com.selklik.zapatos.MainActivity
import com.selklik.zapatos.R
import com.selklik.zapatos.model.User
import com.selklik.zapatos.utils.*
import com.selklik.zapatos.utils.volley.ApiHelper
import kotlinx.android.synthetic.main.activity_sign_up_next.*
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

class SignUpNextActivity : AppCompatActivity() {
    private val RESULT_FROM_CAMERA = 1001
    private val RESULT_LOAD_IMG = 1002

    private val JPEG_FILE_PREFIX = "IMG_"
    private val JPEG_FILE_SUFFIX = ".jpg"

    private val PERMISSIONS_REQUEST_CODE = 1

    private var mFlag: String? = null
    private var bitmap: Bitmap? = null

    private var mPermissions: AndroidPermissions? = null
    private var dialog: AlertDialog? = null

    private var mCurrentPhotoPath: String? = null
    private var byteArray: ByteArray? = null
    private var TAG = SignUpNextActivity::class.java.simpleName
    private var email: String? = null
    private var password: String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up_next)
        var intent = intent
        email = intent.getStringExtra("email")
        password = intent.getStringExtra("password")

        mPermissions = AndroidPermissions(this,
                Manifest.permission.CAMERA,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)

        Log.d(TAG, "email : $email, password : $password")
        ic_close_login.setOnClickListener {
            onBackPressed()
        }
        text_signup_next.typeface = TypefaceHelper().setTypeface("light")
        text_btn_sign_up_next.typeface = TypefaceHelper().setTypeface("bold")
        edittext_username.typeface = TypefaceHelper().setTypeface("regular")
        text_tap_upload.typeface = TypefaceHelper().setTypeface("light")
        text_selklik_signup_title.typeface = TypefaceHelper().setTypeface("light")

        btn_sign_up_email.setOnClickListener {
            if (loginValidation(edittext_username.text.toString())) {
                Log.d(TAG, "bitmap isnull : ${bitmap == null} ")
                postSignUp(email!!, password!!, edittext_username.text.toString())
            }
        }

        layout_profile_picture.setOnClickListener {
            onCameraIconClick()
        }

    }

    private fun postSignUp(email: String, password: String, username: String?) {

        showProgressDialog()
        if (!ConnectivityReceiver.isConnected()) {
            if (dialog!!.isShowing) {
                dialog!!.dismiss()
            }
            Snackbar.make(this.window.decorView.rootView, "No internet connection", Snackbar.LENGTH_SHORT).show()
        } else {
            val jsonParams = HashMap<String, String>()
            jsonParams.put("email", email!!)
            jsonParams.put("password", password!!)
            jsonParams.put("name", username!!)

            var apiHelper = ApiHelper(this)
            apiHelper.makeRequest(
                    Config.API_SIGNUP,
                    Request.Method.POST,
                    jsonParams,
                    false,
                    false,
                    onSuccess = { responseString: String, gson: Gson ->
                        Log.d("Login Activity", "response string : " + responseString)

                        var user = gson.fromJson(responseString, User::class.java)
                        SharedPrefHelper().setSharedPreferenceString(this, Config.PREF_USER_DATA, responseString)
                        SharedPrefHelper().setSharedPreferenceString(this, Config.PREF_ACCESS_TOKEN, user!!.access_token)
                        SharedPrefHelper().setSharedPreferenceBoolean(Config.PREF_KEEP_LOGIN, true)

                        Log.d(TAG, "access token : " + SharedPrefHelper().getSharedPreferenceString(this, Config.PREF_ACCESS_TOKEN, ""))
                        if (bitmap != null) {
                            Log.d(TAG, "bitmap not null")
                            postProfilePicture(user)
                        } else {

                            Log.d(TAG, "bitmap null")
                            fetchAboutUssy(user)
                        }
//
                    },
                    onError = { error: VolleyError ->
                        apiHelper.errorHandler(error)
                        if (dialog!!.isShowing) {
                            dialog!!.dismiss()
                        }
                    })

        }

    }

    private fun postProfilePicture(user: User) {
        if (!ConnectivityReceiver.isConnected()) {
            Log.d(TAG, "No internet connection")
        } else {
            Log.d(TAG, "post image access token : ${user.access_token}")
            val jsonParams = HashMap<String, String>()
            jsonParams.put("access_token", user.access_token)

            ApiHelper(this).makeMultipartRequest(
                    module = Config.API_PROFILE_PIC,
                    method = Request.Method.POST,
                    params = jsonParams,
                    byteArrays = byteArray!!,
                    type = "file",
                    onSuccess = { responseString: String, gson: Gson ->
                        Log.d("Splash Screen", "response string : " + responseString)
                        var user = gson.fromJson(AppController.instance.getUserData(), User::class.java)
                        var photoObj = JsonParser().parse(responseString).asJsonObject
                        user.profile_photos.full_resolution = photoObj.get("urls").asJsonObject.get("full_resolution").asString
                        user.profile_photos.thumbnail = photoObj.get("urls").asJsonObject.get("thumbnail").asString
                        user.profile_photos.square = photoObj.get("urls").asJsonObject.get("square").asString

                        SharedPrefHelper().setSharedPreferenceString(this, Config.PREF_USER_DATA, gson.toJson(user))

                        AppController.instance.setUserIdnotification(this)
                        fetchAboutUssy(user)
                    },
                    onError = { error: VolleyError ->
                        ApiHelper(this).errorHandler(error)

                    })
        }
    }

    private fun fetchAboutUssy(user: User) {
        if (!ConnectivityReceiver.isConnected()) {
            Log.d(TAG, "No internet connection")
        } else {
            println("fetch about access token ${user.access_token}")
            val jsonParams = HashMap<String, String>()
            jsonParams.put("access_token", user!!.access_token)

            ApiHelper(this).makeRequest(
                    Config.API_ABOUT_ARTIST,
                    Request.Method.GET,
                    jsonParams,
                    false,
                    false,
                    onSuccess = { responseString: String, gson: Gson ->
                        Log.d("Splash Screen", "response string : " + responseString)
                        if (dialog!!.isShowing) {
                            dialog!!.dismiss()
                        }
                        SharedPrefHelper().setSharedPreferenceString(this, Config.PREF_ABOUT_ARTIST, responseString)
                        startMainActivity()
                    },
                    onError = { error: VolleyError ->
                        ApiHelper(this).errorHandler(error)
                        if (dialog!!.isShowing) {
                            dialog!!.dismiss()
                        }
                        startMainActivity()
                    })
        }
    }

    private fun startMainActivity() {
        var intent = Intent(this, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
        overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out)
        finish()
    }

    private fun loginValidation(username: String?): Boolean {
        var valid = true

        if (username!!.isEmpty() || username == null) {
            edittext_username.error = "empty name"
            valid = false
        } else {
            valid = true
        }

        return valid
    }

    private fun isIntentAvailable(context: Context, action: String): Boolean {
        val packageManager = context.packageManager
        val intent = Intent(action)
        val list = packageManager.queryIntentActivities(intent,
                PackageManager.MATCH_DEFAULT_ONLY)
        return list.size > 0
    }

    private fun checkPermissions(flag: String) {
        mFlag = flag
        if (mPermissions!!.checkPermissions()) {
            //            myImplementation();
            Log.d(TAG, "Permission granted")
            //            bottomSheetDialog.show();
            //            openCamera();
            when (flag) {
                "camera" -> openCamera()
                "gallery" -> showFileChooser()
            }
        } else {
            Log.i(TAG, "Some needed permissions are missing. Requesting them.")
            mPermissions!!.requestPermissions(PERMISSIONS_REQUEST_CODE)
        }
    }

    private fun openCamera() {

        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        var f: File? = null

        try {
            f = setUpPhotoFile()
            mCurrentPhotoPath = f!!.absolutePath
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, FileProvider.getUriForFile(this,
                    BuildConfig.APPLICATION_ID + ".provider", f))

        } catch (e: IOException) {
            e.message
        }

        startActivityForResult(takePictureIntent, RESULT_FROM_CAMERA)
    }

    @Throws(IOException::class)
    private fun setUpPhotoFile(): File {

        val f = createImageFile()
        mCurrentPhotoPath = f.absolutePath

        return f
    }

    @Throws(IOException::class)
    private fun createImageFile(): File {
        // Create an image file name
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val imageFileName = JPEG_FILE_PREFIX + timeStamp + "_"
        val albumF = getAlbumDir()
        return File.createTempFile(imageFileName, JPEG_FILE_SUFFIX, albumF)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        try {
            // When an Image is picked
            if (requestCode == RESULT_LOAD_IMG && resultCode == Activity.RESULT_OK
                    && null != data) {
                val selectedImage = data.data
                val filePathColumn = arrayOf(MediaStore.Images.Media.DATA)

                val cursor = contentResolver.query(selectedImage!!,
                        filePathColumn, null, null, null)
                cursor!!.moveToFirst()

                val columnIndex = cursor.getColumnIndex(filePathColumn[0])
                val picturePath = cursor.getString(columnIndex)
                cursor.close()

                bitmap = BitmapFactory.decodeFile(picturePath)
                byteArray = getByteArray(bitmap!!)

                Log.d("HelloMoto", byteArray.toString());

                img_profile_picture.setImageBitmap(bitmap)

            } else if (requestCode == RESULT_FROM_CAMERA && resultCode == Activity.RESULT_OK) {

                bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath)
                byteArray = getByteArray(bitmap!!)
                Log.d(TAG, "bitmap : " + bitmap)
                setPic()

            } else {
                Toast.makeText(this, "Anda harus memilih gambar untuk dimuatnaik",
                        Toast.LENGTH_LONG).show()
            }
        } catch (e: Exception) {
            e.printStackTrace()
            Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG)
                    .show()
        }

    }

    private fun setPic() {

        /* There isn't enough memory to open up more than a couple camera photos */
        /* So pre-scale the target bitmap into which the file is decoded */

        /* Get the size of the ImageView */
        val targetW = img_profile_picture.getWidth()
        val targetH = img_profile_picture.getHeight()

        /* Get the size of the image */
        val bmOptions = BitmapFactory.Options()
        bmOptions.inJustDecodeBounds = true
        BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions)
        val photoW = bmOptions.outWidth
        val photoH = bmOptions.outHeight

        /* Figure out which way needs to be reduced less */
        var scaleFactor = 1
        if (targetW > 0 || targetH > 0) {
            scaleFactor = Math.min(photoW / targetW, photoH / targetH)
        }

        /* Set bitmap options to scale the image decode target */
        bmOptions.inJustDecodeBounds = false
        bmOptions.inSampleSize = scaleFactor
        bmOptions.inPurgeable = true

        /* Decode the JPEG file into a Bitmap */
        val bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions)

        /* Associate the Bitmap to the ImageView */
        img_profile_picture.setImageBitmap(bitmap)
        img_profile_picture.visibility = View.VISIBLE
        //        mVideoView.setVisibility(View.INVISIBLE);
    }

    private fun getByteArray(bitmap: Bitmap): ByteArray {
        val byteArrayOutputStream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 60, byteArrayOutputStream)
        return byteArrayOutputStream.toByteArray()
    }

    private fun getAlbumDir(): File? {
        var storageDir: File? = null

        if (Environment.MEDIA_MOUNTED == Environment.getExternalStorageState()) {

            //            storageDir = mAlbumStorageDirFactory.getAlbumStorageDir("NavAms");
            storageDir = File(Environment.getExternalStorageDirectory().toString() + "/dcim/" + "NavAms")

            if (storageDir != null) {
                if (!storageDir.mkdirs()) {
                    if (!storageDir.exists()) {
                        Log.d("CameraSample", "failed to create directory")
                        return null
                    }
                }
            }
        } else {
            Log.v(getString(R.string.app_name), "External storage is not mounted READ/WRITE.")
        }

        return storageDir
    }

    private fun showFileChooser() {
        val galleryIntent = Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        startActivityForResult(galleryIntent, RESULT_LOAD_IMG)
    }

    fun onCameraIconClick() {
        val bottomSheetDialog = BottomSheetDialog(this)
        val view1 = LayoutInflater.from(this).inflate(R.layout.layout_bottom_sheet_picture, null)

        bottomSheetDialog.setContentView(view1)
        val layoutPhoto: LinearLayout
        val layoutGallery: LinearLayout
        val layoutCancel: LinearLayout

        layoutPhoto = view1.findViewById(R.id.add_complaint_bottom_sheet_camera)
        layoutPhoto.setOnClickListener {
            if (!isIntentAvailable(this@SignUpNextActivity, MediaStore.ACTION_IMAGE_CAPTURE)) {
                Toast.makeText(this@SignUpNextActivity, "failed open camera", Toast.LENGTH_SHORT).show()
            } else {
                checkPermissions("camera")
            }
            bottomSheetDialog.dismiss()
        }
        layoutGallery = view1.findViewById(R.id.add_complaint_bottom_sheet_gallery)
        layoutGallery.setOnClickListener {
            checkPermissions("gallery")
            bottomSheetDialog.dismiss()
        }

        layoutCancel = view1.findViewById(R.id.add_complaint_bottom_sheet_cancel)
        layoutCancel.setOnClickListener {
            checkPermissions("gallery")
            bottomSheetDialog.dismiss()
        }

        bottomSheetDialog.show()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (mPermissions!!.areAllRequiredPermissionsGranted(permissions, grantResults)) {
            //            myImplementation();
            //            openCamera();
            when (mFlag) {
                "camera" -> openCamera()
                "gallery" -> showFileChooser()
            }
        } else {
            //            onInsufficientPermissions();
            Snackbar.make(this.findViewById(android.R.id.content),
                    "Please Grant Permissions to use camera",
                    Snackbar.LENGTH_INDEFINITE).setAction("ENABLE"
            ) { mPermissions!!.requestPermissions(PERMISSIONS_REQUEST_CODE) }.show()
        }
    }

    private fun showProgressDialog() {
        val mBuilder = AlertDialog.Builder(this, R.style.CustomDialogTheme)
        val mView = LayoutInflater.from(this).inflate(R.layout.dialog_progress, null)

        mBuilder.setView(mView)
        mBuilder.setCancelable(false)
        dialog = mBuilder.create()
        dialog!!.show()
    }

}
