package com.selklik.zapatos.ui.adapter.ShopAdapter

import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.selklik.zapatos.R
import com.selklik.zapatos.utils.Config
import com.selklik.zapatos.utils.LocalMutators
import kotlinx.android.synthetic.main.raw_cards_list.view.*

class CardListAdapter(context : Context, arrayCards: ArrayList<String>, activity : Activity) : BaseAdapter()
{
    val inflater : LayoutInflater
    var arrayCards : ArrayList<String>

    var context : Context
    var activity : Activity

    init{

        inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

        this.arrayCards = arrayCards
        this.context = context
        this.activity = activity
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {


        var view : View ?= null

        if (position == (count - 1))
        {



        } else {

            view = inflater.inflate(R.layout.raw_cards_list, parent, false)

            var cardString = arrayCards.get(position).split(" DIS ")

            var card = view.card_style
            card.setCardNumber(cardString[0].trim())
            card.setCvcCode(   cardString[1].trim())
            card.setExpiryDate(cardString[3].toInt(), cardString[2].trim().toInt())

            view.card_style.isClickable = false
            view.setOnClickListener(View.OnClickListener {

                LocalMutators.cardNumber = cardString[0].trim()
                LocalMutators.cvcCode = cardString[1].trim()
                LocalMutators.expiryMonth = cardString[2].trim()
                LocalMutators.expiryYear = cardString[3].trim()

                Config.payMethod = "stripe"

                activity.setResult(Activity.RESULT_OK)
                activity.finish()

            })
        }

        return view!!

    }

    override fun getItem(position: Int): Any {

        return position
    }

    override fun getCount(): Int {

        return (arrayCards.size + 1)
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

}