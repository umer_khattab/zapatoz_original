package com.selklik.zapatos.ui.adapter

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.GradientDrawable
import android.support.v4.content.res.ResourcesCompat
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import com.android.volley.toolbox.ImageLoader
import com.google.gson.Gson
import com.selklik.zapatos.R
import com.selklik.zapatos.model.Order
import com.selklik.zapatos.model.purchasedItems.Items
import com.selklik.zapatos.ui.BrowserActivity
import com.selklik.zapatos.ui.BrowserReceiptActivity
import com.selklik.zapatos.ui.TrackingView
import com.selklik.zapatos.utils.AppController
import com.selklik.zapatos.utils.TypefaceHelper
import kotlinx.android.synthetic.main.item_purchased_history.view.*
import kotlinx.android.synthetic.main.item_purchased_history_zapatos.view.*
import org.w3c.dom.Text
import java.lang.NullPointerException

/**
 * Created by faridhadiselklik on 06/12/2017.
 */
class PurchaseHistoryProductAdapter(private var mContext: Context, var purchasedArrayList: ArrayList<Items>) : RecyclerView.Adapter<PurchaseHistoryProductAdapter.ViewHolder>() {


    private var lastOrderItem : String ?= ""
    private var context : Context

    init {

        this.context = mContext

    }

    override fun onBindViewHolder(holder: PurchaseHistoryProductAdapter.ViewHolder, position: Int) {
        holder.bindItems(purchasedArrayList[position])

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) : ViewHolder {
        val itemView = LayoutInflater.from(parent!!.context).inflate(R.layout.item_purchased_history_zapatos, parent, false)

        return ViewHolder(mContext, itemView)

    }

    override fun getItemCount(): Int {
        return purchasedArrayList.size
    }

    inner class ViewHolder(context: Context, itemView: View) : RecyclerView.ViewHolder(itemView) {


        private fun createSize(size_tv_block : LinearLayout, sizeLength : String)
        {

            for (i in 0 until size_tv_block.childCount)
                size_tv_block.getChildAt(i).visibility = View.GONE

            var params = LinearLayout.LayoutParams(60, 60)
            params.gravity = LinearLayout.TEXT_ALIGNMENT_CENTER
            params.setMargins(6, 3, 3, 3)

            var params_txt = RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
            params_txt.addRule(RelativeLayout.CENTER_IN_PARENT)

            var sizeLength = sizeLength.trim().split(", ")

            for (i in 0 until sizeLength.size){

                var relativeLayout = RelativeLayout(context)
                relativeLayout.layoutParams = params

                var textView = TextView(context)
                textView.setText(sizeLength.get(i))
                textView.setTextSize(13f)
                textView.layoutParams = params_txt

                textView.setTextColor( ResourcesCompat.getColor(context.resources, R.color.colorZopatozGold, null))
                textView.setTypeface(null, Typeface.BOLD)

                relativeLayout.addView(textView)
                relativeLayout.id = i

                val shape = GradientDrawable()
                shape.shape = GradientDrawable.OVAL
                shape.setColor(Color.BLACK)
                shape.setStroke(3, ResourcesCompat.getColor(context.resources, R.color.colorZopatoz_showcase, null))

                relativeLayout.setBackgroundDrawable(shape)

                size_tv_block.addView(relativeLayout)

            }

        }

        fun bindItems(items: Items) {

            var imageLoader: ImageLoader = AppController().getImageLoader()

            itemView.item_niv.setImageUrl(items.prod_image, imageLoader)
            itemView.item_niv.animate().alpha(1.0f).setDuration(1000).start()

            itemView.name_tv.setText(items.prod_name)

            createSize(itemView.size_tv_block, items.cus_sizes)

            itemView.quantity_tv.setText("Quantity: "+items.cus_prodquantity)
            itemView.order_id_tv.setText(items.cus_orderid)


            if (!lastOrderItem!!.trim().equals(items.cus_orderid.trim())){

                itemView.parent_orderid.visibility = View.VISIBLE

            } else if (lastOrderItem!!.trim().equals(items.cus_orderid.trim())) {

                itemView.parent_orderid.visibility = View.GONE
            }

            if (!TextUtils.isEmpty(items.trackid))
            {
                itemView.shimmerlayout.startShimmerAnimation()

                itemView.track_id.visibility = View.VISIBLE
                itemView.track_id.setOnClickListener({

                    var intent = Intent(context, TrackingView::class.java)
                    intent.putExtra("trackingId", items.trackid)
                    context.startActivity(intent)

                })
            }


            try {

                 if (items.cus_orderid == purchasedArrayList[position + 1].cus_orderid) {


                        itemView.bottom_date_price_parent.visibility = View.GONE

                    } else {

                        itemView.bottom_date_price_parent.visibility = View.VISIBLE

                        itemView.price_tv.visibility = View.VISIBLE
                        itemView.price_tv.setText(("RM " + items.totalAmount / 100).trim())

                        itemView.date_tv.visibility = View.VISIBLE
                        itemView.date_tv.setText("Order placed on " + items.date.split(" ")[0])
                    }

            } catch (e : java.lang.IndexOutOfBoundsException){e.printStackTrace()

                    itemView.bottom_date_price_parent.visibility = View.VISIBLE

                    itemView.price_tv.visibility = View.VISIBLE
                    itemView.price_tv.setText(("RM " + items.totalAmount / 100).trim())

                    itemView.date_tv.visibility = View.VISIBLE
                    itemView.date_tv.setText("Order placed on "+items.date.split(" ")[0])
            }


            //if (adapterPosition != lastPosition )
                lastOrderItem = items.cus_orderid

            //lastPosition = adapterPosition

        }
    }
}