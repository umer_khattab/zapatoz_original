package com.selklik.zapatos.ui

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.style.RelativeSizeSpan
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.VolleyError
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.selklik.zapatos.R
import com.selklik.zapatos.model.Card
import com.selklik.zapatos.ui.adapter.CardItemAdapter
import com.selklik.zapatos.utils.*
import com.selklik.zapatos.utils.helper.AlertDialogHelper
import com.selklik.zapatos.utils.volley.ApiHelper

import kotlinx.android.synthetic.main.activity_manage_payment_method.*
import kotlinx.android.synthetic.main.content_manage_payment_method.*
import java.util.HashMap

class ManagePaymentMethodActivity : AppCompatActivity() {
    private val TAG = ManagePaymentMethodActivity::class.java.simpleName
    private inline fun <reified T> genericType() = object : TypeToken<T>() {}.type!!
    private var mLayoutManager: RecyclerView.LayoutManager? = null
    private var card: Card? = null
    private var dialog: AlertDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_manage_payment_method)
        setSupportActionBar(toolbar)
        val ab = supportActionBar!!
        ab!!.setHomeButtonEnabled(true)
        ab!!.setDisplayHomeAsUpEnabled(true)
        ab!!.setDisplayShowTitleEnabled(false)

        txt_toolbar_title.typeface = TypefaceHelper().setTypeface("regular")
        mLayoutManager = LinearLayoutManager(this)
       // card_recycler_view.layoutManager = mLayoutManager
       // card_recycler_view.addItemDecoration(DividerItemDecoration(ContextCompat.getDrawable(this!!, R.drawable.item_decorator)!!))

        btn_add_card.typeface = TypefaceHelper().setTypeface("bold")
        btn_add_card.setOnClickListener(onAddCardClick)
        fetchCard()
    }

    private fun fetchCard() {
        if (dialog == null) {
//            if (!dialog!!.isShowing) {
            showProgressDialog()
//            }
        }
        if (!ConnectivityReceiver.isConnected()) {
//            if (dialog!!.isShowing) {
//                dialog!!.dismiss()
//            }
            Log.d("Add Card", "No internet connection")
        } else {
            val jsonParams = HashMap<String, String>()
            jsonParams.put("access_token", AppController.instance.getUserAccessToken())

            ApiHelper(this).makeRequest(
                    Config.API_ME + "/cards",
                    Request.Method.GET,
                    jsonParams,
                    false,
                    false,
                    onSuccess = { responseString: String, gson: Gson ->
                        Log.d("Add Card", "response string : " + responseString)
                        if (dialog!!.isShowing) {
                            dialog!!.dismiss()
                        }

                        if (responseString != "[]") {
                            layout_no_payment_method.visibility = View.GONE
                            var cardArrayList: ArrayList<Card> = Gson().fromJson(responseString, genericType<ArrayList<Card>>())
                            Log.d(TAG, "card size : " + cardArrayList.size)
                           // card_recycler_view.visibility = View.VISIBLE
                            var feedItemAdapter = CardItemAdapter(this!!,
                                    cardArrayList!!,
                                    onItemViewClick = {
                                        card: Card ->
                                        showCardDefaultAlert(this, card)
                                    })
                           // card_recycler_view.adapter = feedItemAdapter
                        } else {
                            layout_no_payment_method.visibility = View.VISIBLE
                          //  card_recycler_view.visibility = View.GONE
                            txt_no_payment_method.typeface = TypefaceHelper().setTypeface("regular")
                        }
                    },
                    onError = { error: VolleyError ->
                        ApiHelper(this).errorHandler(error)
                        if (dialog!!.isShowing) {
                            dialog!!.dismiss()
                        }
                    })
        }

    }

    fun showCardDefaultAlert(context: Context, card: Card) {
        val builder = android.app.AlertDialog.Builder(context)
        builder.setMessage("Set this as default?")
                .setCancelable(false)
        builder.setNegativeButton("OK") {
            dialog, id ->
            setDefaultCard(card)
            dialog.dismiss()
        }
        builder.setPositiveButton("Cancel") { dialog, id -> dialog.dismiss() }

        val ssBuilder = SpannableStringBuilder("Confirmation")

        // Initialize a new relative size span instance
        // Double the text size on this span
        val largeSizeText = RelativeSizeSpan(.8f)

        // Apply the relative size span
        ssBuilder.setSpan(
                largeSizeText, // Span to add
                0, // Start of the span
                "Confirmation".length, // End of the span
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE // Do not extent the span when text add later
        )

        // Set the alert dialog title using spannable string builder
        // It will display double size text on title
        builder.setTitle(ssBuilder)
        val alert = builder.show()

        val messageText = alert.findViewById<View>(android.R.id.message) as TextView
        messageText.gravity = Gravity.CENTER
        messageText.textSize = 12f

    }

    private fun setDefaultCard(card: Card){
            showProgressDialog()
        if (!ConnectivityReceiver.isConnected()) {
//            if (dialog!!.isShowing) {
//                dialog!!.dismiss()
//            }
            Log.d("Add Card", "No internet connection")
        } else {
            val jsonParams = HashMap<String, String>()
            jsonParams.put("access_token", AppController.instance.getUserAccessToken())
            jsonParams.put("is_default", "true")

            ApiHelper(this).makeRequest(
                    Config.API_ME + "/cards/${card.id}",
                    Request.Method.PUT,
                    jsonParams,
                    false,
                    false,
                    onSuccess = { responseString: String, gson: Gson ->
                        Log.d("UpdateCard", "response string : " + responseString)
                        if (dialog!!.isShowing) {
                            dialog!!.dismiss()
                        }
//                        Log.d("Add Card", "stored card info : "
//                                + SharedPrefHelper().getSharedPreferenceString(this, Config.PREF_USER_CARD, ""))
                        fetchCard()
                    },
                    onError = { error: VolleyError ->
                        ApiHelper(this).errorHandler(error)
                        if (dialog!!.isShowing) {
                            dialog!!.dismiss()
                        }
                    })
        }
    }

    private var onAddCardClick: View.OnClickListener = View.OnClickListener { view: View? ->

        AlertDialogHelper().showAddCardDialog(this,
                onSubmitClick = { response: String ->
                    showProgressDialog()
                },
                onCardReady = { cardSourceId: String ->
                    Log.d(TAG, "card Source id : $cardSourceId")
                    AppController.instance.postCard(context = this,
                            sourceId = cardSourceId,
                            onSuccess = { response: String? ->
                                card = Gson().fromJson(response, Card::class.java)
                                fetchCard()

                            },
                            onError = { errorString: String?, error: VolleyError? ->
                                if (dialog!!.isShowing) {
                                    dialog!!.dismiss()
                                }
                                if (errorString != null) {
                                    Toast.makeText(this, "No internet Connection", Toast.LENGTH_SHORT).show()
                                } else {
                                    ApiHelper(this).errorHandler(error!!)
                                }
                            })
//                    if (dialog!!.isShowing) {
//                        dialog!!.dismiss()
//                    }

                })
    }

    private fun showProgressDialog() {
        val mBuilder = AlertDialog.Builder(this, R.style.CustomDialogTheme)
        val mView = LayoutInflater.from(this).inflate(R.layout.dialog_progress, null)

        mBuilder.setView(mView)
        mBuilder.setCancelable(false)
        dialog = mBuilder.create()
        dialog!!.show()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        when (id) {
            android.R.id.home -> onBackPressed()
            else -> return false
        }
        return true
    }

}
