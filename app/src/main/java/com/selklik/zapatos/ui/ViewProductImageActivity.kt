package com.selklik.zapatos.ui

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.view.ViewPager
import android.util.Log
import com.google.gson.Gson
import com.google.gson.JsonParser
import com.selklik.zapatos.R
import com.selklik.zapatos.model.Photo
import com.selklik.zapatos.ui.adapter.ProductImagePagerAdapter
import com.selklik.zapatos.utils.TypefaceHelper
import kotlinx.android.synthetic.main.activity_view_product_image.*

class ViewProductImageActivity : AppCompatActivity() {
    private val TAG = ViewProductImageActivity::class.java.simpleName
    private var photoData: String? = null
    private var gson: Gson = Gson()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_product_image)

        var intent = intent
        photoData = intent.getStringExtra("imageData")
        Log.d(TAG, "image data : " + photoData)
        val productImageList = JsonParser().parse(photoData).asJsonArray.map { gson.fromJson(it, Photo::class.java) }

        txt_close.typeface = TypefaceHelper().setTypeface("semi-bold")
        txt_close.setOnClickListener { onBackPressed() }

        val adapter = ProductImagePagerAdapter(this, productImageList)
        viewpager_img_product.adapter = adapter
        val totalPage = productImageList.size
        txt_page.typeface = TypefaceHelper().setTypeface("regular")
        txt_page.text = " 1 / $totalPage"

        viewpager_img_product.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

            }

            override fun onPageSelected(position: Int) {
                val pageNo = position + 1
                txt_page.text = "$pageNo / $totalPage"
            }

            override fun onPageScrollStateChanged(state: Int) {

            }
        })
    }
}
