package com.selklik.zapatos.ui

import android.content.Context
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.VolleyError
import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.reflect.TypeToken
import com.selklik.zapatos.R
import com.selklik.zapatos.model.Comment
import com.selklik.zapatos.model.CommentDetails
import com.selklik.zapatos.ui.adapter.FeedCommentItemAdapter
import com.selklik.zapatos.utils.*
import com.selklik.zapatos.utils.volley.ApiHelper
import kotlinx.android.synthetic.main.activity_post_comment.*
import java.util.HashMap

/**
 * Created by faridhadiselklik on 18/12/2017.
 */
class CommentActivity : AppCompatActivity() {
    private var TAG = CommentActivity::class.java.simpleName
    private var dialog: AlertDialog? = null
    private var feedCommentItemAdapter: FeedCommentItemAdapter? = null
    inline fun <reified T> genericType() = object : TypeToken<T>() {}.type!!
    private var commentArrayList: ArrayList<CommentDetails>? = null

    private lateinit var mLayoutManager: LinearLayoutManager

    private var commentType: String? = null

    private var commentUrl: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_post_comment)

        var intent = intent
        Log.d(TAG, "post id : " + intent.getStringExtra("post_id"))
        commentType = intent.getStringExtra("commentType")

        setSupportActionBar(toolbar)
        val ab = supportActionBar
        ab!!.setHomeButtonEnabled(true)
        ab.setDisplayHomeAsUpEnabled(true)
        ab.setDisplayShowTitleEnabled(false)

        txt_toolbar_title.typeface = TypefaceHelper().setTypeface("regular")
        edit_comment.typeface = TypefaceHelper().setTypeface("regular")

        mLayoutManager = LinearLayoutManager(this)
        mLayoutManager.setReverseLayout(true);
        mLayoutManager.setStackFromEnd(true);

        comment_recycler_view.layoutManager = mLayoutManager

        when (commentType) {
            "selklik" -> {
                comment_bottom_add.visibility = View.VISIBLE
                commentUrl = Config.API_POST + "/" + intent.getStringExtra("post_id") + "/comments"
            }
            "facebook" -> {
                comment_bottom_add.visibility = View.GONE
                commentUrl = Config.API_FACEBOOK + "/" + intent.getStringExtra("post_id")
            }
            "instagram" -> {
                comment_bottom_add.visibility = View.GONE
                commentUrl = Config.API_INSTAGRAM + "/" + intent.getStringExtra("post_id")
            }
            else -> {
                comment_bottom_add.visibility = View.VISIBLE
                commentUrl = Config.API_POST + "/" + intent.getStringExtra("post_id") + "/comments"
            }
        }

        fetchComment(intent.getStringExtra("post_id"))

        btn_add.setOnClickListener {
            var commentField: String = edit_comment.text.toString()
            if (commentField != "") {
                val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(edit_comment.windowToken, 0)
                edit_comment.text.clear()

                postComment(intent.getStringExtra("post_id"), commentField)

            } else {
                Log.d(TAG, "comment field is empty")
            }
        }
    }


    private fun fetchComment(postId: String) {

        showProgressDialog()
        if (!ConnectivityReceiver.isConnected()) {
            if (dialog!!.isShowing) {
                dialog!!.dismiss()
            }
            Snackbar.make(this.window.decorView.rootView, "No internet connection", Snackbar.LENGTH_SHORT).show()
        } else {
            val jsonParams = HashMap<String, String>()
            jsonParams.put("access_token", AppController.instance.getUserAccessToken()!!)

            var apiHelper = ApiHelper(this)
            apiHelper.makeRequest(
                    commentUrl!!,
                    Request.Method.GET,
                    jsonParams,
                    true,
                    false,
                    onSuccess = { responseString: String, gson: Gson ->
                        Log.d(TAG, "response string : " + responseString)
                        var comment: Comment = gson.fromJson<Comment>(responseString, Comment::class.java)
                        when(commentType){
                            "selklik", null -> {

                                if (comment.count > 0) {
                                    var commentJsonArray: JsonArray = comment.comments
                                    commentArrayList = gson.fromJson(commentJsonArray, genericType<ArrayList<CommentDetails>>())

                                    comment_recycler_view.addItemDecoration(DividerItemDecoration(ContextCompat.getDrawable(this!!, R.drawable.item_decorator)!!))
                                    feedCommentItemAdapter = FeedCommentItemAdapter(this, commentArrayList!!)
                                    comment_recycler_view.adapter = feedCommentItemAdapter
                                    comment_recycler_view.visibility = View.VISIBLE
                                    layout_no_comment.visibility = View.GONE
                                } else {
                                    comment_recycler_view.visibility = View.GONE
                                    layout_no_comment.visibility = View.VISIBLE
                                }
                            }
                            else -> {

                                var commentJsonArray: JsonArray = comment.comments
                                commentArrayList = gson.fromJson(commentJsonArray, genericType<ArrayList<CommentDetails>>())
                                if (commentArrayList!!.size > 0){
                                    comment_recycler_view.addItemDecoration(DividerItemDecoration(ContextCompat.getDrawable(this!!, R.drawable.item_decorator)!!))
                                    feedCommentItemAdapter = FeedCommentItemAdapter(this, commentArrayList!!)
                                    comment_recycler_view.adapter = feedCommentItemAdapter
                                    comment_recycler_view.visibility = View.VISIBLE
                                    layout_no_comment.visibility = View.GONE
                                } else {
                                    comment_recycler_view.visibility = View.GONE
                                    layout_no_comment.visibility = View.VISIBLE
                                }
                            }
                        }


                        if (dialog!!.isShowing) {
                            dialog!!.dismiss()
                        }
                    },
                    onError = { error: VolleyError ->
                        apiHelper.errorHandler(error)
                        if (dialog!!.isShowing) {
                            dialog!!.dismiss()
                        }
                    })

        }

    }

    private fun postComment(postId: String, body: String) {

//        showProgressDialog()
        if (!ConnectivityReceiver.isConnected()) {

            Snackbar.make(this.window.decorView.rootView, "No internet connection", Snackbar.LENGTH_SHORT).show()
        } else {
            val jsonParams = HashMap<String, String>()
            jsonParams.put("access_token", AppController.instance.getUserAccessToken()!!)
            jsonParams.put("body", body)

            var apiHelper = ApiHelper(this)
            apiHelper.makeRequest(
                    Config.API_POST + "/$postId/comments",
                    Request.Method.POST,
                    jsonParams,
                    true,
                    false,
                    onSuccess = { responseString: String, gson: Gson ->
                        Log.d(TAG, "response string : " + responseString)

                        comment_recycler_view.visibility = View.VISIBLE
                        layout_no_comment.visibility = View.GONE
                        if (commentArrayList != null) {
                            val newComment = gson.fromJson(responseString, CommentDetails::class.java)
                            commentArrayList!!.add(newComment)
                            feedCommentItemAdapter!!.notifyDataSetChanged()
                        } else {
                            val newComment = gson.fromJson(responseString, CommentDetails::class.java)
                            Log.d(TAG, "comment count : " + newComment.body)
                            commentArrayList = java.util.ArrayList<CommentDetails>()
                            commentArrayList!!.add(newComment)

                            feedCommentItemAdapter = FeedCommentItemAdapter(this, commentArrayList!!)
                            comment_recycler_view.adapter = feedCommentItemAdapter

                        }
                    },
                    onError = { error: VolleyError ->
                        apiHelper.errorHandler(error)
                        Toast.makeText(this, "failed to post comment", Toast.LENGTH_SHORT).show()
                        edit_comment.setText(body)

                    })

        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        when (id) {
            android.R.id.home -> onBackPressed()
            else -> return false
        }
        return true
    }


    private fun showProgressDialog() {
        val mBuilder = AlertDialog.Builder(this, R.style.CustomDialogTheme)
        val mView = LayoutInflater.from(this).inflate(R.layout.dialog_progress, null)

        mBuilder.setView(mView)
        mBuilder.setCancelable(false)
        dialog = mBuilder.create()
        dialog!!.show()
    }
}