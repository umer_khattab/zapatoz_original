package com.selklik.zapatos.ui

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.BottomSheetDialog
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.VolleyError
import com.google.gson.*
import com.google.gson.reflect.TypeToken
import com.selklik.zapatos.R
import com.selklik.zapatos.model.Messages
import com.selklik.zapatos.model.User
import com.selklik.zapatos.ui.adapter.MessageItemAdapter
import com.selklik.zapatos.utils.*
import com.selklik.zapatos.utils.volley.ApiHelper

import kotlinx.android.synthetic.main.activity_qna.*
import kotlinx.android.synthetic.main.content_qna.*
import kotlinx.android.synthetic.main.new_ques_fragment_bottom_sheet_dialog.view.*
import java.util.HashMap

class QnaActivity : AppCompatActivity() {

    var dialog: AlertDialog? = null
    private inline fun <reified T> genericType() = object : TypeToken<T>() {}.type!!
    private lateinit var messageItemAdapter: MessageItemAdapter
    private var REQUEST_NEW_QUESTION = 1001
    private var quesPrice: String = "5000"

    private var mLayoutManager: RecyclerView.LayoutManager? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_qna)
        setSupportActionBar(toolbar)
        val ab = supportActionBar
        ab!!.setHomeButtonEnabled(true)
        ab.setDisplayHomeAsUpEnabled(true)
        ab.setDisplayShowTitleEnabled(false)
        txt_toolbar_title.typeface = TypefaceHelper().setTypeface("regular")
        text_button_add_question.typeface = TypefaceHelper().setTypeface("regular")

        mLayoutManager = LinearLayoutManager(this)
        msgRecyclerView.layoutManager = mLayoutManager
        fetchMessageThread()

    }

    private fun fetchMessageThread() {
        showProgressDialog()
        if (!ConnectivityReceiver.isConnected()) {
            if (dialog!!.isShowing) {
                dialog!!.dismiss()
            }
            Log.d("Add Card", "No internet connection")
        } else {
            val jsonParams = HashMap<String, String>()
            jsonParams.put("access_token", AppController.instance.getUserAccessToken())

            ApiHelper(this).makeRequest(
                    Config.API_ARTIST +"/"+Config.ARTIST_ID_2+"/messages",
                    Request.Method.GET,
                    jsonParams,
                    false,
                    false,
                    onSuccess = { responseString: String, gson: Gson ->
                        Log.d("QnA Fragment", "response string : " + responseString)
                        if (dialog!!.isShowing) {
                            dialog!!.dismiss()
                        }
                        if(responseString!="[]"){
                            var messageArrayList: ArrayList<Messages> = gson.fromJson(responseString, genericType<ArrayList<Messages>>())
                            Log.d("QnA Fragment", "message size : " + messageArrayList.size)
                            messageItemAdapter = MessageItemAdapter(this!!, messageArrayList!!)
                            msgRecyclerView.adapter = messageItemAdapter
                            msgRecyclerView.visibility = View.VISIBLE

                            if (messageArrayList[messageArrayList.size-1].kind == "answer"){
                                button_ask_ussy.alpha = 1f
                                button_ask_ussy.isClickable = true
                                button_ask_ussy.setOnClickListener{
//                                    startActivityForResult(Intent(this, AddNewQuestionActivity::class.java), REQUEST_NEW_QUESTION)
                                    showBottomSheetDialog()
                                }
                            } else {
                                button_ask_ussy.alpha = .5f
                                button_ask_ussy.isClickable = false
                            }

                        } else {

                        }


                    },
                    onError = { error: VolleyError ->
                        ApiHelper(this).errorHandler(error)
                        if (dialog!!.isShowing) {
                            dialog!!.dismiss()
                        }
                    })
        }
    }

    private fun showBottomSheetDialog() {
        val view = layoutInflater.inflate(R.layout.new_ques_fragment_bottom_sheet_dialog, null)
        view.edittextNewQuestion.typeface = TypefaceHelper().setTypeface("semi-bold")
        view.radioGroup1.setOnCheckedChangeListener({ group, checkedId ->
            val rb = group.findViewById<RadioButton>(checkedId) as RadioButton
            val idx = view.radioGroup1.indexOfChild(rb)

            if (null != rb && checkedId > -1) {
                quesPrice = when (idx) {
                    0 -> {
                        "5000"
                    }
                    1 -> {
                        "10000"
                    }
                    2 -> {
                        "15000"
                    }
                    3 -> {
                        "20000"
                    }
                    4 -> {
                        "25000"
                    }
                    else -> {
                        "5000"
                    }
                }

                Log.d("AddNewQuestion", "price : " + quesPrice)
            }
        })
        view.btnSubmitNewQues.typeface = TypefaceHelper().setTypeface("regular")
        view.btnSubmitNewQues.isEnabled = false
        view.btnSubmitNewQues.alpha = .5f
        view.edittextNewQuestion.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (!s.toString().isEmpty()) {
                    view.btnSubmitNewQues.isEnabled = true
                    view.btnSubmitNewQues.alpha = 1f
                } else {
                    view.btnSubmitNewQues.isEnabled = false
                    view.btnSubmitNewQues.alpha = .5f

                }
            }

            override fun afterTextChanged(s: Editable) {
                if (!s.toString().isEmpty()) {
                    view.btnSubmitNewQues.isEnabled = true
                    view.btnSubmitNewQues.alpha = 1f
                } else {
                    view.btnSubmitNewQues.isEnabled = false
                    view.btnSubmitNewQues.alpha = .5f

                }
            }
        })

        val dialog = BottomSheetDialog(this, R.style.CardDialogTheme)
        dialog.setContentView(view)
        dialog.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE or WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)
        dialog.show()

        view.btnSubmitNewQues.setOnClickListener {
            view.edittextNewQuestion.text?.let {
                showSubmitDialog(it.toString(), quesPrice, dialog)
            }
        }
    }

    private fun showSubmitDialog(txtQuestion: String, price: String, bottomDialog: BottomSheetDialog) {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Are you sure?")
        builder.setMessage("Ask question for $price stars")

        val positiveText = "OK"
        builder.setPositiveButton(positiveText) { dialog, which ->
            dialog.dismiss()
//            Thread(Runnable {
            // do the thing that takes a long time
//                runOnUiThread {
            postNewQuestion(txtQuestion, price, bottomDialog)
//                }
//            }).start()
        }

        val negativeText = "Cancel"
        builder.setNegativeButton(negativeText) { dialog, which ->
            // negative button logic
            dialog.dismiss()
        }

        val dialog = builder.create()
        dialog.setOnShowListener {
            dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(ContextCompat.getColor(this, R.color.colorSelklikMain))
            dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(this!!, R.color.colorSelklikMain))
        }

        // display dialog
        dialog.show()
    }

    private fun postNewQuestion(txtQuestion: String, price: String, bottomDialog: BottomSheetDialog) {
        showProgressDialog()
        if (!ConnectivityReceiver.isConnected()) {
            if (dialog!!.isShowing) {
                dialog!!.dismiss()
            }
            Log.d("Add Card", "No internet connection")
        } else {
            val jsonParams = HashMap<String, String>()
            jsonParams.put("access_token", AppController.instance.getUserAccessToken())
            jsonParams.put("question", txtQuestion)
            jsonParams.put("price", price)

            this?.let {
                ApiHelper(it).makeRequest(
                        Config.API_ARTIST + "/" + Config.ARTIST_ID_2 + "/messages",
                        Request.Method.POST,
                        jsonParams,
                        false,
                        false,
                        onSuccess = { responseString: String, gson: Gson ->
                            Log.d("QnA Fragment", "response string : " + responseString)

                            val user = gson.fromJson(AppController.instance.getUserData(), User::class.java)
                            user.current_balance = (user.current_balance - Integer.parseInt(price.trim()))
                            SharedPrefHelper().setSharedPreferenceString(applicationContext, Config.PREF_USER_DATA, gson.toJson(user))

                            if (dialog!!.isShowing) {
                                dialog!!.dismiss()
                            }
                            if (responseString == "[]") {
                                this?.let {
                                    Toast.makeText(it, "Fail to ask question", Toast.LENGTH_SHORT).show()
                                }
                            } else {
                                bottomDialog?.let {
                                    if (it.isShowing){
                                        it.dismiss()
                                    }
                                }
                                fetchMessageThread()
                            }

                        },
                        onError = { error: VolleyError ->
                            this?.let {
                                ApiHelper(context = it).errorHandler(error)
                                if (dialog!!.isShowing) {
                                    dialog!!.dismiss()
                                }
                            }
                        })
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            REQUEST_NEW_QUESTION -> if (resultCode == Activity.RESULT_OK) {
                Log.d("QnaFragment", "Result ok")
                fetchMessageThread()
            } else {
                Log.d("QnaFragment", "Result cancel")
            }
        }
    }

    private fun showProgressDialog() {
        val mBuilder = AlertDialog.Builder(this, R.style.CustomDialogTheme)
        val mView = LayoutInflater.from(this).inflate(R.layout.dialog_progress, null)

        mBuilder.setView(mView)
        mBuilder.setCancelable(false)
        dialog = mBuilder.create()
        dialog!!.show()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        when (id) {
            android.R.id.home -> onBackPressed()
            else -> return false
        }
        return true
    }

}
