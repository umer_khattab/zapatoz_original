package com.selklik.zapatos.ui.adapter

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import android.opengl.Visibility
import android.support.design.widget.BottomSheetDialog
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.widget.*
import com.android.volley.Request
import com.android.volley.VolleyError
import com.android.volley.toolbox.ImageLoader
import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.JsonParser
import com.selklik.zapatos.R
import com.selklik.zapatos.VideoPlayerActivity
import com.selklik.zapatos.model.Feed
import com.selklik.zapatos.model.User
import com.selklik.zapatos.ui.*
import com.selklik.zapatos.utils.*
import com.selklik.zapatos.utils.helper.LikeFeedHelper
import com.selklik.zapatos.utils.volley.ApiHelper
import kotlinx.android.synthetic.main.bottom_sheet_feed.view.*
import kotlinx.android.synthetic.main.dialog_purchase_premium.view.*
import kotlinx.android.synthetic.main.item_list_feed.view.*
import java.util.HashMap
import android.support.v4.view.ViewPager.OnPageChangeListener
import android.view.*
import java.io.File


/**
 * Created by faridhadiselklik on 15/12/2017.
 */
class FeedItemAdapter(private var mContext: Context, private var feedArrayList: ArrayList<Feed>) :
        RecyclerView.Adapter<FeedItemAdapter.ViewHolder>(), View.OnTouchListener {

    val PREF_HIDE_PLUS_SOCIAL : String = "HIDE_SOCIAL_LOGIC";


    override fun onTouch(v: View?, event: MotionEvent?): Boolean {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        holder.bindItems(feedArrayList[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        val itemView = LayoutInflater.from(parent!!.getContext()).inflate(R.layout.item_list_feed, parent, false)
        return ViewHolder(itemView)
    }

    override fun getItemCount(): Int {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        return feedArrayList.size
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {


        var dialogProgress: AlertDialog? = null
        fun bindItems(feed: Feed) {



            if (SharedPrefHelper().getSharedPreferenceString(mContext, PREF_HIDE_PLUS_SOCIAL, "").equals("hidden"))
                itemView.plus_view.visibility = View.GONE

            else
                if (layoutPosition == 0)
                {
                    itemView.plus_view.visibility = View.VISIBLE
                    itemView.tap.setOnClickListener(View.OnClickListener {

                        SharedPrefHelper().setSharedPreferenceString(mContext, PREF_HIDE_PLUS_SOCIAL, "hidden")
                        itemView.plus_view.visibility = View.GONE

                    })

                    /*itemView.plus_view.setOnClickListener(View.OnClickListener {
                    it.visibility = View.GONE


                    })
                    */

                }


            //Log.d("PROFILEA", feed.artist.name.count().toString() + " " + "Zain Saidin".count().toString())

            var imageLoader: ImageLoader = AppController().getImageLoader()

            itemView.img_artist_profile_picture.setImageUrl(feed.artist.profile_photos.thumbnail, imageLoader)


            //itemView.img_artist_profile_picture.setImageUrl(feed.artist.profile_photos.thumbnail, imageLoader)


            itemView.txt_ar_name.text = feed.artist.name
            itemView.txt_ar_name.typeface = TypefaceHelper().setTypeface("semi-bold")
            if (feed.body != null) {
                itemView.txt_body.text = feed.body
                itemView.txt_body.typeface = TypefaceHelper().setTypeface("regular")
            } else {
                itemView.txt_body.visibility = View.GONE
            }
            itemView.txt_time.typeface = TypefaceHelper().setTypeface("light")
            itemView.txt_likes.typeface = TypefaceHelper().setTypeface("light")
            if (feed.like_count > 0) {
                itemView.txt_likes.text = feed.like_count.toString() + " likes"
            } else {
                itemView.txt_likes.text = "likes"
            }
            if (feed.comment_count > 0) {
                itemView.txt_comment.text = feed.comment_count.toString() + " comments"
            } else {
                itemView.txt_comment.text = "commments"
            }


            when (feed.liked) {
                true -> {
                    itemView.ic_like.setImageResource(R.drawable.ic_feed_liked)
                }
                false -> {
                    itemView.ic_like.setImageResource(R.drawable.ic_feed_like)
                }
            }
            itemView.ic_like.setOnClickListener {
                when (feed.liked) {
                    true -> {
                        itemView.ic_like.setImageResource(R.drawable.ic_feed_like)
                    }
                    false -> {
                        itemView.ic_like.setImageResource(R.drawable.ic_feed_liked)
                    }
                }
                LikeFeedHelper(mContext).doLikeFeedPost(feed.id,
                        feed.liked,
                        likeListener = { liked: Boolean ->
                            Log.d("FeedItemAdapter", "on un like success : $liked")
                            feed.liked = liked
                            if (liked) {
                                feed.like_count += 1
                                itemView.ic_like.setImageResource(R.drawable.ic_feed_liked)
                            } else {
                                feed.like_count -= 1
                                itemView.ic_like.setImageResource(R.drawable.ic_feed_like)
                            }
                            if (feed.like_count > 0) {
                                itemView.txt_likes.text = feed.like_count.toString() + " likes"
                            } else {
                                itemView.txt_likes.text = "likes"
                            }

                        })
            }

            itemView.layout_comment.setOnClickListener {
                mContext.startActivity(Intent(mContext, CommentActivity::class.java).putExtra("post_id", feed.id))
            }

            var time = AppController().setPrettyDateFormat(feed.timestamp)


            itemView.parent_qna.visibility = View.VISIBLE
            when (feed.type) {

                "facebook" -> {

                    itemView.parent_qna.alpha = 0.0f
                    itemView.layout_ic_social.visibility = View.VISIBLE
                    itemView.ic_social.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_feed_fb))
                    itemView.layout_ic_social.setOnClickListener {
                        setFeedSocialBottomSheet("facebook", feed)
                    }

                    itemView.multipleThumbnails.visibility == View.GONE
                    itemView.txt_time.text = "$time on facebook"
                    setSocialPostMedia(feed, imageLoader)

                    itemView.watermark_iv.visibility = View.GONE

                } "twitter" -> {

                    itemView.parent_qna.alpha = 0.0f
                    itemView.layout_ic_social.visibility = View.GONE
                    itemView.ic_social.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_feed_twitter))
                    itemView.layout_ic_social.setOnClickListener {
                        setFeedSocialBottomSheet("twitter", feed)
                    }
                    itemView.txt_time.text = "$time on twitter"

                    itemView.multipleThumbnails.visibility == View.GONE

                    setSocialPostMedia(feed, imageLoader)

                    itemView.watermark_iv.visibility = View.GONE

                } "instagram" -> {


                    itemView.parent_qna.alpha = 0.0f
                    itemView.layout_ic_social.visibility = View.VISIBLE
                    itemView.ic_social.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_feed_insta))
                    itemView.layout_ic_social.setOnClickListener {
                        setFeedSocialBottomSheet("instagram", feed) }

                    itemView.txt_time.text = "$time on instagram"
                    itemView.multipleThumbnails.visibility == View.GONE
                    setSocialPostMedia(feed, imageLoader)

                    itemView.watermark_iv.visibility = View.GONE


                } "premium" -> {


                    itemView.parent_qna.alpha = 0.0f
                    itemView.layout_ic_social.visibility = View.GONE
                    itemView.multipleThumbnails.visibility = View.GONE

                    setPremiumPostMedia(feed, imageLoader)

                    itemView.txt_time.text = "$time as premium"
                    itemView.watermark_iv.visibility = View.VISIBLE // OWAIS EDITION -- //

                } "qna" -> {

                    itemView.parent_qna.alpha = 1.0f
                    itemView.multipleThumbnails.visibility == View.GONE
                    itemView.layout_ic_social.visibility = View.GONE
                    itemView.layout_prem_icon.setVisibility(View.GONE)
                    itemView.txt_time.text = "$time"

                    setQnaPostMedia(feed, imageLoader)
                    itemView.watermark_iv.visibility = View.VISIBLE

                }
            }
        }


        private fun setSocialPostMedia(feed: Feed, imageLoader: ImageLoader) {
            itemView.layout_prem_icon.visibility = View.GONE
            Log.d("FeedAdapter", "feed attachment type : ${feed.attachment_type}")
            if (feed.attachment_type != null) {

                itemView.layout_feed_attachment.visibility = View.VISIBLE


                when (feed.attachment_type) {
                    "photo" -> {

                        var jsonPhoto: JsonArray = feed.photos!!.asJsonArray!!
                        itemView.layout_feed_icon_attachment.visibility = View.GONE


                        if (jsonPhoto.size() > 1) {

                            itemView.totalCount.text = "1/"+jsonPhoto.size()
                            itemView.img_attach.visibility == View.GONE
                            itemView.multipleThumbnails.visibility == View.VISIBLE
                            itemView.multipleThumbnails.adapter = MultipleThumbnailsAdapter(mContext, jsonPhoto, feed)
                            itemView.photoCount.visibility = View.VISIBLE
                            itemView.multipleThumbnails.addOnPageChangeListener(object : OnPageChangeListener {

                                override fun onPageScrollStateChanged(state: Int) {


                                }
                                override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {


                                }

                                override fun onPageSelected(position: Int) {



                                    itemView.totalCount.text = (position+1).toString()+"/"+jsonPhoto.size()


                                }
                            })



                        } else {

                            itemView.multipleThumbnails.visibility == View.GONE

                            if (feed.attachment!! != null) {

                                itemView.img_attach.visibility == View.VISIBLE
                                itemView.img_attach.setImageUrl(feed.attachment, imageLoader)
                                itemView.img_attach.setOnClickListener {
                                    mContext.startActivity(Intent(mContext, ViewImageActivity::class.java).putExtra("feedData", Gson().toJson(feed)))
                                }
                            } else {
                                itemView.img_attach.visibility = View.GONE
                            }

                        }

                    }
                    "video" -> {

                        itemView.multipleThumbnails.visibility == View.GONE
                        itemView.img_attach.visibility == View.VISIBLE
                        itemView.layout_feed_icon_attachment.visibility = View.VISIBLE
                        itemView.ic_play_video.visibility = View.VISIBLE

                        var jsonPhoto: JsonArray = feed.photos!!.asJsonArray!!
                        itemView.img_attach.setImageUrl(jsonPhoto[0].asJsonObject.get("versions").asJsonObject.get("full_resolution").asString, imageLoader)

                        itemView.ic_play_video.setOnClickListener {
                            mContext.startActivity(Intent(mContext, VideoPlayerActivity::class.java).putExtra("video_url", feed.attachment))
                        }
                    }
                }
            } else {

                itemView.layout_feed_attachment.visibility = View.GONE

            }

        }

        private fun setPremiumPostMedia(feed: Feed, imageLoader: ImageLoader) {
            var jsonPhoto: JsonArray = feed.photos!!.asJsonArray

            when (feed.metadata.full_access) {
                true -> {


                    itemView.layout_prem_icon.visibility = View.GONE
                    itemView.img_attach.setImageUrl(jsonPhoto[0].asJsonObject.get("versions").asJsonObject.get("full_resolution").asString, imageLoader)
                    when (feed.attachment_type) {

                        "video" -> {

                                itemView.layout_feed_icon_attachment.visibility = View.VISIBLE
                                itemView.ic_play_small.visibility = View.GONE
                                itemView.layout_prem_icon.visibility = View.GONE
                                itemView.ic_play_video.visibility = View.VISIBLE
                                itemView.ic_play_video.setOnClickListener {

                                val intent = Intent(mContext, VideoPlayerActivity::class.java)
                                intent.putExtra("video_url", feed.attachment)
                                intent.putExtra("premium", true)
                                mContext.startActivity(intent)

                            }
                        }
                        "photo" -> {

                                val intent = Intent(mContext, ViewImageActivity::class.java);
                                intent.putExtra("feedData", Gson().toJson(feed))
                                intent.putExtra("premium", true)
                                itemView.layout_feed_icon_attachment.visibility = View.GONE
                                itemView.img_attach.setOnClickListener {
                                mContext.startActivity(intent)

                            }
                        }
                    }
                }
                false -> {
                    itemView.layout_feed_icon_attachment.visibility = View.VISIBLE
                    itemView.layout_prem_icon.visibility = View.VISIBLE
                    itemView.ic_play_video.visibility = View.GONE
                    itemView.img_attach.setImageUrl(jsonPhoto[0].asJsonObject.get("versions").asJsonObject.get("preview").asString, imageLoader)
                    var price = feed.metadata.price
                    itemView.txt_prim_price.text = "$price star for view"
                    itemView.txt_prim_price.typeface = TypefaceHelper().setTypeface("regular")

                    itemView.img_attach.setOnClickListener {
                        showPurchaseDialog(
                                feed,
                                Gson().fromJson(AppController.instance.getUserData(),
                                User::class.java),
                                jsonPhoto[0].asJsonObject.get("versions").asJsonObject.get("preview").asString)
                    }

                    when (feed.attachment_type) {
                        "video" -> {
                            itemView.ic_play_small.visibility = View.VISIBLE
                        }
                        "photo" -> {
                            itemView.ic_play_small.visibility = View.GONE
                        }
                    }

                }
            }
        }

        private fun setQnaPostMedia(feed: Feed, imageLoader: ImageLoader) {



            var jsonPhoto: JsonArray = feed.photos!!.asJsonArray
            var photoFull : String = jsonPhoto[0].asJsonObject.get("versions").asJsonObject.get("full_resolution").asString;

            if (feed.type.equals("qna")) {

                itemView.img_artist_profile_picture.setImageUrl(feed.artist.profile_photos.square, imageLoader)
                itemView.img_user_profile_picture_qna_dp.setImageUrl(feed.metadata.user.profile_photos.thumbnail,
                         imageLoader)

                itemView.txt_user_name_qna.setText(feed.metadata.user.name)
                itemView.txt_question_qna.setText(feed.body)

                if (feed.metadata.full_access) {
                    if (feed.attachment_type.equals("photo")) {

                        itemView.ic_play_video.setVisibility(View.GONE)

                        itemView.img_attach.setImageUrl(photoFull, imageLoader)
                        itemView.img_attach.setOnClickListener(View.OnClickListener {

                            val intent = Intent(mContext, ViewImageActivity::class.java);
                            intent.putExtra("feedData", Gson().toJson(feed))
                            intent.putExtra("premium", true)
                            mContext.startActivity(intent)

                        })

                    } else if (feed.attachment_type.equals("video")) {

                        itemView.ic_play_video.setVisibility(View.VISIBLE)
                            itemView.img_attach.setImageUrl(photoFull, imageLoader)
                            itemView.ic_play_video.setOnClickListener(View.OnClickListener {

                            if (feed.attachment!!.contains("youtube")) {

                                val uri = Uri.parse(feed.attachment)
                                val intent = Intent(Intent.ACTION_VIEW, uri)
                                mContext.startActivity(intent)

                            } else {

                                val intent = Intent(mContext, VideoPlayerActivity::class.java)
                                intent.putExtra("video_url", feed.attachment)
                                intent.putExtra("premium", true)
                                mContext.startActivity(intent)

                            }

                        })

                    }
                } else {

                    if (feed.attachment_type.equals("photo")) {

                        itemView.ic_play_video.setVisibility(View.GONE)
                        itemView.img_attach.setImageUrl(photoFull, imageLoader)
                        itemView.img_attach.setOnClickListener(View.OnClickListener {

                            val intent = Intent(mContext, ViewImageActivity::class.java);
                            intent.putExtra("feedData", Gson().toJson(feed))
                            intent.putExtra("premium", true)
                            mContext.startActivity(intent)

                        })

                    } else if (feed.attachment_type.equals("video")) {

                        itemView.ic_play_video.setVisibility(View.GONE)
                        itemView.img_attach.setImageUrl(photoFull, imageLoader)
                        itemView.layout_prem_icon.setVisibility(View.VISIBLE)
                        itemView.txt_prim_price.setText(Integer.toString(feed.metadata.price) + " to view")
                        itemView.img_attach.setOnClickListener(View.OnClickListener {

                            setNonFullAccessClick(feed)

                        })

                    }
                }
            }
        }

        fun setNonFullAccessClick(feed: Feed) {

            var jsonPhoto: JsonArray = feed.photos!!.asJsonArray
            var photoFull : String = jsonPhoto[0].asJsonObject.get("versions").
                    asJsonObject.get("full_resolution").asString;

            var photoPreview : String = jsonPhoto[0].asJsonObject.get("versions").
                    asJsonObject.get("full_resolution").asString;


            if (!ConnectivityReceiver.isConnected()) {
                Toast.makeText(mContext, "No internet Connection", Toast.LENGTH_SHORT).show()
            } else {


                val accessToken = SharedPrefHelper().getSharedPreferenceString(mContext, Config.PREF_ACCESS_TOKEN, "")
                val jsonParams = HashMap<String, String>()
                jsonParams.put("access_token", accessToken!!)

                ApiHelper(mContext).makeRequest(Config.API_ME, Request.Method.GET, jsonParams, true, false,

                        onSuccess = { responseString, gson ->

                            if (responseString !== "[]") {
                                val user = Gson().fromJson(responseString, User::class.java)
                                Log.d("current balance", "user current balance : " + user.current_balance)
                                if (feed.type.equals("qna")) {
                                    showPurchaseDialog(feed, user, photoFull)
                                } else {
                                    showPurchaseDialog(feed, user, photoPreview)
                                }

                            } else {
                                Toast.makeText(mContext, "Something went wrong.", Toast.LENGTH_SHORT).show()
                            }



                        },
                        onError = {

                        })

            }
        }

        fun showPurchaseDialog(feed: Feed, user: User, previewUrl: String) {
            val layoutPlayIcon: RelativeLayout

            val mBuilder = AlertDialog.Builder(mContext, R.style.CustomDialogTheme)
            val mView = LayoutInflater.from(mContext).inflate(R.layout.dialog_purchase_premium, null)
            Log.d("Feed Item Adapter", "preview url : " + previewUrl)

            mView.img_post_non_full_access.setImageUrl(previewUrl, AppController.instance.getImageLoader())
            layoutPlayIcon = mView.findViewById(R.id.ic_layout_qna)
            if (feed.type == "qna") {
                layoutPlayIcon.visibility = View.VISIBLE
            } else {
                if (feed.attachment_type == "photo") {
                    layoutPlayIcon.visibility = View.GONE
                } else {
                    layoutPlayIcon.visibility = View.VISIBLE
                }
            }

            mView.txt_purchase_name.typeface = TypefaceHelper().setTypeface("light")
            mView.txt_purchase_from.typeface = TypefaceHelper().setTypeface("bold")
            mView.txt_balance_title.typeface = TypefaceHelper().setTypeface("light")
            mView.txt_balance.typeface = TypefaceHelper().setTypeface("bold")

            mView.btn_cancel.typeface = TypefaceHelper().setTypeface("regular")
            mView.btn_proceed.typeface = TypefaceHelper().setTypeface("regular")

            mBuilder.setView(mView)
            val dialog = mBuilder.create()
            dialog.show()

            mView.txt_purchase_from.text = feed.artist.name + " ?"
            if (user.current_balance < feed.metadata.price) {
                mView.txt_balance_title.text = "Insufficient credit"
                mView.txt_balance.text = Integer.toString(user.current_balance.toInt())
                mView.btn_proceed.text = "Reload?"
                mView.btn_proceed.setOnClickListener {

                    val intent = Intent(mContext, ReloadStarActivity::class.java)
                    mContext.startActivity(intent)

                    if (dialog.isShowing) {
                        dialog.dismiss()
                    }
                }
            } else {
                mView.txt_balance_title.text = "My Credit"
                mView.txt_balance.text = Integer.toString(user.current_balance.toInt())
                mView.btn_proceed.text = "Purchase for " + Integer.toString(feed.metadata.price)
                mView.btn_proceed.setOnClickListener {
                    createPurchase(feed, dialog)
                }
            }

            mView.btn_cancel.setOnClickListener {
                if (dialog.isShowing) {
                    dialog.dismiss()
                }
            }
        }

        private fun setFeedSocialBottomSheet(social: String, feed: Feed) {
            var mSocialBottomSheetDialog = BottomSheetDialog(mContext)
            val sheetView = LayoutInflater.from(mContext).inflate(R.layout.bottom_sheet_feed, null)
            mSocialBottomSheetDialog.setContentView(sheetView)

            sheetView.txt_forth.text = "Cancel"
            sheetView.txt_forth.setTextColor(ContextCompat.getColor(mContext, android.R.color.holo_red_light))
            when (social) {
                "facebook" -> {
                    sheetView.fragment_home_bottom_sheet_first.visibility = View.VISIBLE
                    sheetView.fragment_home_bottom_sheet_social_third.visibility = View.VISIBLE
                    sheetView.txt_first.text = "Total number of like on Facebook " + feed.metadata.like_count
                    sheetView.txt_second.text = "View comment on facebook (" + feed.metadata.comment_count + ")"
                    sheetView.txt_third.text = "View post on Facebook"
                    mSocialBottomSheetDialog.show()
                    sheetView.fragment_home_bottom_sheet_first.isClickable = false

                    sheetView.fragment_home_bottom_sheet_second.setOnClickListener {
                        mSocialBottomSheetDialog.dismiss()
                        val intent = Intent(mContext, CommentActivity::class.java)
                        intent.putExtra("post_id", feed.metadata.post_id)
                        intent.putExtra("commentType", "facebook")
                        mContext.startActivity(intent)
                    }

                    sheetView.fragment_home_bottom_sheet_social_third.setOnClickListener {
                        mSocialBottomSheetDialog.dismiss()
                        val intent: Intent
                        val link: String
                        link = "https://facebook.com/" + feed.metadata.post_id
                        intent = Intent(mContext, BrowserActivity::class.java)
                        intent.putExtra("url", link)
                        intent.putExtra("from", "facebook")
                        mContext.startActivity(intent)
                    }

                    sheetView.fragment_home_bottom_sheet_cancel.setOnClickListener { mSocialBottomSheetDialog.dismiss() }
                }
                "twitter" -> {
                    sheetView.fragment_home_bottom_sheet_first.visibility = View.GONE
                    sheetView.fragment_home_bottom_sheet_second.visibility = View.GONE
                    sheetView.txt_first.text = "View post on Twitter"
                    mSocialBottomSheetDialog.show()

                    sheetView.fragment_home_bottom_sheet_first.setOnClickListener {
                        mSocialBottomSheetDialog.dismiss()
                        val intent: Intent
                        val link: String
                        link = "https://twitter.com/" + feed.metadata.account_username + "/status/" + feed.metadata.post_id
                        intent = Intent(mContext, BrowserActivity::class.java)
                        intent.putExtra("url", link)
                        intent.putExtra("from", "twitter")
                        mContext.startActivity(intent)
                    }
                }
                "instagram" -> {
                    sheetView.fragment_home_bottom_sheet_first.visibility = View.VISIBLE
                    sheetView.fragment_home_bottom_sheet_second.visibility = View.VISIBLE
                    sheetView.txt_first.text = "Total number of like on Instagram " + feed.metadata.like_count
                    sheetView.txt_second.text = "View comment on Instagram (" + feed.metadata.comment_count + ")"
                    sheetView.txt_third.text = "View post on Instagram"
                    mSocialBottomSheetDialog.show()
                    sheetView.fragment_home_bottom_sheet_first.isClickable = false
                    sheetView.fragment_home_bottom_sheet_second.isClickable = false

//                    sheetView.fragment_home_bottom_sheet_second.setOnClickListener {
//                        mSocialBottomSheetDialog.dismiss()
//                        val intent = Intent(mContext, CommentActivity::class.java)
//                        intent.putExtra("post_id", feed.metadata.post_id)
//                        intent.putExtra("commentType", "instagram")
//                        mContext.startActivity(intent)
//                    }

                    sheetView.fragment_home_bottom_sheet_social_third.setOnClickListener {
                        mSocialBottomSheetDialog.dismiss()

                        val intent: Intent
                        val link: String
                        link = "https://www.instagram.com/p/" + feed.metadata.post_id + "/"
                        intent = Intent(mContext, BrowserActivity::class.java)
                        intent.putExtra("url", link)
                        intent.putExtra("from", "instagram")
                        mContext.startActivity(intent)
//                        Log.d(TAG, "insta link : " + link)
                    }
                }
                else -> {
                    sheetView.fragment_home_bottom_sheet_first.visibility = View.VISIBLE
                    sheetView.fragment_home_bottom_sheet_second.visibility = View.VISIBLE
//                    mSocialBottomSheetDialog.show()
                }
            }

            sheetView.fragment_home_bottom_sheet_cancel.setOnClickListener { mSocialBottomSheetDialog.dismiss() }


        }

        private fun showProgressDialog() {
            val mBuilder = AlertDialog.Builder(mContext!!, R.style.CustomDialogTheme)
            val mView = LayoutInflater.from(mContext!!).inflate(R.layout.dialog_progress, null)

            mBuilder.setView(mView)
            mBuilder.setCancelable(false)
            dialogProgress = mBuilder.create()
            dialogProgress!!.show()
        }

        fun createPurchase(feed: Feed, dialog: AlertDialog) {
            showProgressDialog()
            if (!ConnectivityReceiver.isConnected()) {
                if (dialogProgress!!.isShowing) {
                    dialogProgress!!.dismiss()
                }
                Toast.makeText(mContext, "No internet Connection", Toast.LENGTH_SHORT).show()
            } else {
                var postID = feed.id
                val jsonParams = HashMap<String, String>()
                jsonParams.put("access_token", AppController.instance.getUserAccessToken())

                var endPoint : String = ""

                if (feed.type.equals("premium"))
                    endPoint = Config.API_POST + "/$postID/purchases"

                else if (feed.type.equals("qna"))
                    endPoint = Config.API_QNA + File.separator + feed.metadata.question_and_answer_id + "/purchases"

                ApiHelper(mContext).makeRequest(
                        endPoint,
                        Request.Method.POST,
                        jsonParams,
                        false,
                        false,
                        onSuccess = { responseString: String, gson: Gson ->
                            Log.d("QnA Fragment", "response string : " + responseString)
                            if (dialogProgress!!.isShowing) {
                                dialogProgress!!.dismiss()
                            }

                            feed.metadata.full_access = true
                            notifyDataSetChanged()

                            // OWAIS EDITION -- //

                            if (feed.type.equals("premium")) {

                                val user = gson.fromJson(AppController.instance.getUserData(), User::class.java)
                                val currentBalance = JsonParser().parse(responseString).asJsonObject.get("current_balance").asDouble
                                user.current_balance = currentBalance
                                SharedPrefHelper().setSharedPreferenceString(mContext, Config.PREF_USER_DATA, gson.toJson(user))


                            } else if(feed.type.equals("qna")) {

                                val user = gson.fromJson(AppController.instance.getUserData(), User::class.java)
                                user.current_balance = user.current_balance - feed.metadata.price
                                SharedPrefHelper().setSharedPreferenceString(mContext, Config.PREF_USER_DATA, gson.toJson(user))
                            }

                            // OWAIS EDITION -- //

                            if (dialog!!.isShowing) {
                                dialog!!.dismiss()
                            }

                            when (feed.attachment_type) {



                                "video" -> {

                                    mContext.startActivity(Intent(mContext, VideoPlayerActivity::class.java).putExtra("video_url", feed.attachment).putExtra("premium", true))


                                }
                                "photo" -> {

                                    if (feed.type.equals("premium"))
                                        mContext.startActivity(Intent(mContext, ViewImageActivity::class.java).putExtra("feedData", Gson().toJson(feed)).putExtra("premium", true))

                                    else {

                                        val intent = Intent(mContext, ViewImageActivity::class.java);
                                        intent.putExtra("feedData", Gson().toJson(feed))
                                        intent.putExtra("premium", true)
                                        mContext.startActivity(intent)

                                    }
                                }

                            }
//

                        }, onError = { error: VolleyError ->
                            ApiHelper(mContext!!).errorHandler(error)
                            if (dialogProgress!!.isShowing) {
                                dialogProgress!!.dismiss()
                            }
                        })

//                Api(mContext).makeRequest(Config.API_POST + "/" + feed.id + "/purchases", Request.Method.POST, jsonParams, true, false, true, object : Api.ApiListener() {
//                    fun onSuccess(responseString: String, gson: Gson) {
//                        Log.d(TAG, "response : " + responseString)
//                        if (responseString == "[]") {
//                            Toast.makeText(mContext, "Fail to purchase", Toast.LENGTH_SHORT).show()
//                        } else {
//                            feed.metadata.full_access = true
//                            notifyDataSetChanged()
//
//                            val user = gson.fromJson(SharedPrefHelper.getSharedPreferenceString(Config.PREF_USER_DATA, null), User::class.java)
//                            val purchase = gson.fromJson<Purchase>(responseString, Purchase::class.java!!)
//                            user.current_balance = purchase.current_balance
//
//                            SharedPrefHelper.setSharedPreferenceString(Config.PREF_USER_DATA, gson.toJson(user))
//                            val user2 = gson.fromJson(SharedPrefHelper.getSharedPreferenceString(Config.PREF_USER_DATA, null), User::class.java)
//                            Log.d(TAG, "balance : " + Integer.toString(user.current_balance) + ", new balance : " + user2.current_balance)
//
//                            val intent = Intent(mContext, ActivitySinglePost::class.java)
//                            intent.putExtra("feedData", Gson().toJson(feed))
//                            mContext.startActivity(intent)
//                            //
//                        }
//                    }
//
//                    fun onError(error: String) {}
//                })
            }
        }

    }
}