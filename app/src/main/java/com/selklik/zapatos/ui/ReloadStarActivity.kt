package com.selklik.zapatos.ui

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.VolleyError
import com.google.gson.Gson
import com.google.gson.JsonParser
import com.selklik.zapatos.R
import com.selklik.zapatos.model.Reload
import com.selklik.zapatos.model.User
import com.selklik.zapatos.ui.adapter.ReloadStarAdapter
import com.selklik.zapatos.utils.*
import com.selklik.zapatos.utils.InAppBilling.*
import com.selklik.zapatos.utils.volley.ApiHelper

import kotlinx.android.synthetic.main.activity_reload_star.*
import kotlinx.android.synthetic.main.content_reload_star.*
import org.json.JSONException
import org.json.JSONObject
import java.util.ArrayList
import java.util.HashMap
import android.widget.AdapterView.OnItemClickListener



class ReloadStarActivity : AppCompatActivity() {

    private var mHelper: IabHelper? = null
    private var ITEM_SKU: String? = null
    private val mBroadcastReceiver: IabBroadcastReceiver? = null
    private var dialog: AlertDialog? = null

    private var starsSelected = ArrayList<Int>()

    private var TAG = ReloadStarActivity::class.java.simpleName
    private var mLayoutManager: RecyclerView.LayoutManager? = null
    private var base64EncodedPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAl6wwAkUxEz6DXgxGRusL/5pOMTqVaiZoDrBmS+0qJb3KYVzFdIiygnV3DNVSKaA+v9eqGYHSuw+lrmnsvgD7Z8vWjIDEP5+4ixljo7Y70psZSk47Rjn++vIUcDulNn+3lFGdX/yF/ubsvFV9y5VrnMw41OXBKacSg9Erffm8Czd0FFEu/Pvkis0ufNmP2WSAZtr+G/BUvqC76YVtWoKdObzkLrwv7dkwH2aCwqz+WiZSzl1DK1k8dG3mk8fxX0LQ2mZ8T0hOtK5ufnRmGFea6XVdmWi/0zGiRgZwv0YpftlFYFbYvkkohVwNIL25hfZu0ywN4+bJZAng+HE20XH0AwIDAQAB"
    private var list_sku: ArrayList<String>? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reload_star)
        setSupportActionBar(toolbar)
        val ab = supportActionBar!!
        ab!!.setHomeButtonEnabled(true)
        ab!!.setDisplayHomeAsUpEnabled(true)
        ab!!.setDisplayShowTitleEnabled(false)

        txt_toolbar_title.typeface = TypefaceHelper().setTypeface("regular")

        mHelper = IabHelper(this, base64EncodedPublicKey)
        mHelper!!.enableDebugLogging(true)

        list_sku = ArrayList()
        list_sku!!.add("com.selklik.iab..8_000")
        list_sku!!.add("com.selklik.iab.25_000")
        list_sku!!.add("com.selklik.iab.45_000")
        list_sku!!.add("com.selklik.iab.100_000")
        list_sku!!.add("com.selklik.iab.250_000")

        var reloadItemArrayList = ArrayList<Reload>()
            reloadItemArrayList.add(Reload(number_of_star = "8000", price = "0.99", product_id = "com.selklik.iab.8_000"))
            reloadItemArrayList.add(Reload(number_of_star = "25000", price = "1.99", product_id = "com.selklik.iab.25_000"))
            reloadItemArrayList.add(Reload(number_of_star = "45000", price = "4.99", product_id = "com.selklik.iab.45_000"))
            reloadItemArrayList.add(Reload(number_of_star = "100000", price = "9.99", product_id = "com.selklik.iab.100_000"))
            reloadItemArrayList.add(Reload(number_of_star = "250000", price = "29.99", product_id = "com.selklik.iab.250_000"))

            starsSelected.add(8000)
            starsSelected.add(25000)
            starsSelected.add(45000)
            starsSelected.add(100000)
            starsSelected.add(250000)

        mLayoutManager = LinearLayoutManager(this)
        list_reload.layoutManager = mLayoutManager
        mHelper!!.startSetup(IabHelper.OnIabSetupFinishedListener { result ->
            Log.d(TAG, "Setup finished.")
            if (!result.isSuccess) {
                complain("Problem setting up in-app billing: " + result)
                return@OnIabSetupFinishedListener
            } else {
                Log.d(TAG, "In-app Billing is set up OK")
                //                    consumeItem();
                try {
                    mHelper!!.queryInventoryAsync(true, list_sku, null, mReceivedInventoryListener)
                } catch (e: IabHelper.IabAsyncInProgressException) {
                    e.printStackTrace()
                }

                list_reload.adapter = ReloadStarAdapter(this, reloadItemArrayList)
            }
        })

        fun <T : RecyclerView.ViewHolder> T.listen(event: (position: Int, type: Int) -> Unit): T {
            list_reload.setOnClickListener {

                Toast.makeText(applicationContext, position.toString(), Toast.LENGTH_LONG).show()

            }

            return this
        }

    }

    private fun makeReloadRequest(productId: String, purchaseToken: String, packageName: String){
        println("product id : $productId")
        println("purchase token : $purchaseToken")
        showProgressDialog()
        if (!ConnectivityReceiver.isConnected()) {
            if (dialog!!.isShowing) {
                dialog!!.dismiss()
            }
            Log.d("Add Card", "No internet connection")
        } else {
            val jsonParams = HashMap<String, String>()
            jsonParams.put("access_token", AppController.instance.getUserAccessToken())
            jsonParams.put("product_id", productId)
            jsonParams.put("purchase_token", purchaseToken)
            jsonParams.put("package_name", packageName)

            ApiHelper(context = this).makeRequest(
                    module = Config.API_RELOAD_STAR,
                    method = Request.Method.POST,
                    params = jsonParams,
                    onSuccess = { responseString: String, gson: Gson ->
                        Log.d("QnA Fragment", "response string : " + responseString)

                        if (dialog!!.isShowing) {
                            dialog!!.dismiss()
                        }

                        if (responseString == "[]") {
                            Toast.makeText(this, "Fail to ask question", Toast.LENGTH_SHORT).show()
                        } else {


                            val user = gson.fromJson(AppController.instance.getUserData(), User::class.java)
                            user.current_balance = user.current_balance + purchasedStarsSuccess
                            SharedPrefHelper().setSharedPreferenceString(applicationContext, Config.PREF_USER_DATA, gson.toJson(user))

                            val intent = Intent()
                            intent.putExtra("response", responseString)
                            setResult(Activity.RESULT_OK, intent)
                            finish()
                        }

                    },
                    onError = { error: VolleyError ->
                        ApiHelper(this).errorHandler(error)
                        if (dialog!!.isShowing) {
                            dialog!!.dismiss()
                        }
                    })
        }
    }

    private fun showProgressDialog() {
        val mBuilder = AlertDialog.Builder(this!!, R.style.CustomDialogTheme)
        val mView = LayoutInflater.from(this!!).inflate(R.layout.dialog_progress, null)

        mBuilder.setView(mView)
        mBuilder.setCancelable(false)
        dialog = mBuilder.create()
        dialog!!.show()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        when (id) {
            android.R.id.home -> onBackPressed()
            else -> return false
        }
        return super.onOptionsItemSelected(item)
    }


    private var purchasedStarsSuccess : Int = 0;

    fun starPurchased(position : Int)
    {
        purchasedStarsSuccess = starsSelected[position]
    }

    fun launchPurchaseFlow(productId: String) {
        ITEM_SKU = productId
        try {
            mHelper!!.launchPurchaseFlow(this@ReloadStarActivity, ITEM_SKU, 10001,
                    IabHelper.OnIabPurchaseFinishedListener { result, purchase ->
                        Log.d(TAG, "Item sku in listener: " + ITEM_SKU)
                        if (result.isFailure) {
                            // Handle error
                            Log.d(TAG, "Error purchasing : " + result.message)
                            return@OnIabPurchaseFinishedListener
                        } else {

                            if (purchase.sku.equals(ITEM_SKU)) {
                                try {
                                    mHelper!!.queryInventoryAsync(IabHelper.QueryInventoryFinishedListener { result, inv ->
                                        if (result.isFailure) {
                                            // Handle failure
                                            Log.d(TAG, "Error purchasing : " + result.message)
                                        } else {

                                            val starPurchase = inv.getPurchase(ITEM_SKU)
                                            if (starPurchase != null && verifyDeveloperPayload(starPurchase)) {
                                                Log.d(TAG, "Star Available. Consuming it.")
                                                try {
                                                    mHelper!!.consumeAsync(inv.getPurchase(ITEM_SKU)) { purchase, result ->
                                                        if (result.isSuccess) {
                                                            //                        clickButton.setEnabled(true);
                                                            //                                                                                Toast.makeText(ActivityReloadStar.this, "purchase token : " + purchase.getToken(), Toast.LENGTH_LONG).show();
                                                            Log.d(TAG, "Purchased details consume : " + purchase.originalJson)
                                                            try {
                                                                val purchaseObj = JsonParser().parse(purchase.originalJson).asJsonObject
                                                                makeReloadRequest(purchaseObj.get("productId").asString, purchaseObj.get("purchaseToken").asString, purchaseObj.get("packageName").asString)
                                                            } catch (e: JSONException) {
                                                                e.printStackTrace()
                                                            }

                                                        } else {
                                                            // handle error
                                                            Log.d(TAG, "error consume purchases item : " + result.message)
                                                        }
                                                    }
                                                } catch (e: IabHelper.IabAsyncInProgressException) {
                                                    complain("Error consuming star. Another async operation in progress.")
                                                    e.printStackTrace()
                                                }

                                                return@QueryInventoryFinishedListener
                                            }
                                            //updateUi();
                                            Log.d(TAG, "Initial inventory query finished; enabling main UI.")
                                        }
                                        Log.d(TAG, "Query inventory was successful.")
                                    })
                                } catch (e: IabHelper.IabAsyncInProgressException) {
                                    e.printStackTrace()
                                }

                                Log.d(TAG, "Thank user for purchase, go on with server : " + purchase.originalJson)
                                //                buyButton.setEnabled(false);
                            }
                        }
                    }, "")
        } catch (e: IabHelper.IabAsyncInProgressException) {
            e.printStackTrace()

        }

    }

    private var mReceivedInventoryListener: IabHelper.QueryInventoryFinishedListener = IabHelper.QueryInventoryFinishedListener { result, inventory ->
        Log.d(TAG, "result : " + result.getMessage())
        if (result.isFailure()) {
            // Handle failure
            Log.d(TAG, "QueryInventoryFinishedListener Failed")
        } else {
            for (i in 0..list_sku!!.size - 1) {
                if (inventory != null) {
                    Log.d(TAG, "inventory not null : " + list_sku!![i] + " : " + inventory!!.hasPurchase(list_sku!!.get(i)))
                    if (inventory!!.hasPurchase(list_sku!!.get(i))) {
                        Log.d(TAG, "inventory has purchase : " + list_sku!!.get(i) + " : " + inventory!!.hasPurchase(list_sku!!.get(i)))
                        try {
                            mHelper!!.consumeAsync(inventory!!.getPurchase(list_sku!!.get(i)),
                                    mConsumeFinishedListener)
                        } catch (e: IabHelper.IabAsyncInProgressException) {
                            e.printStackTrace()
                        }

                    }
                } else {
                    Log.d(TAG, "inventory is null")
                }
            }
        }
    }

    private var mConsumeFinishedListener: IabHelper.OnConsumeFinishedListener = IabHelper.OnConsumeFinishedListener { purchase, result ->
        if (result.isSuccess()) {
            //                        clickButton.setEnabled(true);
            Toast.makeText(this@ReloadStarActivity, "purchase token : " + purchase.getToken(), Toast.LENGTH_LONG).show()
            Log.d(TAG, "purchase details : " + purchase.getOriginalJson())
        } else {
            // handle error
        }
    }

    private fun verifyDeveloperPayload(p: Purchase): Boolean {
        val payload = p.developerPayload
        return true
    }

    private fun complain(message: String) {
        Log.e(TAG, "**** Selklik purchase Error: " + message)
        alert("Error: " + message)
    }

    private fun alert(message: String) {
        val bld = AlertDialog.Builder(this)
        bld.setMessage(message)
        bld.setNeutralButton("OK", null)
        Log.d(TAG, "Showing alert dialog: " + message)
        bld.create().show()
    }

    public override fun onDestroy() {
        super.onDestroy()

        if (mBroadcastReceiver != null) {
            unregisterReceiver(mBroadcastReceiver)
        }

        if (mHelper != null) {
            try {
                mHelper!!.dispose()
            } catch (e: IabHelper.IabAsyncInProgressException) {
                e.printStackTrace()
            }

        }
        mHelper = null
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        Log.d(TAG, "onActivityResult($requestCode,$resultCode,$data")
        if (mHelper == null) return

        // Pass on the activity result to the helper for handling
        if (!mHelper!!.handleActivityResult(requestCode, resultCode, data)) {
            // not handled, so handle it ourselves (here's where you'd
            // perform any handling of activity results not related to in-app
            // billing...
            super.onActivityResult(requestCode, resultCode, data)
        } else {
            Log.d(TAG, "onActivityResult handled by IABUtil.")
        }
    }

}
