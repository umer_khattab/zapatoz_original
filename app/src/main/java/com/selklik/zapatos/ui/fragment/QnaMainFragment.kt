package com.selklik.zapatos.ui.fragment

import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.BottomSheetDialog
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.selklik.zapatos.R
import kotlinx.android.synthetic.main.fragment_home_qna.*
import android.support.v7.app.AlertDialog
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.WindowManager
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.VolleyError
import com.google.gson.Gson
import com.google.gson.JsonParser
import com.google.gson.reflect.TypeToken
import com.selklik.zapatos.model.AskPagerData
import com.selklik.zapatos.model.User
import com.selklik.zapatos.ui.AddNewQuestionActivity
import com.selklik.zapatos.ui.QnaActivity
import com.selklik.zapatos.ui.adapter.AskUssyPagerAdapter
import com.selklik.zapatos.utils.*
import com.selklik.zapatos.utils.volley.ApiHelper
import kotlinx.android.synthetic.main.new_ques_fragment_bottom_sheet_dialog.view.*
import java.util.HashMap


/**
 * Created by faridhadiselklik on 29/11/2017.
 */
class QnaMainFragment : Fragment() {

    private var dialog: AlertDialog? = null
    private inline fun <reified T> genericType() = object : TypeToken<T>() {}.type!!
    private val REQUEST_NEW_QUESTION = 1001
    private var quesPrice: String = "1"

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.fragment_home_qna, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val myImageList = intArrayOf(R.drawable.ic_qna_tutorial_1, R.drawable.ic_qna_tutorial_2, R.drawable.ic_qna_tutorial_3, R.drawable.ic_qna_tutorial_4)
        val artistName = resources.getString(R.string.artist_short_name)
        val ask_pager_text_1 = "Ask $artistName any question you would like to know the answer to. Her favourite food, her pet’s name, anything!"
        val ask_pager_text_2 = "Put a bounty on your question! $artistName accepts an in-app currency called Selklik Stars"
        val ask_pager_text_3 = "Get more bang for your buck! $artistName answers your questions in the form of short videos."
        val ask_pager_text_4 = "Stars back guarantee! If $artistName can’t find the time to answer your question within 2 days, your stars will be refunded in full."

        var pagerArrayList: ArrayList<AskPagerData> = ArrayList()
        pagerArrayList.add(AskPagerData(myImageList[0], ask_pager_text_1))
        pagerArrayList.add(AskPagerData(myImageList[1], ask_pager_text_2))
        pagerArrayList.add(AskPagerData(myImageList[2], ask_pager_text_3))
        pagerArrayList.add(AskPagerData(myImageList[3], ask_pager_text_4))

//        val pager = view!!.findViewById(R.id.viewpager_qna) as ViewPager
        val adapter = AskUssyPagerAdapter(context!!, pagerArrayList)
        viewpager_qna.adapter = adapter

//        val tabLayout = view!!.findViewById(R.id.tab_indicator_layout) as TabLayout
        tab_indicator_layout.setupWithViewPager(viewpager_qna, true)

        text_button_ask_ussy.typeface = TypefaceHelper().setTypeface("regular")
        text_button_ask_ussy.text = "Ask $artistName Now!"
        button_ask_ussy.setOnClickListener {
                        fetchMessageThread()
//            showBottomSheetDialog()
        }
//        fetchMessageThread()

    }

    private fun showBottomSheetDialog() {
        val view = layoutInflater.inflate(R.layout.new_ques_fragment_bottom_sheet_dialog, null)
        view.edittextNewQuestion.typeface = TypefaceHelper().setTypeface("semi-bold")
        view.radioGroup1.setOnCheckedChangeListener(RadioGroup.OnCheckedChangeListener { group, checkedId ->
            val rb = group.findViewById<RadioButton>(checkedId) as RadioButton
            val idx = view.radioGroup1.indexOfChild(rb)

            if (null != rb && checkedId > -1) {
                quesPrice = when (idx) {
                    0 -> {
                        "5000"
                    }
                    1 -> {
                        "10000"
                    }
                    2 -> {
                        "15000"
                    }
                    3 -> {
                        "20000"
                    }
                    4 -> {
                        "25000"
                    }
                    else -> {
                        "5000"
                    }
                }

                Log.d("AddNewQuestion", "price : " + quesPrice)
            }
        })

        view.btnSubmitNewQues.typeface = TypefaceHelper().setTypeface("regular")
        view.btnSubmitNewQues.isEnabled = false
        view.btnSubmitNewQues.alpha = .5f
        view.edittextNewQuestion.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (!s.toString().isEmpty()) {
                    view.btnSubmitNewQues.isEnabled = true
                    view.btnSubmitNewQues.alpha = 1f
                } else {
                    view.btnSubmitNewQues.isEnabled = false
                    view.btnSubmitNewQues.alpha = .5f

                }
            }

            override fun afterTextChanged(s: Editable) {
                if (!s.toString().isEmpty()) {
                    view.btnSubmitNewQues.isEnabled = true
                    view.btnSubmitNewQues.alpha = 1f
                } else {
                    view.btnSubmitNewQues.isEnabled = false
                    view.btnSubmitNewQues.alpha = .5f

                }
            }
        })

        val dialog = BottomSheetDialog(context!!, R.style.CardDialogTheme)
        dialog.setContentView(view)
        dialog.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE or WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)
        dialog.show()

        view.btnSubmitNewQues.setOnClickListener {
            view.edittextNewQuestion.text?.let {
                showSubmitDialog(it.toString(), quesPrice, dialog)
            }
        }
    }

    private fun showSubmitDialog(txtQuestion: String, price: String, bottomDialog: BottomSheetDialog) {
        val builder = AlertDialog.Builder(context!!)
        builder.setTitle("Are you sure?")
        builder.setMessage("You want to ask zain a question?")

        val positiveText = "OK"
        builder.setPositiveButton(positiveText) { dialog, which ->
            dialog.dismiss()
//            Thread(Runnable {
            // do the thing that takes a long time
//                runOnUiThread {
            postNewQuestion(txtQuestion, price, bottomDialog)
//                }
//            }).start()
        }

        val negativeText = "Cancel"
        builder.setNegativeButton(negativeText) { dialog, which ->
            // negative button logic
            dialog.dismiss()
        }

        val dialog = builder.create()
        dialog.setOnShowListener {
            dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(ContextCompat.getColor(context!!, R.color.colorSelklikMain))
            dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(context!!, R.color.colorSelklikMain))
        }

        // display dialog
        dialog.show()
    }

    private fun postNewQuestion(txtQuestion: String, price: String, bottomDialog: BottomSheetDialog) {
        showProgressDialog()

        //Toast.makeText(context, price, Toast.LENGTH_SHORT).show()

        if (!ConnectivityReceiver.isConnected()) {
            if (dialog!!.isShowing) {
                dialog!!.dismiss()
            }
            Log.d("Add Card", "No internet connection")

        } else {
            val jsonParams = HashMap<String, String>()
            jsonParams.put("access_token", AppController.instance.getUserAccessToken())
            jsonParams.put("question", txtQuestion)
            jsonParams.put("price", price)

            context?.let {
                ApiHelper(it).makeRequest(
                        Config.API_ARTIST + "/" + Config.ARTIST_ID + "/messages",
                        Request.Method.POST,
                        jsonParams,
                        false,
                        false,
                        onSuccess = { responseString: String, gson: Gson ->

                           val user = gson.fromJson(AppController.instance.getUserData(), User::class.java)
                            user.current_balance = (user.current_balance - Integer.parseInt(price.trim()))
                            SharedPrefHelper().setSharedPreferenceString(context!!, Config.PREF_USER_DATA, gson.toJson(user))

                            if (dialog!!.isShowing) {
                                dialog!!.dismiss()

                            }
                            if (responseString == "[]") {
                                context?.let {
                                    Toast.makeText(it, "Fail to ask question", Toast.LENGTH_SHORT).show()
                                }
                            } else {
                                bottomDialog?.let {
                                    if (it.isShowing){
                                        it.dismiss()
                                    }
                                }
                                startActivity(Intent(context, QnaActivity::class.java))
                            }

                        },
                        onError = { error: VolleyError ->
                            context?.let {
                                ApiHelper(context = it).errorHandler(error)
                                if (dialog!!.isShowing) {
                                    dialog!!.dismiss()
                                }
                            }
                        })
            }
        }
    }

    private fun fetchMessageThread() {
        showProgressDialog()
        if (!ConnectivityReceiver.isConnected()) {
            if (dialog!!.isShowing) {
                dialog!!.dismiss()
            }
            Log.d("Add Card", "No internet connection")
        } else {
            val jsonParams = HashMap<String, String>()
            jsonParams.put("access_token", AppController.instance.getUserAccessToken())

            ApiHelper(context!!).makeRequest(
                    Config.API_ARTIST + "/" + Config.ARTIST_ID + "/messages",
                    Request.Method.GET,
                    jsonParams,
                    false,
                    false,
                    onSuccess = { responseString: String, gson: Gson ->
                        Log.d("QnA Fragment", "response string : " + responseString)
                        if (dialog!!.isShowing) {
                            dialog!!.dismiss()
                        }
                        if (responseString != "[]") {
//                            var messageArrayList: ArrayList<Message> = gson.fromJson(responseString, genericType<ArrayList<Message>>())
//                            Log.d("QnA Fragment", "message size : " + messageArrayList.size)
//                            button_ask_ussy.setOnClickListener {

                                startActivity(Intent(context, QnaActivity::class.java))


//                            }
                        } else {
//                            button_ask_ussy.setOnClickListener {
//                                startActivityForResult(Intent(context, AddNewQuestionActivity::class.java), REQUEST_NEW_QUESTION)
//                            val newQuesModel = NewQuestionBottomSheetModal.newInstance(context!!)
//                            val fm: FragmentManager = activity!!.supportFragmentManager
//                            newQuesModel.show(fm, newQuesModel.tag)
                            showBottomSheetDialog()
//                            }
                        }

                    },
                    onError = { error: VolleyError ->
                        ApiHelper(context!!).errorHandler(error)
                        if (dialog!!.isShowing) {
                            dialog!!.dismiss()
                        }
                    })
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            REQUEST_NEW_QUESTION -> if (resultCode == Activity.RESULT_OK) {
                Log.d("QnaFragment", "Result ok")
                button_ask_ussy.setOnClickListener {
                    startActivity(Intent(context, QnaActivity::class.java))
                }
                startActivity(Intent(context, QnaActivity::class.java))
            } else {
                Log.d("QnaFragment", "Result cancel")
            }
        }
    }

    private fun showProgressDialog() {
        val mBuilder = AlertDialog.Builder(context!!, R.style.CustomDialogTheme)
        val mView = LayoutInflater.from(context!!).inflate(R.layout.dialog_progress, null)

        mBuilder.setView(mView)
        mBuilder.setCancelable(false)
        dialog = mBuilder.create()
        dialog!!.show()
    }
}
