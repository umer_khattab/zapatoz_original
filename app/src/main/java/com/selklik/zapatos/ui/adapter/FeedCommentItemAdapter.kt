package com.selklik.zapatos.ui.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.android.volley.toolbox.ImageLoader
import com.google.gson.Gson
import com.google.gson.JsonElement
import com.selklik.zapatos.R
import com.selklik.zapatos.model.CommentDetails
import com.selklik.zapatos.utils.AppController
import com.selklik.zapatos.utils.TypefaceHelper
import kotlinx.android.synthetic.main.item_list_comment.view.*

/**
 * Created by faridhadiselklik on 20/12/2017.
 */
class FeedCommentItemAdapter(private var mContext: Context, private var commentArrayList: ArrayList<CommentDetails>) : RecyclerView.Adapter<FeedCommentItemAdapter.ViewHolder>()  {


    override fun getItemCount(): Int {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        return commentArrayList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        holder.bindItems(commentArrayList[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        val itemView = LayoutInflater.from(parent!!.context).inflate(R.layout.item_list_comment, parent, false)
        return ViewHolder(itemView)
    }

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        lateinit var imageLoader: ImageLoader

        fun bindItems(commentDetails: CommentDetails){
            var time = AppController().setPrettyDateFormat(commentDetails.timestamp)

            imageLoader = AppController.instance.getImageLoader()

            var photoJsonElement: JsonElement = Gson().toJsonTree(commentDetails.from.profile_photos)

            Log.d("Comment adapter", "photo json : " + photoJsonElement)
            if (photoJsonElement.isJsonNull){
                itemView.img_profile.setImageUrl(commentDetails.from.profile_photo, imageLoader)
            } else {
                itemView.img_profile.setImageUrl(commentDetails.from.profile_photos.thumbnail!!, imageLoader)
            }

            itemView.txt_name.typeface = TypefaceHelper().setTypeface("bold")
            itemView.txt_name.text = commentDetails.from.name
            itemView.txt_body.typeface = TypefaceHelper().setTypeface("regular")
            itemView.txt_body.text = commentDetails.body
            itemView.txt_time.typeface = TypefaceHelper().setTypeface("light")
            itemView.txt_time.text = time!!

        }

    }
}