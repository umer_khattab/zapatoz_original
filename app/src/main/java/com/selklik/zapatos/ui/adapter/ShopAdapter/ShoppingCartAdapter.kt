package com.selklik.zapatos.ui.adapter.ShopAdapter

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.GradientDrawable
import android.os.Handler
import android.support.v4.content.res.ResourcesCompat
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.selklik.zapatos.R
import com.selklik.zapatos.ui.CardAddPaymentActivity
import com.selklik.zapatos.utils.AppController
import com.selklik.zapatos.utils.cartdb.Cartdb
import kotlinx.android.synthetic.main.raw_cart_info.view.*
import kotlinx.android.synthetic.main.raw_shoppingcart.view.*
import com.selklik.zapatos.ui.ManageAddressesActivity
import com.selklik.zapatos.utils.Config
import com.selklik.zapatos.utils.LocalMutators
import com.selklik.zapatos.utils.cartdb.CartModel


class ShoppingCartAdapter(context : Context, rv_shopping : RecyclerView, activity: Activity, arrayCartItems: ArrayList<CartModel>) :
      RecyclerView.Adapter<ShoppingCartAdapter.ViewHolder>() {

    private var context : Context
    private var totalAmount : Double = 0.0
    private var rv_shopping : RecyclerView
    private var cardNumber = LocalMutators.cardNumber
    private var activity: Activity

    private var CARD_REQUEST_CODE = 1
    private var ADDRESS_REQUEST_CODE = 2

    private var arrayCartItems : ArrayList<CartModel>
    private var adapter : Adapter ?= null

    init {

        this.arrayCartItems =  arrayCartItems

        this.context = context
        this.rv_shopping = rv_shopping
        this.activity = activity

        adapter = this.adapter

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        if (position == (itemCount - 1))
        {
            holder.bindCarts(position, totalAmount)

            val bottom = rv_shopping.getAdapter().getItemCount() - 1
            rv_shopping.smoothScrollToPosition(bottom)

            totalAmount = 0.00

        } else {

            holder.bindItems(arrayCartItems[position].producT_NAME,
                             arrayCartItems[position].producT_PRICE,
                             arrayCartItems[position].producT_IMG,
                             arrayCartItems[position].producT_QUANTITY,
                             arrayCartItems[position].producT_SIZE,
                             position)
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder{

        var view : View

        if (viewType == (itemCount - 1))
        {
            view = LayoutInflater.from(context).inflate(R.layout.raw_cart_info, parent, false)

            if (Config.payMethod.trim().equals("billplz")) {

                view.via_payment_tv.setText("via Local Banks")
            }

            else if (!cardNumber.equals("null")) {

                if (Config.payMethod.trim().equals("stripe"))
                    view.via_payment_tv.setText("via .." + cardNumber.substring(cardNumber.length - 4))
                else
                    Toast.makeText(context, "Select Payment method first", Toast.LENGTH_SHORT).show()

            } else {

                view.via_payment_tv.setText("via card")
            }

            //getDefaultAddress(view)

        } else {

            view = LayoutInflater.from(context).inflate(R.layout.raw_shoppingcart, parent, false)

        }

        return ViewHolder(view)

    }

    override fun getItemCount(): Int {

        return (arrayCartItems.size + 1)
    }



    inner class ViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView) {

        private fun createSize(size_tv_block : LinearLayout, sizeLength : String)
        {
            var params = LinearLayout.LayoutParams(60, 60)
            params.gravity = LinearLayout.TEXT_ALIGNMENT_CENTER
            params.setMargins(6, 3, 3, 3)

            var params_txt = RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
            params_txt.addRule(RelativeLayout.CENTER_IN_PARENT)

            var sizeLength = sizeLength.trim().split(", ")


            for (i in 0 until sizeLength.size){

                var relativeLayout = RelativeLayout(context)
                relativeLayout.layoutParams = params

                var textView = TextView(context)
                textView.setText(sizeLength.get(i))
                textView.setTextSize(13f)
                textView.layoutParams = params_txt

                textView.setTextColor( ResourcesCompat.getColor(context.resources, R.color.colorZopatozGold, null))
                textView.setTypeface(null, Typeface.BOLD)

                relativeLayout.addView(textView)
                relativeLayout.id = i

                val shape = GradientDrawable()
                shape.shape = GradientDrawable.OVAL
                shape.setColor(Color.BLACK)
                shape.setStroke(3, ResourcesCompat.getColor(context.resources, R.color.colorZopatoz_showcase, null))

                relativeLayout.setBackgroundDrawable(shape)

                size_tv_block.addView(relativeLayout)

            }


        }

        fun bindItems(productName : String,
                      productPrice : String,
                      productImage : String,
                      productQnty : Int,
                      productSize : String,
                      position: Int) {

                      itemView.product_iv.setImageUrl(productImage, AppController().getImageLoader())
                      itemView.name_tv.setText(productName)

                      createSize(itemView.size_tv_block, productSize)

                      itemView.qnty_tv.setText(productQnty.toString())
                      itemView.price_tv.setText(Config.myCurrencyUnit+ String.format("%.2f", (productPrice.toDouble() * productQnty.toString().trim().toInt())))

                      itemView.item_delete.setOnClickListener({

                            Cartdb(context).deleteCart(arrayCartItems[position].producT_ID)

                            arrayCartItems.remove(arrayCartItems.get(position))

                            itemView.animate().alpha(0.0f).setDuration(500).start()

                            if (arrayCartItems.size == 0) activity.finish()

                            Handler().postDelayed(Runnable { rv_shopping.adapter = this@ShoppingCartAdapter }, 500)

                        })

                        itemView.minus_btn.setOnClickListener( {

                            if (itemView.qnty_tv.text.toString().trim().toInt() > 1)
                            {
                                itemView.qnty_tv.text  = (itemView.qnty_tv.text.toString().trim().toInt() - 1).toString()
                                itemView.price_tv.text = Config.myCurrencyUnit + (itemView.price_tv.text.toString().substring(3).trim().toDouble()
                                                         - productPrice.toDouble())
                            }
                        })

                        itemView.plus_btn.setOnClickListener({

                            itemView.qnty_tv.text     = (itemView.qnty_tv.text.toString().trim().toInt() + 1).toString()
                            itemView.price_tv.text    = Config.myCurrencyUnit + (itemView.price_tv.text.toString().substring(3).trim().toDouble()
                                                      + productPrice.toDouble())
                        })


            itemView.quantity_tv.setText("Quantity: "+productQnty)

            totalAmount = totalAmount + (productPrice.trim().toDouble() * productQnty)

            LocalMutators.totalPayment = totalAmount

        }

        fun bindCarts(position : Int, totalAmount : Double)
        {

            if (Config.myCountry.equals("my"))
            {
                itemView.via_payment_tv.setText("Via Local Banks")
                itemView.paymethod_btn.setOnClickListener(null)

            } else {

                itemView.paymethod_btn.setOnClickListener( {

                    var cardPay = Intent(context, CardAddPaymentActivity::class.java)
                    activity.startActivityForResult(cardPay, CARD_REQUEST_CODE)

                })
            }

            itemView.address_btn.setOnClickListener( {

                var address = Intent(context, ManageAddressesActivity::class.java)
                activity.startActivityForResult(address, ADDRESS_REQUEST_CODE)

            })

            itemView.total_tv.setText(Config.myCurrencyUnit+ String.format("%.2f", totalAmount))
        }

    }

    override fun getItemId(position: Int): Long {
        return super.getItemId(position)
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

}