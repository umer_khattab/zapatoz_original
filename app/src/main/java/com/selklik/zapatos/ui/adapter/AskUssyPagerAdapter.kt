package com.selklik.zapatos.ui.adapter

import android.view.View
import android.content.Context
import android.support.v4.content.ContextCompat
import android.support.v4.view.PagerAdapter
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.RelativeLayout
import com.selklik.zapatos.R
import com.selklik.zapatos.model.AskPagerData
import com.selklik.zapatos.utils.TypefaceHelper
import kotlinx.android.synthetic.main.pager_ask_ussy.view.*

/**
 * Created by faridhadiselklik on 13/12/2017.
 */
class AskUssyPagerAdapter(var context: Context, var askPagerArrayList: ArrayList<AskPagerData>): PagerAdapter() {
    override fun isViewFromObject(view: View, `object`: Any): Boolean {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.

        return view === `object` as RelativeLayout
    }

    private val layoutInflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

//    override fun isViewFromObject(view: View, `object`: Any?): Boolean {
////        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
//
//    }

    override fun getCount(): Int {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        return askPagerArrayList.size
    }
    override fun instantiateItem(container: ViewGroup, position: Int): Any {

        val itemView = layoutInflater.inflate(R.layout.pager_ask_ussy, container, false)
        itemView.text_ask_pager.typeface = TypefaceHelper().setTypeface("light")
        itemView.text_ask_pager.text = askPagerArrayList[position].details
        itemView.img_pager_ask.setImageDrawable(ContextCompat.getDrawable(context, askPagerArrayList[position].image))

        container!!.addView(itemView)
        return itemView
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as RelativeLayout)
    }
}