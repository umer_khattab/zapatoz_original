package com.selklik.zapatos.ui

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.text.InputType
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.VolleyError
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.login.LoginResult
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.selklik.zapatos.MainActivity
import com.selklik.zapatos.R
import com.selklik.zapatos.model.User
import com.selklik.zapatos.utils.*
import com.selklik.zapatos.utils.volley.ApiHelper

import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.layout_change_name.view.*
import kotlinx.android.synthetic.main.layout_dialog_default.view.*
import java.util.*
import java.util.regex.Pattern

class LoginActivity : AppCompatActivity() {
    private var TAG = LoginActivity::class.java.simpleName
    private var dialog: AlertDialog? = null

    private lateinit var callbackManager: CallbackManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        setContentView(R.layout.activity_login)
        ic_close_login.setOnClickListener {
            onBackPressed()
        }

        btn_sign_up_email.setOnClickListener {
            if (loginValidation(edittext_email.text.toString(), edittext_username.text.toString())) {
                Log.d(TAG, "login validation : " + java.lang.Boolean.toString(
                        loginValidation(edittext_email.text.toString(), edittext_username.text.toString())))

                imm.hideSoftInputFromWindow(edittext_username.windowToken, 0)
                postLogin(edittext_email.text.toString(), edittext_username.text.toString())
            }
        }

        btn_login_fb.setOnClickListener {
            fb_login_button.performClick()
        }

        text_forget_password.typeface = TypefaceHelper().setTypeface("regular")
        text_forget_password.setOnClickListener {
            showForgetPassword()
        }

        text_selklik_login_title.typeface = TypefaceHelper().setTypeface("light")

        fb_login_button.setReadPermissions(Arrays.asList("public_profile, email"))
        callbackManager = CallbackManager.Factory.create()

        fb_login_button.registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
            override fun onSuccess(loginResult: LoginResult) {
                if (loginResult.accessToken.token != null) {

                    Toast.makeText(this@LoginActivity, "Success facebook login", Toast.LENGTH_SHORT).show()
                    //                    AccessToken.getCurrentAccessToken().getToken();
                    Log.d(TAG, "token : " + loginResult.accessToken.token + ", user id : " + loginResult.accessToken.userId)
                    postFbLogin(loginResult.accessToken.token)
                }
            }

            override fun onCancel() {
                Toast.makeText(this@LoginActivity, "Cancel facebook sign up", Toast.LENGTH_SHORT).show()
            }

            override fun onError(error: FacebookException) {
                Toast.makeText(this@LoginActivity, "Something wrong :" + error.toString(), Toast.LENGTH_SHORT).show()
            }
        })



    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        callbackManager!!.onActivityResult(requestCode, resultCode, data)
    }

    private fun postFbLogin(fbToken: String) {

        showProgressDialog()
        if (!ConnectivityReceiver.isConnected()) {
            if (dialog!!.isShowing) {
                dialog!!.dismiss()
            }
            Snackbar.make(this.window.decorView.rootView, "No internet connection", Snackbar.LENGTH_SHORT).show()
        } else {
            val jsonParams = HashMap<String, String>()
            jsonParams.put("facebook_access_token", fbToken!!)

            var apiHelper = ApiHelper(this)
            apiHelper.makeRequest(
                    Config.API_FB_LOGIN,
                    Request.Method.POST,
                    jsonParams,
                    false,
                    false,
                    onSuccess = { responseString: String, gson: Gson ->
                        Log.d("Login Activity", "response string : " + responseString)

                        var user = gson.fromJson(responseString, User::class.java)
                        SharedPrefHelper().setSharedPreferenceString(this, Config.PREF_USER_DATA, responseString)
                        SharedPrefHelper().setSharedPreferenceString(this, Config.PREF_ACCESS_TOKEN, user!!.access_token)
                        SharedPrefHelper().setSharedPreferenceBoolean(Config.PREF_KEEP_LOGIN, true)

                        AppController.instance.setUserIdnotification(this)
                        Log.d(TAG, "access token : " + SharedPrefHelper().getSharedPreferenceString(this, Config.PREF_ACCESS_TOKEN, ""))

                        startMainActivity()

                        //fetchAboutUssy(user)
                    },
                    onError = { error: VolleyError ->
                        apiHelper.errorHandler(error)
                        if (dialog!!.isShowing) {
                            dialog!!.dismiss()
                        }
                    })

        }

    }

    private fun postLogin(email: String, password: String) {

        showProgressDialog()
        if (!ConnectivityReceiver.isConnected()) {
            if (dialog!!.isShowing) {
                dialog!!.dismiss()
            }
            Snackbar.make(this.window.decorView.rootView, "No internet connection", Snackbar.LENGTH_SHORT).show()
        } else {
            val jsonParams = HashMap<String, String>()
            jsonParams.put("email", email!!)
            jsonParams.put("password", password!!)

            var apiHelper = ApiHelper(this)
            apiHelper.makeRequest(
                    Config.API_LOGIN,
                    Request.Method.POST,
                    jsonParams,
                    false,
                    false,
                    onSuccess = { responseString: String, gson: Gson ->
                        Log.d("Login Activity", "response string : " + responseString)

                        var user = gson.fromJson(responseString, User::class.java)

                        SharedPrefHelper().setSharedPreferenceString(this, Config.PREF_USER_DATA, responseString)
                        SharedPrefHelper().setSharedPreferenceString(this, Config.PREF_ACCESS_TOKEN, user!!.access_token)
                        SharedPrefHelper().setSharedPreferenceBoolean(Config.PREF_KEEP_LOGIN, true)


                        AppController.instance.setUserIdnotification(this)
                        Log.d(TAG, "access token : " + SharedPrefHelper().getSharedPreferenceString(this, Config.PREF_ACCESS_TOKEN, ""))

                        startMainActivity()

                        //fetchAboutUssy(user)
                    },
                    onError = { error: VolleyError ->
                        apiHelper.errorHandler(error)
                        if (dialog!!.isShowing) {
                            dialog!!.dismiss()
                        }
                    })

        }

    }

    private fun fetchAboutUssy(user: User) {
        if (!ConnectivityReceiver.isConnected()) {
            Log.d(TAG, "No internet connection")
        } else {
            val jsonParams = HashMap<String, String>()
            jsonParams.put("access_token", user!!.access_token)

            ApiHelper(this).makeRequest(
                    Config.API_ABOUT_ARTIST,
                    Request.Method.GET,
                    jsonParams,
                    false,
                    false,
                    onSuccess = { responseString: String, gson: Gson ->
                        Log.d("Splash Screen", "response string : " + responseString)
                        if (dialog!!.isShowing) {
                            dialog!!.dismiss()
                        }
                        SharedPrefHelper().setSharedPreferenceString(this, Config.PREF_ABOUT_ARTIST, responseString)
//                        fetchUserAddress(user)
                        startMainActivity()
                    }, onError = {})
        }
    }

//    private fun fetchUserAddress(user: User) {
//        if (!ConnectivityReceiver.isConnected()) {
//            Log.d(TAG, "No internet connection")
//        } else {
//            val jsonParams = HashMap<String, String>()
//            jsonParams.put("access_token", user!!.access_token)
//
//            ApiHelper(this).makeRequest(
//                    Config.API_ME + "/addresses",
//                    Request.Method.GET,
//                    jsonParams,
//                    false,
//                    false,
//                    onSuccess = { responseString: String, gson: Gson ->
//                        Log.d("Splash Screen", "response string : " + responseString)
//
//                        if (responseString != "[]") {
//                            SharedPrefHelper().setSharedPreferenceString(this, Config.PREF_USER_ADDRESS, responseString)
//                            fetchCard()
//                        } else {
//                            fetchCard()
//                        }
//                    }, onError = {})
//        }
//    }

//    private fun fetchCard() {
//        if (!ConnectivityReceiver.isConnected()) {
//            Log.d("Add Card", "No internet connection")
//        } else {
//            val jsonParams = HashMap<String, String>()
//            jsonParams.put("access_token", AppController.instance.getUserAccessToken())
//
//            ApiHelper(this).makeRequest(
//                    Config.API_ME + "/cards",
//                    Request.Method.GET,
//                    jsonParams,
//                    false,
//                    false,
//                    onSuccess = { responseString: String, gson: Gson ->
//                        if (responseString != "[]") {
//                            Log.d("Add Card", "response string : " + responseString)
////                        if (dialog!!.isShowing) {
////                            dialog!!.dismiss()
////                        }
//                            SharedPrefHelper().setSharedPreferenceString(this, Config.PREF_USER_CARD, responseString)
//                            startMainActivity()
//
//                            Log.d("Add Card", "stored card info : "
//                                    + SharedPrefHelper().getSharedPreferenceString(this, Config.PREF_USER_CARD, ""))
//                        } else {
//                            //                            SharedPrefHelper().setSharedPreferenceString(this, Config.PREF_USER_CARD, null)
//                            startMainActivity()
//
//                        }
//                    },
//                    onError = { error: VolleyError ->
//                        ApiHelper(this).errorHandler(error)
////                        if (dialog!!.isShowing) {
////                            dialog!!.dismiss()
////                        }
//                    })
//        }
//
//    }

    private fun startMainActivity() {
        var intent = Intent(this, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
        overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out)
        finish()
    }

    private fun loginValidation(email: String?, password: String?): Boolean {
        var valid = true

        if (email!!.isEmpty() || email == null) {
            edittext_email.error = "empty email"
            valid = false
        } else {
            if (isEmailValid(email)) {
                edittext_email.error = null
                if (password!!.isEmpty() || password == "" || password == null) {
                    edittext_username.error = "empty password"
                    valid = false
                } else {
                    edittext_username.error = null
                }
            } else {
                edittext_email.error = "enter a valid email"
                valid = false
            }

        }

        return valid
    }

    private fun isEmailValid(email: String?): Boolean {
        val expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$"
        val pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE)
        val matcher = pattern.matcher(email)
        return matcher.matches()
    }

    private fun showProgressDialog() {
        val mBuilder = AlertDialog.Builder(this, R.style.CustomDialogTheme)
        val mView = LayoutInflater.from(this).inflate(R.layout.dialog_progress, null)

        mBuilder.setView(mView)
        mBuilder.setCancelable(false)
        dialog = mBuilder.create()
        dialog!!.show()
    }

    private fun showForgetPassword() {
        val mBuilder = AlertDialog.Builder(this, R.style.CustomDialogTheme)
        val dialogView: View = LayoutInflater.from(this).inflate(R.layout.layout_change_name, null)

        mBuilder.setView(dialogView)
        val dialog = mBuilder.create()
        dialog.setCanceledOnTouchOutside(true)

        dialogView.text_change_name_dialog_title.text = "ForgetPassword"
        dialogView.text_change_name_title.text = "Please enter your email"
        dialogView.edittext_name.hint = "Email"
        dialogView.edittext_name.inputType = InputType.TYPE_TEXT_VARIATION_WEB_EMAIL_ADDRESS

        dialogView.text_change_name_submit.setOnClickListener {
            if (dialogView.edittext_name.text.toString().isEmpty() || dialogView.edittext_name.text.toString() == null) {
                dialogView.edittext_name.error = "empty email"
            } else {
                if (isEmailValid(dialogView.edittext_name.text.toString())) {
                    dialogView.edittext_name.error = null
                    forgetPassword(dialogView.edittext_name.text.toString(), dialog)
                } else {
                    dialogView.edittext_name.error = "enter a valid email"
                }
            }
        }

        dialogView.text_change_name_cancel.setOnClickListener {
            dialog.dismiss()
        }

        dialog.show()
    }

    private fun forgetPassword(email: String, alertDialog: AlertDialog) {
//        Toast.makeText(this, "Email not null $email", Toast.LENGTH_SHORT).show()
        showProgressDialog()
        if (!ConnectivityReceiver.isConnected()) {
            if (dialog!!.isShowing) {
                dialog!!.dismiss()
            }
            Toast.makeText(this, "No internet connection", Toast.LENGTH_SHORT).show()
        } else {
            val jsonParams = HashMap<String, String>()
            jsonParams["email"] = email

            var apiHelper = ApiHelper(this)
            apiHelper.makeRequest(
                    module = Config.API_FORGET_PASSWORD,
                    method = Request.Method.POST,
                    params = jsonParams,
                    onSuccess = { responseString: String, gson: Gson ->
                        if (dialog!!.isShowing) {
                            dialog!!.dismiss()
                        }

                        alertDialog?.let {
                            if (it.isShowing){
                                it.dismiss()
                            }
                        }
                        Log.d("Login Activity", "response string : " + responseString)
                        val obj = JsonParser().parse(responseString)
                        (obj as JsonObject).let {
                            if(obj["email_sent"].asBoolean){
                                showSuccessEmailSent()
                            }
                        }

                    },
                    onError = { error: VolleyError ->
                        apiHelper.errorHandler(error)
                        if (dialog!!.isShowing) {
                            dialog!!.dismiss()
                        }
                    })
        }

    }

    private fun showSuccessEmailSent() {
        val mBuilder = AlertDialog.Builder(this, R.style.CustomDialogTheme)
        val dialogView: View = LayoutInflater.from(this).inflate(R.layout.layout_dialog_default, null)

        mBuilder.setView(dialogView)
        val dialog = mBuilder.create()
        dialog.setCanceledOnTouchOutside(true)

        dialogView.text_dialog_title.typeface = TypefaceHelper().setTypeface("bold")
        dialogView.text_dialog_title.text = "Email sent"
        dialogView.text_dialog_body.typeface = TypefaceHelper().setTypeface("regular")
        dialogView.text_dialog_body.text = "Please check your email."

        dialogView.text_dialog_cancel.typeface = TypefaceHelper().setTypeface("semi-bold")
        dialogView.btn_dismiss.setOnClickListener {
            dialog.dismiss()
        }

        dialog.show()
    }
}
