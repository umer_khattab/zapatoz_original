package com.selklik.zapatos.ui.fragment.MainStoreCategoryFragments

import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.android.volley.Request
import com.google.gson.Gson
import com.selklik.zapatos.R
import com.selklik.zapatos.model.shopitems.ProductsShoes
import com.selklik.zapatos.ui.adapter.ShopAdapter.MainStoreCategoryAdapter
import com.selklik.zapatos.ui.adapter.ShopAdapter.MainStoreShowCaseAdapter
import com.selklik.zapatos.utils.Config
import com.selklik.zapatos.utils.volley.ApiHelper
import kotlinx.android.synthetic.main.fragment_mainstore.view.*
import kotlinx.android.synthetic.main.fragment_storemen.view.*

class MenStoreFragment : Fragment()
{

     override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {


        var view = inflater.inflate(R.layout.fragment_storemen, container, false)


         view!!.swipe_container.setOnRefreshListener {

             Handler().postDelayed(Runnable {

                 initApiRequest("Men", view!!)

                 if (view!!.swipe_container.isRefreshing)
                     view!!.swipe_container.isRefreshing = false


             }, 1500)

         }


         initApiRequest("Men", view!!)


         return view

    }


    private fun initApiRequest(category:String, view : View )
    {
        var params = HashMap<String,String>()
        params.put("category", category)


        var gson = Gson()
        var results = ProductsShoes(ArrayList())

        view.gridview.adapter = MainStoreShowCaseAdapter(context!!, results!!, "", true)

        ApiHelper(context!!).makeZopatozRequest(Config.ShopeMen_Url,
                params,
                Request.Method.GET,
                false,
                true,
                onSuccess = { responseString: String, gson: Gson ->

                    var results = gson.fromJson(responseString, ProductsShoes::class.java)
                    view.gridview.adapter = MainStoreShowCaseAdapter(context!!, results, responseString, false)

                }, onError = {

        })

    }

}