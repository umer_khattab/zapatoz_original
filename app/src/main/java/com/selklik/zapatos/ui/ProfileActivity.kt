package com.selklik.zapatos.ui

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.support.design.widget.BottomSheetDialog
import android.support.design.widget.Snackbar
import android.support.v4.content.FileProvider
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.widget.LinearLayout
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.VolleyError
import com.android.volley.toolbox.ImageLoader
import com.google.gson.Gson
import com.google.gson.JsonParser
import com.google.gson.reflect.TypeToken
import com.selklik.zapatos.BuildConfig
import com.selklik.zapatos.R
import com.selklik.zapatos.model.User
import com.selklik.zapatos.utils.*
import com.selklik.zapatos.utils.Config.Companion.PREF_USER_DATA
import com.selklik.zapatos.utils.volley.ApiHelper

import kotlinx.android.synthetic.main.activity_profile.*
import kotlinx.android.synthetic.main.content_profile.*
import kotlinx.android.synthetic.main.layout_change_name.view.*
import kotlinx.android.synthetic.main.layout_change_password.view.*
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

class ProfileActivity : AppCompatActivity() {
    var TAG = ProfileActivity::class.java.simpleName!!
    private var userData: String? = null
    private var imageLoader: ImageLoader? = null
    private var progressDialog: AlertDialog? = null
    private var mFlag: String? = null
    private var bitmap: Bitmap? = null
    private val RESULT_FROM_CAMERA = 1001
    private val RESULT_LOAD_IMG = 1002

    private val JPEG_FILE_PREFIX = "IMG_"
    private val JPEG_FILE_SUFFIX = ".jpg"
    private var mCurrentPhotoPath: String? = null
    private var byteArray: ByteArray? = null

    private val PERMISSIONS_REQUEST_CODE = 1
    private var currentUser: User? = null

    private var mPermissions: AndroidPermissions? = null
//    private var mLayoutManager: RecyclerView.LayoutManager? = null

    private inline fun <reified T> genericType() = object : TypeToken<T>() {}.type!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)
        setSupportActionBar(toolbar)
        val ab = supportActionBar
        ab!!.setHomeButtonEnabled(true)
        ab.setDisplayHomeAsUpEnabled(true)
        ab.setDisplayShowTitleEnabled(false)
        var gson = Gson()
        imageLoader = AppController.instance.getImageLoader()

        txt_toolbar_title.typeface = TypefaceHelper().setTypeface("regular")
        mPermissions = AndroidPermissions(this,
                Manifest.permission.CAMERA,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)

//        mLayoutManager = LinearLayoutManager(this)
//        card_recycler_view.layoutManager = mLayoutManager

        userData = SharedPrefHelper().getSharedPreferenceString(this, PREF_USER_DATA, "")
        Log.d(TAG, "user data : " + userData)
//        text_profile_add_payment_method.typeface = TypefaceHelper().setTypeface("regular")
        currentUser = gson.fromJson(userData, User::class.java)
        parseUserData(gson.fromJson(userData, User::class.java))

    }

    private fun parseUserData(user: User) {
        img_user_profile_picture.setImageUrl(user.profile_photos.thumbnail, imageLoader)

        text_profile_name_title.typeface = TypefaceHelper().setTypeface("regular")
        text_profile_pasword_title.typeface = TypefaceHelper().setTypeface("regular")
        layout_edit_name.setOnClickListener {
            showAddAddressDialog(user)
        }
        text_user_name.typeface = TypefaceHelper().setTypeface("regular")
        text_user_name.text = user.name
        txt_password.typeface = TypefaceHelper().setTypeface("regular")
        ic_pic_edit.setOnClickListener{
            onCameraIconClick()
        }

        ic_edit_password.setOnClickListener {
            showUpdatePasswordDialog(user)
        }
//        txt_user_current_balance.text = user.current_balance.toInt().toString()

//        if (AppController.instance.getUserCardData() != null){
//            layout_no_payment_method.visibility = View.GONE
//            var cardArrayList: ArrayList<Card> = Gson().fromJson(AppController.instance.getUserCardData(), genericType<ArrayList<Card>>())
//            Log.d(TAG, "card size : " + cardArrayList.size)
//            card_recycler_view.visibility = View.VISIBLE
//            var feedItemAdapter = CardItemAdapter(this!!, cardArrayList!!)
//            card_recycler_view.adapter = feedItemAdapter
//            card_recycler_view.isNestedScrollingEnabled = false
//            text_profile_add_payment_method.setOnClickListener {
//                startActivity(Intent(this, AddPaymentMethodActivity::class.java))
//            }
//        } else {
//            layout_no_payment_method.visibility = View.VISIBLE
//            card_recycler_view.visibility = View.GONE
//            txt_no_payment_method.typeface = TypefaceHelper().setTypeface("regular")
//            txt_no_payment_method.setOnClickListener{
//                startActivity(Intent(this, AddPaymentMethodActivity::class.java))
//            }
//        }
//
//        fetchCard()

    }

    private fun updateName(newName: String, dialog: AlertDialog) {
        showProgressDialog()
        if (!ConnectivityReceiver.isConnected()) {
            if (progressDialog!!.isShowing) {
                progressDialog!!.dismiss()
            }
            Toast.makeText(this, "No internet connection.", Toast.LENGTH_SHORT).show()
            Log.d("UpdateName", "No internet connection")
        } else {
            val jsonParams = HashMap<String, String>()
            jsonParams.put("access_token", AppController.instance.getUserAccessToken())
            jsonParams.put("name", newName)

            ApiHelper(this).makeRequest(
                    Config.API_ME,
                    Request.Method.PUT,
                    jsonParams,
                    false,
                    false,
                    onSuccess = { responseString: String, gson: Gson ->
                        Log.d("Add Card", "response string : " + responseString)
                        SharedPrefHelper().setSharedPreferenceString(this, Config.PREF_USER_DATA, responseString)
                        parseUserData(gson.fromJson(responseString, User::class.java))
                        if (progressDialog!!.isShowing) {
                            progressDialog!!.dismiss()
                        }

                        dialog.dismiss()

                    },
                    onError = { error: VolleyError ->
                        ApiHelper(this).errorHandler(error)
                        if (progressDialog!!.isShowing) {
                            progressDialog!!.dismiss()
                        }
                    })
        }

    }

    private fun updatePassword(currentPassword: String,newPassword: String, dialog: AlertDialog) {
        showProgressDialog()
        if (!ConnectivityReceiver.isConnected()) {
            if (progressDialog!!.isShowing) {
                progressDialog!!.dismiss()
            }
            Toast.makeText(this, "No internet connection.", Toast.LENGTH_SHORT).show()
            Log.d("UpdateName", "No internet connection")
        } else {
            val jsonParams = HashMap<String, String>()
            jsonParams.put("access_token", AppController.instance.getUserAccessToken())
            jsonParams.put("current_password", currentPassword)
            jsonParams.put("new_password", newPassword)

            ApiHelper(this).makeRequest(
                    "${Config.API_ME}/password",
                    Request.Method.PUT,
                    jsonParams,
                    false,
                    false,
                    onSuccess = { responseString: String, gson: Gson ->
                        Log.d("Add Card", "response string : " + responseString)
//                        SharedPrefHelper().setSharedPreferenceString(this, Config.PREF_USER_DATA, responseString)
//                        parseUserData(gson.fromJson(responseString, User::class.java))
                        if (progressDialog!!.isShowing) {
                            progressDialog!!.dismiss()
                        }

                        dialog.dismiss()

                    },
                    onError = { error: VolleyError ->
                        ApiHelper(this).errorHandler(error)
                        if (progressDialog!!.isShowing) {
                            progressDialog!!.dismiss()
                        }
                    })
        }

    }

    private fun showProgressDialog() {
        val mBuilder = AlertDialog.Builder(this, R.style.CustomDialogTheme)
        val mView = LayoutInflater.from(this).inflate(R.layout.dialog_progress, null)

        mBuilder.setView(mView)
        mBuilder.setCancelable(false)
        progressDialog = mBuilder.create()
        progressDialog!!.show()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        when (id) {
            android.R.id.home -> onBackPressed()
            else -> return false
        }
        return true
    }

    fun showAddAddressDialog(user: User) {

        val mBuilder = AlertDialog.Builder(this, R.style.CardDialogTheme)

        val nameView: View = LayoutInflater.from(this).inflate(R.layout.layout_change_name, null)
        nameView.text_change_name_dialog_title.typeface = TypefaceHelper().setTypeface("bold")
        nameView.text_change_name_title.typeface = TypefaceHelper().setTypeface("regular")
        nameView.text_change_name_submit.typeface = TypefaceHelper().setTypeface("bold")
        nameView.text_change_name_cancel.typeface = TypefaceHelper().setTypeface("bold")

        nameView.edittext_name.typeface = TypefaceHelper().setTypeface("regular")
        nameView.edittext_name.hint = user.name

        mBuilder.setView(nameView)
        val dialog = mBuilder.create()
        dialog.setCanceledOnTouchOutside(true)
//        Log.d(TAG, "preview url : " + previewUrl)

        nameView.text_change_name_submit.setOnClickListener {
            val username = nameView.edittext_name.text.toString()
            if (username.isEmpty() || username == null) {
                nameView.edittext_name.error = "empty name"

            } else {
                updateName(username, dialog)
            }
//            dialog.dismiss()
        }

        nameView.text_change_name_cancel.setOnClickListener {
            dialog.dismiss()
        }

        dialog.show()
    }

    fun showUpdatePasswordDialog(user: User) {

        val mBuilder = AlertDialog.Builder(this, R.style.CardDialogTheme)

        val passwordView: View = LayoutInflater.from(this).inflate(R.layout.layout_change_password, null)
        passwordView.text_change_password_dialog_title.typeface = TypefaceHelper().setTypeface("bold")
        passwordView.text_change_password_title.typeface = TypefaceHelper().setTypeface("regular")
        passwordView.text_change_password_submit.typeface = TypefaceHelper().setTypeface("bold")
        passwordView.text_change_password_cancel.typeface = TypefaceHelper().setTypeface("bold")

        passwordView.edittext_new_passwrod.typeface = TypefaceHelper().setTypeface("regular")
        passwordView.edittext_current_passwrod.typeface = TypefaceHelper().setTypeface("regular")

        mBuilder.setView(passwordView)
        val dialog = mBuilder.create()
        dialog.setCanceledOnTouchOutside(true)
//        Log.d(TAG, "preview url : " + previewUrl)

        passwordView.text_change_password_submit.setOnClickListener {
            val currentPassword = passwordView.edittext_current_passwrod.text.toString()
            val newPassword = passwordView.edittext_new_passwrod.text.toString()
            if (currentPassword.isEmpty() || currentPassword == null) {
                passwordView.edittext_current_passwrod.error = "empty name"

            } else {
                if (newPassword.isEmpty() || newPassword == null) {
                    passwordView.edittext_new_passwrod.error = "empty name"

                } else {
                    updatePassword(currentPassword, newPassword, dialog)
                }
            }
//            dialog.dismiss()
        }

        passwordView.text_change_password_cancel.setOnClickListener {
            dialog.dismiss()
        }

        dialog.show()
    }

    fun onCameraIconClick() {
        val bottomSheetDialog = BottomSheetDialog(this)
        val view1 = LayoutInflater.from(this).inflate(R.layout.layout_bottom_sheet_picture, null)

        bottomSheetDialog.setContentView(view1)
        val layoutPhoto: LinearLayout
        val layoutGallery: LinearLayout
        val layoutCancel: LinearLayout

        layoutPhoto = view1.findViewById(R.id.add_complaint_bottom_sheet_camera)
        layoutPhoto.setOnClickListener {
            if (!isIntentAvailable(this@ProfileActivity, MediaStore.ACTION_IMAGE_CAPTURE)) {
                Toast.makeText(this@ProfileActivity, "failed open camera", Toast.LENGTH_SHORT).show()
            } else {
                checkPermissions("camera")
            }
            bottomSheetDialog.dismiss()
        }
        layoutGallery = view1.findViewById(R.id.add_complaint_bottom_sheet_gallery)
        layoutGallery.setOnClickListener {
            checkPermissions("gallery")
            bottomSheetDialog.dismiss()
        }

        layoutCancel = view1.findViewById(R.id.add_complaint_bottom_sheet_cancel)
        layoutCancel.setOnClickListener {
            checkPermissions("gallery")
            bottomSheetDialog.dismiss()
        }

        bottomSheetDialog.show()
    }

    private fun isIntentAvailable(context: Context, action: String): Boolean {
        val packageManager = context.packageManager
        val intent = Intent(action)
        val list = packageManager.queryIntentActivities(intent,
                PackageManager.MATCH_DEFAULT_ONLY)
        return list.size > 0
    }

    private fun checkPermissions(flag: String) {
        mFlag = flag
        if (mPermissions!!.checkPermissions()) {
            Log.d(TAG, "Permission granted")
            when (flag) {
                "camera" -> openCamera()
                "gallery" -> showFileChooser()
            }
        } else {
            Log.i(TAG, "Some needed permissions are missing. Requesting them.")
            mPermissions!!.requestPermissions(PERMISSIONS_REQUEST_CODE)
        }
    }

    private fun openCamera() {

        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        var f: File? = null

        try {
            f = setUpPhotoFile()
            mCurrentPhotoPath = f!!.absolutePath
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, FileProvider.getUriForFile(this,
                    BuildConfig.APPLICATION_ID + ".provider", f))

        } catch (e: IOException) {
            e.message
        }

        startActivityForResult(takePictureIntent, RESULT_FROM_CAMERA)
    }

    @Throws(IOException::class)
    private fun setUpPhotoFile(): File {

        val f = createImageFile()
        mCurrentPhotoPath = f.absolutePath

        return f
    }

    @Throws(IOException::class)
    private fun createImageFile(): File {
        // Create an image file name
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val imageFileName = JPEG_FILE_PREFIX + timeStamp + "_"
        val albumF = getAlbumDir()
        return File.createTempFile(imageFileName, JPEG_FILE_SUFFIX, albumF)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        try {
            // When an Image is picked
            if (requestCode == RESULT_LOAD_IMG && resultCode == Activity.RESULT_OK
                    && null != data) {
                val selectedImage = data.data
                val filePathColumn = arrayOf(MediaStore.Images.Media.DATA)

                val cursor = contentResolver.query(selectedImage!!,
                        filePathColumn, null, null, null)
                cursor!!.moveToFirst()

                val columnIndex = cursor.getColumnIndex(filePathColumn[0])
                val picturePath = cursor.getString(columnIndex)
                cursor.close()

                bitmap = BitmapFactory.decodeFile(picturePath)
                byteArray = getByteArray(bitmap!!)
                img_user_profile_picture.setImageDrawable(null)
                progress_bar.visibility = View.VISIBLE
                postProfilePicture(currentUser!!, byteArray!!)


            } else if (requestCode == RESULT_FROM_CAMERA && resultCode == Activity.RESULT_OK) {

                bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath)
                byteArray = getByteArray(bitmap!!)
                Log.d(TAG, "bitmap : " + bitmap)

                img_user_profile_picture.setImageDrawable(null)
                progress_bar.visibility = View.VISIBLE
                postProfilePicture(currentUser!!, byteArray!!)
//                setPic()

            } else {
                Toast.makeText(this, "Anda harus memilih gambar untuk dimuatnaik",
                        Toast.LENGTH_LONG).show()
            }
        } catch (e: Exception) {
            e.printStackTrace()
            Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG)
                    .show()
        }

    }

    private fun setPic() {

        /* There isn't enough memory to open up more than a couple camera photos */
        /* So pre-scale the target bitmap into which the file is decoded */

        /* Get the size of the ImageView */
        val targetW = img_user_profile_picture.getWidth()
        val targetH = img_user_profile_picture.getHeight()

        /* Get the size of the image */
        val bmOptions = BitmapFactory.Options()
        bmOptions.inJustDecodeBounds = true
        BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions)
        val photoW = bmOptions.outWidth
        val photoH = bmOptions.outHeight

        /* Figure out which way needs to be reduced less */
        var scaleFactor = 1
        if (targetW > 0 || targetH > 0) {
            scaleFactor = Math.min(photoW / targetW, photoH / targetH)
        }

        /* Set bitmap options to scale the image decode target */
        bmOptions.inJustDecodeBounds = false
        bmOptions.inSampleSize = scaleFactor
        bmOptions.inPurgeable = true

        /* Decode the JPEG file into a Bitmap */
        val bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions)

        /* Associate the Bitmap to the ImageView */
        img_user_profile_picture.setImageBitmap(bitmap)
        img_user_profile_picture.visibility = View.VISIBLE
        //        mVideoView.setVisibility(View.INVISIBLE);
    }

    private fun getByteArray(bitmap: Bitmap): ByteArray {
        val byteArrayOutputStream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 60, byteArrayOutputStream)
        return byteArrayOutputStream.toByteArray()
    }

    private fun getAlbumDir(): File? {
        var storageDir: File? = null

        if (Environment.MEDIA_MOUNTED == Environment.getExternalStorageState()) {

            //            storageDir = mAlbumStorageDirFactory.getAlbumStorageDir("NavAms");
            storageDir = File(Environment.getExternalStorageDirectory().toString() + "/dcim/" + "NavAms")

            if (storageDir != null) {
                if (!storageDir.mkdirs()) {
                    if (!storageDir.exists()) {
                        Log.d("CameraSample", "failed to create directory")
                        return null
                    }
                }
            }
        } else {
            Log.v(getString(R.string.app_name), "External storage is not mounted READ/WRITE.")
        }

        return storageDir
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (mPermissions!!.areAllRequiredPermissionsGranted(permissions, grantResults)) {
            //            myImplementation();
            //            openCamera();
            when (mFlag) {
                "camera" -> openCamera()
                "gallery" -> showFileChooser()
            }
        } else {
            //            onInsufficientPermissions();
            Snackbar.make(this.findViewById(android.R.id.content),
                    "Please Grant Permissions to use camera",
                    Snackbar.LENGTH_INDEFINITE).setAction("ENABLE"
            ) { mPermissions!!.requestPermissions(PERMISSIONS_REQUEST_CODE) }.show()
        }
    }

    private fun postProfilePicture(user: User, byteArray: ByteArray) {
        showProgressDialog()
        if (!ConnectivityReceiver.isConnected()) {
            if (progressDialog!!.isShowing) {
                progressDialog!!.dismiss()
            }
            Toast.makeText(this, "No internet connection.", Toast.LENGTH_SHORT).show()
            Log.d(TAG, "No internet connection")
        } else {
            Log.d(TAG, "post image access token : ${user.access_token}")
            val jsonParams = HashMap<String, String>()
            jsonParams.put("access_token", user.access_token)

            ApiHelper(this).makeMultipartRequest(
                    module = Config.API_PROFILE_PIC,
                    method = Request.Method.POST,
                    params = jsonParams,
                    byteArrays = byteArray!!,
                    type = "file",
                    onSuccess = { responseString: String, gson: Gson ->
                        Log.d("Splash Screen", "response string : " + responseString)
                        var user = gson.fromJson(AppController.instance.getUserData(), User::class.java)
                        var photoObj = JsonParser().parse(responseString).asJsonObject
                        user.profile_photos.full_resolution = photoObj.get("urls").asJsonObject.get("full_resolution").asString
                        user.profile_photos.thumbnail = photoObj.get("urls").asJsonObject.get("thumbnail").asString
                        user.profile_photos.square = photoObj.get("urls").asJsonObject.get("square").asString
                        if (progressDialog!!.isShowing) {
                            progressDialog!!.dismiss()
                        }
                        SharedPrefHelper().setSharedPreferenceString(this, Config.PREF_USER_DATA, gson.toJson(user))
                        progress_bar.visibility = View.GONE
                        parseUserData(user)
                    },
                    onError = { error: VolleyError ->
                        ApiHelper(this).errorHandler(error)
                        if (progressDialog!!.isShowing) {
                            progressDialog!!.dismiss()
                        }
                    })
        }
    }

    private fun showFileChooser() {
        val galleryIntent = Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        startActivityForResult(galleryIntent, RESULT_LOAD_IMG)
    }
}
