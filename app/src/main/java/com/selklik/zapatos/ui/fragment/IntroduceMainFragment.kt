package com.selklik.zapatos.ui.fragment

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import com.android.volley.Request
import com.facebook.shimmer.ShimmerFrameLayout
import com.selklik.zapatos.R
import com.selklik.zapatos.model.shopitems.Data
import com.selklik.zapatos.model.shopitems.ProductsShoes
import com.selklik.zapatos.ui.BuyItemShowcaseActivity
import com.selklik.zapatos.ui.adapter.ProductsViewPagerAdapter
import com.selklik.zapatos.ui.adapter.SliderAdapters.MainSliderAdapter
import com.selklik.zapatos.utils.PicassoImageLoadingService
import kotlinx.android.synthetic.main.fragment_home_intro.view.*
import ss.com.bannerslider.Slider
import com.selklik.zapatos.ui.adapter.ShopAdapter.ShopIntroduceMainAdapter
import com.selklik.zapatos.utils.AppController
import com.selklik.zapatos.utils.Config
import com.selklik.zapatos.utils.LocalMutators
import com.selklik.zapatos.utils.volley.ApiHelper
import kotlinx.android.synthetic.main.fragment_home_intro.*
import org.json.JSONArray
import org.json.JSONObject

class IntroduceMainFragment : Fragment()
{

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        var view = inflater.inflate(R.layout.fragment_home_intro, container, false)

        Slider.init(PicassoImageLoadingService(context!!))

        (view.cl_ph1 as ShimmerFrameLayout).startShimmerAnimation()
        (view.cl_ph2 as ShimmerFrameLayout).startShimmerAnimation()
        (view.cl_ph3 as ShimmerFrameLayout).startShimmerAnimation()
        (view.cl_ph4 as ShimmerFrameLayout).startShimmerAnimation()

        view.swipe.setOnRefreshListener {


            Handler().postDelayed({

                initSlides(view)
                //initBestSeller(view)
                initOnSale(view)
                //initKKSLayout(view)

                view.swipe.isRefreshing = false

            }, 1500)

        }


        initSlides(view)
        initBestSeller(view)
        initOnSale(view)
        initKKSLayout(view)

        saveUserInfoToExternalServer()

        return view

    }

    private fun saveUserInfoToExternalServer() {

        var json = JSONObject()
        json.put("name",  LocalMutators.fullName)
        json.put("email", LocalMutators.email)

        var hashmap = HashMap<String,String>()

        ApiHelper(context!!).makeZopatozRequest(Config.Base_Url+"?custinfo="+json.toString()
                                        ,hashmap ,Request.Method.POST, false, false,
                                           onSuccess = {responseString, gson ->
                                        }, onError = {error ->

        })

    }

    private fun initSlides(view : View) {


        val arrayOfBanners = ArrayList<String>()

        var params = HashMap<String, String>()

        ApiHelper(context!!).makeZopatozRequest (Config.Banner_Url,
                params,
                Request.Method.GET,
                false,
                true,
                onSuccess = {responseString, gson ->

                    var jsonObject           = JSONObject(responseString)
                    var jsonArray = jsonObject.get("data") as JSONArray


                    //Log.d("Hello", jsonArray.toString())

                    for (i in 0 until jsonArray.length())
                    {

                        arrayOfBanners.add(JSONObject(jsonArray[i].toString()).get("image_url").toString())

                    }

                    view.slider.setAdapter(MainSliderAdapter(arrayOfBanners))
                    view.slider.setInterval(2000)

                },

                onError = {error ->


                })


    }

    private fun initBestSeller(view : View) {

        var params = HashMap<String, String>()

        ApiHelper(context!!).makeZopatozRequest(Config.BestSeller_Url, params, Request.Method.GET,
                false,
                true,
                 onSuccess = { responseString, gson ->

                     var pendulumAnim = AnimationUtils.loadAnimation(context, R.anim.swing_pendulum)
                     view.tag_iv.startAnimation(pendulumAnim)

                     var products = gson.fromJson(responseString, ProductsShoes::class.java)
                     

                     Log.d("HelloOc", products.data.size.toString())


                     if (products.data.size <= 0)
                         bestselling_container.visibility = View.GONE
                     else
                         if (bestselling_container.visibility == View.GONE)
                             bestselling_container.visibility = View.VISIBLE


                     if (products.data.size >= 1)
                     {
                         view.introMin_iv1.setImageUrl(products.data[0].products[0].prod_img, AppController().getImageLoader())
                         var anim1 = AnimationUtils.loadAnimation(context, R.anim.right_to_left)
                         view.constraintLayout.startAnimation(anim1)
                         view.introMin_tv1_desc.setText(products.data[0].prod_name)

                         if (Config.myCountry.equals("my"))
                         {
                             view.introMin_tv1_price.setText(Config.myCurrencyUnit + products.data[0].price)
                         }

                         else {

                             view.introMin_tv1_price.setText(Config.myCurrencyUnit + products.data[0].int_price)
                         }

                         (cl_ph1 as ShimmerFrameLayout).stopShimmerAnimation()
                         (cl_ph1 as ShimmerFrameLayout).visibility = View.GONE


                         ll_1.visibility = View.VISIBLE


                     }else {

                         bestselling_container.visibility = View.GONE
                     }

                     if (products.data.size >= 2)
                     {
                         view.introMin_iv2.setImageUrl(products.data[1].products[0].prod_img, AppController().getImageLoader())
                         var anim2 = AnimationUtils.loadAnimation(context, R.anim.item_3)
                         view.constraintLayout2.startAnimation(anim2)
                         view.introMin_tv2_desc.setText(products.data[1].prod_name)


                         if (Config.myCountry.equals("my"))
                         {
                             view.introMin_tv2_price.setText(Config.myCurrencyUnit + products.data[1].price)
                         }

                         else {

                             view.introMin_tv2_price.setText(Config.myCurrencyUnit + products.data[1].int_price)
                         }

                         (cl_ph2 as ShimmerFrameLayout).stopShimmerAnimation()
                         (cl_ph2 as ShimmerFrameLayout).visibility = View.GONE

                         ll_2.visibility = View.VISIBLE

                     } else {

                         view.constraintLayout2.visibility = View.INVISIBLE

                     }

                     if (products.data.size >= 3)
                     {
                         view.introMin_iv3.setImageUrl(products.data[2].products[0].prod_img, AppController().getImageLoader())
                         var anim3 = AnimationUtils.loadAnimation(context, R.anim.bottom_to_up)
                         view.constraintLayout3.startAnimation(anim3)
                         view.introMin_tv3_desc.setText(products.data[2].prod_name)


                         if (Config.myCountry.equals("my"))
                         {
                             view.introMin_tv3_price.setText(Config.myCurrencyUnit + products.data[2].price)

                         }

                         else {

                             view.introMin_tv3_price.setText(Config.myCurrencyUnit + products.data[2].int_price)

                         }

                         (cl_ph3 as ShimmerFrameLayout).stopShimmerAnimation()
                         (cl_ph3 as ShimmerFrameLayout).visibility = View.GONE

                         ll_3.visibility = View.VISIBLE


                     }  else {

                         view.constraintLayout3.visibility = View.INVISIBLE

                     }

                     if (products.data.size >= 4)
                     {
                         view.introMin_iv4.setImageUrl(products.data[3].products[0].prod_img, AppController().getImageLoader())
                         var anim4 = AnimationUtils.loadAnimation(context, R.anim.bottom_to_up)
                         view.constraintLayout4.startAnimation(anim4)
                         view.introMin_tv4_desc.setText(products.data[3].prod_name)

                         if (Config.myCountry.equals("my"))
                         {
                             view.introMin_tv4_price.setText(Config.myCurrencyUnit + products.data[3].price)
                         }

                         else {

                             view.introMin_tv4_price.setText(Config.myCurrencyUnit + products.data[3].int_price)
                         }

                         (cl_ph4 as ShimmerFrameLayout).stopShimmerAnimation()
                         (cl_ph4 as ShimmerFrameLayout).visibility = View.GONE

                         ll_4.visibility = View.VISIBLE


                     }  else {

                         view.constraintLayout4.visibility = View.INVISIBLE

                     }


                     view.introMin_iv1.setOnClickListener(View.OnClickListener {

                            var intent = Intent(activity, BuyItemShowcaseActivity::class.java)
                                intent.putExtra("position", 0)
                                intent.putExtra("responseString", responseString)

                            startActivity(intent)

                     })
                     view.introMin_iv2.setOnClickListener(View.OnClickListener {

                            var intent = Intent(activity, BuyItemShowcaseActivity::class.java)
                            intent.putExtra("position", 1)
                            intent.putExtra("responseString", responseString)

                            startActivity(intent)
                     })
                     view.introMin_iv3.setOnClickListener(View.OnClickListener {

                         var intent = Intent(activity, BuyItemShowcaseActivity::class.java)
                         intent.putExtra("position", 2)
                         intent.putExtra("responseString", responseString)

                         startActivity(intent)

                     })
                     view.introMin_iv4.setOnClickListener(View.OnClickListener {

                         var intent = Intent(activity, BuyItemShowcaseActivity::class.java)
                         intent.putExtra("position", 3)
                         intent.putExtra("responseString", responseString)

                         startActivity(intent)
                     })



                 }, onError = {})

        var animation = AnimationUtils.loadAnimation(context, R.anim.left_to_right)
        view!!.style_block.startAnimation(animation)

        view.alpha_block_rl.startAnimation(animation)
    }

    private fun initOnSale(view : View) {
        var params = HashMap<String, String>()
        //params.put("category", "Men")

        ApiHelper(context!!).makeZopatozRequest (Config.PromotionSeller_Url,
                params,
                Request.Method.GET,
                false,
                true,
                onSuccess = {responseString, gson ->

                    var product = gson.fromJson(responseString, ProductsShoes::class.java)


                    var newArray = ArrayList<Data>()
                        newArray.add(product.data[0])

                    for (i in 0 until  product.data.size) {
                        newArray.add(product.data[i]) }


                    val anim = AnimationUtils.loadAnimation(context, R.anim.swing_pendulum)
                    view.viewpager.adapter = ProductsViewPagerAdapter(context!!,
                                                                      newArray,
                                                                      view.viewpager,
                                                                      view.alpha_block_rl,
                                                                      view.from_price_tv,
                                                                      responseString)

                    view.swing_iv.startAnimation(anim)

                },

                onError = {error ->


                })

    }

    private fun initKKSLayout(view : View){

        var params = HashMap<String, String>()
        params.put("category", "Men")

        ApiHelper(context!!).makeZopatozRequest (Config.ShopNewArrival,
                                                 params,
                                                 Request.Method.GET,
                                                false,
                                                true,
                onSuccess = {responseString, gson ->

//                    var product = gson.fromJson(responseString, ProductsShoes::class.java)
//
//                    /* SINGLE ITEM KKD STYLE  -- */
//                    view.introduceMain_Rv.setLayoutManager(LinearLayoutManager(context!!));
//                    view.introduceMain_Rv.setNestedScrollingEnabled(false);
//                    view.introduceMain_Rv.adapter = ShopIntroduceMainAdapter(context!!,
//                            activity!!, product!!, responseString)

                },

                onError = {error ->


                })

        }

}
