package com.selklik.zapatos.ui

import android.app.Activity
import android.content.Intent
import android.content.res.ColorStateList
import android.os.Bundle
import com.selklik.zapatos.R
import android.graphics.drawable.GradientDrawable
import android.support.v4.content.res.ResourcesCompat
import android.graphics.Color
import android.graphics.Typeface
import android.os.Build
import android.os.Handler
import android.support.v4.view.ViewPager
import android.view.View
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.view.WindowManager
import android.view.animation.AnimationUtils
import android.widget.*
import com.google.gson.Gson
import com.selklik.zapatos.model.shopitems.ProductsShoes
import com.selklik.zapatos.ui.adapter.ShopAdapter.ColorsShowCaseAdapter
import com.selklik.zapatos.utils.Config
import com.selklik.zapatos.utils.cartdb.Cartdb
import kotlinx.android.synthetic.main.activity_buyitemshowcase.*
import org.json.JSONObject
import android.widget.Toast
import com.github.tbouron.shakedetector.library.ShakeDetector
import android.R.attr.y
import android.R.attr.x
import android.app.AlertDialog
import android.content.DialogInterface
import android.graphics.Point
import android.util.Log
import android.view.Display



class BuyItemShowcaseActivity : Activity(), ViewPager.OnPageChangeListener, View.OnLongClickListener
{

    private var PRODUCT_ID    : String = ""
    private var PRODUCT_NAME  : String = ""
    private var PRODUCT_IMG   : String = ""
    private var PRODUCT_PRICE : Double = 0.0
    private var PRODUCT_COLOR : String = ""
    private var PRODUCT_SIZE : String = ""
    private var PRODUCT_QUANTITY : Int = 1

    private var item_position : Int = 0
    private var product : ProductsShoes ?= null

    private var responseString : String ?= null

    private var firstRun : Boolean = true

    override fun onPageScrollStateChanged(state: Int) {

    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

    }

    override fun onPageSelected(position: Int) {

        firstRun = true

        PRODUCT_ID    = product!!.data[item_position].prod_id + "_P"+ position.toString().trim()
        PRODUCT_COLOR = product!!.data[item_position].products[position].prod_color
        PRODUCT_IMG   = product!!.data[item_position].products[position].prod_img

        size_block.removeAllViewsInLayout()

        Handler().postDelayed(Runnable { createSizesForItems(responseString!!, position) }, 500)

        Handler().postDelayed(Runnable { animCheck.check() }, 500)

    }

    var isItemAdded : Boolean ?= false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_buyitemshowcase)

        val intent = intent

        item_position = intent.getIntExtra("position", 0 )
        responseString = intent.getStringExtra("responseString")

        var gson = Gson()
        product = gson.fromJson(responseString, ProductsShoes::class.java)

        PRODUCT_ID   = product!!.data[item_position].prod_id + "_P0"
        PRODUCT_NAME = product!!.data[item_position].prod_name
        PRODUCT_IMG  = product!!.data[item_position].products[0].prod_img



        PRODUCT_QUANTITY = 1

        name_tv.setText(product!!.data[item_position].prod_name)
        desc_tv.setText(product!!.data[item_position].description)

        createSizesForItems(responseString!!, 0)

        Handler().postDelayed(Runnable { animCheck.check() }, 500);

        back_btn.setOnClickListener(View.OnClickListener { finish() })
        viewpager.adapter = ColorsShowCaseAdapter(applicationContext, product!!.data[item_position].products)
        viewpager.addOnPageChangeListener(this)


        /* listener for carts and block */

            addcart_block.setOnClickListener(View.OnClickListener {

                buy_block.setBackgroundDrawable(null)
                addcart_block.setBackgroundDrawable(ResourcesCompat.getDrawable(resources, R.drawable.bg_buy_focused, null))

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    cart_iv.setBackgroundTintList(ColorStateList.valueOf(ResourcesCompat.getColor(resources, R.color.colorZopatozGold, null)))
                    buy_iv.setBackgroundTintList(ColorStateList.valueOf(ResourcesCompat.getColor(resources, R.color.colorZopatoz_showcase, null)))
                }

                (addcart_block.getChildAt(1) as TextView).setTextColor(ResourcesCompat.getColor(resources, R.color.colorZopatozGold,  null))
                (buy_block.getChildAt(1)     as TextView).setTextColor(ResourcesCompat.getColor(resources, R.color.colorZopatoz_showcase, null))

                addcart_block.alpha = 0f
                addcart_block.animate().alpha(1.0f).setDuration(700).start()

                textView3.alpha = 1.0f
                textView3.animate().alpha(0.0f).setDuration(1500).start()

                if (!isItemAdded!!) {

                    Cartdb(applicationContext).InsertCart(PRODUCT_ID,
                            PRODUCT_NAME,
                            PRODUCT_IMG,
                            PRODUCT_PRICE.toString(),
                            PRODUCT_COLOR,
                            PRODUCT_SIZE,
                            PRODUCT_QUANTITY)

                    isItemAdded = true

                } else {

                    val builder1 = AlertDialog.Builder(this)
                    builder1.setMessage("You have already added the item, do you want to add again?");
                    builder1.setCancelable(true);

                    builder1.setPositiveButton(
                            "Go to Cart",
                            DialogInterface.OnClickListener(function = { dialog, which ->
                                startActivity(Intent(this, ShoppingCartActivity::class.java))

                            }));


                    builder1.setNegativeButton(
                            "Yes",
                            DialogInterface.OnClickListener(function = { dialog, which ->

                                Cartdb(applicationContext).InsertCart(PRODUCT_ID,
                                        PRODUCT_NAME,
                                        PRODUCT_IMG,
                                        PRODUCT_PRICE.toString(),
                                        PRODUCT_COLOR,
                                        PRODUCT_SIZE,
                                        PRODUCT_QUANTITY)

                            }));


                    val alert11 = builder1.create()
                    alert11.show()

                }

            })

            /*buy_block.setOnClickListener(View.OnClickListener {

                addcart_block.setBackgroundDrawable(null)
                buy_block.setBackgroundDrawable(ResourcesCompat.getDrawable(resources, R.drawable.bg_buy_focused, null))


                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    buy_iv.setBackgroundTintList(ColorStateList.valueOf(ResourcesCompat.getColor(resources, R.color.colorZopatozGold, null)))
                    cart_iv.setBackgroundTintList(ColorStateList.valueOf(ResourcesCompat.getColor(resources, R.color.colorZopatoz_showcase, null)))
                };

                (buy_block.getChildAt(1) as TextView).setTextColor(ResourcesCompat.getColor(resources, R.color.colorZopatozGold,  null))
                (addcart_block.getChildAt(1)     as TextView).setTextColor(ResourcesCompat.getColor(resources, R.color.colorZopatoz_showcase, null))

                buy_block.alpha = 0f
                buy_block.animate().alpha(1.0f).setDuration(700).start()

                textView4.alpha = 1.0f
                textView4.animate().alpha(0.0f).setDuration(500).start()

            })*/

            addcart_block.setOnLongClickListener(this)

            buy_block.setOnClickListener( {

                Cartdb(applicationContext).InsertCart(PRODUCT_ID,
                        PRODUCT_NAME,
                        PRODUCT_IMG,
                        PRODUCT_PRICE.toString(),
                        PRODUCT_COLOR,
                        PRODUCT_SIZE,
                        PRODUCT_QUANTITY)

                startActivity(Intent(this, ShoppingCartActivity::class.java))

            })

        /* ------- end ------- */


    }

    override fun onResume() {
        super.onResume()

        Config.myCountry = "my"

        if (Config.myCountry.equals("my"))
        {
            PRODUCT_PRICE = product!!.data[item_position].price.trim().toDouble()
            price_tv.setText(Config.Companion.myCurrencyUnit + product!!.data[item_position].price.trim())

        } else{

            PRODUCT_PRICE = product!!.data[item_position].int_price.trim().toDouble()
            price_tv.setText(Config.Companion.myCurrencyUnit + product!!.data[item_position].int_price.trim())
        }
    }

    override fun onStop() {
        super.onStop()
    }

    override fun onLongClick(v: View?): Boolean {

        if (v!!.id == addcart_block.id)
        {
            startActivity(Intent(this, ShoppingCartActivity::class.java))

        }

        return true
    }

    private fun createSizesForItems(responseString : String, position : Int)
    {


        if (size_block.childCount > 0) size_block.removeAllViewsInLayout()


        var jsonObject =  JSONObject(responseString).getJSONArray("data").getJSONObject(item_position)

        var productObject = jsonObject.getJSONArray("products").getJSONObject(position)
        var sizeQntyArray = productObject.getJSONArray("size_quantity")

        val display = windowManager.defaultDisplay
        val size = Point()
        display.getSize(size)
        val width = size.x
        val height = size.y


         for (i in 0 until  sizeQntyArray.length())
         {
             var params = LinearLayout.LayoutParams(width/12, width/12)
             params.gravity = LinearLayout.TEXT_ALIGNMENT_CENTER
             params.setMargins(5, 5, 5, 5)


             var params_txt = RelativeLayout.LayoutParams(WRAP_CONTENT, WRAP_CONTENT)
             params_txt.addRule(RelativeLayout.CENTER_IN_PARENT)


             try {

                        var size = productObject.getJSONArray("size_quantity").getJSONObject(i)["size"].toString().trim()
                        var quantity = productObject.getJSONArray("size_quantity").getJSONObject(i)["quantity"].toString().trim()

                        var textView = TextView(this)
                            textView.setText(size)
                            textView.setTextColor(ResourcesCompat.getColor(resources, R.color.colorZopatoz_showcase, null))
                            textView.setTextSize(15f)
                            textView.layoutParams = params_txt

                        var relativeLayout = RelativeLayout(this)
                        relativeLayout.layoutParams = params


                        relativeLayout.addView(textView)
                        relativeLayout.id = i

                        val shape = GradientDrawable()
                        shape.shape = GradientDrawable.OVAL
                        shape.setColor(ResourcesCompat.getColor(resources, R.color.colorCustomDialogBackground, null))

                        size_block.addView(relativeLayout)

                        (relativeLayout.getChildAt(0) as TextView).setTextColor(Color.WHITE)
                        (relativeLayout.getChildAt(0) as TextView).setTypeface(null, Typeface.NORMAL)
                         size_block.getChildAt(i).setBackgroundDrawable(shape)

                            if (quantity.trim().toInt() == 0)
                            {
                                var textView = TextView(this)
                                textView.setText("X")
                                textView.setTextColor(ResourcesCompat.getColor(resources, R.color.colorZopatoz_red, null))
                                textView.setTextSize(15f)
                                textView.layoutParams = params_txt

                                relativeLayout.addView(textView)

                            } else {

                                relativeLayout.setOnClickListener({

                                    PRODUCT_SIZE = textView.text.toString().trim()

                                    setSelectedBg(relativeLayout)

                                    if (quantity.toInt() <= 5) {

                                        qnty_tv.visibility = View.VISIBLE
                                        qnty_tv.setText("Left " + quantity)

                                    } else {

                                        qnty_tv.visibility = View.GONE
                                    }

                                    qnty_tv.alpha = 0.0f
                                    qnty_tv.animate().alpha(1.0f).setDuration(500).start()

                                })


                                if (firstRun)
                                    relativeLayout.performClick()

                                firstRun = false

                            }

                        relativeLayout.startAnimation(AnimationUtils.loadAnimation(applicationContext, R.anim.left_bounce_anim))


                    }catch (e : org.json.JSONException) {
                        e.printStackTrace() }



         }

    }

    private fun setSelectedBg(relativeLayout: RelativeLayout)
    {
        for (i in 0..(size_block.childCount - 1))
        {

            if (relativeLayout.id == size_block.getChildAt(i).id)
            {
                val shape = GradientDrawable()
                shape.shape = GradientDrawable.OVAL
                shape.setColor(Color.BLACK)
                shape.setStroke(3, ResourcesCompat.getColor(resources, R.color.colorZopatoz_showcase, null))

                ((size_block.getChildAt(i) as RelativeLayout).getChildAt(0) as TextView).setTextColor( ResourcesCompat.getColor(resources, R.color.colorZopatozGold, null))
                ((size_block.getChildAt(i) as RelativeLayout).getChildAt(0) as TextView).setTypeface(null, Typeface.BOLD)

                size_block.getChildAt(i).setBackgroundDrawable(shape)

            } else {

                val shape = GradientDrawable()
                shape.shape = GradientDrawable.OVAL
                shape.setColor(ResourcesCompat.getColor(resources, R.color.colorCustomDialogBackground, null))
               // shape.setStroke(3, ResourcesCompat.getColor(resources, R.color.colorZopatoz_showcase, null))

                ((size_block.getChildAt(i) as RelativeLayout).getChildAt(0) as TextView).setTextColor(Color.WHITE)
                ((size_block.getChildAt(i) as RelativeLayout).getChildAt(0) as TextView).setTypeface(null, Typeface.NORMAL)
                  size_block.getChildAt(i).setBackgroundDrawable(shape)
            }
        }
    }

}