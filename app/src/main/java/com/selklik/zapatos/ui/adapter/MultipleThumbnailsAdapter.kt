package com.selklik.zapatos.ui.adapter

import android.content.Context
import android.content.Intent
import android.support.v4.view.PagerAdapter
import android.text.Layout
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.Toast
import com.android.volley.toolbox.ImageLoader
import com.google.gson.Gson
import com.google.gson.JsonArray
import com.selklik.zapatos.R
import com.selklik.zapatos.model.Feed
import com.selklik.zapatos.ui.ViewImageActivity
import com.selklik.zapatos.utils.AppController
import kotlinx.android.synthetic.main.raw_multiplethumbnails.view.*


/**
 * Created by Owais on 4/b/2018.
 */

class MultipleThumbnailsAdapter : PagerAdapter
{
    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object` as LinearLayout
    }

    var context : Context
    var jsonPhoto : JsonArray
    var feed : Feed

    constructor(context: Context, jsonPhoto : JsonArray, feed : Feed)
    {
        this.context = context;
        this.jsonPhoto = jsonPhoto;
        this.feed = feed



    }

    override fun getCount(): Int {
        return  jsonPhoto.size()
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {

        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view : View =  inflater.inflate(R.layout.raw_multiplethumbnails, null)
        var imageLoader: ImageLoader = AppController().getImageLoader()

        view.multipleImage.setImageUrl(jsonPhoto[position].asJsonObject.get("versions").
                asJsonObject.get("full_resolution").asString, imageLoader)
        view.setOnClickListener(View.OnClickListener {

            val intent = Intent(context, ViewImageActivity::class.java)
            intent.putExtra("feedData", Gson().toJson(feed))
            intent.putExtra("multipleImage", true)
            intent.putExtra("currentImage" , jsonPhoto[position].asJsonObject.get("versions").
                    asJsonObject.get("full_resolution").asString)

            context.startActivity(intent)

        })

        container.addView(view)

        return view

    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container!!.removeView(`object` as LinearLayout)
    }
}