package com.selklik.zapatos.ui

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.VolleyError
import com.google.gson.Gson
import com.selklik.zapatos.R
import com.selklik.zapatos.model.Feed
import com.selklik.zapatos.ui.adapter.FeedItemAdapter
import com.selklik.zapatos.utils.AppController
import com.selklik.zapatos.utils.Config
import com.selklik.zapatos.utils.ConnectivityReceiver
import com.selklik.zapatos.utils.TypefaceHelper
import com.selklik.zapatos.utils.volley.ApiHelper
import kotlinx.android.synthetic.main.activity_view_post.*
import kotlinx.android.synthetic.main.content_view_post.*

class ViewPostActivity : AppCompatActivity() {
    private var mLayoutManager: RecyclerView.LayoutManager? = null
    var postData: String? = null
    var gson = Gson()
    var feed: Feed? = null
    private var dialog: AlertDialog? = null
    var postId: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_post)
        setSupportActionBar(toolbar)
        var intent = intent

        val ab = supportActionBar!!
        ab!!.setHomeButtonEnabled(true)
        ab!!.setDisplayHomeAsUpEnabled(true)
        ab!!.setDisplayShowTitleEnabled(false)

        txt_toolbar_title.typeface = TypefaceHelper().setTypeface("regular")
        postData = intent.getStringExtra("postData")


        mLayoutManager = LinearLayoutManager(this)
        feed_recycler_view.layoutManager = mLayoutManager
        feed_recycler_view.visibility = View.VISIBLE

        postData?.let {
            feed = gson.fromJson(postData, Feed::class.java)
            var feedList = ArrayList<Feed>()
            feedList.add(feed!!)
//        feedItemAdapter = FeedItemAdapter(context!!, feedArrayList!!)
            feed_recycler_view.adapter = FeedItemAdapter(this!!, feedList)
        }

        postId = intent.getStringExtra("postId")
        postId?.let {
            Log.d("ViewSinglepost", "post id : $postId")
            fetchPostById(postId!!)
        }


    }

    private fun fetchPostById(postId: String){
        showProgressDialog()
        if (!ConnectivityReceiver.isConnected()) {
            Log.d("PostById", "No internet connection")
        } else {
            val jsonParams = HashMap<String, String>()
            jsonParams.put("access_token", AppController.instance.getUserAccessToken())
//            jsonParams.put("post_id", postId)

            ApiHelper(this!!).makeRequest(
                    Config.API_ARTIST+"/${Config.ARTIST_ID}/posts/$postId",
                    Request.Method.GET,
                    jsonParams,
                    false,
                    false,
                    onSuccess = { responseString: String, gson: Gson ->
                        Log.d("PostById", "response string : " + responseString)
                        if (responseString != null) {
                            feed = gson.fromJson(responseString, Feed::class.java)
                            var feedList = ArrayList<Feed>()
                            feedList.add(feed!!)
                            feed_recycler_view.adapter = FeedItemAdapter(this!!, feedList)
                        } else {
                            Toast.makeText(this, "no post", Toast.LENGTH_SHORT).show()
                        }
                        if (dialog!!.isShowing) {
                            dialog!!.dismiss()
                        }
//
                    },
                    onError = { error: VolleyError ->

                        if (dialog!!.isShowing) {
                            dialog!!.dismiss()
                        }
                    })
        }

    }

    private fun showProgressDialog() {
        val mBuilder = AlertDialog.Builder(this!!, R.style.CustomDialogTheme)
        val mView = LayoutInflater.from(this).inflate(R.layout.dialog_progress, null)

        mBuilder.setView(mView)
        mBuilder.setCancelable(false)
        dialog = mBuilder.create()
        dialog!!.show()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        when (id) {
            android.R.id.home -> onBackPressed()
            else -> return false
        }
        return super.onOptionsItemSelected(item)
    }

}
