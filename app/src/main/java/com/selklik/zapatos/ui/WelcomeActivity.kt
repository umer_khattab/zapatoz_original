package com.selklik.zapatos.ui

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.content.res.ResourcesCompat
import android.support.v7.app.AlertDialog
import android.text.InputType
import android.text.SpannableString
import android.text.Spanned
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
import android.view.View.SYSTEM_UI_FLAG_LAYOUT_STABLE
import android.view.WindowManager
import com.selklik.zapatos.R
import kotlinx.android.synthetic.main.activity_welcome.*
import kotlinx.android.synthetic.main.layout_dialog_default.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.VolleyError
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.login.LoginResult
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.selklik.zapatos.MainActivity
import com.selklik.zapatos.model.User
import com.selklik.zapatos.utils.*
import com.selklik.zapatos.utils.volley.ApiHelper
import jp.wasabeef.blurry.Blurry
import kotlinx.android.synthetic.main.layout_change_name.view.*
import java.util.*
import java.util.regex.Pattern


class WelcomeActivity : AppCompatActivity() {

    private lateinit var callbackManager: CallbackManager
    private var dialog: AlertDialog? = null
    private var TAG = LoginActivity::class.java.simpleName

    private val textWelcome = "By creating an account, you agree to Selklik’s Terms of Use and Privacy Policy."
    private val textTerm = "Terms of Use"
    private val textPrivacy = "Privacy Policy"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (Build.VERSION.SDK_INT < 16) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } else {
            window.addFlags(SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN)
            window.addFlags(SYSTEM_UI_FLAG_LAYOUT_STABLE)
        }

        setContentView(R.layout.activity_welcome)

        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager


        imageView.post(Runnable {
            Blurry.with(this)
                    .radius(25)
                    .sampling(2)
                    .animate(500)
                    .onto(rootBlur)
        })

        btn_login_fb.setOnClickListener {
            fb_login_button.performClick()
        }

        fb_login_button.setReadPermissions(Arrays.asList("public_profile, email"))
        callbackManager = CallbackManager.Factory.create()

        fb_login_button.registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
            override fun onSuccess(loginResult: LoginResult) {
                if (loginResult.accessToken.token != null) {

                   // Toast.makeText(applicationContext, "Success facebook login", Toast.LENGTH_SHORT).show()
                    //                    AccessToken.getCurrentAccessToken().getToken();
                    //Log.d(TAG, "token : " + loginResult.accessToken.token + ", user id : " + loginResult.accessToken.userId)
                    postFbLogin(loginResult.accessToken.token)
                }
            }

            override fun onCancel() {
                Toast.makeText(applicationContext, "Cancel facebook sign up", Toast.LENGTH_SHORT).show()
            }

            override fun onError(error: FacebookException) {
                Toast.makeText(applicationContext, "Something wrong :" + error.toString(), Toast.LENGTH_SHORT).show()
            }
        })

        text_forget_password.setOnClickListener(View.OnClickListener {
            showForgetPassword()
        })

        register_btn.setOnClickListener(View.OnClickListener {

            startMainActivity2()
        })

        btn_sign_up_email.setOnClickListener {
            if (loginValidation(edittext_email.text.toString(), edittext_password.text.toString())) {
                Log.d(TAG, "login validation : " + java.lang.Boolean.toString(loginValidation(edittext_email.text.toString(), edittext_password.text.toString())))

                imm.hideSoftInputFromWindow(edittext_password.windowToken, 0)
                postLogin(edittext_email.text.toString(), edittext_password.text.toString())
            }

            //startActivity(Intent(this, MainActivity::class.java))
        }

        text_continue_selklik_btn.typeface = TypefaceHelper().setTypeface("regular")
        btn_continue_selklik.setOnClickListener {

            startActivity(Intent(this, LoginActivity::class.java))
        }

        btn_why_selklik.setOnClickListener {

            startActivity(Intent(this, SignUpActivity::class.java))
            //showWhyNeedSelklikDialog()
        }

        text_why_selklik_btn.typeface = TypefaceHelper().setTypeface("regular")
        text_welcome_term_privacy.typeface = TypefaceHelper().setTypeface("light")

        val ss = SpannableString(textWelcome)
        val clickableSpan = object : ClickableSpan() {
            override fun onClick(textView: View) {
                startActivity(Intent(this@WelcomeActivity, BrowserActivity::class.java).putExtra("url", Config.tosUrl).putExtra("from", "Term of Service"))
            }

            override fun updateDrawState(ds: TextPaint) {
                ds.linkColor = ResourcesCompat.getColor(resources, R.color.colorZopatozGold, null)
                super.updateDrawState(ds)
                ds.isUnderlineText = true
            }
        }
        val clickableSpan1 = object : ClickableSpan() {
            override fun onClick(textView: View) {
                startActivity(Intent(this@WelcomeActivity, BrowserActivity::class.java).putExtra("url", Config.privacyUrl).putExtra("from", "Privacy Policy"))
            }

            override fun updateDrawState(ds: TextPaint) {
                ds.linkColor = ResourcesCompat.getColor(resources, R.color.colorZopatozGold, null)
                super.updateDrawState(ds)
                ds.isUnderlineText = true
            }
        }
        ss.setSpan(clickableSpan, textWelcome.indexOf(textTerm), (textWelcome.indexOf(textTerm)+(textTerm.length)), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        ss.setSpan(clickableSpan1, textWelcome.indexOf(textPrivacy), (textWelcome.indexOf(textPrivacy)+(textPrivacy.length)), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)

//        val textView = findViewById(R.id.hello) as TextView
        text_welcome_term_privacy.text = ss
        text_welcome_term_privacy.movementMethod = LinkMovementMethod.getInstance()
        text_welcome_term_privacy.highlightColor = Color.TRANSPARENT

    }

    private fun showWhyNeedSelklikDialog() {
        val mBuilder = AlertDialog.Builder(this, R.style.CustomDialogTheme)
        val dialogView: View = LayoutInflater.from(this).inflate(R.layout.layout_dialog_default, null)

        mBuilder.setView(dialogView)
        val dialog = mBuilder.create()
        dialog.setCanceledOnTouchOutside(true)

        dialogView.text_dialog_title.typeface = TypefaceHelper().setTypeface("bold")
        dialogView.text_dialog_title.text = "Why do I need a Selklik account?"
        dialogView.text_dialog_body.typeface = TypefaceHelper().setTypeface("regular")
        dialogView.text_dialog_body.text = "Laudya Cynthia Bella Official App is powered by Selklik, the one-stop platform that connects fans with celebrities.\n\nYour Selklik account can be used across all Selklik-powered apps."

        dialogView.text_dialog_cancel.typeface = TypefaceHelper().setTypeface("semi-bold")
        dialogView.btn_dismiss.setOnClickListener {
            dialog.dismiss()
        }

        dialog.show()
    }

    private fun loginValidation(email: String?, password: String?): Boolean {
        var valid = true

        if (email!!.isEmpty() || email == null) {
            edittext_email.error = "empty email"
            valid = false
        } else {
            if (isEmailValid(email)) {
                edittext_email.error = null
                if (password!!.isEmpty() || password == "" || password == null) {
                    edittext_password.error = "empty password"
                    valid = false
                } else {
                    edittext_password.error = null
                }
            } else {
                edittext_email.error = "enter a valid email"
                valid = false
            }

        }

        return valid
    }

    private fun isEmailValid(email: String?): Boolean {
        val expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$"
        val pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE)
        val matcher = pattern.matcher(email)
        return matcher.matches()
    }

    private fun postFbLogin(fbToken: String) {

        showProgressDialog()
        if (!ConnectivityReceiver.isConnected()) {
            if (dialog!!.isShowing) {
                dialog!!.dismiss()
            }
            Snackbar.make(this.window.decorView.rootView, "No internet connection", Snackbar.LENGTH_SHORT).show()
        } else {
            val jsonParams = HashMap<String, String>()
            jsonParams.put("facebook_access_token", fbToken!!)


            Log.d("HelloFB", fbToken!!.toString())

            var apiHelper = ApiHelper(this)
            apiHelper.makeRequest(
                    Config.API_FB_LOGIN,
                    Request.Method.POST,
                    jsonParams,
                    false,
                    false,
                    onSuccess = { responseString: String, gson: Gson ->
                        Log.d("Login Activity", "response string : " + responseString)

                        var user = gson.fromJson(responseString, User::class.java)

                        LocalMutators.fullName = user!!.name
                        LocalMutators.email = user!!.email

                        SharedPrefHelper().setSharedPreferenceString(this, Config.PREF_USER_DATA, responseString)
                        SharedPrefHelper().setSharedPreferenceString(this, Config.PREF_ACCESS_TOKEN, user!!.access_token)
                        SharedPrefHelper().setSharedPreferenceBoolean(Config.PREF_KEEP_LOGIN, true)

                        AppController.instance.setUserIdnotification(this)
                        Log.d(TAG, "access token : " + SharedPrefHelper().getSharedPreferenceString(this, Config.PREF_ACCESS_TOKEN, ""))

                        startMainActivity()

                        //fetchAboutUssy(user)
                    },
                    onError = { error: VolleyError ->
                        apiHelper.errorHandler(error)
                        if (dialog!!.isShowing) {
                            dialog!!.dismiss()
                        }
                    })

        }

    }

    private fun postLogin(email: String, password: String) {

        showProgressDialog()
        if (!ConnectivityReceiver.isConnected()) {
            if (dialog!!.isShowing) {
                dialog!!.dismiss()
            }
            Snackbar.make(this.window.decorView.rootView, "No internet connection", Snackbar.LENGTH_SHORT).show()
        } else {
            val jsonParams = HashMap<String, String>()
            jsonParams.put("email", email!!)
            jsonParams.put("password", password!!)

            var apiHelper = ApiHelper(this)
            apiHelper.makeRequest(
                    Config.API_LOGIN,
                    Request.Method.POST,
                    jsonParams,
                    false,
                    false,
                    onSuccess = { responseString: String, gson: Gson ->
                        Log.d("Login Activity", "response string : " + responseString)

                        var user = gson.fromJson(responseString, User::class.java)

                        LocalMutators.fullName = user!!.name
                        LocalMutators.email = user!!.email

                        SharedPrefHelper().setSharedPreferenceString(this, Config.PREF_USER_DATA, responseString)
                        SharedPrefHelper().setSharedPreferenceString(this, Config.PREF_ACCESS_TOKEN, user!!.access_token)
                        SharedPrefHelper().setSharedPreferenceBoolean(Config.PREF_KEEP_LOGIN, true)


                        AppController.instance.setUserIdnotification(this)
                        Log.d(TAG, "access token : " + SharedPrefHelper().getSharedPreferenceString(this, Config.PREF_ACCESS_TOKEN, ""))

                        startMainActivity()

                        //fetchAboutUssy(user)
                    },
                    onError = { error: VolleyError ->
                        apiHelper.errorHandler(error)
                        if (dialog!!.isShowing) {
                            dialog!!.dismiss()
                        }
                    })

        }

    }

    private fun showProgressDialog() {
        val mBuilder = AlertDialog.Builder(this, R.style.CustomDialogTheme)
        val mView = LayoutInflater.from(this).inflate(R.layout.dialog_progress, null)
        mBuilder.setView(mView)
        mBuilder.setCancelable(false)
        dialog = mBuilder.create()
        dialog!!.show()
    }

    private fun startMainActivity() {
        var intent = Intent(this, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
        overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out)
        finish()
    }

    private fun startMainActivity2() {
        var intent = Intent(this, SignUpActivity::class.java)
        //intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        //intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
        overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out)
        //finish()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        callbackManager!!.onActivityResult(requestCode, resultCode, data)
    }

    private fun showForgetPassword() {
        val mBuilder = AlertDialog.Builder(this, R.style.CustomDialogTheme)
        val dialogView: View = LayoutInflater.from(this).inflate(R.layout.layout_change_name, null)

        mBuilder.setView(dialogView)
        val dialog = mBuilder.create()
        dialog.setCanceledOnTouchOutside(true)

        dialogView.text_change_name_dialog_title.text = "ForgetPassword"
        dialogView.text_change_name_title.text = "Please enter your email"
        dialogView.edittext_name.hint = "Email"
        dialogView.edittext_name.inputType = InputType.TYPE_TEXT_VARIATION_WEB_EMAIL_ADDRESS

        dialogView.text_change_name_submit.setOnClickListener {
            if (dialogView.edittext_name.text.toString().isEmpty() || dialogView.edittext_name.text.toString() == null) {
                dialogView.edittext_name.error = "empty email"
            } else {
                if (isEmailValid(dialogView.edittext_name.text.toString())) {
                    dialogView.edittext_name.error = null
                    forgetPassword(dialogView.edittext_name.text.toString(), dialog)
                } else {
                    dialogView.edittext_name.error = "enter a valid email"
                }
            }
        }

        dialogView.text_change_name_cancel.setOnClickListener {
            dialog.dismiss()
        }

        dialog.show()
    }

    private fun forgetPassword(email: String, alertDialog: AlertDialog) {
//        Toast.makeText(this, "Email not null $email", Toast.LENGTH_SHORT).show()
        showProgressDialog()
        if (!ConnectivityReceiver.isConnected()) {
            if (dialog!!.isShowing) {
                dialog!!.dismiss()
            }
            Toast.makeText(this, "No internet connection", Toast.LENGTH_SHORT).show()
        } else {
            val jsonParams = HashMap<String, String>()
            jsonParams["email"] = email

            var apiHelper = ApiHelper(this)
            apiHelper.makeRequest(
                    module = Config.API_FORGET_PASSWORD,
                    method = Request.Method.POST,
                    params = jsonParams,
                    onSuccess = { responseString: String, gson: Gson ->
                        if (dialog!!.isShowing) {
                            dialog!!.dismiss()
                        }

                        alertDialog?.let {
                            if (it.isShowing){
                                it.dismiss()
                            }
                        }
                        Log.d("Login Activity", "response string : " + responseString)
                        val obj = JsonParser().parse(responseString)
                        (obj as JsonObject).let {
                            if(obj["email_sent"].asBoolean){
                                showSuccessEmailSent()
                            }
                        }

                    },
                    onError = { error: VolleyError ->
                        apiHelper.errorHandler(error)
                        if (dialog!!.isShowing) {
                            dialog!!.dismiss()
                        }
                    })
        }


    }

    private fun showSuccessEmailSent() {
        val mBuilder = AlertDialog.Builder(this, R.style.CustomDialogTheme)
        val dialogView: View = LayoutInflater.from(this).inflate(R.layout.layout_dialog_default, null)

        mBuilder.setView(dialogView)
        val dialog = mBuilder.create()
        dialog.setCanceledOnTouchOutside(true)

        dialogView.text_dialog_title.typeface = TypefaceHelper().setTypeface("bold")
        dialogView.text_dialog_title.text = "Email sent"
        dialogView.text_dialog_body.typeface = TypefaceHelper().setTypeface("regular")
        dialogView.text_dialog_body.text = "Please check your email."

        dialogView.text_dialog_cancel.typeface = TypefaceHelper().setTypeface("semi-bold")
        dialogView.btn_dismiss.setOnClickListener {
            dialog.dismiss()
        }

        dialog.show()
    }


}
