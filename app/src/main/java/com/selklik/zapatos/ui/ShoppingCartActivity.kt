package com.selklik.zapatos.ui

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.Handler
import android.support.v4.content.res.ResourcesCompat
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.LinearLayout
import android.widget.Toast
import com.android.volley.*
import com.google.gson.*
import com.selklik.zapatos.R
import com.selklik.zapatos.model.Address
import com.selklik.zapatos.ui.adapter.ShopAdapter.ShoppingCartAdapter
import com.selklik.zapatos.utils.AppController
import com.selklik.zapatos.utils.Config
import com.selklik.zapatos.utils.ConnectivityReceiver
import com.selklik.zapatos.utils.LocalMutators
import com.selklik.zapatos.utils.cartdb.CartModel
import com.selklik.zapatos.utils.cartdb.Cartdb
import com.selklik.zapatos.utils.volley.ApiHelper
import com.stripe.android.Stripe
import com.stripe.android.TokenCallback
import com.stripe.android.model.Card
import com.stripe.android.model.Token
import kotlinx.android.synthetic.main.activity_shoppingcart.*
import kotlinx.android.synthetic.main.raw_cart_info.view.*
import kotlinx.android.synthetic.main.raw_dialog_payment.*
import kotlinx.android.synthetic.main.raw_dialog_transactionstatus.*
import org.json.JSONArray
import org.json.JSONObject
import java.lang.Exception
import java.util.HashMap

class ShoppingCartActivity : Activity(), TokenCallback {

    private var stripeToken : String ?= ""
    private var dialog : Dialog ?= null

    private var CARD_REQUEST_CODE = 1
    private var ADDRESS_REQUEST_CODE = 2

    private var randomAmount : Double = 0.0

    private var shippingCharges : Double = 0.0

    override fun onSuccess(token: Token?) {

        dialog!!.progress_tv.setText("Creating your order")

        Handler().postDelayed(Runnable {  dialog!!.progress_tv.setText("Processing Transaction") }, 1500)

        stripeToken = token!!.id
        createOrder(Config.payMethod)
    }

    override fun onError(error: Exception?) {
        Toast.makeText(applicationContext, error!!.message.toString(), Toast.LENGTH_LONG).show();
    }

    private fun fetchAddress() {
       // if (dialog == null) {
//            if (!dialog!!.isShowing) {
           // showProgressDialog()
//            }
        //}
        if (!ConnectivityReceiver.isConnected()) {
//            if (dialog!!.isShowing) {
//                dialog!!.dismiss()
//            }
            Log.d("Add Card", "No internet connection")

        } else {
            val jsonParams = HashMap<String, String>()
            jsonParams.put("access_token", AppController.instance.getUserAccessToken())

            ApiHelper(applicationContext).makeRequest(
                    Config.API_ME + "/addresses",
                    Request.Method.GET,
                    jsonParams,
                    false,
                    false,
                     onSuccess = { responseString: String, gson: Gson ->
                        Log.d("Add Card", "response string : " + responseString)
                        /* if (dialog!!.isShowing) {
                            dialog!!.dismiss()
                        }*/
                        val addressJsonArray = JsonParser().parse(responseString).asJsonArray
//
                        if (addressJsonArray.size() > 0) {

                            var addressArrayList: ArrayList<Address> = addressJsonArray.map { gson.fromJson(it, Address::class.java) } as ArrayList<Address>

                            Log.d("Hello", addressArrayList[0].line1)

                        } else {

                        }
                    },
                    onError = { error: VolleyError ->
                        ApiHelper(applicationContext).errorHandler(error)
                        /*if (dialog!!.isShowing) {
                            dialog!!.dismiss()
                        }*/
                    })
        }

    }

    private var arrayCartItems :  ArrayList<CartModel> ?= null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.activity_shoppingcart)

        arrayCartItems = Cartdb(applicationContext).getAllItemsFromCarts()

        if (arrayCartItems!!.size > 0)
        {
            root_shoppingcart.setBackgroundColor(Color.WHITE)
            empty_cart_parent.visibility = View.GONE
            (shoppingcart_rv.parent as LinearLayout).visibility = View.VISIBLE
            shoppingcart_rv.setLayoutManager(LinearLayoutManager(this))
            shoppingcart_rv.adapter = ShoppingCartAdapter(applicationContext,
                                                          shoppingcart_rv,
                                                         this,
                                                          arrayCartItems!!)

        } else {

            empty_cart_parent.visibility = View.VISIBLE
            root_shoppingcart.setBackgroundColor(ResourcesCompat.getColor(resources, R.color.greyColor, null))
        }

        back_btn.setOnClickListener(View.OnClickListener {

            finish()

        })

        checkout_btn.setOnClickListener(View.OnClickListener {


            if (LocalMutators.fullAddress.equals("null"))
                Toast.makeText(applicationContext, "Please select your Shipping Address", Toast.LENGTH_SHORT).show()

            else //if (Config.payMethod.equals("my"))
                makePayment()

            /*else
                if (LocalMutators.getCardNumber().equals(null))
                    Toast.makeText(applicationContext, "Please Select your Card", Toast.LENGTH_SHORT).show()
                else
                    makePayment()*/

        })

        back_btn.setOnClickListener(View.OnClickListener { finish() })

    }

    private fun makePayment() {

        createOrder(Config.payMethod)

        /*if (Config.payMethod.equals("billplz"))
        {


        } else if (Config.payMethod.equals("stripe")){

            dialog = Dialog(this)
            dialog!!.getWindow().setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog!!.setContentView(R.layout.raw_dialog_payment)
            dialog!!.setCancelable(true)
            dialog!!.show()
            dialog!!.progress_tv.setText("Validating your card")

            val card = Card("4242-4242-4242-4242", 12, 2019, "123")

            if (card.validateCard()) {

                dialog!!.progress_tv.setText("Card validated Successfully")

                var stripe = Stripe(applicationContext, "pk_test_5AhFpTSjAyj65ferBLX6W5Bu");
                    stripe.createToken(card,this)

            } else {

                dialog!!.progress_tv.setText("Card couldn't be processed")
                Handler().postDelayed(Runnable { dialog!!.dismiss() }, 2500)
            }

        } else {

            Toast.makeText(applicationContext, "Please Select your payment method", Toast.LENGTH_SHORT).show()
        }*/

    }

    private fun createOrder(payMethod : String) {

        var cartItems = Cartdb(applicationContext).getAllItemsFromCarts()

        if (cartItems.size > 0) {

            var json = JSONObject();
                json.put("cus_name",    LocalMutators.fullName)
                json.put("cus_email",   LocalMutators.email)
                json.put("cus_address", LocalMutators.fullAddress)
                json.put("cus_phone",   LocalMutators.phoneNo)

                json.put("cus_cunit", Config.myCurrencyUnit)
                json.put("cus_payMethod", "Zapatos")
                json.put("description", "Zapatoz Sales Debit")
                json.put("totalAmount", Math.round(randomAmount* 100))

            if (payMethod.equals("stripe")) json.put("stripeToken", stripeToken)

            var arrayOfProduct = JSONArray()

            for (i in 0 until cartItems.size)
            {
                var jsonProductInfo = JSONObject()
                jsonProductInfo.put("prod_id",    cartItems[i].producT_ID.substring(0,cartItems[i].producT_ID.length-3))
                jsonProductInfo.put("prod_qnty",  cartItems[i].producT_QUANTITY)
                jsonProductInfo.put("prod_color", cartItems[i].producT_COLOR)
                jsonProductInfo.put("prod_size",  cartItems[i].producT_SIZE)
                jsonProductInfo.put("prod_img",  cartItems[i].producT_IMG)
                arrayOfProduct .put(jsonProductInfo)
            }

            json.put("products", arrayOfProduct)

            if (Config.payMethod.equals("stripe"))
            {
                makeIntTransaction(json)

            } else if (Config.payMethod.equals("billplz"))
            {
                makeLocalTransaction(json)
            }
        }

        else {

            Toast.makeText(applicationContext, "Your cart is empty!", Toast.LENGTH_SHORT).show()
        }

    }

    private fun makeLocalTransaction(json : JSONObject) {

        var intent = Intent(this, BillplzPaymentActivity::class.java)
        intent.putExtra("product_info", json.toString())
        startActivity(intent)
    }

    private fun makeIntTransaction(json : JSONObject) {
        var hashMap = HashMap<String, String>()

        ApiHelper(applicationContext).makeZopatozRequest("http://shoplot.biz/Stripe/charge-api1.php?prod_info="+json.toString(),
                hashMap,
                Request.Method.POST,
                false,
                false,
                onSuccess = { responseString, gson ->

                    dialog!!.progress_tv.setText("Transaction Completed")
                    Handler().postDelayed(Runnable {

                        dialog!!.dismiss()
                        showTransactionId(responseString)


                    }, 2000)

                },
                onError   = { error ->

                    dialog!!.progress_tv.setText("Sorry, we can't process your order!")
                    Handler().postDelayed(Runnable { dialog!!.dismiss() }, 2000)

                })
    }

    private fun showTransactionId(responseString: String) {

        var obj = JSONObject(responseString)

        dialog = Dialog(this)
        dialog!!.getWindow().setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setContentView(R.layout.raw_dialog_transactionstatus)

        dialog!!.trans_id.setText(obj.getString("txn_id"))

        Handler().postDelayed(Runnable {  }, 1500)
        dialog!!.animCheck.check()

        dialog!!.button2.setOnClickListener(View.OnClickListener {

            dialog!!.dismiss()
        })

        dialog!!.show()

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK){

            if (requestCode == CARD_REQUEST_CODE)
            {
                shoppingcart_rv.getChildAt(shoppingcart_rv.childCount-1).via_payment_tv.setText("via "+ LocalMutators.cardNumber.substring(LocalMutators.cardNumber.length-4, LocalMutators.cardNumber.length).trim() + "..")

            } else if (requestCode == ADDRESS_REQUEST_CODE) {

                var totalItemsQnty : Int = 0

                try {

                    shoppingcart_rv.getChildAt(shoppingcart_rv.childCount-1).via_address_tv.setText("To "+ LocalMutators.fullAddress.substring(0, 10).trim() + "..")

                    totalItemsQnty = Cartdb(applicationContext).getCartQuantity()

                    if (LocalMutators.fullAddress.contains("Sabah") || LocalMutators.fullAddress.contains("Sarawak")) {

                        shippingCharges = 15.00

                        shoppingcart_rv.getChildAt(shoppingcart_rv.childCount-1).shippingPrice_tv.setText("15.00 RM x "+ totalItemsQnty)
                        shoppingcart_rv.getChildAt(shoppingcart_rv.childCount-1).total_tv.setText((String.format("%.2f",LocalMutators.totalPayment!!.plus (shippingCharges * totalItemsQnty )) + " RM"))
                        randomAmount = LocalMutators.totalPayment!!.plus (shippingCharges * totalItemsQnty)

                    } else {

                        shippingCharges = 10.00

                        shoppingcart_rv.getChildAt(shoppingcart_rv.childCount-1).shippingPrice_tv.setText("10.00 RM x "+ totalItemsQnty)
                        shoppingcart_rv.getChildAt(shoppingcart_rv.childCount-1).total_tv.setText((String.format("%.2f",LocalMutators.totalPayment!!.plus (shippingCharges * totalItemsQnty )) + " RM"))
                        randomAmount = LocalMutators.totalPayment!!.plus (shippingCharges * totalItemsQnty)

                    }

                }catch (e : java.lang.StringIndexOutOfBoundsException)
                {e.printStackTrace()}

            }



        } else {

            shoppingcart_rv.getChildAt(shoppingcart_rv.childCount-1).via_address_tv.setText("Default")
            LocalMutators.fullAddress = "null"
        }

    }
}