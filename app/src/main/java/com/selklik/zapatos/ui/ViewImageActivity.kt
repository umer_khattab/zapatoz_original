package com.selklik.zapatos.ui

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.content.Intent
import android.graphics.Matrix
import android.os.Bundle
import android.os.Handler
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import com.android.volley.VolleyError
import com.android.volley.toolbox.ImageLoader
import com.google.gson.Gson
import com.selklik.zapatos.R
import com.selklik.zapatos.model.Feed
import com.selklik.zapatos.utils.AppController
import com.selklik.zapatos.utils.TypefaceHelper
import com.selklik.zapatos.utils.helper.LikeFeedHelper
import kotlinx.android.synthetic.main.activity_view_image.*
import android.view.ViewTreeObserver.OnGlobalLayoutListener
import com.selklik.zapatos.model.User
import com.selklik.zapatos.utils.LocalMutators


/**
 * Created by faridhadiselklik on 21/12/2017.
 */
class ViewImageActivity : AppCompatActivity() {
    var TAG = ViewImageActivity::class.java.simpleName
    var gson: Gson? = null
    var feed: Feed? = null
    val imageLoader: ImageLoader = AppController.instance.getImageLoader()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_image)

        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN)

        setContentView(R.layout.activity_view_image)


        gson = Gson()

        val intent = intent
            feed = gson!!.fromJson<Feed>(intent.getStringExtra("feedData"), Feed::class.java!!)

        if (feed != null) {
            Log.d(TAG, "feed body : " + feed!!.body)
        }

        toolbar.setBackgroundColor(ContextCompat.getColor(this, android.R.color.transparent))
        setSupportActionBar(toolbar)

        val ab = supportActionBar
        ab!!.setHomeButtonEnabled(true)
        ab.setDisplayHomeAsUpEnabled(true)
        ab.setDisplayShowTitleEnabled(false)

        txt_artist_name.setText(feed!!.artist.name)
        txt_artist_name.typeface = TypefaceHelper().setTypeface("bold")
        txt_post_body.setTypeface(TypefaceHelper().setTypeface("regular"))
        txt_post_body.setText(feed!!.body)

        txt_likes.typeface = TypefaceHelper().setTypeface("light")
        if (feed!!.like_count > 0) {
            txt_likes.text = feed!!.like_count.toString() + " likes"
        } else {
            txt_likes.text = "likes"
        }
        if (feed!!.comment_count > 0) {
            txt_comment.text = feed!!.comment_count.toString() + " comments"
        } else {
            txt_comment.text = "commments"
        }

        when (feed!!.liked) {
            true -> {
                ic_like.setImageResource(R.drawable.ic_feed_liked)
            }
            false -> {
                ic_like.setImageResource(R.drawable.ic_feed_like)
            }
        }
        ic_like.setOnClickListener {
            when (feed!!.liked) {
                true -> {
                    ic_like.setImageResource(R.drawable.ic_feed_like)
                }
                false -> {
                    ic_like.setImageResource(R.drawable.ic_feed_liked)
                }
            }
            LikeFeedHelper(this).doLikeFeedPost(feed!!.id,
                    feed!!.liked,
                    likeListener = { liked: Boolean ->
                        Log.d("FeedItemAdapter", "on un like success : $liked")
                        feed!!.liked = liked
                        if (liked) {
                            feed!!.like_count +=1
                            ic_like.setImageResource(R.drawable.ic_feed_liked)
                        } else {
                            feed!!.like_count -=1
                            ic_like.setImageResource(R.drawable.ic_feed_like)
                        }
                        if (feed!!.like_count > 0) {
                            txt_likes.text = feed!!.like_count.toString() + " likes"
                        } else {
                            txt_likes.text = "likes"
                        }

                    })
        }

        layout_comment.setOnClickListener {
            startActivity(Intent(this, CommentActivity::class.java).putExtra("post_id", feed!!.id))
        }



        imageLoader.get(feed!!.attachment, object : ImageLoader.ImageListener {
            override fun onResponse(response: ImageLoader.ImageContainer, isImmediate: Boolean) {
                val bitmap = response.bitmap
                if (bitmap != null) {


                    Log.d("HELLOSS", intent.getBooleanExtra
                    ("multipleImage", false).toString() + " " + intent.getStringExtra("currentImage"));


                    img_post.setVisibility(View.VISIBLE)
                    progress_bar_view_image.setVisibility(View.GONE)


                    LocalMutators.activity  = this@ViewImageActivity

                    img_post.getViewTreeObserver().addOnGlobalLayoutListener(object : OnGlobalLayoutListener {
                        override fun onGlobalLayout() {
                            val f = FloatArray(9)


                            if (intent.getBooleanExtra("multipleImage", false)) {

                                img_post.setImageUrl(intent.getStringExtra("currentImage"), imageLoader)

                            } else {

                                img_post.setImageUrl(feed!!.attachment, imageLoader)
                            }

                            img_post.getImageMatrix().getValues(f)

                            // Extract the scale values using the constants (if aspect ratio maintained, scaleX == scaleY)
                            val scaleX = f[Matrix.MSCALE_X]
                            val scaleY = f[Matrix.MSCALE_Y]

                            // Get the drawable (could also get the bitmap behind the drawable and getWidth/getHeight)
                            val d = img_post.getDrawable()
                            val origW = d.intrinsicWidth
                            val origH = d.intrinsicHeight

                            // Calculate the actual dimensions
                            val actW = Math.round(origW * scaleX)
                            val actH = Math.round(origH * scaleY)

                            viewer_txt.y = ((img_post.getHeight()/2) - actH/2).toFloat()
                            viewer_txt.setText(LocalMutators.email)

                            img_post.getViewTreeObserver().removeOnGlobalLayoutListener { this }

                        }
                    })

                    img_post.setOnClickListener(View.OnClickListener {
                        if (view_image_bottom_bar.getVisibility() == View.VISIBLE) {
                            Handler().postDelayed({
                                view_image_bottom_bar.animate()
                                        .translationY(view_image_bottom_bar.getHeight().toFloat())
                                        .setListener(object : AnimatorListenerAdapter() {
                                            override fun onAnimationEnd(animation: Animator) {
                                                super.onAnimationEnd(animation)
                                                view_image_bottom_bar.setVisibility(View.GONE)
                                            }
                                        })
                            }, 100)
                        } else {
                            Handler().postDelayed({
                                view_image_bottom_bar.animate()
                                        .translationY(0f)
                                        .setListener(object : AnimatorListenerAdapter() {
                                            override fun onAnimationEnd(animation: Animator) {
                                                super.onAnimationEnd(animation)
                                                view_image_bottom_bar.setVisibility(View.VISIBLE)
                                            }
                                        })
                            }, 100)
                        }
                    })
                }

            }

            override fun onErrorResponse(error: VolleyError) {
                img_post.setErrorImageResId(R.drawable.img_bg)
                img_post.visibility = View.VISIBLE
                progress_bar_view_image.visibility = View.GONE
            }
        })




            try {

                var premimumContent = getIntent().getBooleanExtra("premium", false)

                if (premimumContent)
                {
                    watermark_iv.visibility = View.VISIBLE

                    val user = gson!!.fromJson(AppController.instance.getUserData(), User::class.java)
                    viewer_txt.visibility = View.VISIBLE
                    viewer_txt.setText(user!!.email)
                    watermark_iv.bringToFront();
                }
                else{

                    watermark_iv.visibility = View.INVISIBLE
                    viewer_txt.visibility = View.INVISIBLE
                }

            }catch (e : NullPointerException) {e.printStackTrace()
                 watermark_iv.visibility = View.INVISIBLE
                 viewer_txt.visibility = View.INVISIBLE }
        

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        when (id) {
            android.R.id.home -> super.onBackPressed()
            else -> return false
        }
        return true
    }
}