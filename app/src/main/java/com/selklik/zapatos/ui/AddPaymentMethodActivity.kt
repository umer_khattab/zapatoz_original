package com.selklik.zapatos.ui

import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.VolleyError
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.selklik.zapatos.R
import com.selklik.zapatos.model.Card
import com.selklik.zapatos.model.Products
import com.selklik.zapatos.utils.*
import com.selklik.zapatos.utils.volley.ApiHelper
import com.stripe.android.SourceCallback
import com.stripe.android.Stripe
import com.stripe.android.model.Source
import com.stripe.android.model.SourceParams

import kotlinx.android.synthetic.main.activity_add_payment_method.*
import kotlinx.android.synthetic.main.content_add_payment_method.*
import java.util.HashMap

class AddPaymentMethodActivity : AppCompatActivity() {
    private var dialog: AlertDialog? = null

    private inline fun <reified T> genericType() = object : TypeToken<T>() {}.type!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_payment_method)
        setSupportActionBar(toolbar)
        val ab = supportActionBar
        ab!!.setHomeButtonEnabled(true)
        ab.setDisplayHomeAsUpEnabled(true)
        ab.setDisplayShowTitleEnabled(false)

        fetchCard()

        txt_toolbar_title.typeface = TypefaceHelper().setTypeface("regular")

        button_add_payment_method.setOnClickListener{
            addCard()
        }

    }

    private fun addCard(){
        var card = card_input_widget.card
        if (card != null) {
            if (!card!!.validateCard()) {
                // Do not continue token creation.
                Log.d("Add Card", "card invalidate")
            } else {
                Log.d("Add Card", "card validate : " + card.brand)
                val stripe = Stripe(this, "pk_test_O2CtbTj29L33SKIAtkQ4gAU0")
                val cardSourceParams = SourceParams.createCardParams(card)

                showProgressDialog()
                stripe.createSource(cardSourceParams, object : SourceCallback {
                    override fun onError(error: Exception) {
                        Log.d("SOURCE ERROR", error.toString())
                    }

                    override fun onSuccess(source: Source) {
                        Log.d("SOURCE ID:", source.id)
                        postCard(source.id)
//                        if (dialog!!.isShowing) {
//                            dialog!!.dismiss()
//                        }
                        // POST me/cards
                        // Params: source_id=source.getId()
                    }
                })
            }
        }

    }

    private fun fetchCard(){
        if (!ConnectivityReceiver.isConnected()) {
            if (dialog!!.isShowing) {
                dialog!!.dismiss()
            }
            Log.d("Add Card", "No internet connection")
        } else {
            val jsonParams = HashMap<String, String>()
            jsonParams.put("access_token", AppController.instance.getUserAccessToken())

            ApiHelper(this).makeRequest(
                    Config.API_ME+"/cards",
                    Request.Method.GET,
                    jsonParams,
                    false,
                    false,
                    onSuccess = { responseString: String, gson: Gson ->
                        Log.d("Add Card", "response string : " + responseString)
//                        if (dialog!!.isShowing) {
//                            dialog!!.dismiss()
//                        }
                        SharedPrefHelper().setSharedPreferenceString(this, Config.PREF_USER_CARD, responseString)

                        Log.d("Add Card", "stored card info : "
                                + SharedPrefHelper().getSharedPreferenceString(this, Config.PREF_USER_CARD, ""))
                    },
                    onError = { error: VolleyError ->
                        ApiHelper(this).errorHandler(error)
//                        if (dialog!!.isShowing) {
//                            dialog!!.dismiss()
//                        }
                    })
        }

    }

    private fun postCard(sourceId: String){
        if (!ConnectivityReceiver.isConnected()) {
            if (dialog!!.isShowing) {
                dialog!!.dismiss()
            }
            Log.d("Add Card", "No internet connection")
        } else {
            val jsonParams = HashMap<String, String>()
            jsonParams.put("access_token", AppController.instance.getUserAccessToken())
            jsonParams.put("source_id", sourceId)

            ApiHelper(this).makeRequest(
                    Config.API_ME+"/cards",
                    Request.Method.POST,
                    jsonParams,
                    false,
                    false,
                    onSuccess = { responseString: String, gson: Gson ->
                        var userCardData = SharedPrefHelper().getSharedPreferenceString(this, Config.PREF_USER_CARD, null)
                        var featuredProductArrayList: ArrayList<Card> = gson.fromJson(userCardData, genericType<ArrayList<Card>>())
                        Log.d("AddCard", "old size : " + featuredProductArrayList.size)
                        var card = gson.fromJson(responseString, Card::class.java)
                        featuredProductArrayList.add(card)
                        Log.d("AddCard", "new size : " + featuredProductArrayList.size)

                        var newCardData = gson.toJson(featuredProductArrayList)

                        SharedPrefHelper().setSharedPreferenceString(this, Config.PREF_USER_CARD, newCardData)
//
                        Log.d("Add Card", "stored card info : "
                                + SharedPrefHelper().getSharedPreferenceString(this, Config.PREF_USER_CARD, ""))
                        Log.d("Add Card", "response string : " + responseString)
                        if (dialog!!.isShowing) {
                            dialog!!.dismiss()
                        }
                        Toast.makeText(this, "Successfully add card", Toast.LENGTH_SHORT).show()
                        finish()
                    },
                    onError = { error: VolleyError ->
                        ApiHelper(this).errorHandler(error)
                        if (dialog!!.isShowing) {
                            dialog!!.dismiss()
                        }
                    })
        }

    }

    private fun showProgressDialog() {
        val mBuilder = AlertDialog.Builder(this, R.style.CustomDialogTheme)
        val mView = LayoutInflater.from(this).inflate(R.layout.dialog_progress, null)

        mBuilder.setView(mView)
        mBuilder.setCancelable(false)
        dialog = mBuilder.create()
        dialog!!.show()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        when (id) {
            android.R.id.home -> onBackPressed()
            else -> return false
        }
        return true
    }
}
