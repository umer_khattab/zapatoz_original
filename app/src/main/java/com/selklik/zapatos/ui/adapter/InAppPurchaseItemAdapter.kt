package com.selklik.zapatos.ui.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.android.volley.toolbox.ImageLoader
import com.google.gson.Gson
import com.google.gson.JsonElement
import com.selklik.zapatos.R
import com.selklik.zapatos.model.CommentDetails
import com.selklik.zapatos.model.History
import com.selklik.zapatos.utils.AppController
import com.selklik.zapatos.utils.TypefaceHelper
import kotlinx.android.synthetic.main.item_list_comment.view.*
import kotlinx.android.synthetic.main.item_list_history.view.*

/**
 * Created by faridhadiselklik on 20/12/2017.
 */
class InAppPurchaseItemAdapter(private var mContext: Context, private var inAppPurchaseArrayList: ArrayList<History>) : RecyclerView.Adapter<InAppPurchaseItemAdapter.ViewHolder>() {


    override fun getItemCount(): Int {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        return inAppPurchaseArrayList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        holder.bindItems(inAppPurchaseArrayList[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        val itemView = LayoutInflater.from(parent!!.context).inflate(R.layout.item_list_history, parent, false)
        return ViewHolder(itemView)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val imageLoader: ImageLoader = AppController.instance.getImageLoader()

        fun bindItems(history: History) {
            itemView.txt_ref_id.typeface = TypefaceHelper().setTypeface("bold")
            itemView.txt_ref_id.text = history.reference_id
            itemView.txt_history_type.typeface = TypefaceHelper().setTypeface("regular")
            itemView.txt_history_value.typeface = TypefaceHelper().setTypeface("regular")
            itemView.txt_history_value.text = "STAR: ${history.value}"
            itemView.txt_history_time.typeface = TypefaceHelper().setTypeface("regular")
            itemView.txt_history_time.text = AppController.instance.setPrettyDateFormat(history.timestamp!!)

            when (history.type) {
                "premium-post-purchase" -> {
                    itemView.layout_img.visibility = View.VISIBLE
                    history.post?.let {
                        itemView.img_history_post.setImageUrl(history.post!!.photos!!.asJsonArray[0].asJsonObject.get("versions").asJsonObject.get("thumbnail").asString,
                            imageLoader)
                    }
                    itemView.txt_history_type.text = "TYPE: Premium Post"
                }
                "balance-topup" -> {
                    itemView.layout_img.visibility = View.GONE
                    itemView.txt_history_type.text = "TYPE: Stars Reload"
                }
                "question-purchase" -> {
                    itemView.layout_img.visibility = View.GONE
                    itemView.txt_history_type.text = "TYPE: Question (Q&A)"
                }
                "answer-purchase" -> {
                    itemView.layout_img.visibility = View.GONE
                    itemView.txt_history_type.text = "TYPE: Question (Q&A)"
                }
                "free-stars" -> {
                    itemView.layout_img.visibility = View.GONE
                    itemView.txt_history_type.text = "TYPE: Free Stars"
                }
            }

        }

    }
}