package com.selklik.zapatos.ui.adapter.ShopAdapter

import android.content.Context
import android.support.v4.view.PagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.selklik.zapatos.R
import com.selklik.zapatos.model.shopitems.Products
import com.selklik.zapatos.utils.AppController
import kotlinx.android.synthetic.main.raw_itemcolors_showcase.view.*

class ColorsShowCaseAdapter(context : Context, productImg : ArrayList<Products>) : PagerAdapter()
{

    private val arrayDes = ArrayList<String>()

    private var productImg : ArrayList<Products>

    private var layoutInflater: LayoutInflater? = null

    init {

        layoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

        this.productImg = productImg
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {

        return view === `object` as View
    }

    override fun getCount(): Int {
        return productImg.size
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {

        var view : View = layoutInflater!!.inflate(R.layout.raw_itemcolors_showcase, container, false)

        view.productThumb.setImageUrl(productImg.get(position).prod_img, AppController().getImageLoader())

        container.addView(view)

        return view

    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }


}
