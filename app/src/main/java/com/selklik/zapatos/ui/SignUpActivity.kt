package com.selklik.zapatos.ui

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AlertDialog
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.TimeoutError
import com.android.volley.VolleyError
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.gson.Gson
import com.selklik.zapatos.MainActivity
import com.selklik.zapatos.R
import com.selklik.zapatos.model.User
import com.selklik.zapatos.utils.*
import com.selklik.zapatos.utils.volley.ApiHelper
import kotlinx.android.synthetic.main.activity_sign_up.*
import org.json.JSONException
import org.json.JSONObject
import java.util.*
import java.util.regex.Pattern

class SignUpActivity : AppCompatActivity() {
    private val TAG = SignUpActivity::class.java.simpleName
    private var dialog: AlertDialog? = null

    private lateinit var callbackManager: CallbackManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        setContentView(R.layout.activity_sign_up)
        ic_close_login.setOnClickListener {
            onBackPressed()
        }
        text_or_email_password.typeface = TypefaceHelper().setTypeface("light")
        edittext_email.typeface = TypefaceHelper().setTypeface("regular")
        edittext_username.typeface = TypefaceHelper().setTypeface("regular")
        text_btn_login_email.typeface = TypefaceHelper().setTypeface("regular")
        text_login_fb_btn.typeface = TypefaceHelper().setTypeface("bold")
        btn_sign_up_email.setOnClickListener {
            if (loginValidation(edittext_email.text.toString(), edittext_username.text.toString())) {
                Log.d(TAG, "login validation : " + java.lang.Boolean.toString(loginValidation(edittext_email.text.toString(), edittext_username.text.toString())))

                imm.hideSoftInputFromWindow(edittext_username.windowToken, 0)
//                postLogin(edittext_email.text.toString(), edittext_password.text.toString())
                startActivity(Intent(this, SignUpNextActivity::class.java)
                        .putExtra("email",edittext_email.text.toString())
                        .putExtra("password", edittext_username.text.toString()))
            }
        }
        text_selklik_signup_title.typeface = TypefaceHelper().setTypeface("light")
        text_login.typeface = TypefaceHelper().setTypeface("regular")
        text_login.setOnClickListener {
            finish()
        }

        btn_login_fb.setOnClickListener {
            fb_login_button.performClick()
        }

        fb_login_button.setReadPermissions(Arrays.asList("public_profile, email"))
        callbackManager = CallbackManager.Factory.create()

        fb_login_button.registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
            override fun onSuccess(loginResult: LoginResult) {
                if (loginResult.accessToken.token != null) {

                    Toast.makeText(this@SignUpActivity, "Success facebook login", Toast.LENGTH_SHORT).show()
                    //                    AccessToken.getCurrentAccessToken().getToken();
                    Log.d(TAG, "token : " + loginResult.accessToken.token + ", user id : " + loginResult.accessToken.userId)
                    postFbSignUp(loginResult.accessToken.token)
                }
            }
            override fun onCancel() {
                Toast.makeText(this@SignUpActivity, "Cancel facebook sign up", Toast.LENGTH_SHORT).show()
            }

            override fun onError(error: FacebookException) {
                Toast.makeText(this@SignUpActivity, "Something wrong :" + error.toString(), Toast.LENGTH_SHORT).show()
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        callbackManager!!.onActivityResult(requestCode, resultCode, data)
    }

    private fun postFbSignUp(fbToken: String) {

        showProgressDialog()
        if (!ConnectivityReceiver.isConnected()) {
            if (dialog!!.isShowing) {
                dialog!!.dismiss()
            }
            Snackbar.make(this.window.decorView.rootView, "No internet connection", Snackbar.LENGTH_SHORT).show()
        } else {
            val jsonParams = HashMap<String, String>()
            jsonParams.put("facebook_access_token", fbToken!!)

            var apiHelper = ApiHelper(this)
            apiHelper.makeRequest(
                    Config.API_FB_SIGNUP,
                    Request.Method.POST,
                    jsonParams,
                    false,
                    false,
                    onSuccess = { responseString: String, gson: Gson ->

                        Log.d("Signup Activity", "response string : " + responseString)

                        var user = gson.fromJson(responseString, User::class.java)
                        SharedPrefHelper().setSharedPreferenceString(this, Config.PREF_USER_DATA, responseString)
                        SharedPrefHelper().setSharedPreferenceString(this, Config.PREF_ACCESS_TOKEN, user!!.access_token)
                        SharedPrefHelper().setSharedPreferenceBoolean(Config.PREF_KEEP_LOGIN, true)


                        AppController.instance.setUserIdnotification(this)
                        Log.d(TAG, "access token : " + SharedPrefHelper().getSharedPreferenceString(this, Config.PREF_ACCESS_TOKEN, ""))
                        fetchAboutUssy(user)

                    }, onError = { error: VolleyError ->
                        //                        apiHelper.errorHandler(error)
                        var json: String? = null
                        val networkResponse = error.networkResponse
                        if (networkResponse != null) {
                            if (networkResponse.data != null) {
                                val builder = StringBuilder()
                                when (networkResponse.statusCode) {
                                    400 -> {
                                        json = String(networkResponse.data)
                                        try {
                                            val errorObg = JSONObject(json)
                                            val errorArray = errorObg.getJSONArray("errors")
                                            for (i in 0 until errorArray.length()) {
                                                builder.append(errorArray.get(i).toString() + "\n")
                                            }
                                            Log.d("Error Api : ", builder.toString())
                                        } catch (e: JSONException) {
                                            e.printStackTrace()
                                        }
                                        showAlertSignUpFb("Already Sign Up", builder.toString(), fbToken)
//                                        AlertDialogHelper().showAlert("Errors !", builder.toString(), this)
                                    }
                                    else -> {
                                        apiHelper.errorHandler(error)
                                    }
                                }
                            }
                        } else {
                            if (error.javaClass == TimeoutError::class.java) {
                                // Show timeout error message
                                Toast.makeText(this,
                                        "Oops. Timeout error!",
                                        Toast.LENGTH_LONG).show()
                            }
                            Toast.makeText(this, "Failed to load", Toast.LENGTH_SHORT).show()
                        }
                        if (dialog!!.isShowing) {
                            dialog!!.dismiss()
                        }
                    })

        }

    }

    private fun postFbLogin(fbToken: String) {

        showProgressDialog()
        if (!ConnectivityReceiver.isConnected()) {
            if (dialog!!.isShowing) {
                dialog!!.dismiss()
            }
            Snackbar.make(this.window.decorView.rootView, "No internet connection", Snackbar.LENGTH_SHORT).show()
        } else {
            val jsonParams = HashMap<String, String>()
            jsonParams.put("facebook_access_token", fbToken!!)

            var apiHelper = ApiHelper(this)
            apiHelper.makeRequest(
                    Config.API_FB_LOGIN,
                    Request.Method.POST,
                    jsonParams,
                    false,
                    false,
                    onSuccess = { responseString: String, gson: Gson ->
                        Log.d("Login Activity", "response string : " + responseString)

                        var user = gson.fromJson(responseString, User::class.java)
                        SharedPrefHelper().setSharedPreferenceString(this, Config.PREF_USER_DATA, responseString)
                        SharedPrefHelper().setSharedPreferenceString(this, Config.PREF_ACCESS_TOKEN, user!!.access_token)
                        SharedPrefHelper().setSharedPreferenceBoolean(Config.PREF_KEEP_LOGIN, true)

                        Log.d(TAG, "access token : " + SharedPrefHelper().getSharedPreferenceString(this, Config.PREF_ACCESS_TOKEN, ""))
                        fetchAboutUssy(user)
                    },
                    onError = { error: VolleyError ->
                        apiHelper.errorHandler(error)
                        if (dialog!!.isShowing) {
                            dialog!!.dismiss()
                        }
                    })

        }

    }

    private fun fetchAboutUssy(user: User) {
        if (!ConnectivityReceiver.isConnected()) {
            Log.d(TAG, "No internet connection")
        } else {
            val jsonParams = HashMap<String, String>()
            jsonParams.put("access_token", user!!.access_token)

            ApiHelper(this).makeRequest(
                    Config.API_ABOUT_ARTIST,
                    Request.Method.GET,
                    jsonParams,
                    false,
                    false,
                    onSuccess = { responseString: String, gson: Gson ->
                        Log.d("Splash Screen", "response string : " + responseString)
                        if (dialog!!.isShowing) {
                            dialog!!.dismiss()
                        }
                        SharedPrefHelper().setSharedPreferenceString(this, Config.PREF_ABOUT_ARTIST, responseString)
                        startMainActivity()
                    }, onError = {})
        }
    }

    private fun startMainActivity() {
        var intent = Intent(this, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
        overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out)
        finish()
    }

    fun showAlertSignUpFb(title: String, message: String, fbToken: String) {
        val builder = android.app.AlertDialog.Builder(this)
        builder.setMessage(message.replace("[^a-zA-Z0-9 \n]".toRegex(), "")).setTitle(title.toUpperCase())
                .setCancelable(true)
        builder.setPositiveButton("OK") { dialog, id ->
            postFbLogin(fbToken)
            dialog.dismiss()
        }
        builder.setNegativeButton("Cancel") { dialog, id ->
            LoginManager.getInstance().logOut()
            dialog.dismiss()

        }

        //AlertDialog alert = builder.create();
        val alert = builder.show()
        val titlex = TextView(this)
        titlex.gravity = Gravity.CENTER
        titlex.textSize = 22f
        titlex.typeface = TypefaceHelper().setTypeface("semi-bold")
        alert.setCustomTitle(titlex)

        val messageText = alert.findViewById<View>(android.R.id.message) as TextView
        messageText.typeface = TypefaceHelper().setTypeface("regular")
        messageText.gravity = Gravity.CENTER
        messageText.textSize = 18f

    }

    private fun loginValidation(email: String?, password: String?): Boolean {
        var valid = true

        if (email!!.isEmpty() || email == null) {
            edittext_email.error = "empty email"
            valid = false
        } else {
            if (isEmailValid(email)) {
                edittext_email.error = null
                if (password!!.isEmpty() || password == "" || password == null) {
                    edittext_username.error = "empty password"
                    valid = false
                } else {
                    edittext_username.error = null
                }
            } else {
                edittext_email.error = "enter a valid email"
                valid = false
            }

        }

        return valid
    }

    private fun isEmailValid(email: String): Boolean {
        val expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$"
        val pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE)
        val matcher = pattern.matcher(email)
        return matcher.matches()
    }

    private fun showProgressDialog() {
        val mBuilder = AlertDialog.Builder(this, R.style.CustomDialogTheme)
        val mView = LayoutInflater.from(this).inflate(R.layout.dialog_progress, null)

        mBuilder.setView(mView)
        mBuilder.setCancelable(false)
        dialog = mBuilder.create()
        dialog!!.show()
    }
}
