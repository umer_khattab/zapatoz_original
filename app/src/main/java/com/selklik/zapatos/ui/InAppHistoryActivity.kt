package com.selklik.zapatos.ui

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import com.android.volley.Request
import com.android.volley.VolleyError
import com.google.gson.Gson
import com.google.gson.JsonParser
import com.selklik.zapatos.R
import com.selklik.zapatos.model.History
import com.selklik.zapatos.ui.adapter.InAppPurchaseItemAdapter
import com.selklik.zapatos.utils.*
import com.selklik.zapatos.utils.volley.ApiHelper

import kotlinx.android.synthetic.main.activity_in_app_history.*
import kotlinx.android.synthetic.main.content_in_app_history.*

class InAppHistoryActivity : AppCompatActivity() {
    private val TAG = InAppHistoryActivity::class.java.simpleName
    private var dialog: AlertDialog? = null
    private var mLayoutManager: RecyclerView.LayoutManager? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_in_app_history)
        setSupportActionBar(toolbar)
        val ab = supportActionBar!!
        ab!!.setHomeButtonEnabled(true)
        ab!!.setDisplayHomeAsUpEnabled(true)
        ab!!.setDisplayShowTitleEnabled(false)

        txt_toolbar_title.typeface = TypefaceHelper().setTypeface("regular")
        txt_in_app_history_desc.typeface = TypefaceHelper().setTypeface("regular")
        mLayoutManager = LinearLayoutManager(this)
        inapp_transaction_recycler_view.layoutManager = mLayoutManager
        inapp_transaction_recycler_view.addItemDecoration(DividerItemDecoration(ContextCompat.getDrawable(this!!, R.drawable.item_decorator)!!))
        inapp_transaction_recycler_view.isNestedScrollingEnabled = false
        fetchInAppHistory()
    }

    private fun fetchInAppHistory() {
//        showProgressDialog()
        if (!ConnectivityReceiver.isConnected()) {
            Log.d(TAG, "No internet connection")
        } else {
            val jsonParams = HashMap<String, String>()
            jsonParams.put("access_token", AppController.instance.getUserAccessToken())

            ApiHelper(this).makeRequest(
                    module = Config.API_TRANSACTION,
                    method = Request.Method.GET,
                    params = jsonParams,
                    onSuccess = { responseString: String, gson: Gson ->
                        Log.d(TAG, "response string : " + responseString)
                        var historyJsonArray = JsonParser().parse(responseString).asJsonArray

                        transaction_progress_bar.visibility = View.GONE
                        if (historyJsonArray.size() > 0) {
                            layout_empty.visibility = View.GONE
                            inapp_transaction_recycler_view.visibility = View.VISIBLE
                            var historyArrayList = historyJsonArray.map {
                                gson.fromJson(it, History::class.java)
                            } as ArrayList<History>

                            Log.d(TAG, "History size : ${historyArrayList.size}")
                            inapp_transaction_recycler_view.adapter = InAppPurchaseItemAdapter(this, historyArrayList)
                        } else {
                            layout_empty.visibility = View.VISIBLE
                            inapp_transaction_recycler_view.visibility = View.GONE
                        }
                    },
                    onError = { error: VolleyError ->
                        ApiHelper(this!!).errorHandler(error)
                        layout_empty.visibility = View.VISIBLE
                        inapp_transaction_recycler_view.visibility = View.GONE
                    })

        }

    }

    private fun showProgressDialog() {
        val mBuilder = AlertDialog.Builder(this!!, R.style.CustomDialogTheme)
        val mView = LayoutInflater.from(this).inflate(R.layout.dialog_progress, null)

        mBuilder.setView(mView)
        mBuilder.setCancelable(false)
        dialog = mBuilder.create()
        dialog!!.show()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        when (id) {
            android.R.id.home -> onBackPressed()
            else -> return false
        }
        return true
    }

}
