package com.selklik.zapatos.ui

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.view.Window
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import com.selklik.zapatos.R
import kotlinx.android.synthetic.main.activity_localpaymentbillplz.*
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.Toast
import com.selklik.zapatos.MainActivity
import com.selklik.zapatos.utils.cartdb.Cartdb
import kotlinx.android.synthetic.main.raw_dialog_payment.*


class BillplzPaymentActivity : Activity(){

    private var dialog : Dialog?= null

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)
        if (hasFocus) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            or  View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            or  View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            or  View.SYSTEM_UI_FLAG_FULLSCREEN
                            or  View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_localpaymentbillplz)

        dialog = Dialog(this)
        dialog!!.getWindow().setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setContentView(R.layout.raw_dialog_payment)
        dialog!!.setCancelable(false)
        dialog!!.show()

        var product_info = getIntent().getStringExtra("product_info")


        Log.d("productinfo", product_info)

        webview.getSettings().setJavaScriptEnabled(true);
        webview.getSettings().setLoadWithOverviewMode(true);
        webview.getSettings().setUseWideViewPort(true);
        webview.setWebViewClient(object : WebViewClient() {

            override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                view.loadUrl(url)

                return true
            }

            override fun onPageFinished(view: WebView, url: String) {


                if (url.contains("bills")) {

                    if (dialog!!.isShowing)
                        dialog!!.dismiss()

                }

                if (url.contains("success"))
                {
                    status_parent.visibility = View.VISIBLE


                    Cartdb(applicationContext).clearTable()

                    var anim = AnimationUtils.loadAnimation(applicationContext, R.anim.bottom_to_up)
                    status_parent.startAnimation(anim)
                    status.setText("Success")
                    status_parent.setOnClickListener(View.OnClickListener {

                        var intent = Intent(applicationContext, MainActivity::class.java)
                        startActivity(intent)
                        finish()

                    })

                    Handler().postDelayed(Runnable {

                        status.alpha = 0.0f
                        status.setText("Go Back?")
                        status.animate().alpha(1.0f).setDuration(1500).start()

                    }, 2500)


                } else if (url.contains("failure")) {

                    status_parent.visibility = View.VISIBLE

                    var anim = AnimationUtils.loadAnimation(applicationContext, R.anim.bottom_to_up)
                    status_parent.startAnimation(anim)
                    status.setText("Failed")
                    status_parent.setOnClickListener(View.OnClickListener {

                        var intent = Intent(applicationContext, MainActivity::class.java)
                        startActivity(intent)
                        finish()

                    })

                    Handler().postDelayed(Runnable {

                        status.alpha = 0.0f
                        status.setText("Go Back?")
                        status.animate().alpha(1.0f).setDuration(1500).start()

                    }, 2500)
                }
            }
        })

        btn_close.setOnClickListener(View.OnClickListener {

                finish()

        })

          webview.loadUrl("http://shoplot.biz/billplz_sandbox/?prod_info="+product_info);
       // webview.loadData(postData, "application/json; charset=UTF-8", null);


        //webview.postUrl("http://shoplot.biz/billplz_sandbox", postData.toByteArray("UTF-8"))

    }

}