package com.selklik.zapatos.ui

import android.graphics.Canvas
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import com.android.volley.Request
import com.android.volley.VolleyError
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.selklik.zapatos.R
import com.selklik.zapatos.model.purchasedItems.Data
import com.selklik.zapatos.ui.adapter.PurchaseHistoryProductAdapter
import com.selklik.zapatos.utils.*
import com.selklik.zapatos.utils.volley.ApiHelper

import kotlinx.android.synthetic.main.activity_purchased_history.*
import kotlinx.android.synthetic.main.content_purchased_history.*
import java.util.HashMap

class PurchasedHistoryActivity : AppCompatActivity() {
    var dialog: AlertDialog? = null
    inline fun <reified T> genericType() = object : TypeToken<T>() {}.type!!
    private var mLayoutManager: RecyclerView.LayoutManager? = null
    private var visibleItemCount: Int = 0
    private var totalItemCount: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_purchased_history)
        setSupportActionBar(toolbar)
        val ab = supportActionBar!!
        ab!!.setHomeButtonEnabled(true)
        ab!!.setDisplayHomeAsUpEnabled(true)
        ab!!.setDisplayShowTitleEnabled(false)

        txt_toolbar_title.typeface = TypefaceHelper().setTypeface("regular")

        val mLayoutManager = LinearLayoutManager(this)
            //mLayoutManager.reverseLayout = true
            //mLayoutManager.stackFromEnd = true

        purchased_recycler_view.addItemDecoration(DividerItemDecoration(ContextCompat.getDrawable(this!!, R.drawable.item_decorator)!!))
        purchased_recycler_view.layoutManager = mLayoutManager

        //purchased_recycler_view.addOnScrollListener(productScrollListener)
        fetchOrder()

        /*swipeRefreshLayout.setOnRefreshListener {

            fetchOrder()


            Handler().postDelayed({

                if (swipeRefreshLayout.isRefreshing)
                    swipeRefreshLayout.isRefreshing = false


            }, 500)

        }*/

    }



    private fun fetchOrder(){
        showProgressDialog()
        if (!ConnectivityReceiver.isConnected()) {
            Log.d("Add Card", "No internet connection")
        } else {
            val jsonParams = HashMap<String, String>()

            ApiHelper(this).makeZopatozRequest(
                    Config.Base_Url + "?listorder="+ LocalMutators.email,
                    jsonParams,
                    Request.Method.GET,
                    false,
                    false,
                    onSuccess = { responseString: String, gson: Gson ->

                        if (dialog!!.isShowing) {
                            dialog!!.dismiss()
                        }

                        if (responseString != "[]"){
                            purchased_recycler_view.visibility = View.VISIBLE
                            layout_empty.visibility = View.GONE

                            var purchasedArrayList = gson.fromJson(responseString, Data::class.java)

                            purchased_recycler_view.isNestedScrollingEnabled = false
                            purchased_recycler_view.adapter = PurchaseHistoryProductAdapter(this!!, purchasedArrayList.data!!)
                            purchased_recycler_view.visibility = View.VISIBLE


                        } else {
                            purchased_recycler_view.visibility = View.GONE
                            layout_empty.visibility = View.VISIBLE
                            txt_empty.typeface = TypefaceHelper().setTypeface("regular")
                        }
//
                    },
                    onError = { error: VolleyError ->
                        ApiHelper(this).errorHandler(error)
                        if (dialog!!.isShowing) {
                            dialog!!.dismiss()
                        }
                    })
        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        when (id) {
            android.R.id.home -> onBackPressed()
            else -> return false
        }
        return super.onOptionsItemSelected(item)
    }

    private fun showProgressDialog() {
        val mBuilder = AlertDialog.Builder(this, R.style.CustomDialogTheme)
        val mView = LayoutInflater.from(this).inflate(R.layout.dialog_progress, null)

        mBuilder.setView(mView)
        mBuilder.setCancelable(false)
        dialog = mBuilder.create()
        dialog!!.show()
    }
}
