package com.selklik.zapatos.ui.adapter

import android.content.Context
import android.content.Intent
import android.support.constraint.ConstraintLayout
import android.support.v4.view.PagerAdapter
import android.support.v4.view.ViewPager
import android.view.View
import android.view.ViewGroup
import android.view.LayoutInflater
import android.view.animation.*
import android.widget.RelativeLayout
import com.selklik.zapatos.R
import kotlinx.android.synthetic.main.raw_products_design.view.*
import android.graphics.*
import android.graphics.drawable.BitmapDrawable
import android.util.Log
import android.widget.TextView
import com.selklik.zapatos.model.shopitems.Data
import com.selklik.zapatos.ui.BuyItemShowcaseActivity
import com.selklik.zapatos.utils.AppController
import com.selklik.zapatos.utils.Config


class ProductsViewPagerAdapter(context : Context,
                               productList : ArrayList<Data>,
                               viewpager : ViewPager,
                               alpha_block_rl : ConstraintLayout,
                               from_price_tv : TextView,
                               responseString: String) : PagerAdapter(), ViewPager.OnPageChangeListener {


    private var productList = ArrayList<Data>()

    private var layoutInflater: LayoutInflater? = null
    private var context : Context

    private var alpha_block_rl : ConstraintLayout
    private var from_price_tv : TextView

    private var responseString : String

    init {

        layoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

        this.responseString = responseString
        this.context = context
        this.alpha_block_rl = alpha_block_rl
        this.from_price_tv = from_price_tv
        this.productList = productList

        if (productList.size > 0) {

            var minPrice : Double = 0.0

            if (Config.myCountry.equals("my"))
            {
                minPrice = productList[0].price.trim().toDouble()

                for (i in 0 until (productList.size)) {

                    if (productList[i].price.trim().toDouble() < minPrice)
                        minPrice = productList[i].price.trim().toDouble()
                }

            } else {

                minPrice = productList[0].int_price.trim().toDouble()

                for (i in 0 until (productList.size)) {

                    if (productList[i].int_price.trim().toDouble() < minPrice)
                        minPrice = productList[i].int_price.trim().toDouble()

                    Log.d("PRICES", productList[i].int_price)
                }
            }

            from_price_tv.setText("From " + Config.myCurrencyUnit + minPrice.toString().split(".")[0]+".00")
        }

        var paint = Paint(Paint.ANTI_ALIAS_FLAG)
        paint.color = Color.BLACK
        paint.style = Paint.Style.FILL

        var shape = Path()
        shape.moveTo(0f,0f)
        shape.lineTo(170f, 0f)
        shape.lineTo(200f, 35f)
        shape.lineTo(170f, 65f)
        shape.lineTo(0f, 65f)
        shape.close()

        var b = Bitmap.createBitmap(200, 70, Bitmap.Config.ARGB_8888)

        var canvas = Canvas(b)
        canvas.drawPath(shape, paint)

        val d = BitmapDrawable(b)

        (alpha_block_rl.getChildAt(1) as RelativeLayout).setBackgroundDrawable(d)
        viewpager.addOnPageChangeListener(this)

    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {

        return view === `object` as View
    }

    override fun getCount(): Int {
        return productList.size
    }


    override fun instantiateItem(container: ViewGroup, position: Int): Any {

        var view : View ?= null


        if(position == 0)
        {
            view  = layoutInflater!!.inflate(R.layout.raw_products_design_decorator, container, false)


        } else {

            view = layoutInflater!!.inflate(R.layout.raw_products_design, container, false)

            view.was_tv.paintFlags = Paint.STRIKE_THRU_TEXT_FLAG

            view.description_tv.setText(productList[position].prod_name)

            view.productThumb.setImageUrl(productList[position].products[0].prod_img, AppController().getImageLoader())


            if (Config.myCountry == "my")
            {
                view.price_tv.setText(Config.myCurrencyUnit+productList[position].price+" Only")

            } else {

                view.price_tv.setText(Config.myCurrencyUnit+productList[position].int_price+" Only")
            }


            val fadeIn = AlphaAnimation(0f, 1f)
            fadeIn.interpolator = DecelerateInterpolator() //add this
            fadeIn.duration = 2000

            val animation = AnimationSet(false) //change to false
            animation.addAnimation(fadeIn)

            view.productThumb.startAnimation(animation)
            view.productThumb.setOnClickListener(View.OnClickListener {

                var intent = Intent(context, BuyItemShowcaseActivity::class.java)
                intent.putExtra("position", (position-1))
                intent.putExtra("responseString", responseString)

                context.startActivity(intent)

            })

        }

        container.addView(view)

        return view

    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }

    override fun getPageWidth(position: Int): Float {
        return 0.4f
    }

    override fun onPageScrollStateChanged(state: Int) {

    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {


        if (position < 1)
        {
            var finalAlpha = 1.0f - (positionOffset)
            if (finalAlpha <= 1.0f)
            {
                /*alpha_block_rl.getChildAt(0).alpha = finalAlpha
                alpha_block_rl.getChildAt(1).alpha = finalAlpha*/

                alpha_block_rl.alpha = finalAlpha
            }

        } else {

           /* alpha_block_rl.getChildAt(0).alpha = 0f
            alpha_block_rl.getChildAt(1).alpha = 0f*/

            alpha_block_rl.alpha = 0f

        }

    }

    override fun onPageSelected(position: Int) {

    }
}