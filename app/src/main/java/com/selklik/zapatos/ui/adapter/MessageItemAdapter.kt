package com.selklik.zapatos.ui.adapter

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.selklik.zapatos.R
import com.selklik.zapatos.VideoPlayerActivity
import com.selklik.zapatos.model.Messages
import com.selklik.zapatos.utils.AppController
import com.selklik.zapatos.utils.TypefaceHelper
import kotlinx.android.synthetic.main.item_list_message.view.*

/**
 * Created by faridhadiselklik on 15/12/2017.
 */
class MessageItemAdapter(private var mContext: Context, private var messagesArrayList: ArrayList<Messages>) : RecyclerView.Adapter<MessageItemAdapter.ViewHolder>() {
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        holder.bindItems(messagesArrayList[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        val itemView = LayoutInflater.from(parent!!.context).inflate(R.layout.item_list_message, parent, false)
        return ViewHolder(itemView)
    }

    override fun getItemCount(): Int {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        return messagesArrayList.size
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(messages: Messages) {

            itemView.txt_answer_time.typeface = TypefaceHelper().setTypeface("light")
            itemView.txt_answer_time.text = AppController.instance.setPrettyDateFormat(messages.timestamp)

            when (messages.kind) {
                "question" -> setQustionMessage(messages)
                "answer" -> setAnswerMessage(messages)
            }

        }

        private fun setQustionMessage(messages: Messages){
            itemView.img_artist_profile.setImageUrl(messages.user.profile_photos.full_resolution, AppController.instance.getImageLoader())
            itemView.txt_artist_name.typeface = TypefaceHelper().setTypeface("semi-bold")
            itemView.txt_artist_name.text = messages.user.name
            itemView.layout_attach_qna.visibility = View.GONE
            itemView.txt_body.typeface = TypefaceHelper().setTypeface("regular")
            itemView.txt_body.text = messages.question
        }

        private fun setAnswerMessage(messages: Messages){
            itemView.img_attach_qna.setImageUrl(messages.answer.screenshots.full_resolution, AppController.instance.getImageLoader())
            itemView.txt_artist_name.typeface = TypefaceHelper().setTypeface("semi-bold")
            itemView.txt_artist_name.text = messages.artist.name
            itemView.layout_attach_qna.visibility = View.VISIBLE
            itemView.img_artist_profile.setImageUrl(messages.artist.profile_photos.full_resolution, AppController.instance.getImageLoader())
            itemView.txt_body.visibility = View.GONE
            itemView.ic_play_video_qna.setOnClickListener{
                mContext.startActivity(Intent(mContext, VideoPlayerActivity::class.java).putExtra("video_url", messages.answer.video.url))
            }
        }
    }
}