package com.selklik.zapatos.ui

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.VolleyError
import com.google.gson.Gson
import com.google.gson.JsonParser
import com.google.gson.reflect.TypeToken
import com.selklik.zapatos.R
import com.selklik.zapatos.model.Address
import com.selklik.zapatos.model.Card
import com.selklik.zapatos.model.Products
import com.selklik.zapatos.utils.*
import com.selklik.zapatos.utils.helper.AlertDialogHelper
import com.selklik.zapatos.utils.volley.ApiHelper
import kotlinx.android.synthetic.main.activity_buy_product.*
import kotlinx.android.synthetic.main.content_buy_product.*
import java.util.HashMap
import com.selklik.zapatos.model.Order


class BuyProductActivity : AppCompatActivity() {
    private var TAG = BuyProductActivity::class.java.simpleName
    private var productData: String? = null
    private var product: Products? = null
    private inline fun <reified T> genericType() = object : TypeToken<T>() {}.type!!
    private var dialog: AlertDialog? = null
    private var addressArrayList: ArrayList<Address>? = null
    private var cardArrayList: ArrayList<Card>? = null
    private var card: Card? = null
    private var address: Address? = null
    private val REQUEST_MANAGE_CARD = 100
    private val REQUEST_MANAGE_ADDRESS = 101
    private val REQUEST_PAYMENT_BILPLZ = 102

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_buy_product)
        var gson = Gson()
        var intent = intent
        productData = intent.getStringExtra("productData")


        setSupportActionBar(toolbar)
        val ab = supportActionBar!!
        ab!!.setHomeButtonEnabled(true)
        ab!!.setDisplayHomeAsUpEnabled(true)
        ab!!.setDisplayShowTitleEnabled(false)
        txt_toolbar_title.typeface = TypefaceHelper().setTypeface("regular")

        productData?.let {
            product = gson.fromJson(intent.getStringExtra("productData"), Products::class.java)
            img_item_to_purchase.setImageUrl(product!!.photos[0].asJsonObject.get("full_resolution").asString, AppController.instance.getImageLoader())
            txt_item_to_purchase.text = product!!.title
            text_sub_total.text = product!!.price
            txt_price_item_to_purchase.text = product!!.price
        } ?: run {
            finish()
        }

        txt_item_to_purchase.typeface = TypefaceHelper().setTypeface("regular")
        text_title_item_to_purchase.typeface = TypefaceHelper().setTypeface("bold")
        txt_price_item_to_purchase.typeface = TypefaceHelper().setTypeface("regular")

        txt_card_number_payment_method.typeface = TypefaceHelper().setTypeface("regular")
        text_title_payment_method.typeface = TypefaceHelper().setTypeface("bold")
        text_add_payment_method.text = "Manage"
        text_add_payment_method.typeface = TypefaceHelper().setTypeface("light")

        text_add_address.typeface = TypefaceHelper().setTypeface("light")
        text_add_address.text = "Manage"

        text_sub_total_title.typeface = TypefaceHelper().setTypeface("regular")
        text_sub_total.typeface = TypefaceHelper().setTypeface("semi-bold")
        button_buy_product.alpha = .5f
        text_add_payment_method.text = "Manage"

        var userCardData = AppController.instance.getUserCardData()
        Log.d("buyproduct", "user card : " + userCardData.isNullOrEmpty())

        AppController.instance.fetchCard(context = this) { response ->
            response?.let {
                Log.d(TAG, "card response class : ${it!!.javaClass.simpleName}")
                if (it!!.javaClass.simpleName == "String") {
                    (it as String).let {
                        card = gson.fromJson(it, Card::class.java)
                        Log.d(TAG, "card response class : ${card!!.is_default}")
                        setCardUi(card!!)
                        text_add_payment_method.setOnClickListener {
                            startActivityForResult(Intent(this@BuyProductActivity, ManagePaymentMethodActivity::class.java), REQUEST_MANAGE_CARD)
                        }
                    }
                } else {
                    layout_payment_method_details.visibility = View.GONE
                    layout_no_payment_method.visibility = View.VISIBLE
                    txt_no_payment_method.typeface = TypefaceHelper().setTypeface("regular")
                    btn_add_card.setOnClickListener(onAddCardClick)
                }
            }

            AppController.instance.fetchUserAddressDefault(this,
                    onSuccess = { response: String? ->
                        response.let {
                            Log.d(TAG, "card default : $response")
                            if (response != null) {
                                address = gson.fromJson(response, Address::class.java)
                                setAddressUi(address!!)
                                text_add_address.setOnClickListener {
                                    startActivityForResult(Intent(this@BuyProductActivity, ManageAddressesActivity::class.java), REQUEST_MANAGE_ADDRESS)
                                }
                            } else {
                                layout_no_address.visibility = View.VISIBLE
                                txt_no_address.typeface = TypefaceHelper().setTypeface("regular")
                                layout_address_details.visibility = View.GONE
                                btn_add_address.setOnClickListener(onAddAddressClick)
                            }
                        }

                        if (card != null && address != null) {
                            button_buy_product.alpha = 1f
                            button_buy_product.setOnClickListener {
                                makeOrder(card!!.id, address!!.id)
                            }
                        }
                    },
                    onError = { error: VolleyError ->
                        //                    ApiHelper(this).errorHandler(error)
                        layout_no_address.visibility = View.VISIBLE
                        txt_no_address.typeface = TypefaceHelper().setTypeface("regular")
                        layout_address_details.visibility = View.GONE
                        btn_add_address.setOnClickListener(onAddAddressClick)

                        if (card != null && address != null) {
                            button_buy_product.alpha = 1f
                            button_buy_product.setOnClickListener {
                                makeOrder(card!!.id, address!!.id)
                            }
                        }
                    })
        }
    }

    private fun setAddressUi(address: Address) {
        layout_no_address.visibility = View.GONE
        layout_address_details.visibility = View.VISIBLE
        txt_address.text = address!!.formatted
        txt_address.typeface = TypefaceHelper().setTypeface("regular")
        text_title_address.typeface = TypefaceHelper().setTypeface("bold")
        text_add_address.typeface = TypefaceHelper().setTypeface("light")
        text_add_address.text = "Manage"
    }

    private fun setCardUi(card: Card) {
        text_add_payment_method.typeface = TypefaceHelper().setTypeface("light")
        layout_payment_method_details.visibility = View.VISIBLE
        layout_no_payment_method.visibility = View.GONE

        when (card.kind) {
            "fpx" -> {
                txt_card_number_payment_method.text = card.brand
                img_card_payment_method.setImageDrawable(ContextCompat.getDrawable(this@BuyProductActivity, R.drawable.ic_payment_fpx))
            }
            else -> {
                when (card.brand) {
                    "Visa" -> {
                        img_card_payment_method.setImageDrawable(ContextCompat.getDrawable(this@BuyProductActivity, R.drawable.ic_card_visa))
                    }
                    "mastercard" -> {
                        img_card_payment_method.setImageDrawable(ContextCompat.getDrawable(this@BuyProductActivity, R.drawable.ic_card_master))

                    }
                    else -> {
                        img_card_payment_method.setImageDrawable(ContextCompat.getDrawable(this@BuyProductActivity, R.drawable.ic_card_default))
                    }

                }
                txt_card_number_payment_method.text = "\u2022\u2022\u2022\u2022 \u2022\u2022\u2022\u2022 \u2022\u2022\u2022\u2022 " + card.last4
            }
        }
    }

    private var onAddAddressClick: View.OnClickListener = View.OnClickListener {
        AlertDialogHelper().showAddAddressDialog(this,
                onAddressResponse = { response: String ->
                    address = Gson().fromJson(response, Address::class.java)
                    setAddressUi(address!!)
                    if (card != null && address != null) {
                        button_buy_product.alpha = 1f
                        button_buy_product.setOnClickListener {
                            makeOrder(card!!.id, address!!.id)
                        }
                    }
                },
                onError = { errorString: String?, error: VolleyError? ->
                    if (errorString != null) {
                        Toast.makeText(this, "No internet Connection", Toast.LENGTH_SHORT).show()
                    } else {
                        ApiHelper(this).errorHandler(error!!)
                    }
                })
    }

    private var onAddCardClick: View.OnClickListener = View.OnClickListener { view: View? ->
        AlertDialogHelper().showAddCardDialog(this,
                onSubmitClick = { response: String ->
                    showProgressDialog()
                },
                onCardReady = { cardSourceId: String ->
                    Log.d(TAG, "card Source id : $cardSourceId")
                    AppController.instance.postCard(context = this,
                            sourceId = cardSourceId,
                            onSuccess = { response: String? ->

                                card = Gson().fromJson(response, Card::class.java)
                                setCardUi(card!!)
                                if (card != null && address != null) {
                                    button_buy_product.alpha = 1f
                                    button_buy_product.setOnClickListener {
                                        makeOrder(cardId = card!!.id, addressId = address!!.id)
                                    }
                                }
                            },
                            onError = { errorString: String?, error: VolleyError? ->

                                if (errorString != null) {
                                    Toast.makeText(this, "No internet Connection", Toast.LENGTH_SHORT).show()
                                } else {
                                    ApiHelper(this).errorHandler(error!!)
                                }
                            })
                    if (dialog!!.isShowing) {
                        dialog!!.dismiss()
                    }

                })
    }

    private fun makeOrder(cardId: String, addressId: String) {
        showProgressDialog()
        if (!ConnectivityReceiver.isConnected()) {
            Log.d("Add Card", "No internet connection")
        } else {
            val jsonParams = HashMap<String, String>()
            jsonParams["access_token"] = AppController.instance.getUserAccessToken()
            jsonParams["product_id"] = product!!.id
            jsonParams["card_id"] = cardId
            jsonParams["address_id"] = addressId
            ApiHelper(this).makeRequest(
                    Config.API_ME + "/orders",
                    Request.Method.POST,
                    jsonParams,
                    onSuccess = { responseString: String, gson: Gson ->
                        Log.d(TAG, "make order response string : " + responseString)
                        val jsonResponse = JsonParser().parse(responseString).asJsonObject
                        if (jsonResponse.has("url")) {
//                            val chromeCustomTab = ChromeCustomTab(this, jsonResponse.get("url").asString)
//                            chromeCustomTab.warmup()
//                            chromeCustomTab.mayLaunch()
//                            chromeCustomTab.show()
                            val intent = Intent(this, BrowserPaymentActivity::class.java)
                            intent.putExtra("url", jsonResponse.get("url").asString)
                            intent.putExtra("orderData", productData)
                            startActivityForResult(intent, REQUEST_PAYMENT_BILPLZ)
                        } else {
                            val order = gson.fromJson(responseString, Order::class.java)
                            val intent = Intent()
                            intent.putExtra("orderId", order.id)
                            setResult(Activity.RESULT_OK, intent)
                            finish()

                        }
                        if (dialog!!.isShowing) {
                            dialog!!.dismiss()
                        }

                    },
                    onError = { error: VolleyError ->
                        ApiHelper(this).errorHandler(error)
                        if (dialog!!.isShowing) {
                            dialog!!.dismiss()
                        }
                    })
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        when (id) {
            android.R.id.home -> onBackPressed()
            else -> return false
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            REQUEST_MANAGE_CARD -> {
                AppController.instance.fetchCard(context = this) { response ->
                    response?.let {
                        Log.d(TAG, "card response class : ${it!!.javaClass.simpleName}")
                        if (it!!.javaClass.simpleName == "String") {
                            (it as String).let {
                                card = Gson().fromJson(it, Card::class.java)
                                Log.d(TAG, "card response class : ${card!!.is_default}")
                                setCardUi(card!!)
                                text_add_payment_method.setOnClickListener {
                                    startActivityForResult(Intent(this@BuyProductActivity, ManagePaymentMethodActivity::class.java), REQUEST_MANAGE_CARD)
                                }
                            }
                        } else {
                            layout_payment_method_details.visibility = View.GONE
                            layout_no_payment_method.visibility = View.VISIBLE
                            txt_no_payment_method.typeface = TypefaceHelper().setTypeface("regular")
                            btn_add_card.setOnClickListener(onAddCardClick)
                        }
                    }
                }
            }
            REQUEST_MANAGE_ADDRESS -> {
                AppController.instance.fetchUserAddressDefault(this,
                        onSuccess = { response: String? ->
                            Log.d(TAG, "card default : $response")
                            response?.let {
                                if (response != null) {
                                    address = Gson().fromJson(response, Address::class.java)
                                    setAddressUi(address!!)
                                    text_add_address.setOnClickListener {
                                        startActivityForResult(Intent(this@BuyProductActivity, ManageAddressesActivity::class.java), REQUEST_MANAGE_CARD)
                                    }
                                } else {
                                    layout_no_address.visibility = View.VISIBLE
                                    txt_no_address.typeface = TypefaceHelper().setTypeface("regular")
                                    layout_address_details.visibility = View.GONE
                                    btn_add_address.setOnClickListener(onAddAddressClick)
                                }
                            }

                            if (card != null && address != null) {
                                button_buy_product.alpha = 1f
                                button_buy_product.setOnClickListener {
                                    makeOrder(card!!.id, address!!.id)
                                }
                            }
                        },
                        onError = { error: VolleyError ->
                            //                    ApiHelper(this).errorHandler(error)
                            layout_no_address.visibility = View.VISIBLE
                            txt_no_address.typeface = TypefaceHelper().setTypeface("regular")
                            layout_address_details.visibility = View.GONE
                            btn_add_address.setOnClickListener(onAddAddressClick)

                            if (card != null && address != null) {
                                button_buy_product.alpha = 1f
                                button_buy_product.setOnClickListener {
                                    makeOrder(card!!.id, address!!.id)
                                }
                            }
                        })
            }
            REQUEST_PAYMENT_BILPLZ -> {
                if(resultCode == Activity.RESULT_OK) {

                    Log.d("ButProductActivity", "Result ok : id ${data!!.getStringExtra("orderId")}")
                    data!!.getStringExtra("orderId")?.let {
                        var intent = Intent()
                        intent.putExtra("orderId", data!!.getStringExtra("orderId"))
                        setResult(Activity.RESULT_OK, intent)
                        finish()
                    }
                }
            }
        }
    }

    private fun showProgressDialog() {
        val mBuilder = AlertDialog.Builder(this, R.style.CustomDialogTheme)
        val mView = LayoutInflater.from(this).inflate(R.layout.dialog_progress, null)

        mBuilder.setView(mView)
        mBuilder.setCancelable(false)
        dialog = mBuilder.create()
        dialog!!.show()
    }

}

