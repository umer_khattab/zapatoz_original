package com.selklik.zapatos.ui

import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.VolleyError
import com.google.gson.Gson
import com.selklik.zapatos.R
import com.selklik.zapatos.utils.AppController
import com.selklik.zapatos.utils.Config
import com.selklik.zapatos.utils.ConnectivityReceiver
import com.selklik.zapatos.utils.TypefaceHelper
import com.selklik.zapatos.utils.volley.ApiHelper

import kotlinx.android.synthetic.main.activity_add_new_question.*
import kotlinx.android.synthetic.main.content_add_new_question.*
import java.util.HashMap

class AddNewQuestionActivity : AppCompatActivity() {
    var price: Int = 2000
    var dialog: AlertDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_new_question)
        setSupportActionBar(toolbar)
        val ab = supportActionBar
        ab!!.setHomeButtonEnabled(true)
        ab.setDisplayHomeAsUpEnabled(true)
        ab.setDisplayShowTitleEnabled(false)
        txt_toolbar_title.typeface = TypefaceHelper().setTypeface("regular")

        btn_add_question.typeface = TypefaceHelper().setTypeface("regular")
        btn_add_question.isEnabled = false
        btn_add_question.alpha = .5f

        edittext_ask_question.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (!s.toString().isEmpty()) {
                    btn_add_question.isEnabled = true
                    btn_add_question.alpha = 1f
                } else {
                    btn_add_question.isEnabled = false
                    btn_add_question.alpha = .5f

                }
            }

            override fun afterTextChanged(s: Editable) {
                if (!s.toString().isEmpty()) {
                    btn_add_question.isEnabled = true
                    btn_add_question.alpha = 1f
                } else {
                    btn_add_question.isEnabled = false
                    btn_add_question.alpha = .5f

                }
            }
        })
        radio_ask_question.setOnCheckedChangeListener(RadioGroup.OnCheckedChangeListener { group, checkedId ->
            val rb = group.findViewById<RadioButton>(checkedId) as RadioButton
            val idx = radio_ask_question.indexOfChild(rb)

            if (null != rb && checkedId > -1) {
                price = when (idx) {
                    0 -> 2000
                    1 -> 4000
                    2 -> 8000
                    3 -> 10000
                    4 -> 20000
                    else -> 2000
                }

                Log.d("AddNewQuestion", "price : " + price)
            }
        })

        btn_add_question.setOnClickListener {
            showSubmitDialog(edittext_ask_question.text.toString(), price!!.toString())
        }
    }

    private fun showSubmitDialog(txtQuestion: String, price: String) {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Are you sure?")
        builder.setMessage("Ask question for $price stars")

        val positiveText = "OK"
        builder.setPositiveButton(positiveText) { dialog, which ->
            dialog.dismiss()
            Thread(Runnable {
                // do the thing that takes a long time
                runOnUiThread {
                    postNewQuestion(txtQuestion, price)
                }
            }).start()
        }

        val negativeText = "Cancel"
        builder.setNegativeButton(negativeText,
                object : DialogInterface.OnClickListener {
                    override fun onClick(dialog: DialogInterface, which: Int) {
                        // negative button logic
                        dialog.dismiss()
                    }
                })

        val dialog = builder.create()
        dialog.setOnShowListener(object : DialogInterface.OnShowListener {
            override fun onShow(arg0: DialogInterface) {
                dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(ContextCompat.getColor(this@AddNewQuestionActivity, R.color.colorSelklikMain))
                dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(this@AddNewQuestionActivity, R.color.colorSelklikMain))
            }
        })

        // display dialog
        dialog.show()
    }

    private fun postNewQuestion(txtQuestion: String, price: String) {
        showProgressDialog()
        if (!ConnectivityReceiver.isConnected()) {
            if (dialog!!.isShowing) {
                dialog!!.dismiss()
            }
            Log.d("Add Card", "No internet connection")
        } else {
            val jsonParams = HashMap<String, String>()
            jsonParams.put("access_token", AppController.instance.getUserAccessToken())
            jsonParams.put("question", txtQuestion)
            jsonParams.put("price", price)

            ApiHelper(this).makeRequest(
                    Config.API_ARTIST +"/"+Config.ARTIST_ID+"/messages",
                    Request.Method.POST,
                    jsonParams,
                    false,
                    false,
                    onSuccess = { responseString: String, gson: Gson ->
                        Log.d("QnA Fragment", "response string : " + responseString)
                        if (dialog!!.isShowing) {
                            dialog!!.dismiss()
                        }
                        if (responseString == "[]") {
                            Toast.makeText(this@AddNewQuestionActivity, "Fail to ask question", Toast.LENGTH_SHORT).show()
                        } else {
                            val intent = Intent()
                            setResult(Activity.RESULT_OK, intent)
                            finish()
                        }

                    },
                    onError = { error: VolleyError ->
                        ApiHelper(this).errorHandler(error)
                        if (dialog!!.isShowing) {
                            dialog!!.dismiss()
                        }
                    })
        }
    }

    private fun showProgressDialog() {
        val mBuilder = AlertDialog.Builder(this, R.style.CustomDialogTheme)
        val mView = LayoutInflater.from(this).inflate(R.layout.dialog_progress, null)

        mBuilder.setView(mView)
        mBuilder.setCancelable(false)
        dialog = mBuilder.create()
        dialog!!.show()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        when (id) {
            android.R.id.home -> onBackPressed()
            else -> return false
        }
        return true
    }

}
