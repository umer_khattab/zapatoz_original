package com.selklik.zapatos.ui

import android.app.ProgressDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.net.http.SslError
import android.os.Bundle
import android.support.v4.content.ContextCompat.startActivity
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.MenuItem
import android.webkit.*
import android.widget.Toast
import com.selklik.zapatos.R
import com.selklik.zapatos.utils.TypefaceHelper
import kotlinx.android.synthetic.main.activity_browser.*
import android.support.v4.content.ContextCompat.startActivity
import com.google.gson.Gson
import com.selklik.zapatos.model.Order


/**
 * Created by faridhadiselklik on 21/12/2017.
 */
class BrowserReceiptActivity : AppCompatActivity() {

    private var progDailog: ProgressDialog? = null
    private var gson = Gson()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_browser)
        setSupportActionBar(toolbar)

        val intent = intent
        val url = intent.getStringExtra("url")
        val orderData = intent.getStringExtra("orderData")
        val order = gson.fromJson(orderData, Order::class.java)

        val ab = supportActionBar
        ab!!.setHomeButtonEnabled(true)
        ab.setDisplayHomeAsUpEnabled(true)
        ab.setDisplayShowTitleEnabled(false)
        txt_toolbar_title.typeface = TypefaceHelper().setTypeface("semi-bold")

        txt_toolbar_title.text = "Receipt"

        progDailog = ProgressDialog.show(this, "Loading", "Please wait...", true)
        progDailog!!.setCancelable(false)
        webview.settings.javaScriptEnabled = true
        webview.settings.javaScriptCanOpenWindowsAutomatically = true
        webview.loadUrl(url)

        webview.webViewClient = object : WebViewClient() {
            override fun onReceivedSslError(view: WebView, handler: SslErrorHandler, error: SslError) {

                val builder = AlertDialog.Builder(this@BrowserReceiptActivity)
                val alertDialog = builder.create()
                var message = "SSL Certificate error."
                when (error.primaryError) {
                    SslError.SSL_UNTRUSTED -> message = "The certificate authority is not trusted."
                    SslError.SSL_EXPIRED -> message = "The certificate has expired."
                    SslError.SSL_IDMISMATCH -> message = "The certificate Hostname mismatch."
                    SslError.SSL_NOTYETVALID -> message = "The certificate is not yet valid."
                }

                message += " Do you want to continue anyway?"
                alertDialog.setTitle("SSL Certificate Error")
                alertDialog.setMessage(message)
                alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK") { dialog, which ->
                    // Ignore SSL certificate errors
                    handler.proceed()
                }

                alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel") { dialog, which -> handler.cancel() }
                alertDialog.show()
            }

            override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                view.loadUrl(url)
                progDailog!!.show()
                return true
            }

            override fun onPageFinished(view: WebView, url: String) {
                Log.d("Browser Activity", "finished")
                progDailog!!.dismiss()
            }

            override fun onReceivedError(view: WebView, request: WebResourceRequest, error: WebResourceError) {
                super.onReceivedError(view, request, error)

                Log.d("Browser Activity", "error")
                Toast.makeText(this@BrowserReceiptActivity, "Failed load", Toast.LENGTH_SHORT)
                progDailog!!.dismiss()

            }
        }

        webview.addJavascriptInterface(WebAppInterface(this, order.reference_id), "Android")

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        when (id) {
            android.R.id.home -> onBackPressed()
            else -> return false
        }
        return true
    }
}

class WebAppInterface(context: Context, reference_id: String) {
    var mContext = context
    var referenceId = reference_id
    /** Show a toast from the web page */
//    @JavascriptInterface
//    fun showToast(toast: String) {
//        Toast.makeText(mContext, toast, Toast.LENGTH_SHORT).show();
//    }

    @JavascriptInterface
    fun buttonClicked() {
        val testIntent = Intent(Intent.ACTION_VIEW)
        val data = Uri.parse("mailto:?subject=Order id : $referenceId&to=support@selklik.com")
        testIntent.data = data
        startActivity(mContext, testIntent, null)
    }
}