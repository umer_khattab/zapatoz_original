package com.selklik.zapatos.ui

import android.app.ProgressDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.net.http.SslError
import android.os.Build
import android.os.Bundle
import android.support.v4.content.ContextCompat.startActivity
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.MenuItem
import android.webkit.*
import android.widget.Toast
import com.selklik.zapatos.R
import com.selklik.zapatos.utils.TypefaceHelper
import kotlinx.android.synthetic.main.activity_browser.*
import android.support.v4.content.ContextCompat.startActivity



class BrowserActivity : AppCompatActivity() {

    private var progDailog: ProgressDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_browser)
        setSupportActionBar(toolbar)

        val intent = intent
        val url = intent.getStringExtra("url")
        val from = intent.getStringExtra("from")

        val ab = supportActionBar
        ab!!.setHomeButtonEnabled(true)
        ab.setDisplayHomeAsUpEnabled(true)
        ab.setDisplayShowTitleEnabled(false)
        txt_toolbar_title.typeface = TypefaceHelper().setTypeface("semi-bold")

        txt_toolbar_title.text = from
        Log.d("TAG", "URL : " + url)

        progDailog = ProgressDialog.show(this, "Loading", "Please wait...", true)
        progDailog!!.setCancelable(false)
        webview.settings.javaScriptEnabled = true
        webview.settings.javaScriptCanOpenWindowsAutomatically = true

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            webview.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_COMPATIBILITY_MODE)
        };

        webview.loadUrl(url)
        webview.webViewClient = object : WebViewClient() {
            override fun onReceivedSslError(view: WebView, handler: SslErrorHandler, error: SslError) {
                handler.proceed()

                val builder = AlertDialog.Builder(this@BrowserActivity)
                val alertDialog = builder.create()
                var message = "SSL Certificate error."
                when (error.primaryError) {
                    SslError.SSL_UNTRUSTED -> message = "The certificate authority is not trusted."
                    SslError.SSL_EXPIRED -> message = "The certificate has expired."
                    SslError.SSL_IDMISMATCH -> message = "The certificate Hostname mismatch."
                    SslError.SSL_NOTYETVALID -> message = "The certificate is not yet valid."
                }

                message += " Do you want to continue anyway?"
                alertDialog.setTitle("SSL Certificate Error")
                alertDialog.setMessage(message)
                alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK") { dialog, which ->
                    // Ignore SSL certificate errors
                    handler.proceed()
                }

                alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel") { dialog, which -> handler.cancel() }
                alertDialog.show()
            }

            override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                view.loadUrl(url)
                progDailog!!.show()

                Log.d("Hello", url.toString())

                return false
            }

            override fun onPageFinished(view: WebView, url: String) {
                Log.d("Browser Activity", "finished")
                progDailog!!.dismiss()
            }

            override fun onReceivedError(view: WebView, request: WebResourceRequest, error: WebResourceError) {
                super.onReceivedError(view, request, error)

                Log.d("Browser Activity", "error")
                Toast.makeText(this@BrowserActivity, "Failed load", Toast.LENGTH_SHORT)
                progDailog!!.dismiss()

            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        when (id) {
            android.R.id.home -> onBackPressed()
            else -> return false
        }
        return true
    }
}