package com.selklik.zapatos.ui.adapter

import android.view.View
import android.content.Context
import android.support.v4.content.ContextCompat
import android.support.v4.view.PagerAdapter
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.RelativeLayout
import com.selklik.zapatos.R
import com.selklik.zapatos.model.Photo
import com.selklik.zapatos.utils.AppController
import com.selklik.zapatos.utils.TypefaceHelper
import kotlinx.android.synthetic.main.pager_item_product_image.view.*

/**
 * Created by faridhadiselklik on 13/12/2017.
 */
class ProductImagePagerAdapter(var context: Context, private var productImageList: List<Photo>): PagerAdapter() {

    private val layoutInflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        return view === `object` as RelativeLayout
    }

    override fun getCount(): Int {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        return productImageList.size
    }
    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val itemView = layoutInflater.inflate(R.layout.pager_item_product_image, container, false)
        println(productImageList[position].full_resolution)
        itemView.img_product.setImageUrl(productImageList[position].full_resolution, AppController.instance.getImageLoader())
        container!!.addView(itemView)
        return itemView
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as RelativeLayout)
    }
}