package com.selklik.zapatos.ui

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import com.android.volley.Request
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.selklik.zapatos.R
import com.selklik.zapatos.model.Order
import com.selklik.zapatos.model.Products
import com.selklik.zapatos.ui.adapter.FeaturedProductAdapter
import com.selklik.zapatos.utils.AppController
import com.selklik.zapatos.utils.Config
import com.selklik.zapatos.utils.ConnectivityReceiver
import com.selklik.zapatos.utils.DividerItemDecoration
import com.selklik.zapatos.utils.volley.ApiHelper

import kotlinx.android.synthetic.main.activity_product.*
import kotlinx.android.synthetic.main.content_product.*
import java.util.HashMap

class ProductActivity : AppCompatActivity() {

    private var TAG = ProductActivity::class.java.simpleName
    inline fun <reified T> genericType() = object : TypeToken<T>() {}.type!!
    private var gson: Gson = Gson()
    private var dialog: AlertDialog? = null
    private var mLayoutManager: RecyclerView.LayoutManager? = null
    private var visibleItemCount: Int = 0
    private var totalItemCount: Int = 0
    val buyProductRequestCode = 888

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product)
        setSupportActionBar(toolbar)
        val ab = supportActionBar
        ab!!.setHomeButtonEnabled(true)
        ab.setDisplayHomeAsUpEnabled(true)
        ab.setDisplayShowTitleEnabled(false)

        mLayoutManager = LinearLayoutManager(this)
        product_recycler_view.addItemDecoration(DividerItemDecoration(ContextCompat.getDrawable(this!!, R.drawable.item_decorator)!!))
        product_recycler_view.layoutManager = mLayoutManager
        product_recycler_view.addOnScrollListener(productScrollListener)

        fetchFeaturedProduct()
    }

    private var productScrollListener: RecyclerView.OnScrollListener = object : RecyclerView.OnScrollListener() {
        internal var scrollItem: Int = 0

        override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
            val layoutManager = recyclerView!!.layoutManager as LinearLayoutManager
            var firstVisiblesItems = 0


            if (dy > 0) {
                visibleItemCount = mLayoutManager!!.childCount
                totalItemCount = mLayoutManager!!.itemCount
                firstVisiblesItems = layoutManager.findFirstVisibleItemPosition()

                scrollItem += visibleItemCount

//                if (visibleItemCount + firstVisiblesItems >= totalItemCount && !loadMore) {
//                    Log.d(TAG, "load more")
//                    loadMore = true
//                    page += 1
//                    Log.d(TAG, "Page : " + page)
//                    fetchArtistFeed()
//                }
            } else {
                // int topPosition = pos - visibleItemCount;
                firstVisiblesItems = layoutManager.findFirstVisibleItemPosition()
                swipeRefreshLayout.isEnabled = firstVisiblesItems <= 0
            }
        }
    }

    private fun fetchFeaturedProduct() {
        showProgressDialog()
        if (!ConnectivityReceiver.isConnected()) {
            if (dialog!!.isShowing) {
                dialog!!.dismiss()
            }
            Log.d(TAG, "No internet connection")
        } else {
            val jsonParams = HashMap<String, String>()
            jsonParams.put("access_token", AppController.instance.getUserAccessToken()!!)

            ApiHelper(this).makeRequest(
                    Config.API_ARTIST + "/"+Config.ARTIST_ID+"/ProductsShoes",
                    Request.Method.GET,
                    jsonParams,
                    true,
                    false,
                    onSuccess = { responseString: String, gson: Gson ->
                        Log.d(TAG, "featured product : " + responseString)
                        var featuredProductArrayList: ArrayList<Products> = gson.fromJson(responseString, genericType<ArrayList<Products>>())
//                        var newFeaturedProductArraylist: ArrayList<Products> = ArrayList()
//                        (0 until 5).mapTo(newFeaturedProductArraylist) { featuredProductArrayList[it] }



                        Log.d(TAG, "featured product : " + featuredProductArrayList.size)

                        product_recycler_view.adapter = FeaturedProductAdapter(this!!,
                                featuredProductArrayList!!,
                                onItemViewClick = {
                                    product: Products ->
                                    startActivityForResult(Intent(this, ProductDetailsActivity::class.java)
                                            .putExtra("productData", Gson().toJson(product)),
                                            buyProductRequestCode)
                                })
                        product_recycler_view.isNestedScrollingEnabled = false
                        product_recycler_view.visibility = View.VISIBLE

//                        product_progress_bar.visibility = View.GONE
                        if (dialog!!.isShowing) {
                            dialog!!.dismiss()
                        }

                    },
                    onError = {
                        if (dialog!!.isShowing) {
                            dialog!!.dismiss()
                        }
                    })
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        when (id) {
            android.R.id.home -> onBackPressed()
            else -> return false
        }
        return true
    }

    private fun showProgressDialog() {
        val mBuilder = AlertDialog.Builder(this, R.style.CustomDialogTheme)
        val mView = LayoutInflater.from(this).inflate(R.layout.dialog_progress, null)

        mBuilder.setView(mView)
        mBuilder.setCancelable(false)
        dialog = mBuilder.create()
        dialog!!.show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            buyProductRequestCode -> if (resultCode == Activity.RESULT_OK) {
                Log.d("HomeFragment", "Result ok")
                fetchFeaturedProduct()
                data!!.getStringExtra("orderData")?.let {
                    val order = gson.fromJson(it, Order::class.java)
                    var url = "https://selklik.xyz/receipts/${order.id}?access_token=${AppController.instance.getUserAccessToken()}"
                    startActivity(Intent(this, BrowserActivity::class.java).putExtra("url", url).putExtra("from", "Receipt"))
                }
            } else {
                Log.d("HomeFragment", "Result cancel")
            }
        }
    }

}
