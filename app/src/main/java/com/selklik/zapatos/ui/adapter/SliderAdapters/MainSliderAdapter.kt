package com.selklik.zapatos.ui.adapter.SliderAdapters

import android.util.Log
import ss.com.bannerslider.adapters.SliderAdapter
import ss.com.bannerslider.viewholder.ImageSlideViewHolder

class MainSliderAdapter(arrayOfurls: ArrayList<String>) : SliderAdapter()
{

    private val arrayOfurls: ArrayList<String>

    init {

        this.arrayOfurls = arrayOfurls
    }

    override fun getItemCount(): Int {
        return (this.arrayOfurls.size)
    }

    override fun onBindImageSlide(position: Int, imageSlideViewHolder: ImageSlideViewHolder?) {

        imageSlideViewHolder!!.bindImageSlide(this.arrayOfurls.get(position))

    }


}