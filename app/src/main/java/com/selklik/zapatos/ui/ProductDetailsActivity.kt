package com.selklik.zapatos.ui

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.MenuItem
import android.view.View
import com.google.gson.Gson
import com.selklik.zapatos.R
import com.selklik.zapatos.model.Order

import kotlinx.android.synthetic.main.activity_product_details.*
import com.selklik.zapatos.model.Products
import com.selklik.zapatos.utils.AppController
import com.selklik.zapatos.utils.TypefaceHelper


class ProductDetailsActivity : AppCompatActivity() {
    var TAG = ProductDetailsActivity::class.java.simpleName
    var product: Products? = null
    var productData: String? = null
    private val buyProductRequestCode = 888

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_details)
        var gson = Gson()
        var intent = intent
        toolbar.navigationIcon = ContextCompat.getDrawable(this, R.drawable.ic_back_white)
        productData = intent.getStringExtra("productData")

        setSupportActionBar(toolbar)
        val ab = supportActionBar!!
//        ab!!.setHomeButtonEnabled(true)
//        ab!!.setDisplayHomeAsUpEnabled(true)
        ab!!.setDisplayShowTitleEnabled(false)

        toolbar.setNavigationOnClickListener {
            Log.d("Product Details", "nav clicked")
            onBackPressed()
        }

        product = gson.fromJson(intent.getStringExtra("productData"), Products::class.java)

        img_product.setImageUrl(product!!.photos[0].asJsonObject.get("full_resolution").asString, AppController.instance.getImageLoader())
        println(gson.toJson(product!!.photos))
        img_product.setOnClickListener { productData ->
            startActivity(Intent(this, ViewProductImageActivity::class.java).putExtra("imageData",gson.toJson(product!!.photos)))
        }
        txt_img.typeface = TypefaceHelper().setTypeface("semi-bold")
        txt_img.text = "${product!!.photos.size()} Image"
        textProductTitle.text = product!!.title
        textProductTitle.typeface = TypefaceHelper().setTypeface("semi-bold")
        textProductItemPrice.text = product!!.price
        textProductItemPrice.typeface = TypefaceHelper().setTypeface("bold")
        textProductDescription.text = product!!.details
        textProductDescription.typeface = TypefaceHelper().setTypeface("regular")
        textProductItemNoView.text = "${product!!.view_count} views"
        textProductItemNoView.typeface = TypefaceHelper().setTypeface("light")
        Log.d(TAG, "product ProductsShoes " + product!!.photos.size())

        if(product!!.bought_by != null){
            layout_sold_to.visibility = View.VISIBLE
            layout_buy_btn.visibility = View.GONE
            img_profile_sold_to.setImageUrl(product!!.bought_by.profile_photos.full_resolution, AppController.instance.getImageLoader())
            txt_name_sold_to.text = product!!.bought_by.name
            txt_time_sold_to.visibility = View.GONE
        } else {
            layout_sold_to.visibility = View.GONE
            layout_buy_btn.visibility = View.VISIBLE
            button_buy_product.setOnClickListener{
                startActivityForResult(Intent(this, BuyProductActivity::class.java)
                        .putExtra("productData", productData),
                        buyProductRequestCode)
            }
        }

        text_delivery_opt_title.typeface = TypefaceHelper().setTypeface("bold")

        txt_delivery_charge.typeface = TypefaceHelper().setTypeface("semi-bold")
        txt_delivery_charge.text = "FREE"
        txt_delivery_time.typeface = TypefaceHelper().setTypeface("light")
        txt_delivery_time.text = "2-3 days"

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            buyProductRequestCode -> if (resultCode == Activity.RESULT_OK) {
                Log.d("ProductDetails", "Result ok : id ${data!!.getStringExtra("orderId")}")
                data!!.getStringExtra("orderId")?.let {
                    var intent = Intent()
                    intent.putExtra("orderId", data!!.getStringExtra("orderId"))
                    setResult(Activity.RESULT_OK, intent)
                    finish()
                }

            } else {
                Log.d("ProductDetails", "Result cancel")
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        when (id) {
            android.R.id.home -> onBackPressed()
            else -> return false
        }
        return super.onOptionsItemSelected(item)
    }
}
