package com.selklik.zapatos.ui.adapter

import android.content.Context
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.selklik.zapatos.R
import com.selklik.zapatos.model.Card
import com.selklik.zapatos.utils.TypefaceHelper
import kotlinx.android.synthetic.main.item_list_card.view.*

/**
 * Created by faridhadiselklik on 15/12/2017.
 */
class CardItemAdapter(private var mContext: Context,
                      private var cardArrayList: ArrayList<Card>,
                      var onItemViewClick: (card: Card) -> Unit) : RecyclerView.Adapter<CardItemAdapter.ViewHolder>() {
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        holder.bindItems(cardArrayList[position], onItemViewClick)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        val itemView = LayoutInflater.from(parent!!.context).inflate(R.layout.item_list_card, parent, false)
        return ViewHolder(itemView)
    }

    override fun getItemCount(): Int {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        return cardArrayList.size
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(card: Card, onItemViewClick: (card: Card) -> Unit) {
            itemView.txt_card_number_payment_method.typeface = TypefaceHelper().setTypeface("semi-bold")

            when (card.kind){
                "fpx" -> {
                    itemView.txt_card_number_payment_method.text = card.brand
                    itemView.img_card_payment_method.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_payment_fpx))
                }
                else -> {
                    when (card.brand) {
                        "Visa" -> {
                            itemView.img_card_payment_method.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_card_visa))
                        }
                        "mastercard" -> {
                            itemView.img_card_payment_method.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_card_master))

                        }
                        else -> {
                            itemView.img_card_payment_method.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.ic_card_default))
                        }

                    }
                    itemView.txt_card_number_payment_method.text = "\u2022\u2022\u2022\u2022 \u2022\u2022\u2022\u2022 \u2022\u2022\u2022\u2022 " + card.last4

                }
            }


            if (card.is_default) {
                itemView.layout_card_default.visibility = View.VISIBLE
                itemView.txt_card_payment_method_default.typeface = TypefaceHelper().setTypeface("regular")
            } else {
                itemView.layout_card_default.visibility = View.GONE
            }
            itemView.setOnClickListener {
                if (!card.is_default) {
                    onItemViewClick(card)
                }
            }

        }


    }
}