package com.selklik.zapatos.ui.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.android.volley.Request
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.selklik.zapatos.R
import com.selklik.zapatos.model.Feed
import com.selklik.zapatos.model.User
import com.selklik.zapatos.ui.adapter.FeedItemAdapter
import com.selklik.zapatos.utils.*
import com.selklik.zapatos.utils.volley.ApiHelper
import kotlinx.android.synthetic.main.fragment_home_feed.*
import kotlinx.android.synthetic.main.layout_filter_dialog.view.*
import java.util.HashMap

/**
 * Created by faridhadiselklik on 29/11/2017.
 */
class FeedMainFragment : Fragment() {
    private var TAG: String = FeedMainFragment::class.java.simpleName
    private lateinit var user: User
    private var page: Int = 1
    private var userAccessToken: String? = null
    private var mLayoutManager: RecyclerView.LayoutManager? = null
    private var visibleItemCount: Int = 0
    private var totalItemCount: Int = 0
    private var loadMore = false
    private var feedArrayList: ArrayList<Feed>? = null
    private lateinit var feedItemAdapter: FeedItemAdapter
    private var type = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        userAccessToken = SharedPrefHelper().getSharedPreferenceString(context!!, Config.PREF_ACCESS_TOKEN,
                null)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.fragment_home_feed, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (activity as AppCompatActivity).setSupportActionBar(toolbar)
        (activity as AppCompatActivity).supportActionBar!!.setDisplayShowTitleEnabled(false)
        txt_toolbar_title.typeface = TypefaceHelper().setTypeface("regular")
        text_feed_category.typeface = TypefaceHelper().setTypeface("light")
        layout_filter.setOnClickListener {
            showFilterDialog()
        }

        mLayoutManager = LinearLayoutManager(this.context)
        feed_recycler_view.layoutManager = mLayoutManager

        fetchArtistFeed()

        feed_recycler_view.addOnScrollListener(feedScrollListener)

        swipeRefreshLayout.setOnRefreshListener {
//            Handler().postDelayed({
//                swipeRefreshLayout.isRefreshing = false
                page = 1
                fetchArtistFeed()
//            }, 2000)
        }
    }

    inline fun <reified T> genericType() = object : TypeToken<T>() {}.type!!

    private var feedScrollListener: RecyclerView.OnScrollListener = object : RecyclerView.OnScrollListener() {
        internal var scrollItem: Int = 0

        override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
            val layoutManager = recyclerView!!.layoutManager as LinearLayoutManager
            var firstVisiblesItems = 0


            if (dy > 0) {
                visibleItemCount = mLayoutManager!!.childCount
                totalItemCount = mLayoutManager!!.itemCount
                firstVisiblesItems = layoutManager.findFirstVisibleItemPosition()

                scrollItem += visibleItemCount

                if (visibleItemCount + firstVisiblesItems >= totalItemCount && !loadMore) {
                    Log.d(TAG, "load more")
                    loadMore = true
                    page += 1
                    Log.d(TAG, "Page : " + page)
                    fetchArtistFeed()
                }
            } else {
                // int topPosition = pos - visibleItemCount;
                firstVisiblesItems = layoutManager.findFirstVisibleItemPosition()
                swipeRefreshLayout.isEnabled = firstVisiblesItems <= 0
            }
        }
    }

    private fun fetchArtistFeed() {
        if (!ConnectivityReceiver.isConnected()) {
            Log.d(TAG, "No internet connection")
            if(context != null){
                Toast.makeText(context, "No internet connection", Toast.LENGTH_SHORT).show()
            }
        } else {
            if (page == 1 && feedArrayList != null){
                if (feedArrayList!!.size > 0 && feedItemAdapter != null){
                    feedArrayList!!.clear()
                    feedItemAdapter!!.notifyDataSetChanged()
                }

            }

            Log.d(TAG, "type : $type")
            val jsonParams = HashMap<String, String>()
            jsonParams.put("access_token", userAccessToken!!)
            jsonParams.put("type", type)
            jsonParams.put("page", Integer.toString(page))

            //------------------ GET FEEDS 1 -------------//

            ApiHelper(context!!).makeRequest(
                    module = Config.API_ARTIST + "/"+Config.ARTIST_ID+","+Config.ARTIST_ID_2+"/posts",
                    method = Request.Method.GET,
                    params = jsonParams,
                    onSuccess = { responseString: String, gson: Gson ->
                        if (responseString != "[]") {
                            when (page) {
                                1 -> {
                                    swipeRefreshLayout?.let {
                                        if (swipeRefreshLayout.isRefreshing) {
                                            swipeRefreshLayout.isRefreshing = false
                                        }
                                    }
                                    feedArrayList = gson.fromJson(responseString, genericType<ArrayList<Feed>>())
                                    Log.d(TAG, "responseString : " + responseString + ", feed size : " + feedArrayList!!.size)
                                    feedItemAdapter = FeedItemAdapter(context!!, feedArrayList!!)

                                    if (feed_recycler_view != null)
                                    {
                                        feed_recycler_view.adapter = feedItemAdapter
                                        feed_recycler_view.visibility = View.VISIBLE
                                    }
                                }
                                else -> {

                                    Log.d(TAG, "responseString : " + responseString + ", feed size : " + feedArrayList!!.size)
                                    var newFeedArrayList: ArrayList<Feed> = gson.fromJson(responseString, genericType<ArrayList<Feed>>())
                                    for (feed: Feed in newFeedArrayList){
                                        feedArrayList!!.add(feed)
                                    }
                                    feedItemAdapter.notifyDataSetChanged()
                                    feed_recycler_view.invalidate()
                                    loadMore = false
                                }
                            }

                        } else {
                            if (page > 1) {
                                Log.d(TAG, "No more feed")
                                if (context != null) {
                                    Toast.makeText(context, "No more feed", Toast.LENGTH_SHORT).show()
                                }
                                page -=1
                                loadMore = false
                            } else {

                            }
                        }
                    },
                    onError = {

                    })

        }
    }

    private fun showFilterDialog() {

        val mBuilder = AlertDialog.Builder(context!!, R.style.CustomDialogTheme)

        val mView = LayoutInflater.from(context).inflate(R.layout.layout_filter_dialog, null)

        mBuilder.setView(mView)
        val dialog = mBuilder.create()
        dialog.setCanceledOnTouchOutside(true);
//        Log.d(TAG, "preview url : " + previewUrl)
        when (type){
            "all"-> {
                mView.text_show_all.typeface = TypefaceHelper().setTypeface("bold")
                mView.text_show_premium.typeface = TypefaceHelper().setTypeface("regular")
                mView.text_show_social.typeface = TypefaceHelper().setTypeface("regular")
            }
            "premium"->{
                mView.text_show_all.typeface = TypefaceHelper().setTypeface("regular")
                mView.text_show_premium.typeface = TypefaceHelper().setTypeface("bold")
                mView.text_show_social.typeface = TypefaceHelper().setTypeface("regular")
            }
            "social"->{
                mView.text_show_all.typeface = TypefaceHelper().setTypeface("regular")
                mView.text_show_premium.typeface = TypefaceHelper().setTypeface("regular")
                mView.text_show_social.typeface = TypefaceHelper().setTypeface("bold")
            }

        }

        mView.layout_show_all.setOnClickListener {

            if (type != "all"){
                type = "all"
                text_feed_category.text = "All posts"
                page = 1
                fetchArtistFeed()
            }
            if (dialog.isShowing){
                dialog.dismiss()
            }
        }

        mView.layout_show_premium.setOnClickListener {
            if (type != "premium"){
                type = "premium"
                text_feed_category.text = "Premium posts"
                page = 1
                fetchArtistFeed()
            }

            if (dialog.isShowing){
                dialog.dismiss()
            }
        }

        mView.layout_show_social.setOnClickListener {
            if (type != "social"){
                type = "social"
                text_feed_category.text = "Social Posts"
                page = 1
                fetchArtistFeed()
            }

            if (dialog.isShowing){
                dialog.dismiss()
            }
        }
        mView.layout_show_qna.setOnClickListener {
            if (type != "qna"){
                type = "qna"
                text_feed_category.text = "QnA Posts"
                page = 1
                fetchArtistFeed()
            }

            if (dialog.isShowing){
                dialog.dismiss()
            }
        }


        dialog.show()

    }

}