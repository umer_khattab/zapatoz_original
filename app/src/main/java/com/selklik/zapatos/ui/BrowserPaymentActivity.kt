package com.selklik.zapatos.ui

import android.app.Activity
import android.app.ProgressDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.net.http.SslError
import android.os.Build
import android.os.Bundle
import android.support.v4.content.ContextCompat.startActivity
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.MenuItem
import android.webkit.*
import android.widget.Toast
import com.selklik.zapatos.R
import com.selklik.zapatos.utils.TypefaceHelper
import kotlinx.android.synthetic.main.activity_browser.*
import android.support.v4.content.ContextCompat.startActivity
import com.google.gson.Gson
import com.selklik.zapatos.model.Order


/**
 * Created by faridhadiselklik on 21/12/2017.
 */
class BrowserPaymentActivity : AppCompatActivity() {

    private var progDailog: ProgressDialog? = null
    private var gson = Gson()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_browser)
        setSupportActionBar(toolbar)

        val intent = intent
        val url = intent.getStringExtra("url")
        val orderData = intent.getStringExtra("orderData")
        val order = gson.fromJson(orderData, Order::class.java)

        val ab = supportActionBar
        ab!!.setHomeButtonEnabled(true)
        ab.setDisplayHomeAsUpEnabled(true)
        ab.setDisplayShowTitleEnabled(false)
        txt_toolbar_title.typeface = TypefaceHelper().setTypeface("semi-bold")

        txt_toolbar_title.text = "Payment"

        progDailog = ProgressDialog.show(this, "Loading", "Please wait...", true)
        progDailog!!.setCancelable(false)
        webview.settings.javaScriptEnabled = true
        webview.settings.javaScriptCanOpenWindowsAutomatically = true
        webview.loadUrl(url)

        webview.webViewClient = object : WebViewClient() {
            override fun onReceivedSslError(view: WebView, handler: SslErrorHandler, error: SslError) {

                val builder = AlertDialog.Builder(this@BrowserPaymentActivity)
                val alertDialog = builder.create()
                var message = "SSL Certificate error."
                when (error.primaryError) {
                    SslError.SSL_UNTRUSTED -> message = "The certificate authority is not trusted."
                    SslError.SSL_EXPIRED -> message = "The certificate has expired."
                    SslError.SSL_IDMISMATCH -> message = "The certificate Hostname mismatch."
                    SslError.SSL_NOTYETVALID -> message = "The certificate is not yet valid."
                }

                message += " Do you want to continue anyway?"
                alertDialog.setTitle("SSL Certificate Error")
                alertDialog.setMessage(message)
                alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK") { dialog, which ->
                    // Ignore SSL certificate errors
                    handler.proceed()
                }

                alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel") { dialog, which -> handler.cancel() }
                alertDialog.show()
            }

            override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                view.loadUrl(url)
                progDailog!!.show()
                return true
            }

            override fun onPageFinished(view: WebView, url: String) {
                Log.d("Browser Activity", "finished")
                progDailog!!.dismiss()
            }

            override fun onReceivedError(view: WebView, request: WebResourceRequest, error: WebResourceError) {
                super.onReceivedError(view, request, error)

                Log.d("Browser Activity", "error")
                Toast.makeText(this@BrowserPaymentActivity, "Failed load", Toast.LENGTH_SHORT)
                progDailog!!.dismiss()

            }
        }

        webview.addJavascriptInterface(PaymentWebInterface(context = this@BrowserPaymentActivity, webView = webview), "Android")

    }

    fun finishAc(){
        finish()
    }

    fun resultOk(orderId: String){
        Log.d("BrowserPayment", "order id : $orderId")
        var intent = Intent()
        intent.putExtra("orderId", orderId)
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        when (id) {
            android.R.id.home -> onBackPressed()
            else -> return false
        }
        return true
    }
}
class PaymentWebInterface(context: Context,
                          webView: WebView) {
    private var mContext = context
    private var webView = webView
//    private val progressDialog = progressDialog
    private val gson = Gson()
    @JavascriptInterface
    fun eventFired(eventName: String, params: String?) {
        Log.d("WebInterface","event name : $eventName")
        when (eventName) {
            "order-unpaid" -> {
                mContext?.let {
                    (it as BrowserPaymentActivity).finishAc()
                }
//                var post = webView.post({
//                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
//                        webView.evaluateJavascript("window.poleDetails.load('$poleData')", null)
//                    } else {
//                        webView.loadUrl("javascript:window.poleList.load('$poleData')")
//                    }
//                    Log.d("WebInterface","evaluate js")
//                })
            }
            "order-paid" -> {
                Log.d("WebInterface", params)
                params?.let {
                    (mContext as BrowserPaymentActivity).resultOk(params)
                }
            }

        }

    }
}