package com.selklik.zapatos.ui.adapter.ShopAdapter

import android.content.Context
import android.content.Intent
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.view.LayoutInflater
import com.selklik.zapatos.R
import com.selklik.zapatos.model.shopitems.ProductsShoes
import com.selklik.zapatos.ui.BuyItemShowcaseActivity
import com.selklik.zapatos.utils.AppController
import com.selklik.zapatos.utils.Config
import kotlinx.android.synthetic.main.raw_mainstore_showcase.view.*
import kotlinx.android.synthetic.main.raw_mainstore_showcase_fake.view.*


class MainStoreShowCaseAdapter(context: Context,
                               productShoes : ProductsShoes,
                               responseString: String,
                               fake : Boolean) : BaseAdapter()
{

    private var productShoes : ProductsShoes;
    private var inflater : LayoutInflater
    private var responseString : String
    private var fake : Boolean

    init {

        inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        this.productShoes = productShoes
        this.responseString = responseString
        this.fake = fake
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {

        var gridView : View

            if (fake) {

                gridView = inflater.inflate(R.layout.raw_mainstore_showcase_fake, parent, false)
                gridView.shimmer_view_container.startShimmerAnimation()


            } else {

                gridView = inflater.inflate(R.layout.raw_mainstore_showcase, parent, false)

                gridView.image_iv.setImageUrl(productShoes.data[position].products[0].prod_img, AppController().getImageLoader())
                gridView.title_tv.setText(productShoes.data[position].prod_name)


                if (Config.myCountry.equals("my"))
                    gridView.price_tv.setText(Config.myCurrencyUnit+productShoes.data[position].price)
                else
                    gridView.price_tv.setText(Config.myCurrencyUnit+productShoes.data[position].int_price)


                gridView.alpha = 0.0f
                gridView.animate().alpha(1.0f).setDuration(600).start()
                gridView.setOnClickListener({

                    var intent = Intent(gridView.context, BuyItemShowcaseActivity::class.java)
                    intent.putExtra("position", position)
                    intent.putExtra("responseString", responseString)

                    gridView.context.startActivity(intent)

                })

            }


        //gridView = convertView as View


        return gridView
    }

    override fun getItem(position: Int): Any {
        return position
    }

    override fun getItemId(position: Int): Long {
        return  0
    }

    override fun getCount(): Int {

        if (fake) return  6
        else return productShoes.data.size
    }

}