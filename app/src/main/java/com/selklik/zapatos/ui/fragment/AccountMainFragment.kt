package com.selklik.zapatos.ui.fragment

import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.android.volley.Request
import com.android.volley.VolleyError
import com.android.volley.toolbox.ImageLoader
import com.google.gson.Gson
import com.google.gson.JsonParser
import com.google.gson.reflect.TypeToken
import com.selklik.zapatos.model.User
import com.selklik.zapatos.ui.*
import com.selklik.zapatos.ui.adapter.PurchaseHistoryProductAdapter
import com.selklik.zapatos.utils.*
import com.selklik.zapatos.utils.volley.ApiHelper
import java.util.HashMap
import android.widget.CompoundButton
import android.os.Build
import com.selklik.zapatos.R
import com.selklik.zapatos.model.purchasedItems.Data
import kotlinx.android.synthetic.main.fragment_home_account.*


/**
 * Created by faridhadiselklik on 29/11/2017.
 */
class AccountMainFragment : Fragment() {

    private val TAG = AccountMainFragment::class.java.simpleName
    private var dialog: AlertDialog? = null
    inline fun <reified T> genericType() = object : TypeToken<T>() {}.type!!
    private var user: User? = null
    private var mLayoutManager: RecyclerView.LayoutManager? = null
    private var imageLoader: ImageLoader? = null
    private val REQUEST_RELOAD = 10002

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //user = Gson().fromJson(AppController.instance.getUserData(), User::class.java)
        imageLoader = AppController.instance.getImageLoader()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        return inflater!!.inflate(R.layout.fragment_home_account, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val versionRelease = Build.VERSION.RELEASE

        if (!versionRelease.equals("5.1.1")) // Fix Android Google itself bug on OS 5.1.1
            txt_no_payment_method.setText("You have not purchased anything yet. \uD83D\uDE16")

        user = Gson().fromJson(AppController.instance.getUserData(), User::class.java)
        img_user_photo.setImageUrl(user!!.profile_photos.thumbnail, imageLoader)
        txt_account_username.typeface = TypefaceHelper().setTypeface("regular")
        txt_account_username.text = user!!.name
        txt_account_balance.typeface = TypefaceHelper().setTypeface("regular")
        Log.d(TAG, "${user!!.current_balance < 0}")
        if (user!!.current_balance <= 0){
            txt_account_balance.text = "0 Stars"
        } else {
            txt_account_balance.text = user!!.current_balance.toInt().toString() + " Stars"
        }
        txt_purchase_title.typeface = TypefaceHelper().setTypeface("regular")
        txt_transaction.typeface = TypefaceHelper().setTypeface("regular")
        txt_reload_star.typeface = TypefaceHelper().setTypeface("regular")
        txt_term.typeface = TypefaceHelper().setTypeface("regular")
        txt_privacy.typeface = TypefaceHelper().setTypeface("regular")
        txt_faq.typeface = TypefaceHelper().setTypeface("regular")
        txt_logout.typeface = TypefaceHelper().setTypeface("regular")
        layout_btn_logout.setOnClickListener {

            showProgressDialog()
            AppController.instance.logoutSession(context!!, onResponse = { response ->
                if (dialog != null && dialog!!.isShowing) {
                    dialog!!.dismiss()
                }
                Log.d("Account Fragment", "response : " + response.toString())

            })
        }
        mLayoutManager = LinearLayoutManager(this.context!!)
        orders_recycler_view.layoutManager = mLayoutManager

        layout_edit_profile.setOnClickListener{
            startActivity(Intent(context!!, ProfileActivity::class.java))
        }

        txt_purchase_view_all.setOnClickListener {
            startActivity(Intent(context!!, PurchasedHistoryActivity::class.java))
        }

        layout_term.setOnClickListener {
            startActivity(Intent(context, BrowserActivity::class.java).putExtra("url", Config.tosUrl).putExtra("from", "Term of Service"))
        }

        layout_privacy.setOnClickListener {
            startActivity(Intent(context, BrowserActivity::class.java).putExtra("url", Config.privacyUrl).putExtra("from", "Privacy Policy"))
        }

        layout_faq.setOnClickListener {
            startActivity(Intent(context, BrowserActivity::class.java).putExtra("url", Config.faqUrl).putExtra("from", "Frequently Ask Question"))
        }

        layout_reload_star.setOnClickListener {
            startActivityForResult(Intent(context, ReloadStarActivity::class.java), REQUEST_RELOAD)
        }

        layout_account_payment_method.setOnClickListener{
            startActivity(Intent(context, ManagePaymentMethodActivity::class.java))
        }

        layout_account_address.setOnClickListener{
            startActivity(Intent(context, ManageAddressesActivity::class.java))
        }

        layout_transaction.setOnClickListener {
            startActivity(Intent(context, InAppHistoryActivity::class.java))
        }

        val gson = Gson();
        val user = gson.fromJson(AppController.instance.getUserData(), User::class.java)


        Notification()

        swt_notification.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { buttonView, isChecked -> // OWAIS EDITION

            val jsonParams = HashMap<String, String>()
            jsonParams.put("access_token", SharedPrefHelper().getSharedPreferenceString(context!!, Config.PREF_ACCESS_TOKEN, null)!!)
            jsonParams.put("notification", isChecked.toString().trim())

            ApiHelper(context!!).makeRequest(Config.API_ME,
                    Request.Method.PUT,
                    jsonParams,
                    onSuccess = {
                        responseString: String, gson: Gson ->
                        if (responseString !== "[]") {

                            val notificationState = JsonParser().parse(responseString).asJsonObject.get("notification").asBoolean
                            user.notification = notificationState
                            SharedPrefHelper().setSharedPreferenceString(context!!, Config.PREF_USER_DATA, gson.toJson(user))

                        }


                    },
                    onError = {  error: VolleyError ->

                    })

        })

    }


    private fun Notification()
    {
        val jsonParams = HashMap<String, String>()
        jsonParams.put("access_token", SharedPrefHelper().getSharedPreferenceString(context!!, Config.PREF_ACCESS_TOKEN, null)!!)

        ApiHelper(context!!).makeRequest(Config.API_ME,
                Request.Method.PUT,
                jsonParams,
                onSuccess = {
                    responseString: String, gson: Gson ->
                    if (responseString !== "[]") {

                        val notificationState = JsonParser().parse(responseString).asJsonObject.get("notification").asBoolean

                        if (notificationState)
                            swt_notification.isChecked = true
                        else
                            swt_notification.isChecked = false


                    } else {

                    }




                },
                onError = {  error: VolleyError ->

                })

    }

    override fun onResume() {
        super.onResume()
        user = Gson().fromJson(AppController.instance.getUserData(), User::class.java)
        img_user_photo.setImageUrl(user!!.profile_photos.thumbnail, imageLoader)
        txt_account_username.typeface = TypefaceHelper().setTypeface("regular")
        txt_account_username.text = user!!.name
        txt_account_balance.typeface = TypefaceHelper().setTypeface("regular")
        Log.d(TAG, "${user!!.current_balance < 0}")
        if (user!!.current_balance <= 0){
            txt_account_balance.text = "0 Stars"
        } else {
            txt_account_balance.text = user!!.current_balance.toInt().toString() + " Stars"
        }

        fetchOrder()
    }

    override fun onHiddenChanged(hidden: Boolean) {
        if (!hidden){
            fetchOrder()
        }
        super.onHiddenChanged(hidden)
    }

    private fun fetchOrder(){
//        showProgressDialog()
        if (!ConnectivityReceiver.isConnected()) {
            Log.d("Add Card", "No internet connection")
        } else {
            val jsonParams = HashMap<String, String>()

            ApiHelper(context!!).makeZopatozRequest(
                    Config.Base_Url + "?listorder="+ LocalMutators.email,
                    jsonParams,
                    Request.Method.GET,
                    false,
                    false,
                    onSuccess = { responseString: String, gson: Gson ->
                        Log.d("Add Card", "response string : " + responseString)

                        if (responseString != "[]" && responseString != "can't open file") {

                            layout_order_empty.visibility = View.GONE
                            orders_recycler_view.visibility = View.VISIBLE

                            var purchasedArrayList = gson.fromJson(responseString, Data::class.java)

                            if (purchasedArrayList.data.size > 2)
                            {
                                var subArray = purchasedArrayList.data.subList(0, 2)

                                orders_recycler_view.adapter = PurchaseHistoryProductAdapter(context!!, ArrayList(subArray))
                                orders_recycler_view.isNestedScrollingEnabled = false
                                orders_recycler_view.visibility = View.VISIBLE

                            } else {

                                orders_recycler_view.adapter = PurchaseHistoryProductAdapter(context!!, purchasedArrayList.data!!)
                                orders_recycler_view.isNestedScrollingEnabled = false
                                orders_recycler_view.visibility = View.VISIBLE
                            }

                            Log.d("Purchased history", "featured product : " + purchasedArrayList.data.size)




                        } else {
                            orders_recycler_view.visibility = View.GONE
                            layout_order_empty.visibility = View.VISIBLE
                            layout_empty_icon.visibility = View.VISIBLE
                            order_progress_bar.visibility = View.GONE
                        }

                    },
                    onError = { error: VolleyError ->
                        ApiHelper(context!!).errorHandler(error)

                        layout_order_empty.visibility = View.VISIBLE
                        layout_empty_icon.visibility = View.VISIBLE
                        order_progress_bar.visibility = View.GONE
//                        if (dialog!!.isShowing) {
//                            dialog!!.dismiss()
//                        }
                    })
        }

    }

    private fun showProgressDialog() {
        val mBuilder = AlertDialog.Builder(context!!, R.style.CustomDialogTheme)
        val mView = LayoutInflater.from(context).inflate(R.layout.dialog_progress, null)

        mBuilder.setView(mView)
        mBuilder.setCancelable(false)
        dialog = mBuilder.create()
        dialog!!.show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            REQUEST_RELOAD -> if (resultCode == Activity.RESULT_OK) {
                Log.d("AccountFragment", "Result ok, response : " + data!!.getStringExtra("response"))
                var newCurrentBalance = JsonParser().parse(data!!.getStringExtra("response")).asJsonObject.get("current_balance").asInt
                var user = Gson().fromJson(AppController.instance.getUserData(), User::class.java)
                user.current_balance = newCurrentBalance.toDouble()

                SharedPrefHelper().setSharedPreferenceString(context!!, Config.PREF_USER_DATA, Gson().toJson(user))
                txt_account_balance.text = newCurrentBalance.toString()

            } else {
                Log.d("AccountFragment", "Result cancel")
            }
        }
    }
}