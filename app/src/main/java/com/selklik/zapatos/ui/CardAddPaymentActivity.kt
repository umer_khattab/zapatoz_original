package com.selklik.zapatos.ui

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.SharedPreferences
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.Toast
import com.selklik.zapatos.R
import com.selklik.zapatos.ui.adapter.ShopAdapter.CardListAdapter
import com.selklik.zapatos.utils.Config
import kotlinx.android.synthetic.main.content_manage_payment_method.*
import kotlinx.android.synthetic.main.layout_add_card_dialog.*


class CardAddPaymentActivity : Activity()
{

    private var prefs : SharedPreferences ?= null
    private var editor : SharedPreferences.Editor ?= null
    private var array = ArrayList<String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.activity_zopatoz_stripe_pay)

        prefs = getSharedPreferences(Config.PREF_CARDS, Context.MODE_PRIVATE)
        editor = prefs!!.edit()

        if (getCarts().size > 0 )
        {
            layout_no_payment_method.visibility = View.GONE
            cards_lv.visibility = View.VISIBLE
            cards_lv.adapter = CardListAdapter(applicationContext, getCarts(), this)

        } else {

            layout_no_payment_method.visibility = View.VISIBLE
            cards_lv.visibility = View.GONE
        }

        btn_add_card.setOnClickListener(View.OnClickListener {

            showAddCardZapatozDialog(this)
        })

    }

    private fun showAddCardZapatozDialog(activity : Activity) {

        var dialog = Dialog(activity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.layout_add_card_dialog);
            dialog.getWindow().setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

            dialog.btn_submit_add_card.setOnClickListener {

                var card = dialog.card_input_widget.card

                try {

                    var cardNumber    =  card!!.number
                    var cardCvc       =  card!!.cvc
                    var cardYear         =  card!!.expYear
                    var cardMonth       =  card!!.expMonth

                    var cardString : String = cardNumber + " DIS "+ cardCvc + " DIS " + cardYear + " DIS " + cardMonth

                    if (cardString.isNotEmpty()) {

                            saveCard(cardString)
                            dialog.dismiss()

                    } else
                        Toast.makeText(applicationContext, " Please enter card details first!", Toast.LENGTH_SHORT).show()


                }catch (e : NullPointerException) {e.printStackTrace()
                           Toast.makeText(applicationContext, " Please enter card details first!", Toast.LENGTH_SHORT).show()
                       }

            }

            dialog.text_cancel.setOnClickListener(View.OnClickListener {

                dialog.dismiss()

            })

            dialog.show()
    }

    private fun saveCard(card : String)
    {

        if (getCarts().size > 0)
            array = getCarts()

        array!!.add(card)

        val set = HashSet<String>()
            set.addAll(array)

            editor!!.putStringSet("cardList", set)
            editor!!.commit()

    }

    private fun getCarts() : ArrayList<String>
    {
        try {

              var list = prefs!!.getStringSet("cardList", null)

              if (list.size > 0)
                  array!!.addAll(prefs!!.getStringSet("cardList", null))


        }catch (e : NullPointerException)
               {e.printStackTrace()}

        return array!!

    }

}