package com.selklik.zapatos.ui.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.android.volley.toolbox.ImageLoader
import com.selklik.zapatos.R
import com.selklik.zapatos.model.Order
import com.selklik.zapatos.model.Reload
import com.selklik.zapatos.ui.ReloadStarActivity
import com.selklik.zapatos.utils.AppController
import com.selklik.zapatos.utils.TypefaceHelper
import kotlinx.android.synthetic.main.fragment_home_account.view.*
import kotlinx.android.synthetic.main.item_list_reload.view.*

/**
 * Created by faridhadiselklik on 06/12/2017.
 */
class ReloadStarAdapter(private var mContext: Context, var reloadArrayList: ArrayList<Reload>) : RecyclerView.Adapter<ReloadStarAdapter.ViewHolder>() {

//    init {
////        mContext = context
//    }

    override fun onBindViewHolder(holder: ReloadStarAdapter.ViewHolder, position: Int) {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        holder.bindItems(reloadArrayList[position], position)
        Log.d("TAG", "inside adapter")
    }



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) : ViewHolder {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        val itemView = LayoutInflater.from(parent!!.context).inflate(R.layout.item_list_reload, parent, false)
        return ViewHolder(mContext, itemView)

    }

    override fun getItemCount(): Int {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        return reloadArrayList.size
    }

    inner class ViewHolder(context: Context, itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(reload: Reload, position : Int) {

            itemView.txt_star.typeface = TypefaceHelper().setTypeface("regular")
            itemView.txt_star.text = reload.number_of_star
            itemView.txt_price.typeface = TypefaceHelper().setTypeface("semi-bold")
            itemView.txt_price.text = "$ ${reload.price}"

            itemView.setOnClickListener {

                (mContext as ReloadStarActivity).starPurchased(position)
                (mContext as ReloadStarActivity).launchPurchaseFlow(reload.product_id)

            }
        }
    }
}