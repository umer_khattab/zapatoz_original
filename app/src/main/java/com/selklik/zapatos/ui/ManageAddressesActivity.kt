package com.selklik.zapatos.ui

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.style.RelativeSizeSpan
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.VolleyError
import com.google.gson.Gson
import com.google.gson.JsonParser
import com.selklik.zapatos.R
import com.selklik.zapatos.model.Address
import com.selklik.zapatos.model.AddressData
import com.selklik.zapatos.ui.adapter.AddressItemAdapter
import com.selklik.zapatos.utils.*
import com.selklik.zapatos.utils.helper.AlertDialogHelper
import com.selklik.zapatos.utils.volley.ApiHelper

import kotlinx.android.synthetic.main.activity_manage_addresses.*
import kotlinx.android.synthetic.main.content_manage_addresses.*
import java.util.HashMap

class ManageAddressesActivity : AppCompatActivity() {
    private val TAG = ManageAddressesActivity::class.java.simpleName
    private var dialog: AlertDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_manage_addresses)
        setSupportActionBar(toolbar)
        val ab = supportActionBar!!
        ab!!.setHomeButtonEnabled(true)
        ab!!.setDisplayHomeAsUpEnabled(true)
        ab!!.setDisplayShowTitleEnabled(false)

        txt_toolbar_title.typeface = TypefaceHelper().setTypeface("regular")
        val mLayoutManager = LinearLayoutManager(this)
        address_recycler_view.layoutManager = mLayoutManager

        btn_add_address.typeface = TypefaceHelper().setTypeface("bold")
        btn_add_address.setOnClickListener(onAddAddressClick)
        fetchAddress()
    }

    private fun fetchAddress() {
        if (dialog == null) {
//            if (!dialog!!.isShowing) {
            showProgressDialog()
//            }
        }
        if (!ConnectivityReceiver.isConnected()) {
//            if (dialog!!.isShowing) {
//                dialog!!.dismiss()
//            }
        } else {
            val jsonParams = HashMap<String, String>()
            //jsonParams.put("access_token", AppController.instance.getUserAccessToken())

            ApiHelper(this).makeZopatozRequest(Config.Base_Url+"?email="+ LocalMutators.email,
                    jsonParams, Request.Method.GET, false,
                    false,
                    onSuccess = { responseString: String, gson: Gson ->

                        if (!responseString.equals("[]"))
                        {

                            if (dialog!!.isShowing) {
                                dialog!!.dismiss()
                            }

                               var gson = gson.fromJson(responseString, AddressData::class.java)

                                if (gson.data.size > 0) {
                                    layout_no_address.visibility = View.GONE

                                    address_recycler_view.visibility = View.VISIBLE
                                    var feedItemAdapter = AddressItemAdapter(this!!, gson.data!!,
                                     onItemViewClick = { address: Address -> showCardDefaultAlert(this, address) })

                                    address_recycler_view.adapter = feedItemAdapter

                                } else {
                                    layout_no_address.visibility = View.VISIBLE
                                    address_recycler_view.visibility = View.GONE
                                    txt_no_payment_method.typeface = TypefaceHelper().setTypeface("regular")
                                }
                        }
                        else {

                            if (dialog!!.isShowing) {
                                dialog!!.dismiss()
                            }
                            layout_no_address.visibility = View.VISIBLE
                            address_recycler_view.visibility = View.GONE
                            txt_no_payment_method.typeface = TypefaceHelper().setTypeface("regular")
                        }

                    },
                    onError = { error: VolleyError ->
                        ApiHelper(this).errorHandler(error)
                        if (dialog!!.isShowing) {
                            dialog!!.dismiss()
                        }
                    })
        }
    }

    private fun fetchAddressZapatos() {
        if (dialog == null) {
//            if (!dialog!!.isShowing) {
            showProgressDialog()
//            }
        }
        if (!ConnectivityReceiver.isConnected()) {
//            if (dialog!!.isShowing) {
//                dialog!!.dismiss()
//            }
            Log.d("Add Card", "No internet connection")
        } else {
            val jsonParams = HashMap<String, String>()
            jsonParams.put("access_token", AppController.instance.getUserAccessToken())

            ApiHelper(this).makeRequest(
                    Config.API_ME + "/addresses",
                    Request.Method.GET,
                    jsonParams,
                    false,
                    false,
                    onSuccess = { responseString: String, gson: Gson ->
                        Log.d("Add Card", "response string : " + responseString)
                        if (dialog!!.isShowing) {
                            dialog!!.dismiss()
                        }

                        val addressJsonArray = JsonParser().parse(responseString).asJsonArray
//                        Log.d("Add Card", "stored card info : "
//                                + SharedPrefHelper().getSharedPreferenceString(this, Config.PREF_USER_CARD, ""))
                        if (addressJsonArray.size() > 0) {
                            layout_no_address.visibility = View.GONE
                            var addressArrayList: ArrayList<Address> = addressJsonArray.map {
                                gson.fromJson(it, Address::class.java)
                            } as ArrayList<Address>
                            address_recycler_view.visibility = View.VISIBLE
                            var feedItemAdapter = AddressItemAdapter(this!!, addressArrayList!!,
                                    onItemViewClick = {
                                        address: Address ->
                                        showCardDefaultAlert(this, address)
                                    })
                            address_recycler_view.adapter = feedItemAdapter
                        } else {
                            layout_no_address.visibility = View.VISIBLE
                            address_recycler_view.visibility = View.GONE
                            txt_no_payment_method.typeface = TypefaceHelper().setTypeface("regular")
                        }
                    },
                    onError = { error: VolleyError ->
                        ApiHelper(this).errorHandler(error)
                        if (dialog!!.isShowing) {
                            dialog!!.dismiss()
                        }
                    })
        }

    }

    private fun showCardDefaultAlert(context: Context, address: Address) {
        val builder = android.app.AlertDialog.Builder(context)
        builder.setMessage("Set this as default?")
                .setCancelable(false)
        builder.setNegativeButton("OK") {
            dialog, id ->

            setDefaultAddress(address)

            setResult(Activity.RESULT_OK)
            finish()


            dialog.dismiss()
        }
        builder.setPositiveButton("Cancel") { dialog, id -> dialog.dismiss() }

        val ssBuilder = SpannableStringBuilder("Confirmation")

        // Initialize a new relative size span instance
        // Double the text size on this span
        val largeSizeText = RelativeSizeSpan(.8f)

        // Apply the relative size span
        ssBuilder.setSpan(
                largeSizeText, // Span to add
                0, // Start of the span
                "Confirmation".length, // End of the span
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE // Do not extent the span when text add later
        )

        // Set the alert dialog title using spannable string builder
        // It will display double size text on title
        builder.setTitle(ssBuilder)
        val alert = builder.show()

        val messageText = alert.findViewById<View>(android.R.id.message) as TextView
        messageText.gravity = Gravity.CENTER
        messageText.textSize = 12f

    }

    private fun setDefaultAddress(address: Address){
        showProgressDialog()
        if (!ConnectivityReceiver.isConnected()) {
//            if (dialog!!.isShowing) {
//                dialog!!.dismiss()
//            }
            Log.d("Add Card", "No internet connection")
        } else {
            val jsonParams = HashMap<String, String>()
            jsonParams.put("access_token", AppController.instance.getUserAccessToken())
            jsonParams.put("is_default", "true")

            ApiHelper(this).makeRequest(
                    Config.API_ME + "/addresses/${address.id}",
                    Request.Method.PUT,
                    jsonParams,
                    false,
                    false,
                    onSuccess = { responseString: String, gson: Gson ->
                        Log.d("UpdateCard", "response string : " + responseString)
                        if (dialog!!.isShowing) {
                            dialog!!.dismiss()
                        }
//                        Log.d("Add Card", "stored card info : "
//                                + SharedPrefHelper().getSharedPreferenceString(this, Config.PREF_USER_CARD, ""))
                        fetchAddress()
                    },
                    onError = { error: VolleyError ->
                        ApiHelper(this).errorHandler(error)
                        if (dialog!!.isShowing) {
                            dialog!!.dismiss()
                        }
                    })

        }
    }

    private var onAddAddressClick: View.OnClickListener = View.OnClickListener {

        AlertDialogHelper().showAddAddressDialog(this,
                onAddressResponse = {
                    response: String ->

                    fetchAddress()

                },
                onError = {
                    errorString: String?, error: VolleyError? ->
                    if (errorString != null) {
                        Toast.makeText(this, "No internet Connection", Toast.LENGTH_SHORT).show()
                    } else {
                        ApiHelper(this).errorHandler(error!!)
                    }

                })
    }

    private fun showProgressDialog() {
        val mBuilder = AlertDialog.Builder(this, R.style.CustomDialogTheme)
        val mView = LayoutInflater.from(this).inflate(R.layout.dialog_progress, null)

        mBuilder.setView(mView)
        mBuilder.setCancelable(false)
        dialog = mBuilder.create()
        dialog!!.show()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        when (id) {
            android.R.id.home -> onBackPressed()
            else -> return false
        }
        return true
    }


}
