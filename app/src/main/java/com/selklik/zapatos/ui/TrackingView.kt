package com.selklik.zapatos.ui

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.view.Window
import android.view.animation.AnimationUtils
import android.webkit.WebView
import android.webkit.WebViewClient
import com.selklik.zapatos.MainActivity
import com.selklik.zapatos.R
import kotlinx.android.synthetic.main.activity_localpaymentbillplz.*

class TrackingView : Activity() {


    private var dialog : Dialog?= null

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)
        if (hasFocus) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            or  View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            or  View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            or  View.SYSTEM_UI_FLAG_FULLSCREEN
                            or  View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_trackingview)

        dialog = Dialog(this)
        dialog!!.getWindow().setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setContentView(R.layout.raw_dialog_payment)
        dialog!!.setCancelable(false)
        dialog!!.show()

        var trackingId = getIntent().getStringExtra("trackingId")

        webview.getSettings().setJavaScriptEnabled(true);
        webview.getSettings().setLoadWithOverviewMode(true);
        webview.getSettings().setUseWideViewPort(true);

        webview.setWebViewClient(object : WebViewClient() {

            override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                view.loadUrl(url)

                return true
            }

            override fun onPageFinished(view: WebView, url: String) {

                    if (dialog!!.isShowing)
                        dialog!!.dismiss()

            }
        })

        webview.loadUrl("https://www.tracking.my/poslaju/"+trackingId)

        btn_close.setOnClickListener(View.OnClickListener {

            finish()

        })
    }
}