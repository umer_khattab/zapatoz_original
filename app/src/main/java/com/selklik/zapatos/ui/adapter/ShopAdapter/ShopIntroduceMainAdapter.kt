package com.selklik.zapatos.ui.adapter.ShopAdapter


import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.selklik.zapatos.R
import com.selklik.zapatos.utils.AppController
import kotlinx.android.synthetic.main.raw_singleitem_design.view.*
import android.app.Activity
import android.content.Intent
import com.selklik.zapatos.model.shopitems.ProductsShoes
import com.selklik.zapatos.ui.BuyItemShowcaseActivity
import com.selklik.zapatos.utils.Config
import com.selklik.zapatos.R.id.textview




class ShopIntroduceMainAdapter(private var mContext: Context, activity : Activity,
                               product : ProductsShoes,
                               responseString : String) :
                               RecyclerView.Adapter<ShopIntroduceMainAdapter.ViewHolder>() {


    private var activity : Activity
    private var product : ProductsShoes
    private var responseString : String

    init {

        this.responseString = responseString
        this.activity = activity
        this.product = product

        setHasStableIds(true)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {


            if (Config.myCountry.equals("my"))
            {
                holder.initView(product.data[position].products[0].prod_img,
                        product.data[position].prod_name,
                        Config.myCurrencyUnit+product.data[position].price,
                        position,
                        activity)
            } else
            {
                holder.initView(product.data[position].products[0].prod_img,
                        product.data[position].prod_name,
                        Config.myCurrencyUnit+product.data[position].int_price,
                        position,
                        activity)
            }


    }

    inner class ViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView) {

        fun initView(productUrl : String, productDes : String, productPrice : String, position: Int, activity: Activity) {

            itemView.product_iv.setImageUrl(productUrl, AppController().getImageLoader())
            itemView.price_tv.setText(productPrice)
            itemView.description_tv.setText(productDes)

            itemView.shimmer_cart.startShimmerAnimation()

            itemView.setOnClickListener(View.OnClickListener {


               var intent = Intent(activity, BuyItemShowcaseActivity::class.java)
                intent.putExtra("position", position)
                intent.putExtra("responseString", responseString)

                mContext.startActivity(intent)

            })

        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        var itemView : View ?= null

        if (viewType == 5)
        {
            itemView = LayoutInflater.from(mContext).inflate(R.layout.raw_singleitem_design, parent, false)
            //itemView.bottom_container.visibility == View.VISIBLE


        } else {

            itemView = LayoutInflater.from(mContext).inflate(R.layout.raw_singleitem_design, parent, false)
        }

        return ViewHolder(itemView!!)

    }

    override fun getItemCount(): Int {

        if (product.data.size > 6) return 6 else return product.data.size
    }

    override fun getItemViewType(position: Int): Int {

        if (product.data.size > 6) return 6 else return product.data.size
    }

    override fun getItemId(position: Int): Long {
        return super.getItemId(position)
    }


}