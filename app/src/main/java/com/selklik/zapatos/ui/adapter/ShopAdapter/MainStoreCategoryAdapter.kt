package com.selklik.zapatos.ui.adapter.ShopAdapter

import android.content.Context
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.view.View
import com.google.gson.Gson
import com.selklik.zapatos.model.shopitems.ProductsShoes
import com.selklik.zapatos.ui.fragment.MainStoreCategoryFragments.KidsStoreFragment
import com.selklik.zapatos.ui.fragment.MainStoreCategoryFragments.MenStoreFragment
import com.selklik.zapatos.ui.fragment.MainStoreCategoryFragments.WomenStoreFragment


class MainStoreCategoryAdapter(context: Context,
                               fm : FragmentManager) : FragmentPagerAdapter(fm)
{
    var arrayFragments:  ArrayList<Fragment>  = arrayListOf<Fragment>(KidsStoreFragment(), MenStoreFragment(), WomenStoreFragment())
    var arrayNamesFrags: ArrayList<String> = arrayListOf<String>("KIDz", "MEN", "WOMEN")

    var context : Context

    init {

        this.context = context
    }

    override fun getItem(position: Int): Fragment {

        return  arrayFragments!!.get(position)
    }

    override fun getCount(): Int {
        return arrayFragments!!.count()
    }


    override fun getPageTitle(position: Int): CharSequence? {
        return arrayNamesFrags!![position]
    }

    override fun setPrimaryItem(container: View, position: Int, `object`: Any) {
        super.setPrimaryItem(container, position, `object`)
    }
}