package com.selklik.zapatos.ui.adapter.ShopAdapter

import android.content.Context
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter


class MainFragmentsAdapter(context: Context,
                           fm : FragmentManager,
                           arrayFragments: ArrayList<Fragment>) : FragmentPagerAdapter(fm)
{

    var arrayFragments: ArrayList<Fragment>
    var context : Context

    init {

        this.arrayFragments = arrayFragments
        this.context = context

    }

    override fun getItem(position: Int): Fragment {

        return  arrayFragments.get(position)
    }

    override fun getCount(): Int {
        return arrayFragments.count()
    }


}