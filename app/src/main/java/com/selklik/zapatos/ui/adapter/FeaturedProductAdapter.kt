package com.selklik.zapatos.ui.adapter

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.android.volley.toolbox.ImageLoader
import com.android.volley.toolbox.NetworkImageView
import com.google.gson.Gson
import com.selklik.zapatos.R
import com.selklik.zapatos.model.Products
import com.selklik.zapatos.ui.ProductDetailsActivity
import com.selklik.zapatos.utils.AppController
import com.selklik.zapatos.utils.TypefaceHelper
import kotlinx.android.synthetic.main.item_featured_product.view.*

/**
 * Created by faridhadiselklik on 06/12/2017.
 */
class FeaturedProductAdapter(private var mContext: Context,
                             var featuredArrayList: ArrayList<Products>,
                             var onItemViewClick: (product: Products) -> Unit) : RecyclerView.Adapter<FeaturedProductAdapter.ViewHolder>() {

//    init {
////        mContext = context
//    }

    override fun onBindViewHolder(holder: FeaturedProductAdapter.ViewHolder, position: Int) {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        holder.bindItems(featuredArrayList[position], onItemViewClick)
        Log.d("TAG", "inside adapter")
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        val itemView = LayoutInflater.from(parent!!.context).inflate(R.layout.item_featured_product, parent, false)
        return ViewHolder(mContext, itemView)

    }

    override fun getItemCount(): Int {
//        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        return featuredArrayList.size
    }

    inner class ViewHolder(context: Context, itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindItems(products: Products, onItemViewClick: (product: Products) -> Unit) {
            var imageLoader: ImageLoader = AppController().getImageLoader()
            if (products.bought_by != null) {
                itemView.layoutBoughtBy.visibility = View.VISIBLE
                itemView.textBoughtBy.typeface = TypefaceHelper().setTypeface("semi-bold")
                itemView.textBoughtBy.text = "Bought by " + products.bought_by.name
            } else {
                itemView.layoutBoughtBy.visibility = View.GONE
            }
            val imgProduct = itemView.findViewById<NetworkImageView>(R.id.img_product)
            imgProduct.setImageUrl(products.photos[0].asJsonObject.get("full_resolution").asString, imageLoader)
            val textProductTitle = itemView.findViewById<TextView>(R.id.textProductItemTitle)
            textProductTitle.text = products.title
            textProductTitle.typeface = TypefaceHelper().setTypeface("regular")
            val textProductPrice = itemView.findViewById<TextView>(R.id.textProductItemPrice)
            textProductPrice.text = products.price
            textProductPrice.typeface = TypefaceHelper().setTypeface("regular")
            val textProductView = itemView.findViewById<TextView>(R.id.textProductItemNoView)
            textProductView.typeface = TypefaceHelper().setTypeface("light")
            textProductView.text = Integer.toString(products.view_count) + " Views"
//            val textViewAddress  = itemView.findViewById(R.id.textViewAddress) as TextView
//            textViewName.text = user.name
//            textViewAddress.text = user.address

            itemView.setOnClickListener {
                onItemViewClick(products)
                }
        }
    }
}