package com.selklik.zapatos.ui.fragment

import android.app.Dialog
import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.Handler
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.content.res.ResourcesCompat
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.ImageView
import android.widget.Toast
import com.android.volley.Request
import com.google.gson.Gson
import com.oguzdev.circularfloatingactionmenu.library.FloatingActionButton
import com.oguzdev.circularfloatingactionmenu.library.FloatingActionMenu
import com.oguzdev.circularfloatingactionmenu.library.SubActionButton
import com.selklik.zapatos.R
import com.selklik.zapatos.model.shopitems.Data
import com.selklik.zapatos.model.shopitems.ProductsShoes
import com.selklik.zapatos.ui.adapter.ShopAdapter.MainStoreCategoryAdapter
import com.selklik.zapatos.ui.fragment.MainStoreCategoryFragments.KidsStoreFragment
import com.selklik.zapatos.ui.fragment.MainStoreCategoryFragments.MenStoreFragment
import com.selklik.zapatos.ui.fragment.MainStoreCategoryFragments.WomenStoreFragment
import com.selklik.zapatos.utils.Config
import com.selklik.zapatos.utils.SharedPrefHelper
import com.selklik.zapatos.utils.volley.ApiHelper
import kotlinx.android.synthetic.main.fragment_mainstore.*
import kotlinx.android.synthetic.main.fragment_mainstore.view.*

class MainStoreFragment : Fragment()
{


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        var view : View = inflater.inflate(R.layout.fragment_mainstore, container, false)

        var mainAdapter = MainStoreCategoryAdapter(context!!, childFragmentManager)

        view!!.store_viewpager.adapter = mainAdapter
        view!!.store_tablayout.setupWithViewPager(view!!.store_viewpager)

        return view

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

    }

    override fun onStart() {
        super.onStart()


    }



}