package com.selklik.zapatos.ui.adapter

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.android.volley.Request
import com.google.gson.Gson
import com.selklik.zapatos.R
import com.selklik.zapatos.model.Address
import com.selklik.zapatos.utils.Config
import com.selklik.zapatos.utils.LocalMutators
import com.selklik.zapatos.utils.volley.ApiHelper
import kotlinx.android.synthetic.main.item_list_address.view.*
import org.json.JSONObject
import java.util.*
import kotlin.collections.HashMap
import android.content.DialogInterface
import android.content.Intent
import android.os.Handler


/**
 * Created by faridhadiselklik on 15/12/2017.
 */
class AddressItemAdapter(private var mContext: Context, private var addressArrayList: ArrayList<Address>,
                         var onItemViewClick: (address: Address) -> Unit) : RecyclerView.Adapter<AddressItemAdapter.ViewHolder>() {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindItems(addressArrayList[position], position, onItemViewClick)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater.from(parent!!.context).inflate(R.layout.item_list_address, parent, false)
        return ViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return addressArrayList.size
    }

    var defaultAddress : Boolean ?= false;


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(address: Address, position: Int, onItemViewClick: (address: Address) -> Unit) {
            itemView.txt_address.visibility = View.VISIBLE

            if (position == 0){

                itemView.message_delete.visibility = View.VISIBLE
                Handler().postDelayed(Runnable { itemView.message_delete.visibility = View.GONE }, 4000)

            }

            itemView.txt_address.text = address.address.replace(", ","\n")

            if (address.default_address.equals("1"))
                defaultAddress = true
            else
                defaultAddress = false

            if (defaultAddress!!) {
                itemView.layout_card_default.visibility = View.VISIBLE

            } else {
                itemView.layout_card_default.visibility = View.GONE
            }

            itemView.setOnClickListener {

                var jsonParams = HashMap<String, String>()

                var json = JSONObject()
                    json.put("id", address.id)
                    json.put("email", LocalMutators.email)
                    json.put("is_default","1")


                ApiHelper(mContext).makeZopatozRequest(Config.Base_Url+"?defadd="+json.toString(),
                        jsonParams, Request.Method.GET, false,
                        false,
                        onSuccess = { responseString: String, gson: Gson ->

                            LocalMutators.fullAddress = itemView.txt_address.text.toString().trim()
                            LocalMutators.phoneNo = address.address.split("PhoneNo: ")[1].trim()

                            val returnIntent = Intent()
                            (mContext as Activity).setResult(Activity.RESULT_OK, returnIntent);
                            Toast.makeText(mContext, "Your address has been selected as default", Toast.LENGTH_SHORT).show()
                            (mContext as Activity).finish()
                        },

                        onError = {error ->  }

                    )

                /*if (defaultAddress!!) {
                    onItemViewClick(address)
                }*/
            }

           itemView.setOnLongClickListener {

               val dialogClickListener = DialogInterface.OnClickListener { dialog, which ->
                   when (which) {
                       DialogInterface.BUTTON_POSITIVE -> {

                           var jsonParams = HashMap<String, String>()

                           var json = JSONObject()
                           json.put("id", address.id)
                           json.put("email", LocalMutators.email)

                           ApiHelper(mContext).makeZopatozRequest(Config.Base_Url+"?deladd="+json.toString(),
                                   jsonParams, Request.Method.GET, false,
                                   false,
                                   onSuccess = { responseString: String, gson: Gson ->

                                       Toast.makeText(mContext, "Your address has been deleted.", Toast.LENGTH_SHORT).show()
                                       (mContext as Activity).finish()

                                   },

                                   onError = {error ->

                                       Toast.makeText(mContext, "We couldn't process your request.", Toast.LENGTH_SHORT).show()

                                   }

                           )

                       }

                       DialogInterface.BUTTON_NEGATIVE -> {

                           dialog.dismiss()
                       }

                   }//Yes button clicked
                   //No button clicked
               }

               val builder = AlertDialog.Builder(mContext)
               builder.setMessage("Are you sure you want to delete?").setPositiveButton("Yes", dialogClickListener)
                       .setNegativeButton("No", dialogClickListener).show()

               true
           }

        }
    }
}